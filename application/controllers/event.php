<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Event extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('user/user_model','user_model');
        $this->load->library('core_lib');
        $this->load->library('form');
        $this->load->library('default_session');
        $this->load->library('user/user_lib','user_lib');
        $this->load->model('event_model','event_model');

    }


    public function _remap($method)
    {
        $userGroup = $this->core_lib->getUserGroup();
        if($method == "add"){
            if(in_array("admin",$userGroup)){
                $this->add();
            }else{
                die('login required');
            }
        }elseif ($method == "validate"){
            if(in_array("admin",$userGroup)){
                $this->validate();
            }else{
                die('login required');
            }
        }elseif ($method == "listing"){
            if(in_array("admin",$userGroup)){
                $this->listing();
            }else{
                die('login required');
            }
        }elseif ($method == "eventSearchRender"){
            if(in_array("admin",$userGroup)){
                $this->eventSearchRender();
            }else{
                die('login required');
            }
        }elseif ($method == "delete"){
            if(in_array("admin",$userGroup)){
                $this->delete();
            }else{
                die('login required');
            }
        }elseif ($method == "view"){
            if(in_array("admin",$userGroup)){
                $this->view();
            }else{
                die('login required');
            }
        }elseif ($method == "uploadEventImage"){
            if(in_array("admin",$userGroup)){
                $this->uploadEventImage();
            }else{
                die('login required');
            }
        }
    }
    public function add()
    {
        $eventDetails = [];

        $mode = $this->uri->segment(3);
        if($mode == "edit"){
           $eventId = $this->uri->segment(4);
            if($eventId){
                $eventDetails = $this->event_model->getData($eventId);
            }
        }
        $this->load->adminTemplate('events/edit',$eventDetails);
    }

    public function validate()
    {

        $data = json_decode(file_get_contents('php://input'), true);
        /*echo '<pre>';print_r($data);die();*/
        $this->form->setRules($data['name'],'name_error','Please Enter Event Name ');
        $validationArr = $this->form->run();
        if(is_array($validationArr) && count($validationArr) && $validationArr != false){
            header('Content-Type: application/json');
            echo json_encode( $validationArr );
        }else{
            $this->load->model('event_model','event_model');
            if(isset($data['id']) && !empty($data['id'])){
                $temp = [
                    'id' =>$data['id']
                ];
                $this->event_model->update($data,$temp);
            }else{
                $eventId = $this->event_model->add($data);

            }
            if(!isset($eventId) && empty($eventId))$eventId = $data['id'];
            $result = ['id'=>$eventId,'success' =>true];
            header('Content-Type: application/json');
            echo json_encode( $result );
        }

    }

    public function listing()
    {
        $result = $this->eventSearch();
        $resultArr['data'] = $result;
        $this->load->adminTemplate('events/list',$resultArr);
    }

    protected function eventSearch()
    {
        $postData = [
            'page' =>$this->input->post('page'),
            'phrase' =>$this->input->post('phrase')
        ];

        $query = $this->event_model->getSearchQuery($postData);
        $result =$this->event_model->getListByQuery($query,$postData['page'],'');
        return $result;
    }

    protected function eventSearchRender()
    {
        $result = $this->eventSearch();
        $resultArr['data'] = $result;
        $this->load->view('admin/events/search_result',$resultArr);
    }

    protected function delete()
    {
        $eventId = $this->input->post('event_id');
        if ($eventId) {
            $this->event_model->delete(['id'=>$eventId]);
        }
    }
    protected function view()
    {
        $id = $this->uri->segment(4);
        if($id){
            $eventDetails = $this->event_model->getData($id);
            $this->load->adminTemplate('events/view',$eventDetails);
        }
    }

    public function uploadEventImage()
    {
        $eventId = $this->input->post('event_id');
        $this->load->library('upload_lib');
        $resultArr = $this->upload_lib->doUpload();
        if(count($resultArr) && !isset($resultArr['error'])){
            $this->load->model('file_model','file_model');
            $tempData = [
                'file_path' =>$resultArr['upload_data']['full_path'],
                'file_name' =>$resultArr['upload_data']['file_name'],
                'file_extension' =>$resultArr['upload_data']['file_type'],
            ];
            $imgId = $this->file_model->add($tempData);
            if($imgId){
                $fileDetails['event_id'] = $eventId;
                $fileDetails['file_id'] = $imgId;
                $this->load->model('event_image_model','event_image_model');
                $this->event_image_model->add($fileDetails);

            }
        }elseif (isset($resultArr['error'])){
            header('Content-Type: application/json');
            echo json_encode( $resultArr );
        }
    }


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */