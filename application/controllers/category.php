<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('user/user_model','user_model');
        $this->load->library('core_lib');
        $this->load->library('form');
        $this->load->library('user/user_lib','user_lib');
    }

    public function _remap($method)
    {
        $userGroup = $this->core_lib->getUserGroup();
        if($method == "add"){
            if(in_array("admin",$userGroup)){
                $this->add();
            }else{
                die('login required');
            }
        }elseif ($method == "validate"){
            if(in_array("admin",$userGroup)){
                $this->validate();
            }else{
                die('login required');
            }
        }elseif ($method == "listing"){
            if(in_array("admin",$userGroup)){
                $this->listing();
            }else{
                die('login required');
            }
        }elseif ($method == "categorySearchRender"){
            if(in_array("admin",$userGroup)){
                $this->categorySearchRender();
            }else{
                die('login required');
            }
        }elseif ($method == "itemView"){
            if(in_array("admin",$userGroup)){
                $this->itemView();
            }else{
                die('login required');
            }
        }elseif ($method == "delete"){
            if(in_array("admin",$userGroup)){
                $this->delete();
            }else{
                die('login required');
            }
        }elseif ($method == "categoryTree"){
            $this->categoryTree();
        }
    }
    public function add()
    {
        $categoryDetails = [];
        $mode = $this->uri->segment(2);
        if($mode == "edit"){
            $categoryId = $this->uri->segment(3);
            if($categoryId){
                $this->load->model('category_model');
                $categoryDetails = $this->category_model->getData($categoryId);
            }
        }else{
            $parentId = $this->uri->segment(3);
            $categoryDetails = [
                'parent_id' =>$parentId
            ];

        }
        $this->load->template('category/category_edit',$categoryDetails);

    }
    public function validate()
    {
        $postData = [
            'id' =>$this->input->post('id'),
            'name' =>$this->input->post('name'),
            'parent_id' =>$this->input->post('parent_id'),
            'description' =>$this->input->post('description'),
        ];
        $this->form->setRules($postData['name'],'name','Category Name');
        $validationArr = $this->form->run();
        if(is_array($validationArr) && count($validationArr) && $validationArr != false){
            header('Content-Type: application/json');
            echo json_encode( $validationArr );
        }else{
            $this->load->model('category_model');
            if(isset($postData['id']) && !empty($postData['id'])){
                $temp = [
                    'id' =>$postData['id']
                ];
                $this->category_model->update($postData,$temp);
            }else{
                $categoryId = $this->category_model->add($postData);

            }
            if(!isset($categoryId) && empty($categoryId))$categoryId = $postData['id'];
            $result = ['id'=>$categoryId,'success' =>true];
            header('Content-Type: application/json');
            echo json_encode( $result );
        }



    }

    public function listing()
    {
        $result = $this->categorySearch();
        $resultArr['data'] = $result;
        $this->load->template('category/list',$resultArr);
    }

    protected function categorySearch()
    {
        $postData = [
            'page' =>$this->input->post('page'),
            'phrase' =>$this->input->post('phrase'),
            'parent_id' =>$this->input->post('parent_id'),
        ];
        if(empty($postData['parent_id']))$postData['parent_id'] = 0;
        $this->load->model('category_model');
        $query = $this->category_model->getSearchQuery($postData);
        $result =$this->category_model->getListByQuery($query,$postData['page'],'');
        return $result;
    }
    protected function categorySearchRender()
    {
        $result = $this->categorySearch();
        $resultArr['data'] = $result;
        $this->load->view('category/search_result',$resultArr);
    }

    protected function itemView()
    {
        $id = $this->uri->segment(3);
        if($id){
            $this->load->model('category_model');
            $categoryDetails = $this->category_model->getData($id);
            $parentId = $categoryDetails['id'];
            $postData = [
                'parent_id' =>$parentId
            ];
            $query = $this->category_model->getSearchQuery($postData);
            $result =$this->category_model->getListByQuery($query,1,'');
            $outputData = [
                'details'=>$categoryDetails,
                'data'=>$result
            ];
            $this->load->template('category/view',$outputData);
        }
    }
    protected function categoryTree()
    {
        $this->load->model('category_model');
        $categoryTree = $this->category_model->getCategoryTree(0);
        header('Content-Type: application/json');
        echo json_encode( $categoryTree );
    }

    protected function delete()
    {
        $categoryId = $this->input->post('category_id');
        if ($categoryId) {
            $this->load->model('category_model', 'category_model');
            $categoryIds = $this->category_model->categories($categoryId);
            if ($categoryIds) {
                $this->category_model->deleteCategories($categoryIds);
            }
        }
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */