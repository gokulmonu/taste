<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends CI_Controller {


        function __construct() {
            parent::__construct();
        }


         function index(){
             $url = parse_url($_SERVER['REQUEST_URI']);
             if($url){
              $path = $url['path'];
                 $lastString = substr($path,strrpos($path,'/')+1);
                 if($lastString == "contact.html"){
                     $contact = base_url()."contact";
                     header('Location:'.$contact);
                 }elseif($lastString == "about.html"){
                     $about = base_url()."about-us";
                     header('Location:'.$about);
                 }else{
                     header('Location:'.base_url());
                 }
             }

	     }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */