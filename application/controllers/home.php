<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller
{


    function __construct()
    {
        parent::__construct();
    }


    function index()
    {
        $this->load->model('slider/slider_model');
        $query = $this->slider_model->getSearchQuery(['active' => 1]);
        $result['slider'] = $this->slider_model->getListByQuery($query, 1, 9999);

        $this->load->model('category/category_item_model');
        $query = $this->category_item_model->getSearchQuery(['deal_of_day'=>1,'active'=>1], []);
        $items= $this->category_item_model->getListByQuery($query, 1, 10);
        if ($items['rows']) {
            foreach ($items['rows'] as $key => $value) {
                $this->load->model('category/category_item_image_model');
                $itemImages = $this->category_item_image_model->getItemImages($value['id']);
                if (count($itemImages)) {
                    $image = current($itemImages);
                    if (strpos($image['file_path'], "uploads/") !== false) {
                        $loc = substr($image['file_path'], strpos($image['file_path'], "uploads/"));
                    }
                    $path = base_url();
                    $name = substr($image['file_name'], 0, strrpos($image['file_name'], '.'));
                    $ext = substr($image['file_name'], strrpos($image['file_name'], '.'));
                    if(isset($loc)){
                        $thumbPath = $loc;
                    }else{
                        $thumbPath = $path . 'uploads/' . $name . $ext;

                    }
                    $items['rows'][$key]['thumbnail'] = $thumbPath;

                }else{
                    $items['rows'][$key]['thumbnail'] = "";
                }
            }

        }
        $result['items'] =  $items;



      /*  $this->load->model('job/job_model','job_model');
        $latestJobs['result'] = $this->job_model->getLatestJobs();
        if($latestJobs['result']){
            foreach ($latestJobs['result'] as $key => $value){
                $this->load->model('category/category_model');
                $industryData = $this->category_model->getData($value['industry_id']);
                $latestJobs['result'][$key]['industry_name'] = isset($industryData['name']) ? $industryData['name']:"";
            }
        }*/
        /* $this->load->library('../controllers/product');
         $this->product->listing();*/
        /*$result = [];
        $projects = $this->projectSearch(3);
        if ($projects['rows']) {
            foreach ($projects['rows'] as $key => $value) {
                $projectImages = $this->getProjectImages($value['id']);
                if (count($projectImages)) {
                    $image = current($projectImages);
                    $path = base_url();
                    $name = substr($image['file_name'], 0, strrpos($image['file_name'], '.'));
                    $ext = substr($image['file_name'], strrpos($image['file_name'], '.'));
                    $thumbPath = $path . 'uploads/' . $name . $ext;
                    $projects['rows'][$key]['thumbnail'] = $thumbPath;

                }
            }

        }
        $members = $this->getMembers();
        $result['projects'] = $projects['rows'];
        $result['members'] = $members;*/
        $this->load->template('index', $result);

    }

    public function getProjectImages($projectId = 0)
    {
        $projectImages = [];
        if (!empty($projectId)) {
            $this->load->model('project_image_model', 'project_image_model');
            $projectImages = $this->project_image_model->getProjectImages($projectId);
        }
        return $projectImages;

    }

    public function validateContactUs()
    {
        $postData = [
            'name' =>$this->input->post('name'),
            'email' =>$this->input->post('email'),
            'phone' =>$this->input->post('phone'),
            'message' =>$this->input->post('message'),
            'subject' =>$this->input->post('subject'),
        ];
        $this->load->model('contactus_model', 'contactus_model');
        $this->contactus_model->add($postData);

    }
    private function projectSearch($count)
    {
        $postData = [
            'page' => $this->input->post('page'),
            'phrase' => $this->input->post('phrase')
        ];
        $this->load->model('project_model', 'project_model');
        $query = $this->project_model->getSearchQuery($postData);
        $result = $this->project_model->getListByQuery($query, $postData['page'], $count);
        return $result;
    }

    private function getMembers()
    {
        $this->load->model('member_model');
        $memberslist = $this->member_model->getAll();
        if ($memberslist) {
            foreach ($memberslist as $key => $value) {
                if ($value['file_id']) {
                    $this->load->model('file_model');
                    $membersFile = $this->file_model->getData($value['file_id']);
                    if ($membersFile) {
                        $memberslist[$key]['file_ref'] = $membersFile['file_name'];
                    }
                }
            }

        }
        return $memberslist;
    }

    public function contactUs()
    {
        $this->load->template('contact_us');
    }

    public function aboutUs()
    {
        $this->load->template('about_us');

    }

    public function services()
    {
        $this->load->template('services');
    }

    public function careers()
    {
        $this->load->model('career_model');
        $careerList = $this->career_model->getListByQuery('select * from sb_career where is_deleted is null');
        $this->load->template('carriers', $careerList);

    }

    public function stores()
    {
        $this->load->template('our_stores');

    }

    public function sendEmail()
    {
        $to = 'info@glatcomaldives.com';
        $subject = 'Enquiry Mail';
        $name = $this->input->post('name');
        $fromEmail = $this->input->post('email');
        $message = $this->input->post('message');
        $phone = $this->input->post('phone');
        $mailBody = '<html><body>';
        $mailBody .= '<div>
                             <h2>Contact Form Submission</h2>
                               <div style="width: 300px;height:200px;">
                                <p><span>From</span></p>';
        $mailBody .= $name . " " . "</br>";
        $mailBody .= $fromEmail . "</br>";
        $mailBody .= $phone . "</br>";

        $mailBody .= '</div>';
        $mailBody .= '<div><h5>Dear Admin,</h5><h6>&nbsp;&nbsp;&nbsp;&nbsp;
                                Please accept this letter as formal application for using your service.<br>
                                </h6>';

        $mailBody .= '</div>';
        $mailBody .= '</div>';
        $mailBody .= '<table width="100%" border="1">';
        $mailBody .= $message;
        $mailBody .= '</table>';
        $mailBody .= '</body></html>';

        $headers[] = 'MIME-Version: 1.0';
        $headers[] = 'Content-type: text/html; charset=iso-8859-1';

        $headers[] = 'To: Admin <' . $to . '>';
        $headers[] = 'From: ' . $name . ' <' . $fromEmail . '>';
        $params = '-f ' . $fromEmail;

        $status = mail($to, $subject, $mailBody, implode("\r\n", $headers), $params);
        $result = ['success' => $status];
        header('Content-Type: application/json');
        echo json_encode($result);
    }


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */