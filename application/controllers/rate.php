<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rate extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('user/user_model','user_model');
        $this->load->library('core_lib');
        $this->load->library('form');
        $this->load->library('user/user_lib','user_lib');
    }

    public function _remap($method)
    {
        $userGroup = $this->core_lib->getUserGroup();
        if($method == "addRate"){
            if(in_array("admin",$userGroup)){
                $this->addRate();
            }else{
                $this->core_lib->redirectPage();
            }
        }elseif ($method == "validate"){
            if(in_array("admin",$userGroup)){
                $this->validate();
            }else{
                die('login required');
            }
        }elseif ($method == "delete"){
            if(in_array("admin",$userGroup)){
                $this->delete();
            }else{
                die('login required');
            }
        }elseif ($method == "list"){
            if(in_array("admin",$userGroup)){
                $this->listRates();
            }else{
                $this->core_lib->redirectPage();
            }
        }elseif ($method == "listView"){
            if(in_array("admin",$userGroup)){
                $this->listView();
            }else{
                $this->core_lib->redirectPage();
            }
        }elseif ($method == "view"){
            if(in_array("admin",$userGroup)){
                $this->view();
            }else{
                $this->core_lib->redirectPage();
            }
        }elseif ($method == "wbRateListing"){

            $this->wbRateListing();
        }elseif ($method == "wbRateView"){

            $this->wbRateView();
        }
    }

    public function wbRateView()
    {
        $rateId = $this->uri->segment(3);
        $rateDetails = [];
        if($rateId){
            $this->load->model('rate_model');
            $rateDetails = $this->rate_model->getData($rateId);
            if($rateDetails['country']){
                $this->load->model('country_model');
                $countryData = $this->country_model->getData($rateDetails['country']);
                $rateDetails['country_code'] = strtolower($countryData['iso']);
                $rateDetails['country_name'] = $countryData['name'];
            }
        }
        $this->load->template('rate/view',$rateDetails);
    }

    public function wbRateListing()
    {
        $this->load->model('rate_model');
        $rateList = $this->rate_model->getAll();
        if($rateList){
            $today = strtotime('today');
            foreach($rateList as $key => $rate){
                if(($rate['active'] != 1)){
                    unset($rateList[$key]);
                    continue;
                }
                if($rate['country']){
                    $this->load->model('country_model');
                    $countryData = $this->country_model->getData($rate['country']);
                    $rateList[$key]['country_code'] = strtolower($countryData['iso']);
                    $rateList[$key]['country_name'] = $countryData['name'];
                }
            }
        }
        $rateArr['list'] = $rateList;
        $this->load->template('rate/list',$rateArr);
    }
    public function listRates()
    {
        $this->load->model('rate_model');
        $rateList = $this->rate_model->getAll();
        if($rateList){
            foreach($rateList as $key => $rate){
                if($rate['country']){
                    $this->load->model('country_model');
                    $countryData = $this->country_model->getData($rate['country']);
                    $rateList[$key]['country_code'] = strtolower($countryData['iso']);
                    $rateList[$key]['country_name'] = $countryData['name'];
                }
            }
        }
        $rateList['list'] = $rateList;
        $bc = [];
        $bc['admin/rates'] = "Rates";
        $rateList['breadcrumbs'] = $bc;

        $pageTitle = [];
        $pageTitle['title'] = "Rates";
        $pageTitle['icon'] = "fa fa-dollar";
        $rateList['page_header'] = $pageTitle;
        $this->load->adminTemplate('rate/list',$rateList);
    }

    public function listView()
    {
        $this->load->model('rate_model');
        $rateList = $this->rate_model->getAll();
        if($rateList){
            foreach($rateList as $key => $rate){
                if($rate['country']){
                    $this->load->model('country_model');
                    $countryData = $this->country_model->getData($rate['country']);
                    $rateList[$key]['country_code'] = strtolower($countryData['iso']);
                    $rateList[$key]['country_name'] = $countryData['name'];
                }
            }
        }
        $rateList['list'] = $rateList;
        $bc = [];
        $bc['admin/rates'] = "Rates";
        $rateList['breadcrumbs'] = $bc;

        $pageTitle = [];
        $pageTitle['title'] = "Rates";
        $pageTitle['icon'] = "fa fa-dollar";
        $rateList['page_header'] = $pageTitle;
        $this->load->adminTemplate('rate/list_view',$rateList);
    }


    public function view()
    {
        $rateId = $this->uri->segment(4);
        $rateDetails = [];
        if($rateId){
            $this->load->model('rate_model');
            $rateDetails = $this->rate_model->getData($rateId);
            if($rateDetails['country']){
                $this->load->model('country_model');
                $countryData = $this->country_model->getData($rateDetails['country']);
                $rateDetails['country_code'] = strtolower($countryData['iso']);
                $rateDetails['country_name'] = $countryData['name'];
            }
        }

        $bc = [];
        $bc['admin/rates/list'] = "Rates";
        $bc['admin/rate/view/'.$rateId] = "View";
        $rateDetails['breadcrumbs'] = $bc;

        $pageTitle = [];
        $pageTitle['title'] = "Rate View";
        $pageTitle['icon'] = "fa fa-dollar";
        $rateDetails['page_header'] = $pageTitle;
        $this->load->adminTemplate('rate/view',$rateDetails);
    }
    public function addRate()
    {
        $rateDetails = [];
        $mode = $this->uri->segment(3);
        if($mode == "edit"){
            $rateId = $this->uri->segment(4);
            if($rateId){
                $this->load->model('rate_model');
                $rateDetails = $this->rate_model->getData($rateId);
            }
        }
        $this->load->model('country_model');
        $countryList = $this->country_model->getAll();
        $rateDetails['country_list'] = $countryList;
        $bc = [];
        $bc['admin/rates'] = "Rates";
        $bc['admin/rate/add'] = "Add";
        $rateDetails['breadcrumbs'] = $bc;

        $pageTitle = [];
        $pageTitle['title'] = "Rate";
        $pageTitle['icon'] = "fa fa-dollar";
        $rateDetails['page_header'] = $pageTitle;
        $this->load->adminTemplate('rate/edit',$rateDetails);

    }
    public function validate()
    {
        $postData = [
            'id' =>$this->input->post('id'),
            'active' =>$this->input->post('active'),
            'type' =>$this->input->post('type'),
            'title' =>$this->input->post('title'),
            'price' =>$this->input->post('price'),
            'description' =>$this->input->post('description'),
            'start_date' =>$this->input->post('start_date'),
            'finish_date' =>$this->input->post('finish_date'),
            'country' =>$this->input->post('country'),
        ];
        $this->form->setRules($postData['title'],'title_error','Please enter rate title');
        $validationArr = $this->form->run();
        if(is_array($validationArr) && count($validationArr) && $validationArr != false){
            header('Content-Type: application/json');
            echo json_encode( $validationArr );
        }else{
            $this->load->model('rate_model');
            if(isset($postData['id']) && !empty($postData['id'])){
                $temp = [
                    'id' =>$postData['id']
                ];
                $this->rate_model->update($postData,$temp);
            }else{
                $rateId = $this->rate_model->add($postData);

            }
            if(!isset($rateId) && empty($rateId))$rateId = $postData['id'];
            $result = ['id'=>$rateId,'success' =>true];
            header('Content-Type: application/json');
            echo json_encode( $result );
        }



    }



    protected function delete()
    {
        $rateId = $this->input->post('id');
        if ($rateId) {
            $this->load->model('rate_model', 'rate_model');
            $this->rate_model->delete(['id'=>$rateId]);
        }
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */