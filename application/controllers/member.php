<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('user/user_model','user_model');
        $this->load->library('core_lib');
        $this->load->library('form');
        $this->load->library('user/user_lib','user_lib');
    }

    public function _remap($method)
    {
        $userGroup = $this->core_lib->getUserGroup();
        if($method == "edit"){
            if(in_array("admin",$userGroup)){
                $this->edit();
            }else{
                $this->core_lib->redirectPage();
            }
        }elseif ($method == "validate"){
            if(in_array("admin",$userGroup)){
                $this->validate();
            }else{
                die('login required');
            }
        }elseif ($method == "delete"){
            if(in_array("admin",$userGroup)){
                $this->delete();
            }else{
                die('login required');
            }
        }elseif ($method == "removeImage"){
            if(in_array("admin",$userGroup)){
                $this->removeImage();
            }else{
                die('login required');
            }
        }elseif ($method == "listMembers"){
            if(in_array("admin",$userGroup)){
                $this->listMembers();
            }else{
                $this->core_lib->redirectPage();
            }
        }
    }
    public function delete()
    {
        $memberId = $this->input->post('member_id');
        if ($memberId) {
            $this->load->model('member_model', 'member_model');
            $temp = [
                'id' =>$memberId
            ];
            $this->member_model->delete($temp);
        }
    }
    public function validate()
    {
        $postData = [
            'id' =>$this->input->post('id'),
            'name' =>$this->input->post('name'),
            'position' =>$this->input->post('position'),
            'email' =>$this->input->post('email'),
            'phone' =>$this->input->post('phone'),
        ];


        $this->load->library('upload_lib');
        $resultArr = $this->upload_lib->doUpload();
        if(count($resultArr) && !isset($resultArr['error'])){
            $this->load->model('file_model','file_model');
            $tempData = [
                'file_path' =>$resultArr['upload_data']['full_path'],
                'file_name' =>$resultArr['upload_data']['file_name'],
                'file_extension' =>$resultArr['upload_data']['file_type'],
            ];
            $imgId = $this->file_model->add($tempData);
            if($imgId){
                $postData['file_id'] =  $imgId;
            }
        }

        $this->load->model('member_model');
        if(isset($postData['id']) && !empty($postData['id'])){
            $temp = [
                'id' =>$postData['id']
            ];
            $this->member_model->update($postData,$temp);
        }else{
            $memberId = $this->member_model->add($postData);

        }
        if(!isset($memberId) && empty($memberId))$memberId = $postData['id'];
        $result = ['id'=>$memberId,'success' =>true];
        header('Content-Type: application/json');
        echo json_encode( $result );
    }

    public function edit()
    {
        $id = $this->uri->segment(4);
        if($id){
            $this->load->model('member_model');
            $membersDetails = $this->member_model->getData($id);
            if($membersDetails){
               if($membersDetails['file_id']){
                   $this->load->model('file_model');
                   $membersFile = $this->file_model->getData($membersDetails['file_id']);
                   if($membersFile){
                       $membersDetails['file_ref'] = $membersFile['file_name'];
                   }
               }
            }
        }
        $bc = [];
        $bc['members'] = "Members";
        $bc['member/add'] = "Add/Edit";
        $membersDetails['breadcrumbs'] = $bc;

        $pageTitle = [];
        $pageTitle['title'] = "Our Team";
        $pageTitle['icon'] = "fa fa-user";
        $membersDetails['page_header'] = $pageTitle;
        $this->load->adminTemplate('member/edit',$membersDetails);
    }

    public function listMembers()
    {
        $this->load->model('member_model');
        $membersDetails['list'] = $this->member_model->getAll();
        if($membersDetails['list']){
            foreach($membersDetails['list'] as $key => $value){
                if($value['file_id']){
                    $this->load->model('file_model');
                    $membersFile = $this->file_model->getData($value['file_id']);
                    if($membersFile){
                        $membersDetails['list'][$key]['file_ref'] = $membersFile['file_name'];
                    }
                }
            }

        }
        $bc = [];
        $bc['members'] = "Members";
        $membersDetails['breadcrumbs'] = $bc;

        $pageTitle = [];
        $pageTitle['title'] = "Our Team";
        $pageTitle['icon'] = "fa fa-user";
        $membersDetails['page_header'] = $pageTitle;
        $this->load->adminTemplate('member/list',$membersDetails);
    }

    public function removeImage()
    {
        $fileId = $this->input->post('file_id');
        if($fileId){
            $this->load->model('file_model','file_model');
            $fileDetails =  $this->file_model->getData($fileId);
            $temp = [
                'id' =>$fileId
            ];
            $this->file_model->delete($temp);
            $path = $fileDetails['file_path'];
            $fileUrl = substr($path,0,strrpos($path,'/'));
            $name = substr($fileDetails['file_name'],0,strrpos($fileDetails['file_name'],'.'));
            $thumbPath = $fileUrl.'/thumbnail/'.$name.'_thumb.png';
            unlink($fileDetails['file_path']);
            unlink($thumbPath);
        }
    }


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */