<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {


    public function __construct() {
        parent::__construct();
        $this->load->model('user/user_model','user_model');
        $this->load->library('core_lib');
        $this->load->library('form');
        $this->load->library('user/user_lib','user_lib');
        $this->load->model('product_model','product_model');

    }

    public function _remap($method)
    {
        $userGroup = $this->core_lib->getUserGroup();
        if($method == "login"){
           $this->login();
        }elseif(trim($method) == "validate"){
            $this->validate();
        }elseif(trim($method) == "logout"){
            $this->logout();
        }elseif(trim($method) == "dashboard"){
            if(in_array("admin",$userGroup)){
                $this->dashboard();
            }else{
                $this->core_lib->redirectPage();
            }

        }elseif(trim($method) == "editProfile"){
            if(in_array("admin",$userGroup)){
                $this->editProfile();
            }else{
                $this->core_lib->redirectPage();
            }

        }elseif(trim($method) == "profileValidate"){
            if(in_array("admin",$userGroup)){
                $this->profileValidate();
            }else{
                die('login required');
            }

        }

    }

        
       public function login()
       {
           $isLoggedIn = $this->core_lib->getLoginUserId();
           if(!$isLoggedIn){
               $data = [];
               $url = parse_url($_SERVER['REQUEST_URI']);
               if(isset($url['query'])){
                   parse_str($url['query'], $params);
                   if(isset($params['redirect']) && !empty($params['redirect'])){
                       $data['redirect'] = $params['redirect'];
                   }
               }


               $this->load->view('admin_login',$data);
           }else{
               $baseUrl = base_url();
               header('Location:'.$baseUrl);
           }

       }
        public function logout()
        {
            $this->load->library('default_session');
            $this->default_session->destroy();
            redirect('admin/login', 'refresh');
        }
       public function validate()
        {
            $password = $this->input->post('password');
            $username = $this->input->post('username');
            $decodedString = base64_decode($password);
            $this->form->setRules($username,'username_error','Please enter your username');
            $this->form->setRules($password,'password_error','Please enter your password');
            $validationArr = $this->form->run();
            if(is_array($validationArr) && count($validationArr) && $validationArr != false){
                header('Content-Type: application/json');
                echo json_encode( $validationArr );
            }else{
               // echo password_hash("rasmuslerdorf", PASSWORD_DEFAULT);
                $passwordHash = $this->user_model->getPasswordHash($username);
                if(count($passwordHash)){
                    if(password_verify($decodedString,$passwordHash['password'])){
                        $this->load->library('default_session');
                        $this->default_session->set('user_id',$passwordHash['id']);
                        $redirectUrl['url'] = [
                            'success' => true,
                        ];
                        header('Content-Type: application/json');
                        echo json_encode( $redirectUrl );
                    }else{
                        header('Content-Type: application/json');
                        $redirectUrl['url'] = [
                            'success' => false
                        ];
                        echo json_encode( $redirectUrl );
                    }
                }
            }
        }

    public function dashboard()
    {
        $this->load->adminTemplate('index','');
    }

    public function editProfile()
    {
        $isLoggedIn = $this->core_lib->getLoginUserId();
        $userDetails = $this->user_model->getData($isLoggedIn);
        $bc = [];
        $bc['my-profile'] = "My Profile";
        $userDetails['breadcrumbs'] = $bc;

        $pageTitle = [];
        $pageTitle['title'] = "My Profile";
        $pageTitle['icon'] = "fa fa-user";
        $userDetails['page_header'] = $pageTitle;

        $this->load->adminTemplate('profile/profile',$userDetails);
    }

    public function profileValidate()
    {
        $postData = [
            'id' =>$this->input->post('id'),
            'first_name' =>$this->input->post('first_name'),
            'last_name' =>$this->input->post('last_name'),
            'position' =>$this->input->post('position')
        ];

        $this->form->setRules($postData['first_name'],'first_name_error','Please enter your first name');
        $this->form->setRules($postData['last_name'],'last_name_error','Please enter your last name');
        $validationArr = $this->form->run();

        if(is_array($validationArr) && count($validationArr) && $validationArr != false){
            header('Content-Type: application/json');
            echo json_encode( $validationArr );
        }else{
            $this->load->library('upload_lib');
            $resultArr = $this->upload_lib->doUpload();
            if(count($resultArr) && !isset($resultArr['error'])){
                $this->load->model('file_model','file_model');
                $tempData = [
                    'file_path' =>$resultArr['upload_data']['full_path'],
                    'file_name' =>$resultArr['upload_data']['file_name'],
                    'file_extension' =>$resultArr['upload_data']['file_type'],
                ];
                $imgId = $this->file_model->add($tempData);
                if($imgId){
                    $postData['file_ref'] =  $tempData['file_name'];
                }
            }

            if(isset($postData['id']) && !empty($postData['id'])){
                $temp = [
                    'id' =>$postData['id']
                ];
                $this->user_model->update($postData,$temp);
            }else{
                $memberId = $this->user_model->add($postData);

            }
            if(!isset($memberId) && empty($memberId))$memberId = $postData['id'];
            $result = ['id'=>$memberId,'success' =>true];
            header('Content-Type: application/json');
            echo json_encode( $result );
        }


    }


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */