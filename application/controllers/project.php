<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Project extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user/user_model', 'user_model');
        $this->load->library('core_lib');
        $this->load->library('form');
        $this->load->library('default_session');
        $this->load->library('user/user_lib', 'user_lib');
        $this->load->model('project_model', 'project_model');

    }


    public function _remap($method)
    {
        $userGroup = $this->core_lib->getUserGroup();
        if ($method == "add") {
            if (in_array("admin", $userGroup)) {
                $this->add();
            } else {
                $this->core_lib->redirectPage();
            }
        } elseif ($method == "validate") {
            if (in_array("admin", $userGroup)) {
                $this->validate();
            } else {
                die('login required');
            }
        } elseif ($method == "listing") {
            if (in_array("admin", $userGroup)) {
                $this->listing();
            } else {
                $this->core_lib->redirectPage();
            }
        } elseif ($method == "projectSearchRender") {
            if (in_array("admin", $userGroup)) {
                $this->projectSearchRender();
            } else {
                die('login required');
            }
        } elseif ($method == "delete") {
            if (in_array("admin", $userGroup)) {
                $this->delete();
            } else {
                die('login required');
            }
        } elseif ($method == "view") {
            if (in_array("admin", $userGroup)) {
                $this->view();
            } else {
                $this->core_lib->redirectPage();
            }
        } elseif ($method == "uploadProjectImage") {
            if (in_array("admin", $userGroup)) {
                $this->uploadProjectImage();
            } else {
                die('login required');
            }
        } elseif ($method == "getProjectImageForRender") {
            if (in_array("admin", $userGroup)) {
                $this->getProjectImageForRender();
            } else {
                die('login required');
            }
        } elseif ($method == "removeImage") {
            if (in_array("admin", $userGroup)) {
                $this->removeImage();
            } else {
                die('login required');
            }
        } elseif ($method == "getProjectsForWebsite") {
            $this->getProjectsForWebsite();
        } elseif ($method == "websiteView") {
            $this->websiteView();
        }
    }

    public function websiteView()
    {

        $id = $this->uri->segment(3);
        if ($id) {
            $projectDetails = $this->project_model->getData($id);
            $projectImages = $this->getProjectImages($id);
            if (count($projectImages)) {
                foreach ($projectImages as $key => $image) {
                    $path = base_url();
                    $name = substr($image['file_name'], 0, strrpos($image['file_name'], '.'));
                    $ext = substr($image['file_name'], strrpos($image['file_name'], '.'));
                    $thumbPath = $path . 'uploads/' . $name . $ext;
                    $projectImages[$key]['thumbnail'] = $thumbPath;
                }

            }
            $projectDetails['images'] = $projectImages;
            $this->load->template('app_view', $projectDetails);
        }
    }
    public function getProjectsForWebsite()
    {
        $result = [];
        $projects = $this->projectSearch();
        if ($projects['rows']) {
            foreach ($projects['rows'] as $key => $value) {
                $projectImages = $this->getProjectImages($value['id']);
                if (count($projectImages)) {
                    $image = current($projectImages);
                    $path = base_url();
                    $name = substr($image['file_name'], 0, strrpos($image['file_name'], '.'));
                    $ext = substr($image['file_name'], strrpos($image['file_name'], '.'));
                    $thumbPath = $path . 'uploads/' . $name . $ext;
                    $projects['rows'][$key]['thumbnail'] = $thumbPath;

                }
            }

        }
        $result['projects'] = $projects['rows'];
        $this->load->template('apps', $result);
    }

    public function add()
    {
        $projectDetails = [];

        $mode = $this->uri->segment(3);
        if ($mode == "edit") {
            $projectId = $this->uri->segment(4);
            if ($projectId) {
                $projectDetails = $this->project_model->getData($projectId);
            }
        }
        $this->load->adminTemplate('project/edit', $projectDetails);
    }

    public function validate()
    {

        $data = json_decode(file_get_contents('php://input'), true);
        $this->form->setRules($data['name'], 'name_error', 'Please Enter Project Name ');
        $validationArr = $this->form->run();
        if (is_array($validationArr) && count($validationArr) && $validationArr != false) {
            header('Content-Type: application/json');
            echo json_encode($validationArr);
        } else {
            $this->load->model('project_model', 'project_model');
            if (isset($data['id']) && !empty($data['id'])) {
                $temp = [
                    'id' => $data['id']
                ];
                $this->project_model->update($data, $temp);
            } else {
                $projectId = $this->project_model->add($data);

            }
            if (!isset($projectId) && empty($projectId)) $projectId = $data['id'];
            $result = ['id' => $projectId, 'success' => true];
            header('Content-Type: application/json');
            echo json_encode($result);
        }

    }

    public function listing()
    {
        $result = $this->projectSearch();
        $resultArr['data'] = $result;
        $bc = [];
        $bc['projects'] = "Projects";
        $resultArr['breadcrumbs'] = $bc;

        $pageTitle = [];
        $pageTitle['title'] = "App Listing";
        $pageTitle['icon'] = "fa fa-edit";
        $resultArr['page_header'] = $pageTitle;

        $this->load->adminTemplate('project/list', $resultArr);
    }

    protected function projectSearch()
    {
        $postData = [
            'page' => $this->input->post('page'),
            'phrase' => $this->input->post('phrase')
        ];

        $query = $this->project_model->getSearchQuery($postData);
        $result = $this->project_model->getListByQuery($query, $postData['page'], '');
        return $result;
    }

    protected function projectSearchRender()
    {
        $result = $this->projectSearch();
        $resultArr['data'] = $result;
        $this->load->view('admin/project/search_result', $resultArr);
    }

    protected function delete()
    {
        $projectId = $this->input->post('project_id');
        if ($projectId) {
            $this->project_model->delete(['id' => $projectId]);
        }
    }

    protected function view()
    {
        $id = $this->uri->segment(4);
        if ($id) {
            $projectDetails = $this->project_model->getData($id);
            $projectImages = $this->getProjectImages($id);
            if (count($projectImages)) {
                foreach ($projectImages as $key => $image) {
                    $path = base_url();
                    $name = substr($image['file_name'], 0, strrpos($image['file_name'], '.'));
                    $ext = substr($image['file_name'], strrpos($image['file_name'], '.'));
                    $thumbPath = $path . 'uploads/' . $name . $ext;
                    $projectImages[$key]['thumbnail'] = $thumbPath;
                }

            }
            $projectDetails['images'] = $projectImages;

            $bc = [];
            $bc['projects'] = "Apps";
            $bc['project/view/' . $id] = $projectDetails['name'];
            $projectDetails['breadcrumbs'] = $bc;

            $pageTitle = [];
            $pageTitle['title'] = "App View";
            $pageTitle['icon'] = "fa fa-edit";
            $projectDetails['page_header'] = $pageTitle;


            $this->load->adminTemplate('project/view', $projectDetails);
        }
    }

    public function uploadProjectImage()
    {
        $projectId = $this->input->post('project_id');
        $this->load->library('upload_lib');
        $resultArr = $this->upload_lib->doUpload();
        if (count($resultArr) && !isset($resultArr['error'])) {
            $this->load->model('file_model', 'file_model');
            $tempData = [
                'file_path' => $resultArr['upload_data']['full_path'],
                'file_name' => $resultArr['upload_data']['file_name'],
                'file_extension' => $resultArr['upload_data']['file_type'],
            ];
            $imgId = $this->file_model->add($tempData);
            if ($imgId) {
                $fileDetails['project_id'] = $projectId;
                $fileDetails['file_id'] = $imgId;
                $this->load->model('project_image_model', 'project_image_model');
                $this->project_image_model->add($fileDetails);

            }
        } elseif (isset($resultArr['error'])) {
            header('Content-Type: application/json');
            echo json_encode($resultArr);
        }
    }

    public function getProjectImageForRender()
    {
        $projectId = $this->input->post('id');
        $imagesList = $this->getProjectImages($projectId);
        if (count($imagesList)) {
            foreach ($imagesList as $key => $image) {
                /*$path = base_url();
                $name = substr($image['file_name'],0,strrpos($image['file_name'],'.'));
                $ext = substr($image['file_name'],strrpos($image['file_name'],'.'));
                $thumbPath = $path.'uploads/thumbnail/'.$name.'_thumb'.$ext;
                $imagesList[$key]['thumbnail'] = $thumbPath;*/
                $path = base_url();
                $name = substr($image['file_name'], 0, strrpos($image['file_name'], '.'));
                $ext = substr($image['file_name'], strrpos($image['file_name'], '.'));
                $thumbPath = $path . 'uploads/' . $name . $ext;
                $imagesList[$key]['thumbnail'] = $thumbPath;
            }

        }
        $data['images'] = $imagesList;
        $this->load->view('admin/project/image_list', $data);
    }


    public function getProjectImages($projectId = 0)
    {
        $projectImages = [];
        if (!empty($projectId)) {
            $this->load->model('project_image_model', 'project_image_model');
            $projectImages = $this->project_image_model->getProjectImages($projectId);
        }
        return $projectImages;

    }

    public function removeImage()
    {
        $fileId = $this->input->post('file_id');
        if ($fileId) {
            $this->load->model('file_model', 'file_model');
            $fileDetails = $this->file_model->getData($fileId);
            $temp = [
                'id' => $fileId
            ];
            $this->file_model->delete($temp);
            $path = $fileDetails['file_path'];
            $fileUrl = substr($path, 0, strrpos($path, '/'));
            $name = substr($fileDetails['file_name'], 0, strrpos($fileDetails['file_name'], '.'));
            $thumbPath = $fileUrl . '/thumbnail/' . $name . '_thumb.png';
            unlink($fileDetails['file_path']);
            unlink($thumbPath);
        }

        if ($fileId) {
            $this->load->model('project_image_model', 'project_image_model');
            $temp = [
                'file_id' => $fileId
            ];
            $this->project_image_model->delete($temp);
        }
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */