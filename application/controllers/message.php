<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Message extends CI_Controller {


    public function __construct() {
        parent::__construct();
        $this->load->model('user/user_model','user_model');
        $this->load->library('core_lib');
        $this->load->library('form');
        $this->load->library('user/user_lib','user_lib');
        $this->load->model('message_model','message_model');

    }

    public function _remap($method)
    {
        $userGroup = $this->core_lib->getUserGroup();
        if(trim($method) == "listMessage"){
            if(in_array("admin",$userGroup)){
                $this->listMessage();
            }else{
                $this->core_lib->redirectPage();
            }

        }elseif(trim($method) == "getConversations"){
            if(in_array("admin",$userGroup)){
                $this->getConversations();
            }else{
                $this->core_lib->redirectPage();
            }

        }elseif(trim($method) == "view"){
            if(in_array("admin",$userGroup)){
                $this->view();
            }else{
                $this->core_lib->redirectPage();
            }

        }elseif(trim($method) == "getMessages"){
            if(in_array("admin",$userGroup)){
                $this->getMessages();
            }else{
                $this->core_lib->redirectPage();
            }

        }elseif(trim($method) == "generate"){
            if(in_array("admin",$userGroup)){
                $this->generate();
            }else{
                $this->core_lib->redirectPage();
            }

        }elseif(trim($method) == "updateLastSeen"){
            if($this->core_lib->getLoginUserId()){
                $this->updateLastSeen();
            }
        }elseif(trim($method) == "updateStatus"){
            if($this->core_lib->getLoginUserId()){
                $this->updateStatus();
            }
        }elseif(trim($method) == "sendMessage"){
            if($this->core_lib->getLoginUserId()){
                $this->sendMessage();
            }
        }elseif(trim($method) == "wbChat"){
            if($this->core_lib->getLoginUserId()){
                $this->wbChat();
            }else{
            $this->loginChat();
            }
        }elseif(trim($method) == "wbGetConversations"){
            if($this->core_lib->getLoginUserId()){
                $this->wbGetConversations();
            }else{
            $this->loginChat();
            }
        }elseif(trim($method) == "wbView"){
            if($this->core_lib->getLoginUserId()){
                $this->wbView();
            }else{
            $this->loginChat();
            }
        }elseif(trim($method) == "wbGetMessages"){
            if($this->core_lib->getLoginUserId()){
                $this->wbGetMessages();
            }else{
            $this->loginChat();
            }
        }elseif(trim($method) == "wbNewChat"){
            if($this->core_lib->getLoginUserId()){
                $this->wbNewChat();
            }else{
            $this->loginChat();
            }
        }elseif(trim($method) == "wbNewChatSend"){
            if($this->core_lib->getLoginUserId()){
                $this->wbNewChatSend();
            }else{
            $this->loginChat();
            }
        }elseif(trim($method) == "uploadFile"){
            if($this->core_lib->getLoginUserId()){
                $this->uploadFile();
            }else{
            $this->loginChat();
            }
        }elseif(trim($method) == "downLoadFile"){
            if($this->core_lib->getLoginUserId()){
                $this->downLoadFile();
            }else{
            $this->loginChat();
            }
        }elseif(trim($method) == "wbLogout"){
            if($this->core_lib->getLoginUserId()){
                $this->wbLogout();
                $this->loginChat();
            }else{
            $this->loginChat();
            }
        }

    }

    public function downLoadFile()
    {
        $fileId = $_GET['file_id'];
        $this->load->model('file_model', 'file_model');
        $fileDetails = $this->file_model->getData($fileId);
        if($fileDetails)
        {
            // required for IE
            if(ini_get('zlib.output_compression')) { ini_set('zlib.output_compression', 'Off'); }

            // Build the headers to push out the file properly.
            header('Pragma: public');     // required
            header('Expires: 0');         // no cache
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Last-Modified: '.gmdate ('D, d M Y H:i:s', filemtime ($fileDetails['file_path'])).' GMT');
            header('Cache-Control: private',false);
            header('Content-Type: '.$fileDetails['file_extension']);  // Add the mime type from Code igniter.
            header('Content-Disposition: attachment; filename="'.basename($fileDetails['file_name']).'"');  // Add the file name
            header('Content-Transfer-Encoding: binary');
            header('Content-Length: '.filesize($fileDetails['file_path'])); // provide file size
            header('Connection: close');
            readfile($fileDetails['file_path']); // push it out
            exit();
        }
    }
    public function wbLogout()
    {
        $this->load->library('default_session');
        $this->default_session->destroy();
    }
    public function wbNewChatSend()
    {
        $success = false;
        $fromId = $this->core_lib->getLoginUserId();
        $toEmail = $this->input->post('to_email');
        if($toEmail){
            $userDetails = $this->user_model->loadByFields(['email'=>$toEmail]);
            if($userDetails){
                $message = $this->input->post('msg');
                date_default_timezone_set("Asia/Kolkata");
                $currentTime = date('H:i:s');
                $temp = [
                    'from_id' =>$fromId,
                    'to_id' =>$userDetails['id'],
                    'message' =>$message,
                    'msg_created'=>$currentTime
                ];
                $this->message_model->add($temp);
                $success = true;

            }

        }
        $result = ['success' => $success];
        header('Content-Type: application/json');
        echo json_encode($result);
    }


    public function wbNewChat()
    {

        $this->load->model('user/user_group_model','user_group_model');
        $adminList['admin'] = $this->user_group_model->getAdminList();
        $this->load->chatTemplate('new_chat',$adminList);
    }
    public function wbChat()
    {
        $resultArr = [];
        $userId = $this->core_lib->getLoginUserId();
        $messages = $this->message_model->getAllMessages($userId,0);
        if($messages){
            foreach($messages as $msg){
                $resultArr[$msg['to_id']]['messages'][] = $msg;
            }
        }
        $fromMessages = $this->message_model->getAllMessages(0,$userId);
        if($fromMessages){
            foreach($fromMessages as $msg){
                $resultArr[$msg['from_id']]['messages'][] = $msg;
            }
        }
        if($resultArr){
            foreach($resultArr as $key => $user){
                $resultArr[$key]['user_data']= $this->user_model->getData($key);
            }
        }

        $arr = [];
        $arr['list'] = $resultArr;
        $this->load->chatTemplate('search_result',$arr);
    }

    public function wbGetConversations()
    {
        $resultArr = [];
        $userId = $this->core_lib->getLoginUserId();
        $fromMessages = $this->message_model->getAllMessages(0,$userId);

        if($fromMessages){
            foreach($fromMessages as $msg){
                $resultArr[$msg['from_id']][] = $msg;
            }
        }

        if ($resultArr) {
            foreach ($resultArr as $key => $value) {
                $lastFromMessage = current($value);
                $toId = $lastFromMessage['from_id'];
                $fromId = $lastFromMessage['to_id'];
                $currentMsgId = $lastFromMessage['id'];
                $lastToMessage = $this->message_model->getMyLastMessage($toId, $fromId, $currentMsgId);
                if ($lastToMessage) {
                    $lastMsg = current($lastToMessage);
                    array_unshift($value, $lastMsg);
                    $resultArr[$key] = $value;
                }
            }
        }

        $finalArr = [];
        if($resultArr){
            foreach($resultArr as $key => $value){
                $kk = (string)$key;
                $finalArr["u_".$kk]['messages'] = $value;
            }
        }

        $loginUserId = $this->core_lib->getLoginUserId();
        if($finalArr){
            foreach($finalArr as $key => $user){
                $userId = substr($key,2);
                $userDetails = $this->user_model->getData($userId);
                $unreadCount = $this->message_model->getUnreadMessageCount($userId,$loginUserId);

                $status ="away";
                if(isset($userDetails['last_seen'])){
                    date_default_timezone_set("Asia/Kolkata");
                    $currentDateTime = date('Y-m-d H:i:s');
                    $lastSeenAdded = strtotime($userDetails['last_seen']) + 60;
                    $currentStr = strtotime($currentDateTime);
                    if($lastSeenAdded >$currentStr){
                        $status = "online";
                    }
                }
                $userDetails['status'] = $status;
                $finalArr[$key]['user_data'] = $userDetails;
                $finalArr[$key]['unread_msg_count'] = $unreadCount;
            }
        }
        $result = ['result' => $finalArr, 'success' => true];
        header('Content-Type: application/json');
        echo json_encode($result);
    }


    public function wbView()
    {
        $id = $this->uri->segment(3);
        $arr['user_id']= $id;

        $userId = $this->core_lib->getLoginUserId();
        $arr['user_details'] = $this->user_model->getData($userId);
        $this->load->chatTemplate('view',$arr);
    }

    public function wbGetMessages()
    {

        $toId = $this->core_lib->getLoginUserId();
        $fromId = $this->input->post('id');
        $lastId = $this->input->post('last_id');

        $messages = $this->message_model->getMessages($toId,$fromId,$lastId);

        $result = ['result' => $messages, 'success' => true];
        header('Content-Type: application/json');
        echo json_encode($result);

    }

    public function loginChat()
    {
        $this->load->chatTemplate('login');
    }

    public function sendMessage()
    {
        $fromId = $this->core_lib->getLoginUserId();
        $toId = $this->input->post('to_id');
        $message = $this->input->post('msg');
        date_default_timezone_set("Asia/Kolkata");
        $currentTime = date('H:i:s');
        $temp = [
            'from_id' =>$fromId,
            'to_id' =>$toId,
            'message' =>$message,
            'msg_created' =>$currentTime
        ];
        $this->message_model->add($temp);
    }

    public function uploadFile()
    {

        $this->load->library('upload_lib');
        $resultArr = $this->upload_lib->doUpload(false);
        if (count($resultArr) && !isset($resultArr['error'])) {
            $this->load->model('file_model', 'file_model');
            $tempData = [
                'file_path' => $resultArr['upload_data']['full_path'],
                'file_name' => $resultArr['upload_data']['file_name'],
                'file_extension' => $resultArr['upload_data']['file_type'],
            ];
            $imgId = $this->file_model->add($tempData);
            if ($imgId) {
                $fromId = $this->core_lib->getLoginUserId();
                $toId = $this->input->post('to_id');
                date_default_timezone_set("Asia/Kolkata");
                $currentTime = date('H:i:s');
                $temp = [
                    'from_id' => $fromId,
                    'to_id' => $toId,
                    'file_id' => $imgId,
                    'msg_created' => $currentTime
                ];
                $this->message_model->add($temp);

            }
        }else{
            echo '<pre>';print_r($resultArr);die();
        }


    }
    public function updateStatus()
    {
        $toId = $this->core_lib->getLoginUserId();
        $fromId = $this->input->post('id');
        $temp = [
            'from_id' =>$fromId,
            'to_id' =>$toId
        ];
        $this->message_model->update(['status'=>1],$temp);
    }

    public function updateLastSeen()
    {
        $userId = $this->core_lib->getLoginUserId();
        date_default_timezone_set("Asia/Kolkata");
        $currentDateTime = date('Y-m-d H:i:s');
        $temp = [
            'id' => $userId
        ];
        $this->user_model->update(['last_seen'=>$currentDateTime],$temp);
    }

    public function view()
    {
        $id = $this->uri->segment(4);
        $bc = [];
        $bc['messages'] = "Messages";
        $bc['message/view/'.$id] = "View";
        $arr['breadcrumbs'] = $bc;

        $pageTitle = [];
        $pageTitle['title'] = "View Message";
        $pageTitle['icon'] = "fa fa-comments";
        $arr['page_header'] = $pageTitle;
        $arr['user_id']= $id;

        $userId = $this->core_lib->getLoginUserId();
        $arr['user_details'] = $this->user_model->getData($userId);
        $this->load->adminTemplate('message/view',$arr);
    }

    public function getMessages()
    {

        $toId = $this->core_lib->getLoginUserId();
        $fromId = $this->input->post('id');
        $messages = $this->message_model->getMessages($toId,$fromId);
        $result = ['result' => $messages, 'success' => true];
        header('Content-Type: application/json');
        echo json_encode($result);

    }
      public function listMessage()
      {
          $resultArr = [];
          $userId = $this->core_lib->getLoginUserId();
          $messages = $this->message_model->getAllMessages($userId,0);
          if($messages){
              foreach($messages as $msg){
                  $resultArr[$msg['to_id']]['messages'][] = $msg;
              }
          }
          $fromMessages = $this->message_model->getAllMessages(0,$userId);
          if($fromMessages){
              foreach($fromMessages as $msg){
                  $resultArr[$msg['from_id']]['messages'][] = $msg;
              }
          }
          if($resultArr){
              foreach($resultArr as $key => $user){
                  $resultArr[$key]['user_data']= $this->user_model->getData($key);
              }
          }

          $arr = [];
          $arr['list'] = $resultArr;
          $bc = [];
          $bc['messages'] = "Messages";
          $arr['breadcrumbs'] = $bc;

          $pageTitle = [];
          $pageTitle['title'] = "Messages";
          $pageTitle['icon'] = "fa fa-comments";
          $arr['page_header'] = $pageTitle;

          $this->load->adminTemplate('message/search_result',$arr);
      }

    public function getConversations()
    {
        $resultArr = [];
        $userId = $this->core_lib->getLoginUserId();
        $fromMessages = $this->message_model->getAllMessages(0,$userId);

        if($fromMessages){
            foreach($fromMessages as $msg){
                $resultArr[$msg['from_id']][] = $msg;
            }
        }

        if ($resultArr) {
            foreach ($resultArr as $key => $value) {
                $lastFromMessage = current($value);
                $toId = $lastFromMessage['from_id'];
                $fromId = $lastFromMessage['to_id'];
                $currentMsgId = $lastFromMessage['id'];
                $lastToMessage = $this->message_model->getMyLastMessage($toId, $fromId, $currentMsgId);
                if ($lastToMessage) {
                    $lastMsg = current($lastToMessage);
                    array_unshift($value, $lastMsg);
                    $resultArr[$key] = $value;
                }
            }
        }

        $finalArr = [];
        if($resultArr){
            foreach($resultArr as $key => $value){
                $kk = (string)$key;
                $finalArr["u_".$kk]['messages'] = $value;
            }
        }
        $loginUserId = $this->core_lib->getLoginUserId();
        if($finalArr){
            foreach($finalArr as $key => $user){
                $userId = substr($key,2);
                $userDetails = $this->user_model->getData($userId);
                $unreadCount = $this->message_model->getUnreadMessageCount($userId,$loginUserId);
                $status ="away";
                if(isset($userDetails['last_seen'])){
                    date_default_timezone_set("Asia/Kolkata");
                    $currentDateTime = date('Y-m-d H:i:s');
                    $lastSeenAdded = strtotime($userDetails['last_seen']) + 60;
                    $currentStr = strtotime($currentDateTime);
                    if($lastSeenAdded >$currentStr){
                        $status = "online";
                    }
                }
                $userDetails['status'] = $status;
                $finalArr[$key]['user_data'] = $userDetails;
                $finalArr[$key]['unread_msg_count'] = $unreadCount;
            }
        }
        $result = ['result' => $finalArr, 'success' => true];
        header('Content-Type: application/json');
        echo json_encode($result);
    }


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */