<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cart extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('user/user_model','user_model');
        $this->load->library('core_lib');
        $this->load->library('form');
        $this->load->library('user/user_lib','user_lib');
    }

    public function _remap($method)
    {
        $userGroup = $this->core_lib->getUserGroup();
        if($method == "addCart"){
            $this->addCart();
        }elseif ($method == "view"){
            if(in_array("user",$userGroup)){
                $this->view();
            }else{
                die('login required');
            }
        }elseif ($method == "remove"){
            if(in_array("user",$userGroup)){
                $this->remove();
            }else{
                die('login required');
            }
        }elseif ($method == "sendEmail"){
            if(in_array("user",$userGroup)){
                $this->sendEmail();
            }else{
                die('login required');
            }
        }
    }
    public function addCart()
    {
        $productId =  $this->input->post('product_id');
        $userGroup = $this->core_lib->getUserGroup();
        if(in_array("user",$userGroup)){
            $dataVariant = json_decode($this->input->post('data'), true);
            $quantity = $this->input->post('quantity');
            $this->load->model('product_cart_model','product_cart_model');
            $loggedUserId = $this->core_lib->getLoginUserId();
            $temp = [
                'user_id' =>$loggedUserId,
                'product_id' =>$productId,
                'p_quantity' =>$quantity,
                'is_checkout' => 0
            ];
            $cartId = $this->product_cart_model->add($temp);
            if($cartId && count($dataVariant)){
                foreach($dataVariant as $value){
                    $this->load->model('product_cart_variant_model','product_cart_variant_model');
                    $variantTemp = [
                        'cart_id' =>$cartId,
                        'v_type' =>$value['type'],
                        'v_value' =>$value['value']
                    ];
                    $this->product_cart_variant_model->add($variantTemp);
                }
            }
            $result = ['url'=>null,'success' =>true];
            header('Content-Type: application/json');
            echo json_encode( $result );

        }else{
            $reUrl = "product/view/product-name/".$productId;
            $eURL = base64_encode($reUrl);
            $result = ['url'=>$eURL,'success' =>false];
            header('Content-Type: application/json');
            echo json_encode( $result );

        }


    }

    protected function view()
    {
        $cartData = [];
        $isLoggedIn = $this->core_lib->getLoginUserId();
        if($isLoggedIn){
            $this->load->model('product_cart_model');
            $this->load->model('product_image_model');
            $this->load->model('product_cart_variant_model');
            $cartList = $this->product_cart_model->getCart($isLoggedIn);
            if($cartList){
                foreach ($cartList as $key => $value){
                    $productId = $value['id'];
                    $thumbPath = '';
                    $orgThumbPath ='';
                    $image = $this->product_image_model->getProductImages($productId,true);
                    if(empty($image)){
                        $image = $this->product_image_model->getProductImages($productId);
                    }
                    if(isset($image[0]) && !empty($image[0])){
                        $path = base_url();
                        $name = substr($image[0]['file_name'],0,strrpos($image[0]['file_name'],'.'));
                        $ext = substr($image[0]['file_name'],strrpos($image[0]['file_name'],'.'));
                        $thumbPath = $path.'uploads/thumbnail/'.$name.'_thumb'.$ext;

                        $orgFileName = $image[0]['file_name'];
                        $orgThumbPath = $path.'uploads/'.$orgFileName;
                    }
                    $cartList[$key]['default_image'] = $orgThumbPath;
                    $cartList[$key]['thumbnail'] = $thumbPath;
                    $fieldsArr = [
                        'cart_id'=>$value['cart_id']
                    ];
                    $productVariant = $this->product_cart_variant_model->listByFields($fieldsArr);
                    $cartList[$key]['variant'] = $productVariant;
                    $cartData['cart'] = $cartList;
                }
            }

        }
        $this->load->template('view_cart',$cartData);
    }

    public function sendEmail()
    {
        $cartData = [];
        $isLoggedIn = $this->core_lib->getLoginUserId();
        if($isLoggedIn){
            $this->load->model('product_cart_model');
            $this->load->model('product_image_model');
            $this->load->model('product_cart_variant_model');
            $cartList = $this->product_cart_model->getCart($isLoggedIn);
            if($cartList){
                foreach ($cartList as $key => $value){
                    $fieldsArr = [
                        'cart_id'=>$value['cart_id']
                    ];
                    $fieldsTempArr = [
                        'id'=>$value['cart_id']
                    ];

                    $this->product_cart_model->update(['is_checkout' =>1],$fieldsTempArr);

                    $productVariant = $this->product_cart_variant_model->listByFields($fieldsArr);
                    $cartList[$key]['variant'] = $productVariant;
                    $cartData = $cartList;
                }

                $to      = 'info@glatcomaldives.com';
                $subject = 'Enquiry Mail';

                $this->load->model('user_model');
                $userDetails = $this->user_model->getData($isLoggedIn);

                if($cartData){
                    $tr = '<tr><th>Product Id</th><th>Product Name</th><th>Variant</th><th>Quantity</th></tr>';
                    foreach($cartData as $cart){
                        $tr.= "<tr><td>".$cart['id']."</td>";
                        $tr.= "<td>".$cart['name']."</td>";
                        $tr.="<td>";
                        if($cart['variant']){

                            foreach($cart['variant'] as $v){
                                $tr.= "<span>".$v['v_type']." ".$v['v_value']."</span><br>";
                            }


                        }
                        $tr.="</td>";
                        $tr.="<td>".$cart['p_quantity']."</td>";
                        $tr.="</tr>";
                    }


                    $mailBody = '<html><body>';
                    $mailBody .='<div>
                             <h2>Product Request</h2>
                               <div style="width: 300px;height:200px;">
                                <p><span>From</span></p>';
                    $mailBody .= $userDetails['first_name']." ".$userDetails['last_name']."</br>";
                    $mailBody .= $userDetails['email']."</br>";
                    $mailBody .= $userDetails['phone']."</br>";

                    $mailBody .= '</div>';
                    $mailBody .='<div><h5>Dear Admin,</h5><h6>&nbsp;&nbsp;&nbsp;&nbsp;
                                Please accept this letter as formal application for using your service.<br>
                                The product we request are listed below</h6>';

                    $mailBody .= '</div>';
                    $mailBody .= '</div>';
                    $mailBody .='<table width="100%" border="1">';
                    $mailBody.= $tr;
                    $mailBody .='</table>';
                    $mailBody .='</body></html>';

                    $headers[] = 'MIME-Version: 1.0';
                    $headers[] = 'Content-type: text/html; charset=iso-8859-1';

                    $headers[] = 'To: Admin <'.$to.'>';
                    $headers[] = 'From: '.$userDetails['first_name'].' <'.$userDetails['email'].'>';
                    $params = '-f ' . $userDetails['email'];

                    $status = mail($to, $subject, $mailBody, implode("\r\n", $headers),$params);
                    $result = ['success' =>$status];
                    header('Content-Type: application/json');
                    echo json_encode( $result );

                }


            }

        }
    }

    public function remove()
    {
        $cartId = $this->input->post('cart_id');
        $isLoggedIn = $this->core_lib->getLoginUserId();
        if($cartId){
            $this->load->model('product_cart_model','product_cart_model');
            $cartDetails =  $this->product_cart_model->getData($cartId);
            if($cartDetails){
                if($cartDetails['user_id'] == $isLoggedIn){
                    $temp = [
                        'id' =>$cartId
                    ];
                    $this->product_cart_model->delete($temp);
                }
            }
        }
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */