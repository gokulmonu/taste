<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Career extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('user/user_model','user_model');
        $this->load->library('core_lib');
        $this->load->library('form');
        $this->load->library('user/user_lib','user_lib');
    }

    public function _remap($method)
    {
        $userGroup = $this->core_lib->getUserGroup();
        if($method == "addCareer"){
            if(in_array("admin",$userGroup)){
                $this->addCareer();
            }else{
                $this->core_lib->redirectPage();
            }
        }elseif ($method == "validate"){
            if(in_array("admin",$userGroup)){
                $this->validate();
            }else{
                die('login required');
            }
        }elseif ($method == "delete"){
            if(in_array("admin",$userGroup)){
                $this->delete();
            }else{
                die('login required');
            }
        }elseif ($method == "list"){
            if(in_array("admin",$userGroup)){
                $this->listCareers();
            }else{
                $this->core_lib->redirectPage();
            }
        }
    }

    public function listCareers()
    {
        $this->load->model('career_model');
        $careerList['list'] = $this->career_model->getAll();
        $bc = [];
        $bc['admin/careers'] = "Careers";
        $careerList['breadcrumbs'] = $bc;

        $pageTitle = [];
        $pageTitle['title'] = "Career";
        $pageTitle['icon'] = "fa fa-suitcase";
        $careerList['page_header'] = $pageTitle;
        $this->load->adminTemplate('career/list',$careerList);
    }
    public function addCareer()
    {
        $careerDetails = [];
        $mode = $this->uri->segment(3);
        if($mode == "edit"){
            $careerId = $this->uri->segment(4);
            if($careerId){
                $this->load->model('career_model');
                $careerDetails = $this->career_model->getData($careerId);
            }
        }
        $bc = [];
        $bc['admin/careers'] = "Careers";
        $bc['admin/career/add'] = "Add";
        $careerDetails['breadcrumbs'] = $bc;

        $pageTitle = [];
        $pageTitle['title'] = "Career";
        $pageTitle['icon'] = "fa fa-suitcase";
        $careerDetails['page_header'] = $pageTitle;
        $this->load->adminTemplate('career/edit',$careerDetails);

    }
    public function validate()
    {
        $postData = [
            'id' =>$this->input->post('id'),
            'title' =>$this->input->post('title'),
            'salary' =>$this->input->post('salary'),
            'description' =>$this->input->post('description'),
            'experience' =>$this->input->post('experience'),
        ];
        $this->form->setRules($postData['title'],'title_error','Please enter career title');
        $validationArr = $this->form->run();
        if(is_array($validationArr) && count($validationArr) && $validationArr != false){
            header('Content-Type: application/json');
            echo json_encode( $validationArr );
        }else{
            $this->load->model('career_model');
            if(isset($postData['id']) && !empty($postData['id'])){
                $temp = [
                    'id' =>$postData['id']
                ];
                $this->career_model->update($postData,$temp);
            }else{
                $careerId = $this->career_model->add($postData);

            }
            if(!isset($careerId) && empty($careerId))$careerId = $postData['id'];
            $result = ['id'=>$careerId,'success' =>true];
            header('Content-Type: application/json');
            echo json_encode( $result );
        }



    }



    protected function delete()
    {
        $careerId = $this->input->post('id');
        if ($careerId) {
            $this->load->model('career_model', 'career_model');
            $this->career_model->delete(['id'=>$careerId]);
        }
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */