<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Enquiry extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user/user_model', 'user_model');
        $this->load->library('core_lib');
        $this->load->library('form');
        $this->load->library('default_session');
        $this->load->library('user/user_lib', 'user_lib');
        $this->load->model('contactus_model', 'contactus_model');

    }


    public function _remap($method)
    {
        $userGroup = $this->core_lib->getUserGroup();
       if ($method == "listing") {
            if (in_array("admin", $userGroup)) {
                $this->listing();
            } else {
                $this->core_lib->redirectPage();
            }
        } elseif ($method == "delete") {
            if (in_array("admin", $userGroup)) {
                $this->delete();
            } else {
                die('login required');
            }
        } elseif ($method == "view") {
            if (in_array("admin", $userGroup)) {
                $this->view();
            } else {
                $this->core_lib->redirectPage();
            }
        }elseif ($method == "enquirySearchRender") {
            if (in_array("admin", $userGroup)) {
                $this->enquirySearchRender();
            }
        }
    }



    public function listing()
    {
        $result = $this->enquirySearch();
        $resultArr['data'] = $result;
        $bc = [];
        $bc['projects'] = "Enquiry";
        $resultArr['breadcrumbs'] = $bc;

        $pageTitle = [];
        $pageTitle['title'] = "Enquiry Listing";
        $pageTitle['icon'] = "fa fa-envelope-o";
        $resultArr['page_header'] = $pageTitle;

        $this->load->adminTemplate('enquiry/list', $resultArr);
    }

    protected function enquirySearch()
    {
        $postData = [
            'page' => $this->input->post('page'),
            'phrase' => $this->input->post('phrase')
        ];

        $query = $this->contactus_model->getSearchQuery($postData);
        $result = $this->contactus_model->getListByQuery($query, $postData['page'], '');
        return $result;
    }

    protected function enquirySearchRender()
    {
        $result = $this->enquirySearch();
        $resultArr['data'] = $result;
        $this->load->view('admin/enquiry/search_result', $resultArr);
    }

    protected function delete()
    {
        $enquiryId = $this->input->post('enquiry_id');
        if ($enquiryId) {
            $this->contactus_model->delete(['id' => $enquiryId]);
        }
    }

    protected function view()
    {
        $id = $this->uri->segment(4);
        if ($id) {
            $enquiryDetails = $this->contactus_model->getData($id);

            $temp = [
                'id' => $id
            ];
            $this->contactus_model->update(['status' => 1], $temp);

            $bc = [];
            $bc['enquiry'] = "Enquiry";
            $bc['enquiry/view/' . $id] = $enquiryDetails['name'];
            $enquiryDetails['breadcrumbs'] = $bc;

            $pageTitle = [];
            $pageTitle['title'] = "Enquiry View";
            $pageTitle['icon'] = "fa fa-envelope-o";
            $enquiryDetails['page_header'] = $pageTitle;


            $this->load->adminTemplate('enquiry/view', $enquiryDetails);
        }
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */