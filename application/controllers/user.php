<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('user/user_model','user_model');
        $this->load->library('core_lib');
        $this->load->library('form');
        $this->load->library('user/user_lib','user_lib');
        $this->load->model('product_model','product_model');
    }

    public function _remap($method)
    {
        $userGroup = $this->core_lib->getUserGroup();
        if($method == "add"){
               $this->add();
        }elseif ($method == "validate"){
                $this->validate();
        }elseif ($method == "wbSignup"){
                $this->wbSignup();
        }elseif ($method == "editProfile"){
            if($this->core_lib->getLoginUserId()){
                $this->editProfile();
            }else{
                die("login required");
            }
        }elseif ($method == "profileValidate"){
            if($this->core_lib->getLoginUserId()){
                $this->profileValidate();
            }else{
                die("login required");
            }
        }elseif ($method == "signupReseller"){
            $this->signupReseller();
        }elseif ($method == "validateReseller"){
            $this->validateReseller();
        }elseif ($method == "reseller"){
            if(in_array("admin",$userGroup)){
                $this->reseller();
            }else{
                die('login required');
            }
        }elseif ($method == "resellerView"){
            if(in_array("admin",$userGroup)){
                $this->resellerView();
            }else{
                die('login required');
            }
        }elseif ($method == "resellerDelete"){
            if(in_array("admin",$userGroup)){
                $this->resellerDelete();
            }else{
                die('login required');
            }
        }elseif ($method == "addToSwitch"){
            if(in_array("admin",$userGroup)){
                $this->addToSwitch();
            }else{
                die('login required');
            }
        }elseif ($method == "signupUser"){
            $this->signupUser();
        }elseif ($method == "validateUser"){
            $this->validateUser();
        }elseif ($method == "listUser"){
            if(in_array("admin",$userGroup)){
                $this->listUser();
            }else{
                die('login required');
            }
        }elseif ($method == "userView"){
            if(in_array("admin",$userGroup)){
                $this->userView();
            }else{
                die('login required');
            }
        }elseif ($method == "userDelete"){
            if(in_array("admin",$userGroup)){
                $this->userDelete();
            }else{
                die('login required');
            }
        }
    }

    public function resellerDelete()
    {
        $id = $this->input->post('id');
        if ($id) {
            $this->load->model('tokup_reseller_model');
            $this->tokup_reseller_model->delete(['id' => $id]);
        }
    }
    public function userDelete()
    {
        $id = $this->input->post('id');
        if ($id) {
            $this->load->model('tokup_user_model');
            $this->tokup_user_model->delete(['id' => $id]);
        }
    }

    public function resellerView()
    {
        $id = $this->uri->segment(4);
        $this->load->model('tokup_reseller_model');
        $data= $this->tokup_reseller_model->getData($id);
        $result = [];
        if($data){
            $result = $this->user_model->getData($data['user_id']);
            $result['tokup_user_id'] = $data['id'];
            $result['message'] = $data['message'];


            $bc = [];
            $bc['admin/reseller'] = "Reseller";
            $result['breadcrumbs'] = $bc;

            $pageTitle = [];
            $pageTitle['title'] = "Reseller";
            $pageTitle['icon'] = "fa fa-sign-in";
            $result['page_header'] = $pageTitle;
            $this->load->adminTemplate('registration/reseller_view',$result);
        }

    }

    public function userView()
    {
        $id = $this->uri->segment(4);
        $this->load->model('tokup_user_model');
        $data= $this->tokup_user_model->getData($id);
        $result = [];
        if($data){
            $result = $this->user_model->getData($data['user_id']);
            $result['tokup_user_id'] = $data['id'];
            $result['message'] = $data['message'];
            $result['username'] = $data['username'];
            $result['password'] = $data['password'];


            $bc = [];
            $bc['admin/reseller'] = "User";
            $result['breadcrumbs'] = $bc;

            $pageTitle = [];
            $pageTitle['title'] = "User";
            $pageTitle['icon'] = "fa fa-sign-in";
            $result['page_header'] = $pageTitle;
            $this->load->adminTemplate('registration/user_view',$result);
        }

    }


    public function reseller()
    {
        $this->load->model('tokup_reseller_model');
        $list = $this->tokup_reseller_model->getAll();
        if ($list) {
            foreach ($list as $key => $value) {
                $list[$key] = $this->user_model->getData($value['user_id']);
                $list[$key]['tokup_user_id'] = $value['id'];
                $list[$key]['message'] = $value['message'];
            }
        }

        $resellerList['list'] = $list;
        $bc = [];
        $bc['admin/reseller'] = "Reseller";
        $resellerList['breadcrumbs'] = $bc;

        $pageTitle = [];
        $pageTitle['title'] = "Reseller List";
        $pageTitle['icon'] = "fa fa-user";
        $resellerList['page_header'] = $pageTitle;
        $this->load->adminTemplate('registration/reseller_list',$resellerList);
    }

    public function listUser()
    {
        $this->load->model('tokup_user_model');
        $list = $this->tokup_user_model->getAll();
        if ($list) {
            foreach ($list as $key => $value) {
                $list[$key] = $this->user_model->getData($value['user_id']);
                $list[$key]['tokup_user_id'] = $value['id'];
                $list[$key]['message'] = $value['message'];
            }
        }

        $resellerList['list'] = $list;
        $bc = [];
        $bc['admin/reseller'] = "User";
        $resellerList['breadcrumbs'] = $bc;

        $pageTitle = [];
        $pageTitle['title'] = "User List";
        $pageTitle['icon'] = "fa fa-user";
        $resellerList['page_header'] = $pageTitle;
        $this->load->adminTemplate('registration/user_list',$resellerList);
    }


    public function signupReseller()
    {
        $this->load->template('tokup_reseller');
    }
    public function signupUser()
    {
        $this->load->template('tokup_user');
    }

    public function validateUser()
    {
        $postData = [
            'first_name' => $this->input->post('first_name'),
            'last_name' => $this->input->post('last_name'),
            'email' => $this->input->post('email'),
            'street_address_1' => $this->input->post('address'),
            'phone' => $this->input->post('phone'),
        ];
        $this->form->setRules($this->input->post('username'), 'username_error', 'Please Enter  UserName ');
        $this->form->setRules($this->input->post('password'), 'password_error', 'Please Enter  Password ');
        $this->form->setRules($this->input->post('user_id'), 'user_id_error', 'Please Enter  UserID ');
        $validationArr = $this->form->run();
        if (is_array($validationArr) && count($validationArr) && $validationArr != false) {
            header('Content-Type: application/json');
            echo json_encode($validationArr);
        } else {
            $response = $this->addToSwitch($this->input->post('username'), $this->input->post('password'), $this->input->post('user_id'));
            if ($response && isset($response['status']) && $response['status'] == "success") {
                $userId = $this->user_model->add($postData);
                if ($userId) {
                    $reseller['user_id'] = $userId;
                    $reseller['message'] = $this->input->post('message');
                    $reseller['username'] = $this->input->post('username');
                    $reseller['password'] = $this->input->post('password');
                    $reseller['user_unique_id'] = $this->input->post('user_id');
                    $reseller['response_msg'] = json_encode($response);

                    $this->load->model('tokup_user_model');
                    $this->tokup_user_model->add($reseller);
                }
                $result = ['success' => true];
                header('Content-Type: application/json');
                echo json_encode($result);
            } else {
                $result = ['success' => false, 'response_message' => isset($response['message']) ? $response['message'] : "Please try again later"];
                header('Content-Type: application/json');
                echo json_encode($result);
            }

        }

    }


    private function addToSwitch($userName, $password, $userId)
    {

        $returnArr = [];
        $username = "tokuponline";
        $password = "online@tokup";
        $url = "http://www.tokupportal.in/protected/loginAction";

        $postdata = "actionId=userLoginWithJsonWithAuth&action_uri=loginNew&returnUrl=www.tokupportal.in/protected&url=www.tokupportal.in&userId=" . $username . "&password=" . $password;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.6) Gecko/20070725 Firefox/2.0.0.6");
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_COOKIEJAR, dirname(__FILE__) . '/cookie.txt');
        curl_setopt($ch, CURLOPT_COOKIEFILE, dirname(__FILE__) . '/cookie.txt');  // <-- add this line
        curl_setopt($ch, CURLOPT_REFERER, $url);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        curl_setopt($ch, CURLOPT_POST, 1);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        if ($result && $result['status'] == "success") {
            $curlOptions = [
                CURLOPT_URL => 'http://www.tokupportal.in/generic/invoke',
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POSTFIELDS => [
                    'actionId' => "addUser",
                    'action_uri' => "userManagement",
                    'billCycle' => "Default",
                    'billPlan' => "defaultbp",
                    'billType' => "Prepaid",
                    'forceCodecGB' => true,
                    'userOverdrLimit' => 0,
                    'usrId' => $userId,
                    'usrLimit' => 0.05,
                    'usrName' => $userName,
                    'usrOwner' => 'tokuponline',
                    'usrPassword' => $password,
                    'usrSimCallLimt' => -1,
                    'usrStatus' => "Active"
                ]
            ];
            curl_setopt_array($ch, $curlOptions);
            $result = curl_exec($ch);
            curl_close($ch);
            $returnArr = json_decode($result, true);
        }

        return $returnArr;
    }

    public function validateReseller()
    {
        $postData = [
            'first_name' => $this->input->post('first_name'),
            'last_name' => $this->input->post('last_name'),
            'email' => $this->input->post('email'),
            'street_address_1' => $this->input->post('address'),
            'phone' => $this->input->post('phone'),
        ];
        $userId = $this->user_model->add($postData);
        if ($userId) {
            $reseller['user_id'] = $userId;
            $reseller['message'] = $this->input->post('message');
            $this->load->model('tokup_reseller_model');
            $this->tokup_reseller_model->add($reseller);
        }
        $result = ['success' => true];
        header('Content-Type: application/json');
        echo json_encode($result);

    }
    public function editProfile()
    {
        $isLoggedIn = $this->core_lib->getLoginUserId();
        $userDetails = $this->user_model->getData($isLoggedIn);
        $this->load->chatTemplate('profile',$userDetails);
    }

    public function profileValidate()
    {
        $postData = [
            'id' =>$this->input->post('id'),
            'first_name' =>$this->input->post('first_name'),
            'last_name' =>$this->input->post('last_name'),
        ];

        $this->form->setRules($postData['first_name'],'first_name_error','Please enter your first name');
        $this->form->setRules($postData['last_name'],'last_name_error','Please enter your last name');
        $validationArr = $this->form->run();

        if(is_array($validationArr) && count($validationArr) && $validationArr != false){
            header('Content-Type: application/json');
            echo json_encode( $validationArr );
        }else{
            $this->load->library('upload_lib');
            $resultArr = $this->upload_lib->doUpload();
            if(count($resultArr) && !isset($resultArr['error'])){
                $this->load->model('file_model','file_model');
                $tempData = [
                    'file_path' =>$resultArr['upload_data']['full_path'],
                    'file_name' =>$resultArr['upload_data']['file_name'],
                    'file_extension' =>$resultArr['upload_data']['file_type'],
                ];
                $imgId = $this->file_model->add($tempData);
                if($imgId){
                    $postData['file_ref'] =  $tempData['file_name'];
                }
            }

            if(isset($postData['id']) && !empty($postData['id'])){
                $temp = [
                    'id' =>$postData['id']
                ];
                $this->user_model->update($postData,$temp);
            }else{
                $memberId = $this->user_model->add($postData);

            }
            if(!isset($memberId) && empty($memberId))$memberId = $postData['id'];
            $result = ['id'=>$memberId,'success' =>true];
            header('Content-Type: application/json');
            echo json_encode( $result );
        }


    }
    public function wbSignup()
    {
        $this->load->chatTemplate('signup');
    }
    public function add()
    {
        $this->load->view('signup');
    }
    public function validate()
    {

        $password = $this->input->post('password');
        $username = $this->input->post('username');
        $name = $this->input->post('name');
        $mobile = $this->input->post('mobile');
        $email = $this->input->post('email');
        $decodedString = base64_decode($password);
        $this->form->setRules($username,'username_error','Please enter your username','',3,20);
        $this->form->setRules($decodedString,'password_error','Please enter your password','',3);
        $this->form->setRules($name,'name_error','Please enter your name','',3,20);
        $this->form->setRules($mobile,'mobile_error','Please enter valid mobile number','integer');
        $this->form->setRules($email,'email_error','Please enter your email','email');
        $validationArr = $this->form->run();
        $this->load->model('user/user_model','user_model');
        $isUsernameExist = $this->user_model->isValueExist('username',$username);
        if($isUsernameExist){
            if($validationArr){
                $key = array_search('username_error', array_column($validationArr, 'field_id'));
                if($key !== false){
                    $validationArr[$key]['label'] = "Username already exist";
                }
            }else{
                $tempArr = [
                    'field_id' =>'username_error',
                    'label' => 'Username already exist'
                ];
                $validationArr[] = $tempArr;
            }
        }

        if(is_array($validationArr) && count($validationArr) && $validationArr != false){
            header('Content-Type: application/json');
            echo json_encode( $validationArr );
        }else{
            $passwordHash = password_hash($decodedString, PASSWORD_DEFAULT);
            $dataArr = [
                'first_name' => $name,
                'email' => $email,
                'phone' => $mobile,
                'password' => $passwordHash,
                'forgot_token' => NULL,
                'username' => $username,
                'ip_address' => 'string',
                'is_active' => 1,
                'is_blocked' => 0,
                'is_admin' =>false

            ];
            $this->user_lib->addUser($dataArr);
            $redirectUrl['url'] = [
                'success' => true
            ];
            header('Content-Type: application/json');
            echo json_encode( $redirectUrl );
        }


    }




}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */