<?php

class User_group_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('Base', 'base');

    }

    private $tableName = "sb__user_group";

    public $formFields = [
        'id' => 'integer',
        'user_id' => 'integer',
        'group_id' => 'integer'

    ];

    public function add($dataArr)

    {
        $resultArr = $this->base->add($dataArr, $this->formFields);
        if ($resultArr) {
            $this->db->insert($this->tableName, $resultArr);
            return $this->db->insert_id();
        } else {
            return false;
        }

    }

    public function update($dataArr, $whereArr)
    {
        $whereArrSanitize = $this->base->validate($whereArr, $this->formFields);
        $resultArr = $this->base->update($dataArr, $this->formFields);
        if (count($resultArr) && count($whereArrSanitize)) {
            $this->db->update($this->tableName, $resultArr, $whereArrSanitize);
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    public function delete($whereArr, $strongDelete = false)
    {
        $whereArrSanitize = $this->base->validate($whereArr, $this->formFields);
        if (isset($whereArrSanitize) && !empty($whereArrSanitize)) {
            if ($strongDelete) {
                $this->db->delete($this->tableName, $whereArrSanitize);
            } else {
                $resultArr = $this->base->delete();
                $this->db->update($this->tableName, $resultArr, $whereArrSanitize);
            }
        }
    }

    public function getAdminList()
    {
        $this->db->select('u.*')
            ->from($this->tableName .' ug')
            ->join('sb__user u','u.id = ug.user_id','left')
            ->where('ug.is_deleted',NULL,false)
            ->where('u.is_deleted',NULL,false);
        $this->db->where('ug.group_id',1);
        $q = $this->db->get();
        return $q->result_array();
    }


}