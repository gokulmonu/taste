<?php

class User_model extends MY_Model
{
    function __construct()
    {
        //$this->CI = & get_instance();
        parent::__construct();
        $this->load->library('Base', 'base');

    }

    protected $tableName = "sb__user";

    public $formFields = [
        'id' => 'integer',
        'first_name' => 'string',
        'last_name' => 'string',
        'email' => 'string',
        'gender' => 'string',
        'dob' => 'date',
        'phone' => 'string',
        'password' => 'string',
        'forgot_token' => 'string',
        'username' => 'string',
        'ip_address' => 'string',
        'street_address_1' => 'string',
        'street_address_2' => 'string',
        'is_active' => 'boolean',
        'is_blocked' => 'boolean',
        'position' =>'string',
        'file_ref' =>'text',
        'last_seen' => 'datetime'

    ];

    public function add($dataArr)

    {
        $resultArr = $this->base->add($dataArr, $this->formFields);//echo '<pre>';print_r($resultArr);die();
        if ($resultArr) {
            $this->db->insert($this->tableName, $resultArr);
            return $this->db->insert_id();
        } else {
            return false;
        }

    }

    public function update($dataArr, $whereArr)
    {
        $whereArrSanitize = $this->base->validate($whereArr, $this->formFields);
        $resultArr = $this->base->update($dataArr, $this->formFields);
        if (count($resultArr) && count($whereArrSanitize)) {
            $this->db->update($this->tableName, $resultArr, $whereArrSanitize);
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    public function delete($whereArr, $strongDelete = false)
    {
        $whereArrSanitize = $this->base->validate($whereArr, $this->formFields);
        if (isset($whereArrSanitize) && !empty($whereArrSanitize)) {
            if ($strongDelete) {
                $this->db->delete($this->tableName, $whereArrSanitize);
            } else {
                $resultArr = $this->base->delete();
                $this->db->update($this->tableName, $resultArr, $whereArrSanitize);
            }
        }
    }

    public function getUserGroup($userId)
    {

        $result = [];
        $query = $this->db->query(" SELECT g.name FROM `sb__user` u 
                                    LEFT JOIN sb__user_group ug on ug.user_id = u.id 
                                    LEFT JOIN sb__group g on g.id = ug.group_id
                                    WHERE u.id = '$userId' AND u.is_deleted IS NULL AND ug.is_deleted IS NULL AND g.is_deleted IS NULL ");

        $resultArr = $query->result_array();
        if(count($resultArr)){
            $result = array_column($resultArr,'name');
        }
        return $result;
    }

    public function getPasswordHash($username = '')
    {
        if($username){
            $fieldsArr = [
                'username'=>$username
            ];
            $query = $this->db->select('password,id');
            $query = $this->db->get_where($this->tableName,$fieldsArr);
            if ($query->num_rows() > 0)
            {
                $row = $query->row();
                $resultArr = [
                    'id'=>$row->id,
                    'password'=>$row->password
                ];
                return $resultArr;
            }

            return false;
        }else return false;
    }


}