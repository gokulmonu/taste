<?php

class Contactus_model extends MY_Model
{
    function __construct()
    {
        //$this->CI = & get_instance();
        parent::__construct();
        $this->load->library('Base', 'base');

    }

    protected $tableName = "sb_contactus";

    public $formFields = [
        'id' => 'integer',
        'name' => 'string',
        'message' => 'text',
        'email' => 'string',
        'phone' => 'string',
        'subject' => 'string',
        'status' => 'integer'
    ];

    public function add($dataArr)

    {
        $resultArr = $this->base->add($dataArr, $this->formFields);
        if ($resultArr) {
            $this->db->insert($this->tableName, $resultArr);
            return $this->db->insert_id();
        } else {
            return false;
        }

    }

    public function update($dataArr, $whereArr)
    {
        $whereArrSanitize = $this->base->validate($whereArr, $this->formFields);
        $resultArr = $this->base->update($dataArr, $this->formFields);
        if (count($resultArr) && count($whereArrSanitize)) {
            $this->db->update($this->tableName, $resultArr, $whereArrSanitize);
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    public function delete($whereArr, $strongDelete = false)
    {
        $whereArrSanitize = $this->base->validate($whereArr, $this->formFields);
        if (isset($whereArrSanitize) && !empty($whereArrSanitize)) {
            if ($strongDelete) {
                $this->db->delete($this->tableName, $whereArrSanitize);
            } else {
                $resultArr = $this->base->delete();
                $this->db->update($this->tableName, $resultArr, $whereArrSanitize);
            }
        }
    }

    public function getSearchQuery($params = [])
    {
        $sql = "SELECT * FROM $this->tableName e where e.is_deleted is null";

        if (isset($params['phrase']) && !empty($params['phrase'])) {
            $sql.= " and e.name like '%".$params['phrase'] ."%' or e.email like '%".$params['phrase'] ."%'" ;
        }
        if (isset($params['status']) && !empty($params['status']) && $params['status'] == "unread") {
            $sql.= " and e.status is null or e.status=0" ;
        }
        return $sql;


    }


}