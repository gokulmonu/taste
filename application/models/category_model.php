<?php

class Category_model extends MY_Model
{
    function __construct()
    {
        //$this->CI = & get_instance();
        parent::__construct();
        $this->load->library('Base', 'base');

    }

    protected $tableName = "sb_category";

    public $category = [];
    public $formFields = [
        'id' => 'integer',
        'name' => 'string',
        'parent_id' => 'integer',
        'description' => 'text',
    ];

    public function add($dataArr)

    {
        $resultArr = $this->base->add($dataArr, $this->formFields);
        if ($resultArr) {
            $this->db->insert($this->tableName, $resultArr);
            return $this->db->insert_id();
        } else {
            return false;
        }

    }

    public function update($dataArr, $whereArr)
    {
        $whereArrSanitize = $this->base->validate($whereArr, $this->formFields);
        $resultArr = $this->base->update($dataArr, $this->formFields);
        if (count($resultArr) && count($whereArrSanitize)) {
            $this->db->update($this->tableName, $resultArr, $whereArrSanitize);
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    public function delete($whereArr, $strongDelete = false)
    {
        $whereArrSanitize = $this->base->validate($whereArr, $this->formFields);
        if (isset($whereArrSanitize) && !empty($whereArrSanitize)) {
            if ($strongDelete) {
                $this->db->delete($this->tableName, $whereArrSanitize);
            } else {
                $resultArr = $this->base->delete();
                $this->db->update($this->tableName, $resultArr, $whereArrSanitize);
            }
        }
    }

    public function getSearchQuery($params = [])
    {
        $sql = "SELECT * FROM $this->tableName c where c.is_deleted is null";
        if (isset($params['parent_id'])) {
           $sql.= " and c.parent_id=".$params['parent_id'];
        }
        if (isset($params['phrase']) && !empty($params['phrase'])) {
            $sql.= " and c.name like '%".$params['phrase'] ."%'";
        }
        return $sql;


    }

    public function getSubCategories($categoryId)
    {
        if($categoryId){
            $this->db->select('*')
                ->from($this->tableName)
                ->where('parent_id',$categoryId)
                ->where('is_deleted',NULL,false);
            $q =$this->db->get();
            $result = $q->result_array();
            if($result){
              $ids = array_column($result,'id');
               return $ids;
            }else return false;
        }

        return false;
    }

    public function categories($categoryId)
    {
        $result = [];
        if($categoryId){
            $ids = $this->getSubCategories($categoryId);
            if($ids){
                $arrayIterator = new ArrayIterator($ids);
                foreach ($arrayIterator as $id){
                    $result[] = $id;
                    $subIds = $this->getSubCategories($id);
                    if($subIds){
                        foreach ($subIds as $sub){
                            $arrayIterator[] =  $sub;
                        }
                    }
                }
            }
            $result [] = $categoryId;
           return $result;
        }else return false;
    }

    private function isChildNodeExistFor($id)
    {
        $this->db->select('id')
            ->from($this->tableName)
            ->where('parent_id',$id)
            ->where('is_deleted',NULL,false);
        $q =$this->db->get();
        $result = $q->num_rows();
        if($result){
            return true;
        }else{
            return false;
        }
    }
    public function getCategoryTree($parentId)
    {
        $arr = array();
        $this->db->select('*')
            ->from($this->tableName)
            ->where('parent_id',$parentId)
            ->where('is_deleted',NULL,false);
        $q =$this->db->get();
        $result = $q->result_array();
        if($result){
            foreach ( $result as $item) {
                $childNode = $this->isChildNodeExistFor($item["id"]);
                if($childNode){
                    $arr[] = array(
                        "text" => $item["name"],
                        "href" => $item["id"],
                        "nodes" => $this->getCategoryTree($item["id"])
                    );
                }else{
                    $arr[] = array(
                        "text" => $item["name"],
                        "href" => $item["id"],
                    );
                }

          }
        }
        return $arr;
    }

    public function deleteCategories($categoryIds)
    {
        $ids = implode(',',$categoryIds);
        $this->db->query("DELETE from $this->tableName where id in ($ids)");
    }

}