<?php

class Event_image_model extends MY_Model
{
    function __construct()
    {
        //$this->CI = & get_instance();
        parent::__construct();
        $this->load->library('Base', 'base');

    }

    protected $tableName = "sb_event_images";

    public $formFields = [
        'id' => 'integer',
        'event_id' => 'integer',
        'file_id' => 'integer',
        'set_default' =>'integer',
    ];

    public function add($dataArr)

    {
        $resultArr = $this->base->add($dataArr, $this->formFields);
        if ($resultArr) {
            $this->db->insert($this->tableName, $resultArr);
            return $this->db->insert_id();
        } else {
            return false;
        }

    }

    public function update($dataArr, $whereArr)
    {
        $whereArrSanitize = $this->base->validate($whereArr, $this->formFields);
        $resultArr = $this->base->update($dataArr, $this->formFields);
        if (count($resultArr) && count($whereArrSanitize)) {
            $this->db->update($this->tableName, $resultArr, $whereArrSanitize);
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    public function delete($whereArr, $strongDelete = false)
    {
        $whereArrSanitize = $this->base->validate($whereArr, $this->formFields);
        if (!empty($whereArrSanitize)) {
            if ($strongDelete) {
                $this->db->delete($this->tableName, $whereArrSanitize);
            } else {
                $resultArr = $this->base->delete();
                $this->db->update($this->tableName, $resultArr, $whereArrSanitize);
            }
        }
    }

    public function getProductImages($productId ,$isDefault = false)
    {
        $result = [];
        if($productId){
            $this->db->select('sb__file.*,pf.product_id,pf.set_default')
            ->from($this->tableName .' pf')
            ->join('sb__file','sb__file.id = pf.file_id','left')
            ->where('pf.product_id',$productId)
            ->where('pf.is_deleted',NULL,false)
            ->where('sb__file.is_deleted',NULL,false);
            if($isDefault){
                $this->db->where('pf.set_default',1);
            }
            $q =$this->db->get();
            $result = $q->result_array();
        }

        return $result;
    }

}