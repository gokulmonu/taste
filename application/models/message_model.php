<?php

class Message_model extends MY_Model
{
    function __construct()
    {
        //$this->CI = & get_instance();
        parent::__construct();
        $this->load->library('Base', 'base');

    }

    protected $tableName = "sb_message";

    public $formFields = [
        'id' => 'integer',
        'message' => 'text',
        'from_id' => 'integer',
        'to_id' => 'integer',
        'status'=>'integer',
        'msg_created'=>'datetime',
        'file_id'=> 'integer'
    ];

    public function add($dataArr)

    {
        $resultArr = $this->base->add($dataArr, $this->formFields);
        if ($resultArr) {
            $this->db->insert($this->tableName, $resultArr);
            return $this->db->insert_id();
        } else {
            return false;
        }

    }

    public function update($dataArr, $whereArr)
    {
        $whereArrSanitize = $this->base->validate($whereArr, $this->formFields);
        $resultArr = $this->base->update($dataArr, $this->formFields);
        if (count($resultArr) && count($whereArrSanitize)) {
            $this->db->update($this->tableName, $resultArr, $whereArrSanitize);
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    public function delete($whereArr, $strongDelete = false)
    {
        $whereArrSanitize = $this->base->validate($whereArr, $this->formFields);
        if (isset($whereArrSanitize) && !empty($whereArrSanitize)) {
            if ($strongDelete) {
                $this->db->delete($this->tableName, $whereArrSanitize);
            } else {
                $resultArr = $this->base->delete();
                $this->db->update($this->tableName, $resultArr, $whereArrSanitize);
            }
        }
    }

    public function getSearchQuery($params = [])
    {
        $sql = "SELECT * FROM $this->tableName e where e.is_deleted is null";

        if (isset($params['phrase']) && !empty($params['phrase'])) {
            $sql.= " and e.name like '%".$params['phrase'] ."%' or e.title like '%".$params['phrase'] ."%'" ;
        }
        return $sql;


    }

    public function getAllMessages($userId = 0,$user2 = 0 ,$status = "")
    {
        $this->db->select('m.*,u1.last_seen as from_last_seen,u2.last_seen as to_last_seen,u1.first_name as from_first_name,u2.first_name as to_first_name,u1.file_ref as from_file_ref,u2.file_ref as to_file_ref')
            ->from($this->tableName .' m')
            ->join('sb__user u1','u1.id = m.from_id','left')
            ->join('sb__user u2','u2.id = m.to_id','left')
            ->where('u1.is_deleted',NULL,false)
            ->where('u2.is_deleted',NULL,false)
            ->where('m.is_deleted',NULL,false);
        if($userId){
            $this->db->where('m.from_id',$userId);
        }
        if($user2){
            $this->db->where('m.to_id',$user2);
        }
        if($status && $status == "unread"){
            $this->db->where('m.status is null or m.status=0');
        }
        $this->db->order_by("id", "desc");
        $q = $this->db->get();
        return $q->result_array();
    }

    public function getMessages($toId,$fromId,$lastId = 0)
    {

        $this->db->select('m.*,f.file_name,f.file_extension,f.file_path,u1.last_seen as from_last_seen,u2.last_seen as to_last_seen,u1.first_name as from_first_name,u2.first_name as to_first_name,u1.file_ref as from_file_ref,u2.file_ref as to_file_ref')
            ->from($this->tableName .' m')
            ->join('sb__user u1','u1.id = m.from_id','left')
            ->join('sb__user u2','u2.id = m.to_id','left')
            ->join('sb__file f','f.id = m.file_id','left')
            ->where('u1.is_deleted',NULL,false)
            ->where('u2.is_deleted',NULL,false)
            ->where('m.is_deleted',NULL,false);
        if($lastId){
            $this->db->where('m.id >',$lastId);
        }
        if($toId){
           /* $this->db->where('m.from_id',$fromId);
            $this->db->where('m.to_id',$toId);
            $this->db->or_where('m.to_id',$fromId);
            $this->db->where('m.from_id',$toId);*/
            $this->db->where('((m.from_id='.$fromId.' and m.to_id='.$toId.') or (m.to_id='.$fromId.' and m.from_id='.$toId.'))');
        }

        $this->db->order_by("id", "asc");
        $q = $this->db->get();
        return $q->result_array();
    }

    public function getUnreadMessageCount($fromId,$loginUserId)
    {
        $this->db->select('m.*')
            ->from($this->tableName .' m')
            ->where('m.is_deleted',NULL,false)
            ->where('m.status' ,NULL,false);
        if($fromId){
            $this->db->where('m.from_id',$fromId);
            $this->db->where('m.to_id',$loginUserId);
        }
        $q = $this->db->get();
        return $q->num_rows();
    }

    public function getMyLastMessage($toId,$fromId,$currentMsgId)
    {
        $this->db->select('m.*,u1.last_seen as from_last_seen,u2.last_seen as to_last_seen,u1.first_name as from_first_name,u2.first_name as to_first_name,u1.file_ref as from_file_ref,u2.file_ref as to_file_ref')
            ->from($this->tableName .' m')
            ->join('sb__user u1','u1.id = m.from_id','left')
            ->join('sb__user u2','u2.id = m.to_id','left')
            ->where('u1.is_deleted',NULL,false)
            ->where('u2.is_deleted',NULL,false)
            ->where('m.is_deleted',NULL,false);
        if($fromId){
            $this->db->where('m.from_id',$fromId);
        }
        if($toId){
            $this->db->where('m.to_id',$toId);
        }
        if($currentMsgId){
            $this->db->where('m.id >',$currentMsgId);
        }
        $this->db->order_by("id", "desc");
        $q = $this->db->get();
        return $q->result_array();
    }

}