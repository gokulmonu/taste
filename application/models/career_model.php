<?php

class Career_model extends MY_Model
{
    function __construct()
    {
        //$this->CI = & get_instance();
        parent::__construct();
        $this->load->library('Base', 'base');

    }

    protected $tableName = "sb_career";

    public $formFields = [
        'id' => 'integer',
        'title' => 'string',
        'description' => 'text',
        'salary' => 'integer',
        'experience' => 'integer',
    ];

    public function add($dataArr)

    {
        $resultArr = $this->base->add($dataArr, $this->formFields);
        if ($resultArr) {
            $this->db->insert($this->tableName, $resultArr);
            return $this->db->insert_id();
        } else {
            return false;
        }

    }

    public function update($dataArr, $whereArr)
    {
        $whereArrSanitize = $this->base->validate($whereArr, $this->formFields);
        $resultArr = $this->base->update($dataArr, $this->formFields);
        if (count($resultArr) && count($whereArrSanitize)) {
            $this->db->update($this->tableName, $resultArr, $whereArrSanitize);
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    public function delete($whereArr, $strongDelete = false)
    {
        $whereArrSanitize = $this->base->validate($whereArr, $this->formFields);
        if (isset($whereArrSanitize) && !empty($whereArrSanitize)) {
            if ($strongDelete) {
                $this->db->delete($this->tableName, $whereArrSanitize);
            } else {
                $resultArr = $this->base->delete();
                $this->db->update($this->tableName, $resultArr, $whereArrSanitize);
            }
        }
    }

}