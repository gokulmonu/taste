<?php

class Tokup_user_model extends MY_Model
{
    function __construct()
    {
        //$this->CI = & get_instance();
        parent::__construct();
        $this->load->library('Base', 'base');

    }

    protected $tableName = "sb_tokup_user";

    public $formFields = [
        'id' => 'integer',
        'message' => 'string',
        'user_id' => 'integer',
        'username' => 'string',
        'password' => 'string',
        'user_unique_id' => 'string',
        'response_msg' => 'text',
    ];

    public function add($dataArr)

    {
        $resultArr = $this->base->add($dataArr, $this->formFields);
        if ($resultArr) {
            $this->db->insert($this->tableName, $resultArr);
            return $this->db->insert_id();
        } else {
            return false;
        }

    }

    public function update($dataArr, $whereArr)
    {
        $whereArrSanitize = $this->base->validate($whereArr, $this->formFields);
        $resultArr = $this->base->update($dataArr, $this->formFields);
        if (count($resultArr) && count($whereArrSanitize)) {
            $this->db->update($this->tableName, $resultArr, $whereArrSanitize);
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    public function delete($whereArr, $strongDelete = false)
    {
        $whereArrSanitize = $this->base->validate($whereArr, $this->formFields);
        if (isset($whereArrSanitize) && !empty($whereArrSanitize)) {
            if ($strongDelete) {
                $this->db->delete($this->tableName, $whereArrSanitize);
            } else {
                $resultArr = $this->base->delete();
                $this->db->update($this->tableName, $resultArr, $whereArrSanitize);
            }
        }
    }

}