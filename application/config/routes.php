<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "home";

$route['about-us'] = "aboutus/itemView";
$route['contact-us'] = "home/contactUs";
$route['contact-us/validate'] = "home/validateContactUs";
$route['services'] = "home/services";
$route['careers'] = "home/careers";
$route['our-stores'] = "home/stores";
$route['contactus/send-email'] = "home/sendEmail";
$route['404_override'] = 'page';


$route['user/add'] = "user/add";
$route['user/validate'] = "user/validate";

$route['signup/reseller'] = "user/signupReseller";
$route['signup/reseller/validate'] = "user/validateReseller";
$route['admin/reseller'] = "user/reseller";
$route['admin/reseller/view/:num'] = "user/resellerView";
$route['admin/reseller/delete'] = "user/resellerDelete";

$route['signup/user'] = "user/signupUser";
$route['signup/user/validate'] = "user/validateUser";
$route['admin/user'] = "user/listUser";
$route['admin/user/view/:num'] = "user/userView";
$route['admin/user/delete'] = "user/userDelete";



$route['admin/switch/add/user'] = "user/addToSwitch";

/*num refers to category id*/
$route['product/add'] = "product/add";
$route['product/edit/:num'] = "product/add";
$route['product/uploadProductImage'] = "product/uploadProductImage";
$route['product/getProductImages'] = "product/getProductImages";
$route['product/getProductImageForRender'] = "product/getProductImageForRender";
$route['product/removeImage'] = "product/removeImage";
$route['product/validate'] = "product/validate";

/*career routes*/
$route['career/add'] = "career/addCareer";
$route['career/edit/:num'] = "career/addCareer";
$route['career/validate'] = "career/validate";
$route['career/delete/:num'] = "career/delete";



/*product routes*/
$route['product/auto-suggest/search'] = "product/autoSuggest";
$route['product/search'] = "product/searchRender";
$route['product/view/(:any)/:num'] = "product/itemView";
$route['product/set-default-image'] = "product/setDefaultImage";
$route['product/delete'] = "product/delete";

/*cart routes*/
$route['cart/add'] = "cart/addCart";
$route['cart/view'] = "cart/view";
$route['cart/remove'] = "cart/remove";
$route['cart/send-mail'] = "cart/sendEmail";

/*add about us */
$route['aboutus/add'] = "aboutus/add";
$route['aboutus/validate'] = "aboutus/validate";


/*Add members*/
$route['member/validate'] = "member/validate";
$route['member/edit/:num'] = "member/edit";
$route['member/delete'] = "member/delete";
$route['member/removeImage'] = "member/removeImage";

/*category routes*/
/*$route['category/add/:num'] = "category/add";
$route['category/edit/:num'] = "category/add";
$route['category/validate'] = "category/validate";

$route['category'] = "category/listing";
$route['category/view/:num'] = "category/itemView";
$route['category/search'] = "category/categorySearchRender";
$route['category/find'] = "category/categoryDemo";
$route['category/getTree'] = "category/categoryTree";
$route['category/delete'] = "category/delete";*/

/*Admin routes*/
$route['admin/dashboard'] = "admin/dashboard";
$route['admin/events'] = "event/listing";
$route['admin/event/add'] = "event/add";
$route['admin/event/validate'] = "event/validate";
$route['admin/event/edit/:num'] = "event/add";
$route['admin/event/search'] = "event/eventSearchRender";
$route['admin/event/delete'] = "event/delete";
$route['admin/event/view/:num'] = "event/view";
$route['admin/event/uploadEventImage'] = "event/uploadEventImage";
$route['admin/event/getEventImageForRender'] = "event/getEventImageForRender";

$route['admin/projects'] = "project/listing";
$route['admin/project/add'] = "project/add";
$route['admin/project/validate'] = "project/validate";
$route['admin/project/edit/:num'] = "project/add";
$route['admin/project/search'] = "project/projectSearchRender";
$route['admin/project/delete'] = "project/delete";
$route['admin/project/view/:num'] = "project/view";
$route['admin/project/uploadProjectImage'] = "project/uploadProjectImage";
$route['admin/project/removeImage'] = "project/removeImage";
$route['admin/project/getProjectImageForRender'] = "project/getProjectImageForRender";



$route['apps'] = "project/wbListing";


$route['admin/enquiry'] = "enquiry/listing";
$route['admin/enquiry/search'] = "enquiry/enquirySearchRender";
$route['admin/enquiry/delete'] = "enquiry/delete";
$route['admin/enquiry/view/:num'] = "enquiry/view";


/*product routes*/
$route['admin/jobs'] = "job/getJobs";


/*career routes*/
$route['admin/rates'] = "rate/list";
$route['admin/rates/list'] = "rate/listView";
$route['admin/rate/add'] = "rate/addRate";
$route['admin/rate/edit/:num'] = "rate/addRate";
$route['admin/rate/validate'] = "rate/validate";
$route['admin/rate/delete'] = "rate/delete";
$route['admin/rate/view/:num'] = "rate/view";

//website rate
$route['rates'] = "rate/wbRateListing";
$route['rate/view/:num'] = "rate/wbRateView";


/*login*/
$route['login'] = "admin/login";
$route['login/validate'] = "admin/validate ";
$route['logout'] = "admin/logout ";

/*Admin Profile*/
$route['admin/my-profile'] = "admin/editProfile ";
$route['admin/profile/validate'] = "admin/profileValidate ";

/*Add members*/
$route['admin/member/validate'] = "member/validate";
$route['admin/member/edit/:num'] = "member/edit";
$route['admin/member/add'] = "member/edit";
$route['admin/member/delete'] = "member/delete";
$route['admin/members'] = "member/listMembers";

/*Admin Message routes */
$route['admin/messages'] = "message/listMessage";
$route['admin/get-all-conversations'] = "message/getConversations";
$route['admin/message/view/:num'] = "message/view";
$route['admin/get-message'] = "message/getMessages";
$route['admin/generate/message/:num'] = "message/generate";


/*message common routes*/

$route['update/last-seen'] = "message/updateLastSeen";
$route['update/message-status'] = "message/updateStatus";
$route['message/send'] = "message/sendMessage";

/*website message routes*/
$route['chat'] = "message/wbChat";
$route['get-all-conversations'] = "message/wbGetConversations";
$route['message/view/:num'] = "message/wbView";
$route['get-message'] = "message/wbGetMessages";
$route['chat/new'] = "message/wbNewChat";
$route['chat/new/send'] = "message/wbNewChatSend";
$route['chat/signup'] = "user/wbSignup";
$route['chat/logout'] = "message/wbLogout";
$route['my-profile'] = "user/editProfile";
$route['profile/validate'] = "user/profileValidate";
$route['message/upload/file'] = "message/uploadFile";
$route['download/file'] = "message/downLoadFile";


/*Website routes*/
$route['apps'] = "project/getProjectsForWebsite";
$route['app/view/:num'] = "project/websiteView";

/* End of file routes.php */
/* Location: ./application/config/routes.php */