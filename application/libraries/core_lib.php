<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Core_lib
{


    protected $userId;
    /**
     * __construct
     *
     * @return void
     **/
    public function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->library('default_session');

    }

    public function getLoginUserId()
    {
        $userId = $this->CI->default_session->get('user_id');
        return $userId;
    }

    public function getUserGroup($userId = 0)
    {
        if(empty($userId)){
            $userId = $this->getLoginUserId();
        }
        $this->CI->load->model('user/user_model');
        $groupArr = $this->CI->user_model->getUserGroup($userId);
        return $groupArr;

    }

    public function getUserName($userId = 0)
    {
        if(empty($userId)){
            $userId = $this->getLoginUserId();
        }
        $this->CI->load->model('user/user_model');
        $userData = $this->CI->user_model->getData($userId);
        return isset($userData['first_name'])?$userData['first_name'] : false;

    }

    public function getCartCount()
    {
        $this->CI->load->model('product_cart_model');
        $userId = $this->getLoginUserId();
        $cartArr = $this->CI->product_cart_model->getCart($userId);
        return count($cartArr);
    }

    public function getLoggedUserData()
    {
        $userId = $this->getLoginUserId();
        $this->CI->load->model('user/user_model');
        $userData = $this->CI->user_model->getData($userId);
        return $userData;
    }


    public function redirectPage()
    {
        $url = base_url() . "admin/login";
        $requestUrl = parse_url($_SERVER['REQUEST_URI']);
        if (isset($requestUrl['path'])) {
            $path = $requestUrl['path'];
            $eURL = base64_encode($path);
            $url .= "?redirect=" . $eURL;
            header('Location:' . $url);
        }
    }

    public function getActiveEnquiry()
    {
        $postData = [
            'status' => 'unread',
        ];
        $this->CI->load->model('contactus_model', 'contactus_model');
        $query = $this->CI->contactus_model->getSearchQuery($postData);
        $result = $this->CI->contactus_model->getListByQuery($query, 1, '4');
        return $result;
    }

    public function getUnreadMessages()
    {
        $this->CI->load->model('message_model','message_model');
        $toId = $this->getLoginUserId();
        $messages = $this->CI->message_model->getAllMessages('',$toId,'unread');
        return $messages;
    }

    public function getMenu()
    {
        $this->CI->load->model('category/category_model');
        $parentCategories = $this->CI->category_model->getParentCategory();
        return $parentCategories;
    }
}
