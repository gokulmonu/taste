<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Base
{
    public function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->library('core_lib');

    }

    public function validate($dataArr, $formFields)
    {
        $resultArr = [];
        if (is_array($dataArr) && count($dataArr) > 0 && is_array($formFields) && count($formFields)) {
            foreach ($dataArr as $key => $value) {
                $fieldType = '';
                if (array_key_exists($key, $formFields)) {
                    $fieldType = $formFields[$key];
                    switch ($fieldType) {
                        case "integer":
                            if (is_int($value)) {
                                $resultArr[$key] = $value;
                            } else {
                                $resultArr[$key] = intval($value);
                            }
                            break;

                        case "string":
                            if (is_string($value)) {
                                $resultArr[$key] = $value;
                            } else {
                                $resultArr[$key] = strval($value);
                            }
                            break;
                        case "decimal":
                                $resultArr[$key] = floatval($value);
                            break;

                        case "boolean":
                            if (is_bool($value)) {
                                $resultArr[$key] = $value;
                            } else {
                                $resultArr[$key] = (bool)$value;
                            }
                            break;

                        case "date":
                            $d = DateTime::createFromFormat('Y-m-d', $value);
                            if ($d && $d->format('Y-m-d') === $value) {
                                $resultArr[$key] = $value;
                            } else $resultArr[$key] = NULL;

                            break;

                        case "datetime":
                            $resultArr[$key] = $value;
                            break;

                        case "time":
                            $resultArr[$key] = $value;
                            break;
                        case "text":
                            $resultArr[$key] = trim($value);
                            break;

                    }

                }
            }


        }

        return $resultArr;
    }

    public function add($dataArr, $formFields)
    {
        $resultArr = [];

        if (is_array($dataArr) && count($dataArr)) {
            $resultArr = $this->validate($dataArr, $formFields);
            if (count($resultArr)) {
                $authorId =  $this->CI->core_lib->getLoginUserId();
                $resultArr['created'] = date("Y-m-d H:i:s");
                $resultArr['is_updated'] = date("Y-m-d H:i:s");
                $resultArr['is_deleted'] = null;
                if($authorId){
                    $resultArr['author_id'] = $authorId;
                }else $resultArr['author_id'] =0;



            }
        }
        return $resultArr;
    }

    public function update($dataArr, $formFields)
    {
        $resultArr = [];
        if (is_array($dataArr) && count($dataArr)) {
            $resultArr = $this->validate($dataArr, $formFields);
            if (count($resultArr)) {
                $resultArr['is_updated'] = date("Y-m-d H:i:s");

            }
        }
        return $resultArr;
    }

    public function delete()
    {
        $authorId =  $this->CI->core_lib->getLoginUserId();

         $resultArr = [
            'is_deleted' => date("Y-m-d H:i:s"),
            'deleted_by' => $authorId,
            'is_updated' => date("Y-m-d H:i:s")
        ];
        return $resultArr;
    }
}