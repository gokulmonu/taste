<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/* load the MX_Loader class */
require APPPATH."third_party/MX/Loader.php";

class MY_Loader extends MX_Loader {
    public function template($template_name, $vars = array(), $return = FALSE)
    {
        $content  = $this->view('header');
        $content .= $this->view($template_name, $vars, $return);
        $content .= $this->view('footer');

        if ($return)
        {
            return $content;
        }
    }

    public function adminTemplate($template_name, $vars = array(), $return = FALSE)
    {
        $content  = $this->view('admin/header',$vars);
        $content .= $this->view('admin/'.$template_name, $vars, $return);
        $content .= $this->view('admin/footer');

        if ($return)
        {
            return $content;
        }
    }
    public function moduleTemplate($template_name, $vars = array(), $return = FALSE)
    {
        $content  = $this->view('admin/header',$vars);
        $content .= $this->view($template_name, $vars, $return);
        $content .= $this->view('admin/footer');

        if ($return)
        {
            return $content;
        }
    }

    public function chatTemplate($template_name, $vars = array(), $return = FALSE)
    {
        $content  = $this->view('message/header',$vars);
        $content .= $this->view('message/'.$template_name, $vars, $return);
        $content .= $this->view('message/footer');

        if ($return)
        {
            return $content;
        }
    }
}