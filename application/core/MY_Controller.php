<?php

class MY_Controller extends CI_Controller
{
    public function __construct($type = 'admin')
    {
        parent::__construct();
        $userGroup = $this->core_lib->getUserGroup();
        if (!in_array("admin", $userGroup) && $type == "admin") {
            $baseUrl = base_url() . "login/";
            $url = $_SERVER['REQUEST_URI'];
            $eURL = base64_encode($url);
            $baseUrl .= "?redirect=" . $eURL;
            header('Location:' . $baseUrl);
        }

    }
}