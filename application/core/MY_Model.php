<?php

class MY_Model extends CI_Model
{
    function __construct()
    {
        //$this->CI = & get_instance();
        parent::__construct();
        $this->load->library('Base', 'base');
    }

    protected $tableName;

    public function getListByQuery($query,$page = null,$pageCount = null)
    {

        if (!$page) $page = 1;
        if (!$pageCount) $pageCount = 20;
        $pageLinks = $this->myPagination($query,$page,$pageCount);
        $data["rows"] = $this->getQueryResult($query,$pageCount, $page);
        $data["pagination"] = $pageLinks;
        return $data;



    }

    public function getQueriesCount($query)
    {
       $q = $this->db->query($query);
        $count = $q->num_rows();
        return $count;
    }

    public function getQueryResult($query,$perPage,$page)
    {
        $offset = ($page - 1) * $perPage;
        $sql = $query;
        $sql.= " LIMIT $offset,$perPage";
        $q = $this->db->query($sql);
        $result = $q->result_array();
        return $result;

    }

    public function myPagination($query,$page,$pageCount)
    {
        $numLinks = 4 ;
        $sp =1;
        $pagingArray = [];
        $totalRows = $this->getQueriesCount($query);
        $numPages = (int)ceil($totalRows/$pageCount);
        if($page < $numLinks ){
            $sp = 1;
        }elseif($page >= ($numPages - floor($numLinks/2))){
            $sp = $numPages - $numLinks + 1;
        }elseif($page >= $numLinks){
            $sp = $page - floor($numLinks/2);
        }
        for($i = $sp;$i <=($sp + $numLinks-1 );$i ++) {
            if ($i > $numPages)
                continue;
            $temp = [];
            $temp['page_num'] = $i;
            $temp['text'] = $i;
            if ($page == $i) {
                $temp['current_page'] = true;
            } else $temp['current_page'] = false;
            $pagingArray[] = $temp;
        }
        if($page >= $numLinks){
            $tempArrShift = [];
            $tempArrShift['page_num'] = 1;
            $tempArrShift['text'] = "First";
            $tempArrShift['current_page'] = false;
            array_unshift($pagingArray,$tempArrShift);
        }
        if($page <= $numLinks && $totalRows > $pageCount && $numPages != $page){
            $tempArrShift = [];
            $tempArrShift['page_num'] = $numPages;
            $tempArrShift['text'] = "Last";
            $tempArrShift['current_page'] = false;
            array_push($pagingArray,$tempArrShift);
        }
     return $pagingArray;
    }

    public function getData($id)
    {
        if($id){
            $query = $this->db->get_where($this->tableName,array('id'=>$id ,'is_deleted' =>null));
            $result = $query->row_array();
            return $result;
        }else return false;
    }

    public function listByFields($fieldsArr = [])
    {
        if(count($fieldsArr)){
            $fieldsArr['is_deleted'] = null;
            $query = $this->db->get_where($this->tableName,$fieldsArr);
            $result = $query->result_array();
            return $result;
        }else return false;
    }

    public function loadByFields($fieldsArr = [])
    {
        $resultArr = [];
        if (count($fieldsArr)) {
            $fieldsArr['is_deleted'] = null;
            $query = $this->db->get_where($this->tableName, $fieldsArr);
            $result = $query->result_array();
            if ($result) {
                $resultArr = current($result);
            }

        }
        return $resultArr;
    }

    public function getAll()
    {
            $fieldsArr['is_deleted'] = null;
            $query = $this->db->get_where($this->tableName,$fieldsArr);
            $result = $query->result_array();
            return $result;
    }

    public function isValueExist($fieldName ,$fieldValue)
    {
        $fieldsArr = [
            $fieldName => $fieldValue,
            'is_deleted' =>null
        ];

        $query = $this->db->get_where($this->tableName,$fieldsArr);
        $result = $query->num_rows();
        if($result){
            return true;
        }else{
            return false;
        }
    }
}