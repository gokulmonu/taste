<script>
    function removeCart(cartId) {
        if(confirm("Are you sure want to remove this product from cart?")){
            var postData = {};
            postData['cart_id'] = cartId;
            $.ajax({
                url: '<?php echo base_url()?>cart/remove',
                type: 'POST',
                data: postData,
                success: function (data) {
                    location.reload();
                },
                error: function (e) {
                    //called when there is an error
                    //console.log(e.message);
                }
            });
        }else{
            return false;
        }

    }

    function sendMail()
    {
        $.ajax({
            url: '<?php echo base_url()?>cart/send-mail',
            type: 'POST',
            data: '',
            success: function (data) {
                if(data.success){
                 alert("You have successfully placed product request. Our admin will contact you as soon as possible");
                    window.location.href ='<?php echo base_url() ?>';
                }

            },
            error: function (e) {
                //called when there is an error
                //console.log(e.message);
            }
        });
    }
</script>

<section class="s-wrp s-hi-pad"><!-- section wrp-->
    <div class="s-container"><!-- s-container-->

        <div class="s-wrp"><!--s-wrp-->

            <div class="s-row"><!--s row-->

                <div class="s-col-lg-12 s-col-md-12 s-col-sm-12 s-col-xs-12"><!--s col-->

                    <span class="fl-rt s-md-pad">
                    <a class="pg-btn" href="javascript:location.reload();"><i class="fa fa-refresh"></i> Update Cart</a>
                    </span>

                    <div class="tbl-box s-wrp"><!--tbl box-->

                        <div class="scroll-div">

                            <table class="table-responsive normal-table custom-table cart-table">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>PRODUCT</th>
                                    <th>VARIANT</th>
                                    <th>QUANTITY</th>

                                </tr>
                                </thead>
                                <tbody>


                                    <?php
                                    if(isset($cart) && count($cart)){

                                    foreach($cart as $value){
                                        $thumbimg = !empty($value['thumbnail'])?$value['thumbnail']:$this->config->item('default_thumb_image_url');
                                        ?>
                                    <tr>
                                        <td><a class="close-btn" href="javascript:;" onclick="removeCart('<?php echo $value['cart_id'] ?>')"><i class="fa fa-close"></i></a></td>
                                        <td>
                                            <div class="s-wrp">

                                                <div class="s-row"><!--s row-->

                                                    <div class="s-col-lg-2 s-col-md-2 s-col-sm-4 s-col-xs-4"><!--s col-->

                                                        <span class="cart-prod-img"><img src="<?php echo $thumbimg; ?>"></span>

                                                    </div>
                                                    <!--/. s col-->

                                                    <div class="s-col-lg-10 s-col-md-10 s-col-sm-8 s-col-xs-8"><!--s col-->

                                                        <a class="cart-prod-title" href="#"><?php echo $value['name'] ?></a>
                                                    </div>
                                                    <!--/. s col-->

                                                </div>
                                                <!--/. s row-->
                                            </div>
                                        </td>
                                        <td><span class="cart-price"><?php
                                                if(isset($value['variant']) && count($value['variant'])){
                                                    foreach($value['variant'] as $v){
                                                        echo $v['v_type'] ." : " .$v['v_value'] ."<br>";
                                                    }
                                                }

                                                ?></span></td>
                                        <td><span><?php echo $value['p_quantity'] ?></span></td>
                                    </tr>
                                    <?php
                                    }

                                    }else{
                                        ?>
                                        <tr>
                                            <td colspan="4">No products in cart</td>
                                        </tr>
                                        <?php
                                    }
                                    ?>



                                </tbody>
                            </table>

                            <section class="s-wrp s-lt-pad">
                                <div class="s-wrp s-md-pad total-cart-box">
                                    <h3 class="s-txt-right">CART TOTALS</h3>
                                    <h6 class="s-txt-right">Sub Total: Not available</h6>
                                    <h6 class="s-txt-right">Grand Total: Not available</h6>
                                </div>
                                <div class="s-wrp s-md-pad">
                                    <div class="fl-rt">
                                        <?php
                                        if(isset($cart) && count($cart)){?>
                                            <a class="pg-btn" href="javascript:;" onclick="sendMail()"><i class="fa fa-send"></i>Send A Mail</a>
                                        <?php }
                                        ?>

                                    </div>

                                </div>


                            </section>

                        </div>

                    </div>



                </div>
                <!--/. s col-->

            </div>
            <!--/. s row-->

        </div>
        <!--/. s-wrp-->
    </div>
    <!--/. s-container-->
</section>
<!--/. section wrp-->