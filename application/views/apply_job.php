<style>


    .contentform.info.p {
        text-align: center;
        color: #999;
        text-transform: none;
        font-weight: 600;
        font-size: 15px;
        margin-top: 2px
    }

    .contentform.info.i {
        color: #F6AA93;
    }

    .user_form h1 {
        font-size: 18px;
        background: #F6AA93 none repeat scroll 0% 0%;
        color: rgb(255, 255, 255);
        padding: 22px 25px;
        border-radius: 5px 5px 0px 0px;
        margin: auto;
        text-shadow: none;
        text-align: left
    }

    .user_form {
        border-radius: 5px;
        max-width: 700px;
        width: 100%;
        margin: 5% auto;
        background-color: #FFFFFF;
        overflow: hidden;
    }

    h1 {
        text-align: center;
        color: #666;
        text-shadow: 1px 1px 0px #FFF;
        margin: 50px 0px 0px 0px
    }

    .contentform input,select {
        border-radius: 0px 5px 5px 0px;
        border: 1px solid #eee;
        margin-bottom: 15px;
        width: 75%;
        height: 40px;
        float: left;
        padding: 0px 15px;
    }

    .contentform a {
        text-decoration: inherit
    }

    .form-group {
        overflow: hidden;
        clear: both;
    }

    .icon-case {
        width: 35px;
        float: left;
        border-radius: 5px 0px 0px 5px;
        background: #eeeeee;
        height: 42px;
        position: relative;
        text-align: center;
        line-height: 40px;
    }

    i {
        color: #555;
    }

    .contentform {
        padding: 40px 30px;
    }

    .bouton-contact {
        background-color: #81BDA4;
        color: #FFF;
        text-align: center;
        width: 100%;
        border: 0;
        padding: 17px 25px;
        border-radius: 0px 0px 5px 5px;
        cursor: pointer;
        margin-top: 40px;
        font-size: 18px;
    }

    .leftcontact {
        width: 49.5%;
        float: left;
        border-right: 1px dotted #CCC;
        box-sizing: border-box;
        padding: 0px 15px 0px 0px;
    }

    .rightcontact {
        width: 49.5%;
        float: right;
        box-sizing: border-box;
        padding: 0px 0px 0px 15px;
    }

    .validation {
        display: none;
        margin: 0 0 10px;
        font-weight: 400;
        font-size: 13px;
        color: #DE5959;
    }

    #sendmessage {
        border: 1px solid #fff;
        display: none;
        text-align: center;
        margin: 10px 0;
        margin-bottom: 30px;
        background-color: #EBF6E0;
        color: #5F9025;
        border: 1px solid #B3DC82;
        padding: 13px 40px 13px 18px;
        border-radius: 3px;
        box-shadow: 0px 1px 1px 0px rgba(0, 0, 0, 0.03);
    }

    #sendmessage.show, .show {
        display: block;
    }
</style>

<script>

    $(function () {

        $("#resume_submit_btn").click(function( event ) {
            event.preventDefault();
            var formData = getFormData();
            $.ajax({
                url: '<?php echo base_url()?>job/apply/validate',
                type: 'POST',
                data:formData,
                async : false,
                cache : false,
                contentType : false,
                processData : false,
                success: function (data) {
                    $('.validation').show().html('');
                    if(!data.success){
                        for(key in data){
                            $('#'+data[key]['field_id']).html(data[key]['label']);
                        }
                    }else{
                        alert('Successfully submitted your resume. Admin will contact you as soon as possible.');
                        location.href="<?php echo base_url()?>job/search";
                    }



                },
                error: function (e) {
                    //called when there is an error
                    //console.log(e.message);
                }
            });

        });

        function getFormData() {
            var formElement = new FormData();
            formElement.append('name',$('#first_name').val());
            formElement.append('phone',$('#phone').val());
            formElement.append('email',$('#email').val());
            formElement.append('experiance',$('#work_experiance').val());
            formElement.append('industry_id',$('#industry').val());
            formElement.append('job_id','<?= isset($job_id)?$job_id:"" ?>');
            formElement.append('file',$('#resume')[0].files[0]);
            return formElement;
        }

    });
</script>
<div class="f-container"><!--f container-->

    <section class="f-wrp main-box" style="margin-top: 0"><!--f-wrp-->
        <section class="f-wrp">
            <div class=f-container>
                <h1>Job Application</h1>

                <form class="user_form">
                    <h1>Should you have any questions, please do not hesitate to contact me :</h1>

                    <div class="contentform">
                        <div id="sendmessage"></div>


                        <div class="f-col-lg-6 f-col-sm-12">
                            <div class="form-group">
                                <p>Full Name<span></span></p>
                                <span class="icon-case"><i class="fa fa-user"></i></span>
                                <input type="text" name="first_name" id="first_name"/>

                                <div class="validation" id="name_error"></div>
                            </div>

                            <div class="form-group">
                                <p>Phone<span></span></p>
                                <span class="icon-case"><i class="fa fa-phone"></i></span>
                                <input type="text" name="phone" id="phone"/>

                                <div class="validation" id="name_error"></div>
                            </div>

                            <div class="form-group">
                                <p>E-mail <span>*</span></p>
                                <span class="icon-case"><i class="fa fa-envelope-o"></i></span>
                                <input type="email" name="email" id="email"/>

                                <div class="validation" id="email_error"></div>
                            </div>

                        </div>

                        <div class="f-col-lg-6 f-col-sm-12">

                            <div class="form-group">
                                <p>Work Experience<span>*</span></p>
                                <span class="icon-case"><i class="fa fa-star"></i></span>
                                <input type="text" name="work_experiance" placeholder="(years)" id="work_experiance"/>

                                <div class="validation" id="experiance_error"></div>
                            </div>

                            <div class="form-group">
                                <p>Industry<span>*</span></p>
                                <span class="icon-case"><i class="fa fa-industry"></i></span>
                                <select id="industry" class="">
                                    <option value="" >--select--</option>
                                    <?php if(isset($category['rows'])): ?>
                                        <?php foreach($category['rows'] as $cat):?>
                                            <option  value="<?= $cat['id']?>"><?= $cat['name']?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>

                                <div class="validation" id="industry_id_error"></div>
                            </div>
                            <div class="form-group">
                                <p>Resume<span>*</span></p>
                                <span class="icon-case"><i class="fa fa-file"></i></span>
                                <input type="file" name="resume" id="resume"/>

                                <div class="validation"></div>
                            </div>

                        </div>
                    </div>
                    <button type="button" id="resume_submit_btn" class="bouton-contact">Submit</button>

                </form>
            </div>
        </section>
    </section>
</div>