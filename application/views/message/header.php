<head>
    <!-- META SECTION -->
    <title>My Company - Admin</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <!-- END META SECTION -->

    <!-- CSS INCLUDE -->
    <link rel="stylesheet" type="text/css" id="theme" href="<?php echo base_url('bootstrap/css/admin/theme-default.css'); ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>bootstrap/css/admin/dropzone/dropzone.css"/>
    <!-- EOF CSS INCLUDE -->



    <!-- START SCRIPTS -->
    <!-- START PLUGINS -->
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/plugins/jquery/jquery.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/plugins/jquery/jquery-ui.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/plugins/bootstrap/bootstrap.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/plugins/smartwizard/jquery.smartWizard-2.0.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/plugins/summernote/summernote.js');?>"></script>
    <!-- END PLUGINS -->

    <!-- START THIS PAGE PLUGINS-->
    <script type='text/javascript' src="<?php echo base_url('bootstrap/js/admin/dropzone/dropzone.js');?>"></script>
    <script type='text/javascript' src="<?php echo base_url('bootstrap/js/admin/plugins/icheck/icheck.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js');?>"></script>

    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/plugins/morris/raphael-min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/plugins/morris/morris.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/plugins/rickshaw/d3.v3.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/plugins/rickshaw/rickshaw.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js');?>"></script>
    <script type='text/javascript' src="<?php echo base_url('bootstrap/js/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js');?>"></script>
    <script type='text/javascript' src="<?php echo base_url('bootstrap/js/admin/plugins/bootstrap/bootstrap-datepicker.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/plugins/owl/owl.carousel.min.js');?>"></script>

    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/plugins/moment.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/plugins/daterangepicker/daterangepicker.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/plugins/blueimp/jquery.blueimp-gallery.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/faq.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/jquery.caret.1.02.js');?>"></script>

    <!-- END THIS PAGE PLUGINS-->

    <!-- START TEMPLATE -->
    <!--<script type="text/javascript" src="<?php /*echo base_url('bootstrap/js/admin/settings.js');*/?>"></script>
-->
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/plugins.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/actions.js');?>"></script>

   <!-- <script type="text/javascript" src="<?php /*echo base_url('bootstrap/js/admin/demo_dashboard.js');*/?>"></script>
   --> <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->

</head>

<body>
<div id="loader_screen"></div>
<?php
$userData = $this->core_lib->getLoggedUserData();
?>

