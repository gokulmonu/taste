<script>
    $(function () {
        $('#admin').on('change',function(){
           if($(this).val()){
               $('#to_user').attr('disabled',true);
               $('#to_user').val($(this).val());
           }else{
               $('#to_user').attr('disabled',false);
               $('#to_user').val("");

           }
        });

        $('#send_msg').on('click',function(e){
            e.preventDefault();
            var msg =  $('#msg').val();
            if(msg){
                sendMessage();
            }else{
                alert('You cant send a blank message');
            }
        });

        $('#go_back').on('click',function(e){
            e.preventDefault();
            location.href = "<?php echo base_url()?>chat";
        });
    });

    function sendMessage(){
        var postData = {};
        postData['to_email'] = $('#to_user').val();
        postData['msg'] = $('#msg').val();
        $.ajax({
            url: '<?php echo base_url()?>chat/new/send',
            type: 'POST',
            data: postData,
            success: function (json) {
                if(json.success){
                    location.href = "<?php echo base_url()?>chat";
                }else{
                    alert('Message not send');
                    location.href = "<?php echo base_url()?>chat";
                }
            }
        });
    }
</script>
<div class="block">
    <h4>New Conversation</h4>
    <form role="form" class="form-horizontal">

        <div class="form-group">
            <label class="col-md-2 control-label">From</label>
            <div class="col-md-10">
                <input type="text" value="@You" readonly="" class="form-control">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">To</label>
            <div class="col-md-10">
                <input type="text" id="to_user" placeholder="Please enter email" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label"> or Admin</label>
            <div class="col-md-9">
                <select class="form-control" id="admin">
                    <option value="">--select--</option>
                    <?php
                    if(isset($admin) && $admin){
                        foreach($admin as $user){
                            ?>
                            <option value="<?= $user['email'] ?>"><?= $user['first_name'] ." " .$user['last_name'] ?></option>
                        <?php
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">Message</label>
            <div class="col-md-10">
                <textarea rows="5" id="msg" class="form-control"></textarea>
            </div>
        </div>
        <div class="col-md-12">
            <div class="pull-left">
                <button class="btn btn-default" id="go_back"><span class="fa fa-arrow-left"></span>Go Back</button>
            </div>
            <div class="pull-right">
                <button class="btn btn-danger" id="send_msg"><span class="fa fa-envelope"></span> Send Message</button>
            </div>
        </div>
    </form>
</div>
<!-- END DEFAULT FORM ELEMENTS -->
