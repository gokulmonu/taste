<style>
    [contenteditable] {
        border: 1px solid #DDDDDD;
        line-height: 1.4em;
        -webkit-appearance: textfield;
        appearance: textfield;
        min-height:30px;
        height: auto;
        width: 250px;
        margin-left: 2px;
        word-wrap: break-word
    }

</style>
<script>
    var userProfileUrl = '<?= isset($user_details['file_ref'])?$user_details['file_ref']:""?>';
    var userName = '<?= isset($user_details['first_name'])?$user_details['first_name']:"" ?>';
    var userId = '<?= isset($user_details['id'])?$user_details['id']:0 ?>';
    var currentPage = 1;

    updateMessagesStatus();
    $(function () {
        var emoticons = {
            ':angry:' : 'angry-emoji.png',
            ':sob:'  : 'cry-emoji.png',
            ':stuck_out_tongue_winking_eye:'  : 'fun-emoji.png',
            ':joy:'  : 'laughing-emoji.png',
            ':heart_eyes:'  : 'love-emoji.png',
            ':flushed:'  : 'pling-emoji.png',
            ':astonished:'  : 'wow-emoji.png',
            ':y-angry:' : 'y_angry.png',
            ':y-cry:' : 'y_cry.png',
            ':y-kiss:' : 'y_kiss.png',
            ':y-love:' : 'y_love.png',
            ':y-love2:' : 'y_love2.png',
            ':y-love3:' : 'y_love3.png',
            ':y-loveu:' : 'y_loveu.png',
            ':y-thinking:' : 'y_thinking.png'

        };
        reloadMessages();
        setInterval(reloadMessages,10000);
        setInterval(updateLastSeen,10000);
        Dropzone.autoDiscover = false;
        $("div#drop_zone").dropzone({
            url: '<?php echo base_url()?>message/upload/file',
            init :function(){
                this.on('sending',function(file,xhr,formData){
                    formData.append('to_id','<?= isset($user_id)?$user_id:"";?>');
                })
            },
            success: function (file, response) {
                if(response.error){
                    $('#upload_error').html(response.error);
                }
                this.removeFile(file);
                $('#close_file_modal').trigger('click');

            }
        });

        function replaceTextWithEmoji(text) {
            var cText = text;
            if(cText){
                $.each(emoticons, function (key, code) {
                    var image = '<img style="width: 20px;height: 20px;" src="<?= base_url() ?>bootstrap/images/emoji/' + code + '">';
                    cText =  cText.replace(new RegExp(key, "gi"), image);
                });
            }
            return cText;
        }

        $('#send_msg_btn').on('click',function(){

            var content = $('#msg_show').html();
            $('#msg_hidden').html(content);
            $('#msg_hidden').find('img').each(function(){
                var imgCode = $(this).attr('data-content');
                $(this).replaceWith(imgCode);
            });

            var msg = $('#msg_hidden').html();
            if(msg){
                var postData = {};
                postData['to_id'] = '<?= isset($user_id)?$user_id:"";?>';
                postData['msg'] = msg;
                $.ajax({
                    url: '<?php echo base_url()?>message/send',
                    type: 'POST',
                    data: postData,
                    success: function (json) {
                        $('#msg_hidden').html("");
                        $('#msg_show').html("");
                        reloadMessages();
                    }
                });
            }else{
                alert('You cant send a blank message');
            }

        });

        function reloadMessages() {
            var postData = {};
            var lastMsgId = $('.msg_cls').last().attr('data-content');
            postData['id'] = '<?= isset($user_id)?$user_id:"";?>';
            postData['page'] = currentPage;
            postData['last_id'] = lastMsgId;
            $.ajax({
                url: '<?php echo base_url()?>get-message',
                type: 'POST',
                data: postData,
                success: function (json) {
                    if (json.success) {
                        if (json.result) {
                            for (key in json.result) {
                                if(!$('#msg_'+json.result[key]['id']).length){
                                    var div = document.createElement('div');
                                    if (json.result[key]['to_id'] == userId) {
                                        div.setAttribute('class', 'item in item-visible msg_cls');
                                    } else {
                                        div.setAttribute('class', 'item item-visible msg_cls');
                                    }
                                    div.setAttribute('id', 'msg_'+json.result[key]['id']);
                                    div.setAttribute('data-content',json.result[key]['id']);
                                    var innerDiv = document.createElement('div');
                                    innerDiv.setAttribute('class', 'image');

                                    var img = document.createElement('img');
                                    if (json.result[key]['to_id'] == userId) {
                                        img.setAttribute('src', '<?= base_url() ?>uploads/' + json.result[key]['from_file_ref']);
                                    } else {
                                        img.setAttribute('src', '<?= base_url() ?>uploads/' + userProfileUrl);
                                    }
                                    innerDiv.appendChild(img);

                                    var textDiv = document.createElement('div');
                                    textDiv.setAttribute('class', 'text');

                                    var titleDiv = document.createElement('div');
                                    titleDiv.setAttribute('class', 'heading');

                                    var aTag = document.createElement('a');
                                    aTag.setAttribute('href', '#');
                                    var aTextElem;
                                    if (json.result[key]['to_id'] == userId) {
                                        aTextElem = document.createTextNode(json.result[key]['from_first_name']);

                                    } else {
                                        aTextElem = document.createTextNode(userName);

                                    }
                                    aTag.appendChild(aTextElem);


                                    var created = json.result[key]['msg_created'];
                                    var span = document.createElement('span');
                                    span.setAttribute('class', 'date');
                                    span.innerHTML = created;

                                    titleDiv.appendChild(aTag);
                                    titleDiv.appendChild(span);

                                    textDiv.appendChild(titleDiv);

                                    var msgDiv = document.createElement('div');
                                    var textNode = document.createTextNode(replaceTextWithEmoji(json.result[key]['message']));
                                    if(json.result[key]['file_id']){
                                        var download = document.createElement('a');
                                        download.setAttribute('href', 'javascript:;');
                                        download.setAttribute('onclick','downloadFile('+json.result[key]['file_id']+')');
                                        var aTextDownloadElem = document.createTextNode("Download " +json.result[key]['file_name']);
                                        download.appendChild(aTextDownloadElem);
                                        msgDiv.appendChild(download);
                                    }else{
                                        msgDiv.innerHTML = replaceTextWithEmoji(json.result[key]['message']);
                                    }
                                    textDiv.appendChild(msgDiv);

                                    div.appendChild(innerDiv);
                                    div.appendChild(textDiv);

                                    $('#message_container').append(div);
                                    scrollToBottom();
                                   // resortMessages();

                                }

                            }
                        }
                        updateMessagesStatus();
                    }

                },
                error: function (e) {
                }
            });

        }



        function resortMessages()
        {
            var divList = $(".msg_cls");
            divList.sort(function(a, b){
                return $(a).attr('data-content')-$(b).attr('data-content')
            });
            $("#message_container").html(divList);
            scrollToBottom();
        }

        function updateLastSeen()
        {
            $.ajax({
                url: '<?php echo base_url()?>update/last-seen'
            });
        }

        $('.selectable-icons').on('click',function(e){
            $('#close_modal').trigger('click');
            if (e.target.tagName.toLowerCase() === 'img') {

                document.querySelector('[contenteditable]').appendChild(e.target.cloneNode(true));
            }
        });

    });
    function updateMessagesStatus()
    {
        var postData = {};
        postData['id'] = '<?= isset($user_id)?$user_id:"";?>';
        $.ajax({
            url: '<?php echo base_url()?>update/message-status',
            type: 'POST',
            data: postData,
            success: function (json) {

            }
        });
    }
    function scrollToBottom()
    {
        $(window).scrollTop($('#msg_text_container').offset().top);
    }
    function goBackToConversations()
    {
        location.href = "<?php echo base_url()?>chat";
    }

    function downloadFile(id)
    {
        location.href = "<?php echo base_url()?>download/file?file_id="+id;
    }
</script>

<div class="col-md-8">

	<div class="message-wrapper">
    	
        <div class="messages messages-img" id="message_container">

    </div>
    <div class="panel panel-default push-up-10 send-msg">
        <div class="panel-body panel-body-search">
            <div class="input-group">
                <div class="input-group-btn">
                    <button class="btn btn-default" data-toggle="modal" data-target="#modal_basic"><i style="line-height: 20px;" class="fa fa-smile-o"></i></button>
                    <button class="btn btn-default" data-toggle="modal" data-target="#file_upload"><i style="line-height: 20px;" class="fa fa-paperclip"></i></button>
                </div>
                <div id="msg_hidden" style="display: none">


                </div>
                <div contenteditable="true" id="msg_show">

                </div>
                <div class="input-group-btn" id="msg_text_container">
                    <button class="btn btn-default" id="send_msg_btn">Send</button>
                </div>
            </div>
        </div>
    </div>
        
        
    </div>

    
</div>


<!-- MODALS -->
<div class="modal" id="modal_basic" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="defModalHead">Emoji</h4>
            </div>
            <div class="modal-body">
                <div id="emoji_dialog" class="selectable-icons">
                    <img data-content=":angry:" style="width: 20px;height: 20px;" src="<?php echo base_url('bootstrap/images/emoji/angry-emoji.png');?>"/>
                    <img data-content=":sob:" style="width: 20px;height: 20px;" src="<?php echo base_url('bootstrap/images/emoji/cry-emoji.png');?>"/>
                    <img data-content=":stuck_out_tongue_winking_eye:" style="width: 20px;height: 20px;" src="<?php echo base_url('bootstrap/images/emoji/fun-emoji.png');?>"/>
                    <img data-content=":joy:" style="width: 20px;height: 20px;" src="<?php echo base_url('bootstrap/images/emoji/laughing-emoji.png');?>"/>
                    <img data-content=":heart_eyes:" style="width: 20px;height: 20px;" src="<?php echo base_url('bootstrap/images/emoji/love-emoji.png');?>"/>
                    <img data-content=":flushed:" style="width: 20px;height: 20px;" src="<?php echo base_url('bootstrap/images/emoji/pling-emoji.png');?>"/>
                    <img data-content=":astonished:" style="width: 20px;height: 20px;" src="<?php echo base_url('bootstrap/images/emoji/wow-emoji.png');?>"/>


                    <img data-content=":y-angry:" style="width: 20px;height: 20px;" src="<?php echo base_url('bootstrap/images/emoji/y_angry.png');?>"/>
                    <img data-content=":y-cry:" style="width: 20px;height: 20px;" src="<?php echo base_url('bootstrap/images/emoji/y_cry.png');?>"/>
                    <img data-content=":y-kiss:" style="width: 20px;height: 20px;" src="<?php echo base_url('bootstrap/images/emoji/y_kiss.png');?>"/>
                    <img data-content=":y-love:" style="width: 20px;height: 20px;" src="<?php echo base_url('bootstrap/images/emoji/y_love.png');?>"/>
                    <img data-content=":y-love2:" style="width: 20px;height: 20px;" src="<?php echo base_url('bootstrap/images/emoji/y_love2.png');?>"/>
                    <img data-content=":y-love3:" style="width: 20px;height: 20px;" src="<?php echo base_url('bootstrap/images/emoji/y_love3.png');?>"/>
                    <img data-content=":y-loveu:" style="width: 20px;height: 20px;" src="<?php echo base_url('bootstrap/images/emoji/y_loveu.png');?>"/>
                    <img data-content=":y-thinking:" style="width: 20px;height: 20px;" src="<?php echo base_url('bootstrap/images/emoji/y_thinking.png');?>"/>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="close_modal" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="file_upload" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="defModalHead">Emoji</h4>
            </div>
            <div class="modal-body">
                <div id="drop_zone" class="dropzone" style="min-height: 180px;">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="close_file_modal" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div
