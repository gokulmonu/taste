
<script>
    $(function () {
        $("#signup_btn").click(function( event ) {
            event.preventDefault();
            var formData = getMemberFormData();
            $.ajax({
                url: '<?php echo base_url()?>signup/user/validate',
                type: 'POST',
                data:  formData,
                contentType : false,
                processData : false,
                success: function (data) {
                    if(data.success){
                        alert('Thank you for your interest in our services .Our admin will contact you as soon as possible.');
                        location.href = '<?php echo base_url()?>';

                    }

                    if (!data.success) {
                        var string = '';
                        if (data.response_message) {
                            string = data.response_message;
                        } else {

                            for (key in data) {
                                if (data[key]['label']) {
                                    string += data[key]['label'] + "<br>";
                                }
                            }
                        }
                        jQuery('#error-message').html(string);
                        jQuery('#show_alert_popup_btn').trigger('click');
                    }

                },
                error: function (e) {
                    //called when there is an error
                    //console.log(e.message);
                }
            });

        });


        function getMemberFormData()
        {
            var formElement = new FormData();
            formElement.append('first_name',$('#first_name').val());
            formElement.append('last_name',$('#last_name').val());
            formElement.append('email',$('#email').val());
            formElement.append('user_id',$('#user_id').val());
            formElement.append('phone',$('#phone').val());
            formElement.append('address',$('#address').val());
            formElement.append('message',$('#message').val());
            formElement.append('username',$('#username').val());
            formElement.append('password',$('#password').val());
            return formElement;
        }

    });
</script>
<audio id="audio-fail" src="<?php echo base_url() ?>bootstrap/audio/fail.mp3" preload="auto"></audio>
<div class="page-content-wrap bg-light">
    <!-- page content holder -->
    <div class="page-content-holder no-padding">
        <!-- page title -->
        <div class="page-title">
            <h1>User Registration</h1>
            <!-- breadcrumbs -->
            <ul class="breadcrumb">
                <li><a href="<?=base_url(); ?>">Home</a></li>
                <li><a href="javascript:;">User Signup</a></li>
            </ul>
            <!-- ./breadcrumbs -->
        </div>
        <!-- ./page title -->
    </div>
    <!-- ./page content holder -->
</div>
<!-- page content wrapper -->
<div class="page-content-wrap">

    <div class="divider"><div class="box"><span class="fa fa-angle-down"></span></div></div>

    <!-- page content holder -->
    <div class="page-content-holder">

        <div class="row">
            <div class="col-md-7 this-animate" data-animate="fadeInLeft">

                <div class="text-column">
                    <h4>User Signup</h4>
                    <div class="text-column-info">
                        Please fill up basic details
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>First Name <span class="text-hightlight"></span></label>
                            <input type="text" id="first_name" class="form-control"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Last Name <span class="text-hightlight"></span></label>
                            <input type="text" id="last_name" class="form-control"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label style="color:#05bb12; ">User ID<span class="text-hightlight"></span>*</label>
                            <input style="border:1px solid #05bb12;" type="text" id="user_id" class="form-control"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label style="color:#05bb12; ">UserName<span class="text-hightlight"></span>*</label>
                            <input style="border:1px solid #05bb12;" type="text" id="username" class="form-control"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label style="color:#05bb12; ">Password<span class="text-hightlight"></span>*</label>
                            <input style="border:1px solid #05bb12;" type="text" id="password" class="form-control"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Email <span class="text-hightlight"></span></label>
                            <input type="text" id="email" class="form-control"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Phone <span class="text-hightlight"></span></label>
                            <input type="text" id="phone" class="form-control"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Address <span class="text-hightlight"></span></label>
                            <input type="text" id="address" class="form-control"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Message <span class="text-hightlight"></span></label>
                            <textarea class="form-control" id="message" rows="8"></textarea>
                        </div>
                        <button class="btn btn-primary btn-lg pull-right" id="signup_btn">Signup</button>
                        <button style="display: none" id="show_alert_popup_btn" type="button" class="btn btn-danger mb-control" data-box="#message-box-sound-2">Fail</button>

                    </div>
                </div>

            </div>

        </div>

    </div>
    <!-- ./page content holder -->
</div>
<!-- ./page content wrapper -->
<!-- danger with sound -->
<div class="message-box message-box-danger animated fadeIn" data-sound="fail" id="message-box-sound-2">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-times"></span>Error</div>
            <div class="mb-content">
                <p id="error-message"></p>
            </div>
            <div class="mb-footer">
                <button class="btn btn-default btn-lg pull-right mb-control-close">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- end danger with sound -->