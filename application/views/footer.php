
<footer class="wrapper footer-wrapper" style="background: url(<?php echo base_url('bootstrap/images/web/overlay-banner.png') ?>) repeat #22202e;"><!-- footer-wrapper-->

    <div class="container"><!-- container-->

        <div class="contact-details wrapper"><!--contact-details-->

            <div class="row"><!--row-->

                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 address-box detail-box"><!--address-box-->

                    <i>
                        <img alt="Loactaion" src="<?php echo base_url('bootstrap/images/web/ftr-location.png') ?>">
                    </i>
                    <h4 class="white-txt"> address</h4>
                    <p class="white-txt">E44, Design Street, Web Corner</p>
                    <p class="white-txt">Melbourne - 005</p>

                </div><!--/. address-box-->


                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 address-box detail-box"><!--address-box-->

                    <i>
                        <img alt="Loactaion" src="<?php echo base_url('bootstrap/images/web/ftr-phone.png') ?>">
                    </i>
                    <h4 class="white-txt">phone</h4>
                    <p class="white-txt">Mobile: (+1) 800 433 633</p>
                    <p class="white-txt">Toll Free : (+1) 800 123 456</p>

                </div><!--/. address-box-->


                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 address-box detail-box"><!--address-box-->

                    <i>
                        <img alt="Loactaion" src="<?php echo base_url('bootstrap/images/web/ftr-email.png') ?>">
                    </i>
                    <h4 class="white-txt">mail us</h4>
                    <p class="white-txt">info@maxihealth.com</p>
                    <p class="white-txt">support@maxihealth.com</p>

                </div><!--/. address-box-->

            </div><!--/. row-->


        </div><!--/. contact-details-->


        <div class="footer-block wrapper"><!-- footer-block-->

            <div class="row"><!-- row-->

                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"><!-- col-->

                    <div class="wrapper each-footer-block"><!-- each-footer-block-->

                        <a href="#" class="logo-a"><img src="<?php echo base_url('bootstrap/images/web/logo.png') ?>" alt="Logo"/></a>

                        <p>
                            Sed ullamcorper efficitur tortor eu consectetur. Etiam tincidunt nisi quis ex convallis, vitae accumsan massa aliquam.
                            Sed ullamcorper efficitur tortor eu consectetur
                            Etiam tincidunt nisi quis ex convallis, vitae accumsan massa aliquam
                        </p>

                    </div><!-- /. each-footer-block-->

                    <div class="socialmedia-block wrapper"><!-- socialmedia-block -->

                        <ul>

                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                        
                        </ul>

                    </div><!-- /. socialmedia-block -->


                </div><!-- /. col-->

                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"><!-- col-->

                    <div class="wrapper each-footer-block"><!-- each-footer-block-->

                        <h1 class="footer-title">quick links</h1>

                        <ul class="quick-nav-footer">

                            <li><a href="#">home</a></li>
                            <li><a href="#">about us</a></li>
                            <li><a href="#">products</a></li>
                            <li><a href="#">Contact us</a></li>

                        </ul>

                    </div><!-- /. each-footer-block-->


                </div><!-- /. col-->

                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"><!-- col-->

                    <div class="wrapper each-footer-block"><!-- each-footer-block-->

                        <h1 class="footer-title">Business Hours</h1>

                        <div class="time-table-footer"><!--time-table-footer-->

                            <table class="working-hours">
                                <tbody>
                                <tr>
                                    <th>Monday</th>
                                    <td>9am &gt; 5pm</td>
                                </tr>
                                <tr>
                                    <th>Tuesday</th>
                                    <td>9am &gt; 5pm</td>
                                </tr>
                                <tr>
                                    <th>Wendsday</th>
                                    <td>9am &gt; 5pm</td>
                                </tr>
                                <tr>
                                    <th>Thursday</th>
                                    <td>9am &gt; 5pm</td>
                                </tr>
                                <tr>
                                    <th>Friday</th>
                                    <td>Holiday</td>
                                </tr>
                                <tr>
                                    <th>Saturday</th>
                                    <td>9am &gt; 5pm</td>
                                </tr>
                                <tr>
                                    <th>Sunday</th>
                                    <td>9am &gt; 5pm</td>
                                </tr>

                                </tbody>

                            </table>


                        </div>



                    </div><!-- /. each-footer-block-->


                </div><!-- /. col-->

            </div><!--/. row-->

        </div><!-- /. footer-block-->

    </div><!-- /. container-->

</footer><!--/. footer-wrapper-->

</div><!--page-wrapper-->
</body>
</html>