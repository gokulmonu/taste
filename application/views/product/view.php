<script>
    $(function () {

    $("#success_container").iziModal({
        title: 'Cart Status',
        headerColor: '#008000',
        width: 600,
        autoOpen: 0,
        timeout : 3000,
        attached: 'bottom',
        timeoutProgressbar : true,
        fullscreen:false,
        onClosed: function(){
            history.pushState('', document.title, window.location.pathname);

        }
    });

    });
    function addToCart(id)
    {
        var quantity = $('#quantity').val();
         var variantArr = [];
        $("#variant_container").find('select').each(function () {
            var variant = [];
            var tempVariant = {};
            var sectionId = $(this).attr('id');
            var id = sectionId.substring(sectionId.lastIndexOf('_')+1);

            tempVariant['value'] = $('#'+sectionId).val();
            tempVariant['type'] = id;
            variantArr.push(tempVariant);
        });
        $.ajax({
            url: '<?php echo base_url()?>cart/add',
            type: 'POST',
            data: {'data' : JSON.stringify(variantArr),'product_id' :id,'quantity' :quantity },
            success: function (data) {
                if(data.success){
                    $('#success_container').iziModal('open');
                    setTimeout('reload()',5000);

                }else{
                    window.location.href ="<?php echo base_url() ?>"+"login/?redirect=" +data.url;
                }

            },
            error: function (e) {
                //called when there is an error
                //console.log(e.message);
            }
        });
    }

    function reload()
    {
        location.reload();
    }
    function deleteProduct(id)
    {
            if(confirm("Are you sure want to delete this product?")){
                var postData = {};
                postData['product_id'] = id;
                $.ajax({
                    url: '<?php echo base_url()?>product/delete',
                    type: 'POST',
                    data: postData,
                    success: function (data) {
                        window.location.href = '<?php echo base_url(); ?>';
                    },
                    error: function (e) {
                        //called when there is an error
                        //console.log(e.message);
                    }
                });
            }else{
                return false;
            }
    }
</script>

<div class="s-pgwrp"><!--s-pgwrp-->



    <section class="s-wrp s-hi-pad "><!-- section wrp-->
        <div class="s-container"><!-- s-container-->

            <div class="s-wrp"><!--s-wrp-->

                <div class="s-row"><!--f row-->




                    <div class="s-col-lg-12 s-col-md-12 s-col-sm-12 s-col-xs-12"><!--f col-->

        <article class="s-wrp s-hi-pad single-block">

            <div class="s-row"><!--f row-->

                <div class="s-col-lg-5 s-col-md-5 s-col-sm-12 s-col-xs-12"><!--f col-->



                    <div id="sync1" class="owl-carousel"><!--owl-carousel-->
                    <?php if(isset($images) && count($images))
                        foreach ($images as $img){
                        ?>
                            <div class="item">
                                <div class="cover-pic s-wrp">
                                    <span class="full-cvr-baner"><img alt="<?php echo $img['file_name']; ?>" src="<?php echo base_url().'uploads/'.$img['file_name']?>"></span>
                                </div>
                            </div>
                        <?php
                    }

                        ?>

                    </div>



                    <div id="sync2" class="owl-carousel">
                        <?php if(isset($images) && count($images))
                            foreach ($images as $img){
                                $path = base_url();
                                $imgname = substr($img['file_name'],0,strrpos($img['file_name'],'.'));
                                $thumbPath = $path.'uploads/thumbnail/'.$imgname.'_thumb.png';
                                ?>
                                <div class="item">
                                    <span class="thumbnail"><img alt="<?php echo $img['file_name']; ?>" src="<?php echo base_url().'uploads/'.$img['file_name']?>"></span>
                                </div>
                                <?php
                            }

                        ?>
                    </div><!--/. owl-carousel-->


                </div><!--/. f col-->

                <div class="s-col-lg-7 s-col-md-7 s-col-sm-12 s-col-xs-12"><!--f col-->

                    <article class="s-wrp">

                        <article class="s-wrp main-wrp-product-details">

                            <div class="title-wrp">

                                <h4 class="product-title"><?php echo $name?></h4>


                                <ul class="tile-review-list s-lt-pad">

<!--
                                    <li><a><i class="fa fa-bookmark-o" aria-hidden="true"></i>Medium Hinges
                                        </a></li>-->
                                </ul>

                                <ul class="tile-stock-list s-md-pad">
                                    <li class="ui-green"><i class="fa fa-check-circle ui-green"></i>  Available in stock</li>
                                    <!--<li class="ui-red"><i class="fa fa-times-circle ui-red"></i> Not Available in stock</li>
-->
                                </ul>



                            </div>


                            <article class="s-wrp s-hi-pad" id="variant_container">

                                    <?php
                                    if (isset($variant) && count($variant)) {
                                        foreach ($variant as $key => $value) {
                                            ?>
                                            <div class="variant-box">
                                            <label class="ui-green"><?php echo $key ?>:</label>
                                            <?php
                                            if (count($value)) {?>
                                                <select id="<?php echo "variant_".$key ?>">
                                                    <?php
                                                    foreach ($value as $v) {?>
                                                        <option value="<?php echo $v ?>"><?php echo $v ?></option>
                                                        <?php

                                                    }
                                                    ?>
                                                </select>
                                                </div>
                                                <?php

                                            }

                                        }
                                    } ?>
                            </article>

                                <article class="s-wrp s-hi-pad ">

                                <ul class="add-cart-list">

                                    <li>

                                        <input type="number" class="spin-txtbox" id="quantity" min="1" max="100" value="1"/>

                                    </li>
                                    <?php

                                    $isLoggedIn = $this->core_lib->getLoginUserId();
                                    $admin = false;
                                    $userGroup= $this->core_lib->getUserGroup();
                                    if(in_array("admin",$userGroup)){
                                        $admin  = true;
                                    }
                                    if($admin){?>

                                        <li>
                                            <span class="btn-wrp"><a href="javascript:;" class="pg-btn" onclick="deleteProduct('<?php echo $id?>')">DELETE THIS PRODUCT</a></span>
                                        </li>


                                        <?php

                                    }else {?>

                                    <li>
                                        <span class="btn-wrp"><a href="javascript:;" class="pg-btn" onclick="addToCart('<?php echo $id?>')">ADD TO CART</a></span>
                                    </li>

                                    <?php } ?>

                                </ul>


                            </article>


                        </article>





                    </article>

                </div><!--/. f col-->


            </div><!--/. f row-->

        </article>

        <article class="s-wrp tab-block"><!--/. tab-block-->

            <div class="hor_tab">

                <div class="s-col-lg-12 s-col-md-12 s-col-sm-12 s-col-xs-12 "><!--s-col-->
                    <ul class="resp-tabs-list hor_1 s-align-center">
                        <li>DESCRIPTION</li>
                        <li>SPECIFICATIONS</li>

                    </ul>

                </div><!--/. s-col-->
                <div class="resp-tabs-container hor_1">



                    <div class="each_tab_content">

                       <p><?php echo $description ?></p>

                    </div>


                    <div class="each_tab_content">

                        <article class="s-wrp">
                            <div class="s-row"><!--s row-->
                                <div class="s-col-lg-3 s-col-md-3 s-col-sm-3 s-col-xs-5 "><!--s-col-->

                                    <ul class="specific-list-param">
                                        <?php if(isset($specifications) && count($specifications)){
                                            foreach ($specifications as $specs){
                                                ?>
                                                <li><span><?php echo $specs['s_type'] ?></span></li>
                                        <?php
                                            }
                                        }

                                            ?>
                                    </ul>

                                </div><!--/. s-col-->

                                <div class="s-col-lg-9 s-col-md-9 s-col-sm-9 s-col-xs-7 "><!--s-col-->

                                    <ul class="specific-list-soln">
                                        <?php if(isset($specifications) && count($specifications)){
                                            foreach ($specifications as $specs){
                                                ?>
                                                <li><span><?php echo $specs['s_value'] ?></span></li>
                                                <?php
                                            }
                                        }

                                        ?>
                                    </ul>

                                </div><!--/. s-col-->
                            </div><!--/. s row-->
                        </article>

                    </div>

                    </div>





                </div>
            </div>

        </article><!--/. tab-block-->


    </div>
    <!--/. f col-->



                </div>
                <!--/. f row-->

            </div>
            <!--/. s-wrp-->
        </div>
        <!--/. s-container-->
    </section>
    <!--/. section wrp-->
</div>
<div id="success_container">
    <span>Successfully added to cart</span>
</div>