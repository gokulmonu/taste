<script>
    $(function(){
        $('#btn_submit').on('click',function(event){
            event.preventDefault();
            var formData = getFormData();
            $.ajax({
                url: '<?php echo base_url()?>contact-us/validate',
                type: 'POST',
                data:formData,
                async : false,
                cache : false,
                contentType : false,
                processData : false,
                success: function (data) {
                    alert('We have received you queries. Admin will contact you as soon as possible');
                    location.href = '<?= base_url() ?>';
                },
                error: function (e) {
                    //called when there is an error
                    //console.log(e.message);
                }
            });
        });

        function getFormData()
        {
            var formElement = new FormData();
            formElement.append('name',$('#name').val());
            formElement.append('email',$('#email').val());
            formElement.append('message',$('#message').val());
            return formElement;
        }
    });
</script>
<div class="wrapper page-banner-wrapper"><!--page-banner-wrapper-->

<div class="container"><!--container-->

    <div class="wrapper page-banner-block"><!--page-banner-block-->

        <h1>contact us</h1>

        <h2>if you want to make query with us</h2>

    </div><!--/. page-banner-block-->

</div><!--/. container-->

</div><!--/. page-banner-wrapper-->

<div class="wrapper map-area-wrapper"><!--map-area-wrapper-->

    <div id="google-map" class="wrapper">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d126271.88556177283!2d76.85432093707206!3d8.49972669194606!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3b05bbb805bbcd47%3A0x15439fab5c5c81cb!2sThiruvananthapuram%2C+Kerala!5e0!3m2!1sen!2sin!4v1501915050166" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>

</div><!--/. map-area-wrapper-->

<div class="wrapper contact-form-wrapper"><!--contact-form-wrapper-->

     <div class="container"><!--container-->
            
        <div class="wrapper title-grp"><!--title-grp-->

            <h1 class="main-title">Enquire us</h1>

        </div>	<!--/. title-grp-->

        <div class="wrapper contact-form-block"><!--contact-form-block-->

                <div class="row"><!--row-->

                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"><!--col-->

                        <div class="wrapper form-group"><!--form-group-->
                        <input  id="name" type="text" class="form-control" placeholder="Name"/>
                        </div><!--/. form-group-->

                    </div><!--col-->

                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"><!--col-->

                        <div class="wrapper form-group"><!--form-group-->
                        <input  id="email" type="text" class="form-control" placeholder="Email"/>
                        </div><!--/. form-group-->

                    </div><!--col-->

                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"><!--col-->

                        <div class="wrapper form-group"><!--form-group-->
                            <input  id="phone" type="text" class="form-control" placeholder="Phone"/>
                        </div><!--/. form-group-->

                   </div><!--col-->

                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><!--col-->

                        <div class="wrapper form-group"><!--form-group-->
                            <textarea id="message" class="form-control textarea" placeholder="Message"></textarea>
                        </div><!--/. form-group-->

                   </div><!--col-->

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><!--col-->
                        <button id="btn_submit" class="page-btn">Submit</button>
                     </div><!--col-->

                </div><!--/. row-->


        </div> <!--/. contact-form-block-->


     </div><!--/. container-->

</div><!--/. contact-form-wrapper-->