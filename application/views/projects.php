<section class="wrp innerpage-content-wrp"><!--innerpage-content-wrp-->

    <div class="container"><!--container-->

        <div class="wrp projects-wrp"><!--projects-wrp-->

            <h2 class="section-title">our projects</h2>


            <div class="wrp projects-list-wrp"><!--projects-list-wrp-->

                <div class="row"><!--row-->

                    <?php

                    if (isset($projects)) {
                    foreach ($projects as $project) {
                    ?>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12"><!--col-->

                        <div class="wrp each-product"><!--each-product-->

                            <div class="product-box"><!--product box-->

                                <a href="#" class="product-img"><img
                                        src="<?php echo isset($project['thumbnail']) ? $project['thumbnail'] : ""; ?>"
                                        alt="product image"></a>

                                <div class="product-content slideup-content"><!--slideup-content-->

                                    <a href="#" class="area-title"><h4><?= $project['name']; ?></h4></a>

                                    <div class="product-content wrp"><!--product content-->

                                        <p><?= $project['description']; ?></p>

                                    </div><!--/. product content-->

                                </div><!--/. slideup-content-->

                                <div class="product-fix-content wrp"><!--product-fix-content-->

                                    <a href="#" class="area-title"><h4><?= $project['name']; ?></h4></a>

                                    <a class="pg-btn" href="<?php echo base_url() ?>project/view/<?= $project['id']; ?>"><em>read more</em></a>

                                </div><!--/. product-fix-content-->

                            </div><!--/. product box-->

                        </div><!--/. each-product-->


                    </div><!--/. col-->

                <?php }
                }
                ?>
                </div><!--/. row-->

            </div><!--/. projects-list-wrp-->


        </div><!--/. projects-wrp-->

    </div><!--/. container-->

</section><!--/. innerpage-content-wrp-->