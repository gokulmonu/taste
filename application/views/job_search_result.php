<script>
    function openPage(pageNum) {
        var phrase = $("#phrase_btn").val();
        if (phrase) {
            location.href = "<?=base_url() ?>job/search/" + pageNum + "/" + phrase;
        } else {
            location.href = "<?=base_url() ?>job/search/" + pageNum;

        }
    }
</script>
<div id="container" class="list-wrp"><!--Dynamic Loading from Here-->
    <ul class="list">
        <?php $imgArr = ['company-default3.jpg']; ?>
        <?php if(isset($data['rows'])): ?>
            <?php foreach ($data['rows'] as $result): ?>
                <?php shuffle($imgArr); ?>
        <li>
            <div class="search-single col-md-12 col-sm-12 col-xs-12" style="border: 1px #FF00FF">
                <!--Dynamic Loading from Here-->
                <div class="single_job_listing">
                    <!-- style="border:1px solid blue; width:100px; height:100px;" -->
                    <div class="col-md-2 col-sm-12 col-xs-12 img-list-only">
                        <a href="javascript:;" class="view_job_cls" data-content="<?= $result['title'];?>" data-content-id="<?= $result['id'];?>"><img class="img-responsive"
                                src="<?= base_url()."bootstrap/images/web/".$imgArr[0] ?>"
                                alt="#"></a>
                    </div>
                    <div class="col-md-8 col-sm-12 col-xs-12 auto-width">
                        <div class="job-basic-details">
                            <a href="javascript:;" class="view_job_cls" data-content="<?= $result['title'];?>" data-content-id="<?= $result['id'];?>"><h4><?= $result['title'];?></h4></a><!--Job Name -->
                            <h5 style="color:#777777"><?= $result['company_name'];?></h5><!--Company Name -->
                            <h6 style="color:#777777"><?= $result['industry_name'];?></h6><!--Company Name -->
                        </div>
                        <div class="job-desc-wrp">

                            <p> <?= $result['candidate_profile']; ?>
                                <a style="font-size: 14px;font-weight: bold" href="javascript:;" class="view_job_cls" data-content="<?= $result['title'];?>" data-content-id="<?= $result['id'];?>">Read More</a></p>
                        </div>

                        <div class="col-md-2 col-sm-12 col-xs-12 apply_btn">
                            <a href="<?=base_url() ?>job/apply?job_id=<?=$result['id']?>">Apply</a>
                        </div>

                    </div>
                </div>

            </div>
        </li>
                <?php endforeach; ?>
        <?php endif; ?>
        <?php if(empty($data['rows'])): ?>
            <p>No Jobs Found.</p>
        <?php endif; ?>
    </ul>
    <?php
    if(isset($data) && isset($data['pagination'])){
        $dataArr = [
            'pagination'=>$data['pagination']
        ];
        $this->view('pagination',$dataArr);
    }

    ?>
</div>