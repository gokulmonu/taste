<div class="f-container"><!--f container-->

    <section class="f-wrp main-box" style="margin-top: 0"><!--f-wrp-->
        <section class="f-wrp">
            <div class=f-container>
                <div class="row">
                <h3><?= isset($title) ? $title : "" ?></h3>
                    <div class="job-view-header-blk">
                <div class="col-md-2 col-sm-12 col-xs-12 img-list-only">
                    <img class="img-responsive"
                         src="<?= base_url() . "bootstrap/images/web/company-default.png" ?>" ;
                         alt="#">

                </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <?= isset($candidate_profile) ? $candidate_profile : "" ?>
                        </div>
                    </div>
                <div class="col-md-12 col-sm-12 col-xs-12 auto-width ">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                    <p>
                        <span><b>Industry: </b><?= isset($industry_name) ? $industry_name : "" ?></span><br>
                        <span><b>Active: </b>Yes</span><br>
                        <span><b>Tags: </b><?= isset($_tags) ? $_tags : "" ?></span><br>
                        <span><b>Minimum Work Experience: </b><?= isset($min_work_exp) ? $min_work_exp : "" ?>/span><br>
                        <span><b>Maximum Work Experience: </b><?= isset($max_work_exp) ? $max_work_exp : "" ?></span><br>
                        <span><b>Minimum Annual Salary: </b><?= isset($min_annual_ctc) ? $min_annual_ctc : "" ?>
                            (<?= isset($price_type) ? $price_type : "" ?>)</span><br>
                        <span><b>Maximum Annual Salary: </b><?= isset($max_annual_ctc) ? $max_annual_ctc : "" ?>
                            (<?= isset($price_type) ? $price_type : "" ?>)</span><br>
                        <span><b>Other Salary Details: </b><?= isset($other_salary_details) ? $other_salary_details : "" ?></span><br>
                        <span><b>No Of Vacancies: </b><?= isset($no_of_vacancies) ? $no_of_vacancies : "" ?></span><br>
                        <span><b>Job Location: </b><?= isset($location) ? $location : "" ?></span><br>
                        <span><b>Employment Type: </b><?= isset($employment_type) ? $employment_type : "" ?></span><br>
                        <span><b>UG Qualification: </b><?= isset($selected_ug) ? $selected_ug : "" ?></span><br>
                            </p>
                        </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <p>
                        <span><b>PG Qualification: </b><?= isset($selected_pg) ? $selected_pg : "" ?></span><br>
                        <span><b>PHd Qualification: </b><?= isset($selected_phd) ? $selected_phd : "" ?></span><br>
                        <span><b>Company Name: </b><?= isset($company_name) ? $company_name : "" ?></span><br>
                        <span><b>Company Website: </b><?= isset($company_website) ? $company_website : "" ?></span><br>
                        <span><b>Contact Person: </b><?= isset($contact_person) ? $contact_person : "" ?></span><br>
                        <span><b>Contact Email: </b><?= isset($contact_email) ? $contact_email : "" ?></span><br>
                        <span><b>Contact Number: </b><?= isset($contact_number) ? $contact_number : "" ?></span><br>
                        <table>
                            <tr>
                                <td style="width: 130px;">
                                   <b><span style="font-size: 14px;">Candidate Profile:</span> </b>
                                </td>
                                <td>

                                </td>
                            </tr>
                            <tr>
                                <td style="width: 130px;">
                                   <b><span style="font-size: 14px;">About Company:</span> </b>
                                </td>
                                <td>
                                   <?= isset($about_company) ? $about_company : "" ?>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <div class="col-md-2 col-sm-12 col-xs-12 apply_btn">
                                    <a href="<?=base_url() ?>job/apply?job_id=<?=isset($id) ? $id : "" ?>">Apply</a>
                                    </div>

                                </td>
                            </tr>
                        </table>


                    </p>
                        </div>
                </div>
            </div>
            </div>
        </section>
    </section>
</div>