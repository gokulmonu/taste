<style>
    .col-md-12 {
        width: 99%;
    }

    .search-wrp {
        margin-bottom: 10px;
    }

    .search {
        position: relative;
        color: #aaa;
    }

    .search input {
        width: 60%;
        height: 32px;
        border: 1px solid #aaa;
        border-radius: 5px;
    }

    .search input {
        text-indent: 32px;
    }

    .search .fa-search {
        position: absolute;
        top: 9px;
        left: 21px;
    }


</style>
<script>
    var page = 1;
    jQuery(function () {
        var _changeInterval = null;
        $("#phrase_btn").keyup(function() {
            clearInterval(_changeInterval);
            _changeInterval = setInterval(function() {
                searchJobs();
                clearInterval(_changeInterval)
            }, 2000);

        });
        function searchJobs() {
            var phrase = $("#phrase_btn").val();
            if (phrase) {
                location.href = "<?=base_url() ?>job/search/" + page + "/" + phrase;
            } else {
                location.href = "<?=base_url() ?>job/search/" + page;

            }
        }
    });
</script>
<div class="f-container"><!--f container-->
    <section class="f-wrp main-box" style="margin-top: 0"><!--f-wrp-->
        <section class="f-wrp">
            <div class="container">

                <div class="row" style="margin-right: 0">
                    <div class="col-md-12 col-sm-12 col-xs-12">

                        <div class="ndiv search-button-holder">
                            <div class="col-md-4 col-sm-4 col-xs-12 search">
                                <span class="fa fa-search"></span>
                                <input id="phrase_btn" placeholder="Search term" value="<?= isset($phrase)?$phrase:"" ?>">
                            </div>

                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?php $this->view('job_search_result'); ?>
                    </div>

                </div>
        </section>
    </section>
</div>