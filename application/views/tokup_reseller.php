
<script>
    $(function () {
        $("#signup_btn").click(function( event ) {
            event.preventDefault();
            var formData = getMemberFormData();
            $.ajax({
                url: '<?php echo base_url()?>signup/reseller/validate',
                type: 'POST',
                data:  formData,
                contentType : false,
                processData : false,
                success: function (data) {
                    if(data.success){
                        alert('Thank you for your interest in our services .Our admin will contact you as soon as possible.');
                        location.href = '<?php echo base_url()?>';

                    }

                    if(!data.success){
                        for(key in data){
                            $('#'+data[key]['field_id']).html(data[key]['label']);
                        }
                    }

                },
                error: function (e) {
                    //called when there is an error
                    //console.log(e.message);
                }
            });

        });


        function getMemberFormData()
        {
            var formElement = new FormData();
            formElement.append('first_name',$('#first_name').val());
            formElement.append('last_name',$('#last_name').val());
            formElement.append('email',$('#email').val());
            formElement.append('phone',$('#phone').val());
            formElement.append('address',$('#address').val());
            formElement.append('message',$('#message').val());
            return formElement;
        }

    });
</script>

<div class="page-content-wrap bg-light">
    <!-- page content holder -->
    <div class="page-content-holder no-padding">
        <!-- page title -->
        <div class="page-title">
            <h1>Reseller Registration</h1>
            <!-- breadcrumbs -->
            <ul class="breadcrumb">
                <li><a href="<?=base_url(); ?>">Home</a></li>
                <li><a href="javascript:;">Reseller Signup</a></li>
            </ul>
            <!-- ./breadcrumbs -->
        </div>
        <!-- ./page title -->
    </div>
    <!-- ./page content holder -->
</div>
<!-- page content wrapper -->
<div class="page-content-wrap">

    <div class="divider"><div class="box"><span class="fa fa-angle-down"></span></div></div>

    <!-- page content holder -->
    <div class="page-content-holder">

        <div class="row">
            <div class="col-md-7 this-animate" data-animate="fadeInLeft">

                <div class="text-column">
                    <h4>Reseller Signup</h4>
                    <div class="text-column-info">
                        Please fill up basic details
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>First Name <span class="text-hightlight"></span></label>
                            <input type="text" id="first_name" class="form-control"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Last Name <span class="text-hightlight"></span></label>
                            <input type="text" id="last_name" class="form-control"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Email <span class="text-hightlight"></span></label>
                            <input type="text" id="email" class="form-control"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Phone <span class="text-hightlight"></span></label>
                            <input type="text" id="phone" class="form-control"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Address <span class="text-hightlight"></span></label>
                            <input type="text" id="address" class="form-control"/>
                        </div>
                        <div class="form-group">
                            <label>Message <span class="text-hightlight"></span></label>
                            <textarea class="form-control" id="message" rows="8"></textarea>
                        </div>
                        <button class="btn btn-primary btn-lg pull-right" id="signup_btn">Signup</button>
                    </div>
                </div>

            </div>

        </div>

    </div>
    <!-- ./page content holder -->
</div>
<!-- ./page content wrapper -->