<script>

    $(document).ready(function(){

        $('.tooltip-add').attr('data-toggle', 'tooltip');
        $('.tooltip-add').attr('data-placement', 'top');
        $('[data-toggle="tooltip"]').tooltip();


        $(".rateyo-readonly-widg").rateYo({
            rating: 4.2,
            readOnly: true,
            starWidth: "15px"
        });


        $(".each-product-list").hover(
            function () {
                $(this).find(".product-cntrls").removeClass('fadeOut');
                $(this).find(".product-cntrls").addClass('zoomIn');
            },
            function () {
                $(this).find(".product-cntrls").addClass('fadeOut');
                $(this).find(".product-cntrls").removeClass('zoomIn');
            }
        );

        jQuery('.product_btn_cls').on('click', function () {
            var str = jQuery(this).attr('data-content');
            var id = jQuery(this).attr('data-content-id');
            str = str.replace(/\s+/g, '-').toLowerCase();
            location.href = "<?=base_url() ?>category/product/view/" + id + "/" + str
        });

    });


</script>
<?php if(isset($slider['rows'])): ?>
<div class="wrapper slider-wrapper"><!--slider-wrapper-->
    <div class="slider-active owl-carousel">
    <?php foreach ($slider['rows'] as $s): ?>
        <div class="slider-items">
            <img src="<?= base_url() ?>uploads/<?=$s['file_ref'] ?>" alt="" class="slider">
            <div class="slider-content flex-style">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <h2><?= $s['name'] ?></h2>
                            <p><?= $s['description'] ?></p>
                            <ul>
                                <li><a href="<?= base_url() ?>/<?=$s['url'] ?>" class="page-btn">Click Here</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
    </div>

</div>  <!--/. slider-wrapper-->
<?php endif; ?>


<div class="wrapper product-wrapper"><!--product-wrapper-->

    <div class="container"><!--container-->

        <div class="wrapper product-list-block"><!--product-list-block-->

            <div class="wrapper title-grp"><!--title-grp-->

                <h1 class="main-title">Deal of the day</h1>

                <h2 class="subtitle">breads every day</h2>

            </div>	<!--/. title-grp-->


            <div class="wrapper products-listings-area">   <!--products-listings-area-->

                <div class="row">  <!--row-->
                    <?php foreach ($items['rows'] as $t): ?>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 each-product-list"><!--each-product-list-->

                        <div class="wrapper"><!--wrapper-->

                            <div class="row"><!--row-->

                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 product-img-grp"><!--product-img-grp-->
                                    <?php
                                    $isNew = false;
                                    $today = date('Y-m-d');
                                    $prevWeek = date('Y-m-d', strtotime($today . " -5 days"));
                                    if ($t['created'] >= $prevWeek) {
                                        $isNew = true;
                                    }
                                    ?>
                                    <?php if ($isNew): ?>
                                        <span class="product-badge new-badge tooltip-add" title="New Arrival">New</span>
                                    <?php endif; ?>
                                    <div class="wrapper"><!--wrapper-->

                                        <div class="product-imgarea-grp"><!--product-imgarea-grp-->

                                            <span><img src="<?=base_url() ?><?=$t['thumbnail'] ?>" alt="Product"/></span>

                                            <div class="product-cntrls animated fadeOut"><!--product-cntrls-->

                                                <ul>
                                                    <li><a href="javascript:;" data-content-id="<?= $t['id'] ?>"
                                                           data-content="<?= $t['title'] ?>" class="tooltip-add product_btn_cls" title="View Detail"><i class="fa fa-eye"></i></a></li>
                                                   <!-- <li><a href="#" class="tooltip-add" title="Add To Cart"><i class="fa fa-shopping-basket"></i></a></li>
-->
                                                </ul>

                                            </div><!--/. product-cntrls-->


                                        </div><!--/. product-imgarea-grp-->

                                    </div><!--/. wrapper-->

                                </div><!--/. product-img-grp-->

                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 product-list-detail"><!--product-list-detail-->

                                    <div class="wrapper"><!--wrapper-->

                                        <h1 class="main-title"><a class="product_btn_cls" href="javascript:;" data-content-id="<?= $t['id'] ?>"
                                            data-content="<?= $t['title'] ?>"><?= $t['title'] ?></a></h1>
                                        <span class="subtitle"><?= $t['price_description'] ?></span>

                                        <div class="rtng-block wrapper"><!--rtng-block-->
                                            <div class="rateyo-readonly-widg"></div>
                                        </div><!--/. rtng-block-->

                                        <span class="product-price">Rs <?= $t['price'] ?></span>

                                    </div><!--/. wrapper-->

                                </div><!--/. product-list-detail-->

                            </div><!--/. row-->

                        </div><!--/. wrapper-->

                    </div><!--/. each-product-list-->
                    <?php endforeach; ?>
                </div>   <!--/. row-->

            </div>   <!--/. products-listings-area-->

        </div><!--/. product-list-block-->

    </div><!--/. container-->

</div><!--/. product-wrapper-->
<div class="wrapper choose-content-wrp"><!-- choose-content-wrp-->

    <div class="container"><!-- container-->

        <div class="wrapper title-grp"><!--title-grp-->

            <h1 class="main-title">Deal of the day</h1>

            <h2 class="subtitle">breads every day</h2>

        </div>	<!--/. title-grp-->


        <div class="choose-content-block wrapper"><!-- choose-content-block-->

            <div class="row"><!--row-->

                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"><!--col-->

                    <div class="wrapper content-block first-block-chose"><!--content-block-->

                        <div class="wrapper each-choose-content"><!--each-choose-content-->

                            <div class="row"><!--row-->

                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 chose-content-div"><!--col-->
                                    <span>
                                        	Sed ullamcorper efficitur tortor eu consectetur. Etiam tincidunt nisi quis ex convallis, vitae accumsan massa aliquam
                                         </span>
                                </div><!--/. col-->

                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 count-box-wrp"><!--col-->

                                    <span class="choose-count-box">1</span>

                                </div><!--/. col-->


                            </div><!--/. row-->

                        </div><!--/. each-choose-content-->


                        <div class="wrapper each-choose-content"><!--each-choose-content-->

                            <div class="row"><!--row-->

                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 chose-content-div"><!--col-->
                                    <span>
                                        	Sed ullamcorper efficitur tortor eu consectetur. Etiam tincidunt nisi quis ex convallis, vitae accumsan massa aliquam
                                         </span>
                                </div><!--/. col-->

                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 count-box-wrp"><!--col-->

                                    <span class="choose-count-box">2</span>

                                </div><!--/. col-->


                            </div><!--/. row-->

                        </div><!--/. each-choose-content-->


                        <div class="wrapper each-choose-content"><!--each-choose-content-->

                            <div class="row"><!--row-->

                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 chose-content-div"><!--col-->
                                    <span>
                                        	Sed ullamcorper efficitur tortor eu consectetur. Etiam tincidunt nisi quis ex convallis, vitae accumsan massa aliquam
                                         </span>
                                </div><!--/. col-->

                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 count-box-wrp"><!--col-->

                                    <span class="choose-count-box">3</span>

                                </div><!--/. col-->


                            </div><!--/. row-->

                        </div><!--/. each-choose-content-->


                    </div><!--/. content-block-->

                </div><!--/. col-->

                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 chose-img-wrp"><!--col-->

                    <div class="wrapper content-block"><!--content-block-->

                        <span class="chose-sec-image"><img src="<?php echo base_url('bootstrap/images/web/worker.png') ?>" alt="worker image"></span>

                    </div><!--/. content-block-->

                </div><!--/. col-->


                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"><!--col-->

                    <div class="wrapper content-block content-rt-block second-block-chose"><!--content-block-->

                        <div class="wrapper each-choose-content"><!--each-choose-content-->

                            <div class="row"><!--row-->

                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 count-box-wrp"><!--col-->

                                    <span class="choose-count-box">4</span>

                                </div><!--/. col-->


                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 chose-content-div"><!--col-->
                                    <span>
                                        	Sed ullamcorper efficitur tortor eu consectetur. Etiam tincidunt nisi quis ex convallis, vitae accumsan massa aliquam
                                         </span>
                                </div><!--/. col-->




                            </div><!--/. row-->

                        </div><!--/. each-choose-content-->


                        <div class="wrapper each-choose-content"><!--each-choose-content-->

                            <div class="row"><!--row-->

                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 count-box-wrp"><!--col-->

                                    <span class="choose-count-box">5</span>

                                </div><!--/. col-->


                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 chose-content-div"><!--col-->
                                    <span>
                                        	Sed ullamcorper efficitur tortor eu consectetur. Etiam tincidunt nisi quis ex convallis, vitae accumsan massa aliquam
                                         </span>
                                </div><!--/. col-->




                            </div><!--/. row-->

                        </div><!--/. each-choose-content-->


                        <div class="wrapper each-choose-content"><!--each-choose-content-->

                            <div class="row"><!--row-->

                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 count-box-wrp"><!--col-->

                                    <span class="choose-count-box">6</span>

                                </div><!--/. col-->


                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 chose-content-div"><!--col-->
                                    <span>
                                        	Sed ullamcorper efficitur tortor eu consectetur. Etiam tincidunt nisi quis ex convallis, vitae accumsan massa aliquam
                                         </span>
                                </div><!--/. col-->


                            </div><!--/. row-->

                        </div><!--/. each-choose-content-->


                    </div><!--/. content-block-->

                </div><!--/. col-->


            </div><!--/. row-->

        </div><!-- /. choose-content-block-->

    </div><!--/. container-->

</div><!--/. choose-content-wrp-->


