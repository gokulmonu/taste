<section class="wrp innerpage-content-wrp"><!--innerpage-content-wrp-->

    <div class="container"><!--container-->

        <div class="project-detail-wrp wrp"><!--project-detail-wrp-->

            <div class="row"><!--row-->

                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12"><!--col-->

                    <div class="wrp project-detail-block"><!--project-detail-block-->

                        <div class="row"><!--row-->

                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12"><!--col-->

                                <span class="project-main-img"><img src="<?php
                                    if(isset($images) && count($images)){
                                        echo $images[0]['thumbnail'];
                                    }

                                    ?>" alt="Project"/></span>

                            </div><!--/. col-->

                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12"><!--col-->

                                <div class="wrp project-main-contents"><!--project-main-contents-->

                                    <h2 class="section-title"><?= isset($name)?$name:""; ?></h2>

                                    <div class="wrp project-descrip"><!--project-descrip-->
                                        <p><?= isset($description)?$description:""; ?> </p>


                                    </div><!--/. project-descrip-->

                                    <div class="wrp project-sub-section"><!--project-sub-section-->

                                        <div class="owl-carousel projects-sub-slider"><!--projects-sub-slider-->

                                           <?php
                                           if (isset($images)) {
                                               foreach ($images as $image) {
                                                   ?>
                                                   <div class="item"><!--item-->
                                                       <a href="<?php echo $image['thumbnail'] ?>" class="project-sub-img fancybox-effects-d">
                                                           <img src="<?php echo $image['thumbnail'] ?>" alt="Project"/>
                                                       </a>
                                                   </div><!--/. item-->
                                                   <?php
                                               }
                                           }
                                           ?>



                                        </div><!--/. projects-sub-slider-->

                                    </div><!--/. project-sub-section-->

                                </div><!--/. project-main-contents-->

                            </div><!--/. col-->


                        </div><!--/. row-->

                    </div><!--/. project-detail-block-->

                </div><!--/. col-->


            </div><!--/. row-->

        </div><!--/. project-detail-wrp-->

    </div><!--/. container-->

</section><!--/. innerpage-content-wrp-->