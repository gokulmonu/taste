<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Rates</h3>

            <div class="btn-group pull-right">
                <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export
                    Data
                </button>
                <ul class="dropdown-menu">
                    <li><a href="#" onClick="$('#customers2').tableExport({type:'csv',escape:'false'});"><img
                                src='<?= base_url() ?>bootstrap/images/admin/icons/csv.png' width="24"/> CSV</a></li>
                    <li><a href="#" onClick="$('#customers2').tableExport({type:'txt',escape:'false'});"><img
                                src='<?= base_url() ?>bootstrap/images/admin/icons/txt.png' width="24"/> TXT</a></li>
                    <li class="divider"></li>
                    <li><a href="#" onClick="$('#customers2').tableExport({type:'excel',escape:'false'});"><img
                                src='<?= base_url() ?>bootstrap/images/admin/icons/xls.png' width="24"/> XLS</a></li>
                    <li><a href="#" onClick="$('#customers2').tableExport({type:'doc',escape:'false'});"><img
                                src='<?= base_url() ?>bootstrap/images/admin/icons/word.png' width="24"/> Word</a></li>
                    <li><a href="#" onClick="$('#customers2').tableExport({type:'powerpoint',escape:'false'});"><img
                                src='<?= base_url() ?>bootstrap/images/admin/icons/ppt.png' width="24"/> PowerPoint</a>
                    </li>
                    <li class="divider"></li>
                    <li><a href="#" onClick="$('#customers2').tableExport({type:'png',escape:'false'});"><img
                                src='<?= base_url() ?>bootstrap/images/admin/icons/png.png' width="24"/> PNG</a></li>
                    <li><a href="#" onClick="$('#customers2').tableExport({type:'pdf',escape:'false'});"><img
                                src='<?= base_url() ?>bootstrap/images/admin/icons/pdf.png' width="24"/> PDF</a></li>
                </ul>
            </div>

        </div>
        <div class="panel-body">
            <table id="customers2" class="table datatable">
                <thead>
                <tr>
                    <th>Country</th>
                    <th>Name</th>
                    <th>Type</th>
                    <th>Price</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>

                <?php
                if (isset($list) && !empty($list)) {
                    foreach ($list as $value) {
                        ?>
                        <tr>
                            <td>
                                <?= isset($value['country_name'])?$value['country_name']:"" ?></td>
                            <td><?= $value['title'] ?></td>
                            <td><?= $value['type'] ?></td>
                            <td><?= $value['price'] ?></td>
                            <td>
                                <a style="padding: 3px 1px;" class="btn"
                                   href="<?php echo base_url('rate/view') ?>/<?= isset($value['id']) ? $value['id'] : 0 ?>"><i
                                        class="fa fa-eye"></i></a>
                            </td>
                        </tr>

                        <?php
                    }
                }
                ?>


                </tbody>
            </table>

        </div>
    </div>
    <!-- END DATATABLE EXPORT -->
</div>