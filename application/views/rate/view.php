
<div class="page-content-wrap bg-light">
    <!-- page content holder -->
    <div class="page-content-holder no-padding">
        <!-- page title -->
        <div class="page-title">
            <h1>Rate View</h1>
            <!-- breadcrumbs -->
            <ul class="breadcrumb">
                <li><a href="<?=base_url(); ?>">Home</a></li>
                <li><a href="<?=base_url(); ?>rates">Rates</a></li>
                <li><a href="javascript:;">View</a></li>
            </ul>
            <!-- ./breadcrumbs -->
        </div>
        <!-- ./page title -->
    </div>
    <!-- ./page content holder -->
</div>
<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-body">
            <table class="table table-bordered">
                <tr>
                    <th width="20%">
                        Name
                    </th>
                    <td>
                        <?= isset($title)?$title:"" ?>
                    </td>
                </tr>
                <tr>
                    <th width="20%">
                        Active
                    </th>
                    <td>
                        <?= $active==1?"Yes":"No" ?>
                    </td>
                </tr>
                <tr>
                    <th width="20%">
                        Country
                    </th>
                    <td>
                        <img border="1" alt="" src="http://images.nationmaster.com/thumb.php?path=images/nm/flags/<?php echo isset($country_code)?$country_code:""; ?>-flag.gif">
                        <br><br>
                        <?= isset($country_name)?$country_name:"" ?>
                    </td>
                </tr>
                <tr>
                    <th width="20%">
                        Type
                    </th>
                    <td>
                        <?= isset($type)?$type:"" ?>
                    </td>
                </tr>
                <tr>
                    <th width="20%">
                        Price
                    </th>
                    <td>
                        $<?= isset($price)?$price:"" ?>
                    </td>
                </tr>

                <tr>
                    <th width="20%">
                        Description
                    </th>
                    <td>
                        <?= isset($description)?nl2br($description):"" ?>
                    </td>
                </tr>

            </table>
        </div>
    </div>
</div>

