
<script>
    $(function () {
        $("#add_member_form").submit(function( event ) {
            event.preventDefault();
            var formData = getMemberFormData();
            $.ajax({
                url: '<?php echo base_url()?>member/validate',
                type: 'POST',
                data:  formData,
                async : false,
                cache : false,
                contentType : false,
                processData : false,
                success: function (data) {
                    $('#product_name_error').html('');
                    if(data.success){
                        alert('successfully updated');
                        location.reload();

                    }

                    if(!data.success){
                        for(key in data){
                            $('#'+data[key]['field_id']).html(data[key]['label']);
                        }
                    }

                },
                error: function (e) {
                    //called when there is an error
                    //console.log(e.message);
                }
            });

        });


        function getMemberFormData()
        {
            var formElement = new FormData();
            formElement.append('id',$('#id').val());
            formElement.append('member_name',$('#member_name').val());
            formElement.append('member_position',$('#member_position').val());
            formElement.append('member_email',$('#member_email').val());
            formElement.append('member_phone',$('#member_phone').val());
            formElement.append('file',$('#member_photo')[0].files[0]);
            return formElement;
        }

        $("#member_photo").on('change',function(){
            $('#image_container').html('');
        });
    });

    function removeImage(fileId) {
        var postData = {};
        postData['file_id'] = fileId;
        $.ajax({
            url: '<?php echo base_url()?>member/removeImage',
            type: 'POST',
            data: postData,
            success: function (data) {

                location.reload();
            },
            error: function (e) {
                //called when there is an error
                //console.log(e.message);
            }
        });
    }


</script>
<section class="s-wrp"><!-- section wrp-->
    <div class="s-container"><!-- s-container-->

        <div class="s-wrp"><!--s-wrp-->

            <div class="s-row"><!--s row-->

                <div class="s-col-lg-12 s-col-md-12 s-col-sm-12 s-col-xs-12"><!--s col-->


                    <form id="add_member_form" class="custom-form" enctype="multipart/form-data" accept-charset="utf-8">
                        <div class="s-form-group">
                            <input type="hidden" id="id" value="<?php if(isset($id))echo $id; ?>">
                            <label for="member_name">Name:</label>
                            <input type="text" id="member_name" name="member_name" placeholder="Enter Name" value="<?php if(isset($name))echo $name; ?>"/>
                            <span class="error-msg" id="member_name_error"></span>
                        </div>
                        <div class="s-form-group">
                            <label for="member_position">Position:</label>
                            <input type="text" id="member_position" name="member_position" placeholder="Enter position" value="<?php if(isset($position))echo $position; ?>"/>
                            <span class="error-msg" id="member_position_error"></span>
                        </div>
                        <div class="s-form-group">
                            <label for="member_email">Email:</label>
                            <input type="text" id="member_email" name="member_email" placeholder="Enter Email" value="<?php if(isset($email))echo $email; ?>" />
                            <span class="error-msg" id="member_email_error"></span>
                        </div>
                        <div class="s-form-group">
                            <label for="member_phone">Phone:</label>
                            <input type="text" id="member_phone" name="member_phone" placeholder="Enter Phone Number" value="<?php if(isset($phone))echo $phone; ?> "/>
                            <span class="error-msg" id="member_phone_error"></span>
                        </div>
                        <div class="s-form-group">
                            <label for="member_photo">Image:</label>
                            <input type="file" id="member_photo" name="member_photo"/>
                            <span class="error-msg" id="member_photo_error"></span>
                        </div>
                        <div class="s-wrp s-in">
                            <ul class="image-gallery s-wrp" id="image_container">
                                <?php
                                if(isset($images)){

                                    $counter = 1;
                                    foreach ($images as $value){
                                        ?>


                                        <li>
                                            <a><img src="<?php

                                                $path = base_url();
                                                $name = substr($value['file_name'],0,strrpos($value['file_name'],'.'));
                                                $ext = substr($value['file_name'],strrpos($value['file_name'],'.'));
                                                $thumbPath = $path.'uploads/thumbnail/'.$name.'_thumb'.$ext;

                                                echo $thumbPath ?>"></a>
                                            <h6 class="s-txt-center"><?php if(strlen($value['file_name'])>15) {
                                                  echo substr($value['file_name'], 0, 15) . "..";
                                                        }else{
                                                    echo $value['file_name'];
                                                } ?></h6>
                                            <ul class="img-btns">
                                                <li><a onclick="removeImage('<?php echo $value['id'] ?>')"><i class="fa fa-close"></i> </a></li>
                                            </ul>
                                        </li>

                                        <?php
                                        $counter ++;
                                    }
                                }
                                ?>
                            </ul>
                        </div>

                        <div class="s-form-group s-wrp">
                            <input type="submit" class="pg-btn" value="Save">
                            <a class="pg-btn" href="<?php echo base_url() ?>">Cancel</a>
                        </div>

                    </form>


                </div>
                <!--/. s col-->

            </div>
            <!--/. s row-->

        </div>
        <!--/. s-wrp-->
    </div>
    <!--/. s-container-->
</section>