<!DOCTYPE html>
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Taste Of The World</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" sizes="32x32" href="fav.png">

    <!--stylesheets-->

    <link
        href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url() ?>bootstrap/taste/js/bootstrap/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>bootstrap/taste/css/font-awesome.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>bootstrap/taste/css/animate.css"/>

    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>bootstrap/taste/js/nav/nav.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url() ?>bootstrap/taste/js/owl-carousel/owl.carousel.min.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url() ?>bootstrap/taste/js/star-rating/jquery.rateyo.css"/>
<link rel="stylesheet" type="text/css"
          href="<?php echo base_url() ?>bootstrap/taste/js/flex-slider/flexslider.css"/>


    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>bootstrap/taste/css/reset.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>bootstrap/taste/css/custom.css"/>


    <script type="text/javascript" src="<?php echo base_url() ?>bootstrap/taste/js/jquery-3.3.1.slim.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>bootstrap/taste/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>bootstrap/taste/js/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript"
            src='<?php echo base_url() ?>bootstrap/taste/js/nav/jquery-accessibleMegaMenu.js'></script>
    <script type="text/javascript" src="<?php echo base_url() ?>bootstrap/taste/js/nav/mobile_menu.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>bootstrap/taste/js/nav/nav-script.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>bootstrap/taste/js/wow.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>bootstrap/taste/js/flex-slider/jquery.flexslider.js"></script>
    <script type="text/javascript"
            src="<?php echo base_url() ?>bootstrap/taste/js/owl-carousel/owl.carousel.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>bootstrap/taste/js/owl-carousel/owl-script.js"></script>
    <script type="text/javascript"
            src="<?php echo base_url() ?>bootstrap/taste/js/star-rating/jquery.rateyo.js"></script>
    <script>
        jQuery(function () {
            jQuery('.menu_cls').on('click', function () {
                var str = jQuery(this).attr('data-content');
                var id = jQuery(this).attr('data-content-id');
                str = str.replace(/\s+/g, '-').toLowerCase();
                location.href = "<?=base_url() ?>category/view/" + id + "/" + str;
            });

        });
    </script>
</head>

<body>
<div class="page-wrapper wrapper"><!--page-wrapper-->
    <header class="wrapper header-wrapper"><!--header-wrapper-->

        <div class="wrapper top-bar-wrapper"><!--top-bar-wrapper-->

            <div class="container"><!--container-->

                <div class="wrapper top-bar-block"><!--top-bar-block-->

                    <div class="row"><!--row-->

                        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-xs-12"><!--col-->

                            <a href="#" class="logo-a"><img
                                    src="<?php echo base_url('bootstrap/images/web/logo.png') ?>" alt="Logo"/></a>

                        </div><!--/. col-->

                        <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-xs-12 quick-cnt-head"><!--col-->

                            <div class="wrapper quick-contact-block"><!--quick-contact-block-->

                                <ul>

                                    <li><span class="tp-addr">184 Collins Street West<br>
                                            Victoria, United States, 8007</span></li>

                                    <li><span class="tp-phone">(1800) 123 4568<br>
                                            (1800) 123 4568</span></li>

                                    <li><span class="tp-mail">info@Orenburg.com <br>
                                                support@Orenburg.com</span></li>

                                </ul>

                            </div><!--/. quick-contact-block-->

                        </div><!--/. col-->


                    </div>     <!--/. row-->

                </div>   <!--/. top-bar-block-->

            </div><!--/. container-->

        </div><!--/. top-bar-wrapper-->

        <div class="wrapper navigation-wrapper"
             style="background: url(<?php echo base_url('bootstrap/images/web/overlay-banner.png') ?>) repeat #242a2d;">
            <!--navigation-wrapper-->

            <div class="container"><!--container-->

                <div class="wrapper navigation-block"><!--navigation-block-->

                    <nav class="main-menu" role="navigation">
                        <ul class="nav-menu">
                            <li class="nav-item">
                                <a href="<?= base_url(); ?>" class="hover">Home</a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url(); ?>about-us" class="hover">about us</a>
                            </li>

                         <!--   <li class="nav-item">
                                <a href="#" class="hover">products</a>
                            </li>-->

                            <li class="nav-item">
                                <?php
                                $resultCategory = [];
                                $categories = $this->core_lib->getMenu();
                                if ($categories) {
                                    $resultCategory = array_chunk($categories, 5);
                                }
                                ?>
                                <a href="#" aria-expanded="false" class=""><span class=""><i class="icon-user"></i>categories</span></a>
                                <div class="sub-nav full padding" role="group" aria-expanded="false" aria-hidden="true">
                                    <div class="row">
                                        <?php if (isset($resultCategory)): ?>
                                            <?php foreach ($resultCategory as $cat): ?>
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <ul class="sub-nav-group sub-nav-grey">
                                                        <?php foreach ($cat as $c): ?>
                                                            <li><a href="javascript:;" class="menu_cls"
                                                                   data-content="<?= $c['name']; ?>"
                                                                   data-content-id="<?= $c['id']; ?>"><span><?= $c['name']; ?></span></a>
                                                            </li>
                                                        <?php endforeach; ?>
                                                    </ul>
                                                </div>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </div>

                                </div>
                            </li>


                            <li class="nav-item">
                                <a href="<?=base_url() ?>contact-us" class=""><span class=""><i class="icon-call-in"></i> Contact Us</span></a>
                            </li>

                        </ul>
                    </nav>


                    <!-- mobile menu begin -->
                    <div class="mobile-menu">
                        <nav>
                            <div class="mobile-menu-button">
                                MENU
                                <a href="#" class="mobile-menu-toggler"><span></span><span></span><span></span></a>
                            </div>
                            <div class="mobile-menu-body">
                                <div class="mobile-menu-search clearfix">
                                    <form>
                                        <div class="form-group wrapper">
                                            <input type="text" class="form-control" placeholder="Search"/>
                                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i>
                                            </button>
                                        </div>

                                    </form>
                                </div>
                                <ul>
                                    <li><a href="<?= base_url(); ?>">Home</a></li>
                                    <li><a href="<?= base_url(); ?>about-us">about us</a></li>
                                    <!--<li><a href="#">products</a></li>-->
                                    <li>
                                        <a href="#"><i class="icon-user"></i> categories</a>
                                        <a class="submenu-toggler" href="#"><i class="fa fa-angle-down"></i></a>
                                        <ul class="sub-mob-menu">
                                            <?php if (isset($resultCategory)): ?>
                                                <?php foreach ($resultCategory as $cat): ?>

                                                    <?php foreach ($cat as $c): ?>
                                                        <li><a href="javascript:;" class="menu_cls"
                                                               data-content="<?= $c['name']; ?>"
                                                               data-content-id="<?= $c['id']; ?>"><span><?= $c['name']; ?></span></a>
                                                        </li>
                                                    <?php endforeach; ?>

                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </ul>
                                    </li>

                                    <li><a href="<?= base_url(); ?>contact-us">Contact Us</a></li>

                                </ul>

                            </div>
                        </nav>
                    </div>
                    <!-- mobile menu end -->


                </div><!--/. navigation-block-->

            </div><!--/. container-->


        </div><!--/. navigation-wrapper-->


    </header><!--/. header-wrapper-->
