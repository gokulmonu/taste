<?php if (isset($pagination) && count($pagination) > 1):?>
<div class="wrapper pagination-block">
    <ul class="">
        <?php
        if (isset($pagination) && count($pagination) > 1) {
            $loopCount = count($pagination);
            if ($pagination[0]['text'] != "First") {
                ?>
                <li class="disabled"><a href="#">First</a></li>
                <?php
            }
            foreach ($pagination as $value) {
                ?>

                <li <?php if ($value['current_page']){ ?>class="active" <?php

                } ?>>
                    <a href="javascript:;"
                       onclick="openPage('<?php echo $value['page_num'] ?>')"><?php echo $value['text'] ?></a>
                </li>
                <?php
            }
            if ($pagination[$loopCount - 1]['text'] != 'Last') {
                ?>
                <li class="disabled"><a href="#">Last</a></li>
                <?php

            }
        }
        ?>
    </ul>
</div>
<?php endif ?>