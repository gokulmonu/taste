<div class="wrapper page-banner-wrapper"><!--page-banner-wrapper-->

            <div class="container"><!--container-->

                <div class="wrapper page-banner-block"><!--page-banner-block-->

                    <h1>about us</h1>

                    <h2>Take a look at our bakery and discover how we work</h2>

                </div><!--/. page-banner-block-->

            </div><!--/. container-->

        </div><!--/. page-banner-wrapper-->


        <div class="wrapper inner-wrapper about-us-wrapper"><!--about-us-wrapper-->

            <div class="container"><!--container-->

                <div class="wrapper aboutus-block"><!--aboutus-block-->

                    <div class="row"><!--row-->

                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12"><!--col-->

                            <span class="abtus-img">
                                <img src="<?php echo base_url('bootstrap/images/web/abtus.png') ?>" alt="business"/>
                            </span>

                        </div><!--/. col-->

                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12"><!--col-->

                            <div class="wrapper abtus-contentarea"><!--abtus-contentarea-->

                                <div class="wrapper title-grp"><!--title-grp-->
                                    <h1 class="main-title">our business</h1>
                                </div><!--/. title-grp-->

                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>

                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>

                            </div><!--/. abtus-contentarea-->

                        </div><!--/. col-->

                    </div><!--/. row-->


                </div><!--/. aboutus-block-->


            </div><!--/. containerr-->
        
        </div><!--/. about-us-wrapper-->


<div class="wrapper inner-wrapper who-weare-wrapper"><!--who-weare-wrapper-->

<div class="container"><!--container-->

    <div class="wrapper who-weare-block"><!--who-weare-block-->

        <div class="row"><!--row-->

            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12"><!--col-->

                <div class="wrapper who-weare-contentarea"><!--who-weare-contentarea-->

                    <div class="wrapper title-grp"><!--title-grp-->
                        <h1 class="main-title">who we are</h1>
                    </div><!--/. title-grp-->

                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>

                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>

                </div><!--/. abtus-contentarea-->

            </div><!--/. col-->

             <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12"><!--col-->

                <span class="who-weare-img">
                    <img src="<?php echo base_url('bootstrap/images/web/whowe-are.jpg') ?>" alt="who we are"/>
                </span>

                </div><!--/. col-->

        </div><!--/. row-->


    </div><!--/. who-weare-block-->


</div><!--/. container-->

</div><!--/. who-weare-wrapper-->


<div class="testimonial-wrapper wrapper"><!-- testimonial-wrapper-->

    <div class="container"><!-- container-->

        <div class="wrapper testimonials-block"><!-- testimonials-block-->

            <div class="wrapper title-grp"><!--title-grp-->

                <h1 class="main-title">Clients Testimonials</h1>

                <h2 class="subtitle">did you know that</h2>

            </div>	<!--/. title-grp-->

            <div class="testimonials-slider wrapper"><!--testimonials-slider-->

                <div class="owl-carousel testi-slider"><!--testi-slider-->

                    <div class="item"><!--item-->

                        <div class="f-wrp each-testi-block"><!--each-testi-block-->

                            <p>We just wanted to thank you for the beautiful cake you created for our wedding. It was simply delicious and meticulously decorated. 
                                You made the process easy, and put us at ease. You are a true professional and a talented baker and we are forever thankful for helping 
                                our dream wedding come true.</p>

                            <h1>William Hence</h1>
                            <span>Business Owner</span>

                        </div><!--/. each-testi-block-->

                    </div><!--/. item-->

                    <div class="item"><!--item-->

                        <div class="f-wrp each-testi-block"><!--each-testi-block-->

                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                Elit pellentesque habitant morbi tristique senectus et netus et. 
                                Mauris vitae ultricies leo integer malesuada nunc vel risus. Eu mi bibendum neque egestas congue quisque egestas.</p>

                            <h1>Richard Brooke</h1>
                            <span>Marketing Manager</span>

                        </div><!--/. each-testi-block-->

                        </div><!--/. item-->

                </div><!--/. testi-slider-->


            </div><!--/. testimonials-slider-->


        </div><!-- /. testimonials-block-->

    </div><!--/. container-->


</div><!--/. testimonial-wrapper-->