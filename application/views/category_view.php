<script>
    jQuery(function () {
        jQuery('.product_btn_cls').on('click', function () {
            var str = jQuery(this).attr('data-content');
            var id = jQuery(this).attr('data-content-id');
            str = str.replace(/\s+/g, '-').toLowerCase();
            location.href = "<?=base_url() ?>category/product/view/" + id + "/" + str
        });

        jQuery(".each-product-list").hover(
            function () {
                $(this).find(".product-cntrls").removeClass('fadeOut');
                $(this).find(".product-cntrls").addClass('zoomIn');
            },
            function () {
                $(this).find(".product-cntrls").addClass('fadeOut');
                $(this).find(".product-cntrls").removeClass('zoomIn');
            }
        );
    });
    function openPage(pageNum) {
        var phrase = $("#phrase_btn").val();
        location.href = "<?=base_url() ?>category/view/search/<?=$data['id']?>/" + pageNum + "/<?=$data['name']?>?phrase=" + phrase;

    }
</script>
<div class="wrapper page-banner-wrapper"><!--page-banner-wrapper-->

    <div class="container"><!--container-->

        <div class="wrapper page-banner-block"><!--page-banner-block-->

            <h1>products</h1>
            <h2 class="subtitle">Browse through our freshly baked range</h2>

        </div><!--/. page-banner-block-->

    </div><!--/. container-->

</div><!--/. page-banner-wrapper-->

<div class="wrapper products-pg product-wrapper"><!--product-wrapper-->

    <div class="container"><!--container-->

        <div class="wrapper product-list-block"><!--product-list-block-->


            <div class="wrapper title-grp"><!--title-grp-->

                <h1 class="main-title">our products</h1>

                <h2 class="subtitle">Our main products we provide</h2>

            </div>	<!--/. title-grp--> 

            <div class="search-area-block"><!--search-area-block-->

                <input type="text" class="form-control search-txtbox" id="phrase_btn" placeholder="Search Products" value="<?= isset($phrase) ? $phrase : ""; ?>"/>
                <button type="submit" class="search-submit" id="phrase_btn_submit" onclick="openPage(1)"><i class="fa fa-search" aria-hidden="true"></i></button>

            </div>   <!--/. search-area-block--> 
            
            <div class="wrapper products-listings-area">   <!--products-listings-area-->

                <div class="row">  <!--row-->
                    <?php foreach ($items['rows'] as $t): ?>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 each-product-list"><!--each-product-list-->

                            <div class="wrapper"><!--wrapper-->

                                <div class="row"><!--row-->

                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 product-img-grp">
                                        <!--product-img-grp-->
                                        <?php
                                        $isNew = false;
                                        $today = date('Y-m-d');
                                        $prevWeek = date('Y-m-d', strtotime($today . " -5 days"));
                                        if ($t['created'] >= $prevWeek) {
                                            $isNew = true;
                                        }
                                        ?>
                                        <?php if ($isNew): ?>
                                            <span class="product-badge new-badge tooltip-add"
                                                  title="New Arrival">New</span>
                                        <?php endif; ?>
                                        <div class="wrapper"><!--wrapper-->

                                            <div class="product-imgarea-grp"><!--product-imgarea-grp-->

                                                <span><img src="<?= base_url() ?><?= $t['thumbnail'] ?>" alt="Product"/></span>

                                                <div class="product-cntrls animated fadeOut"><!--product-cntrls-->

                                                    <ul>
                                                        <li><a href="javascript:;" data-content-id="<?= $t['id'] ?>"
                                                               data-content="<?= $t['title'] ?>"
                                                               class="tooltip-add product_btn_cls"
                                                               title="View Detail"><i class="fa fa-eye"></i></a></li>
                                                        <!-- <li><a href="#" class="tooltip-add" title="Add To Cart"><i class="fa fa-shopping-basket"></i></a></li>
     -->
                                                    </ul>

                                                </div><!--/. product-cntrls-->


                                            </div><!--/. product-imgarea-grp-->

                                        </div><!--/. wrapper-->

                                    </div><!--/. product-img-grp-->

                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 product-list-detail">
                                        <!--product-list-detail-->

                                        <div class="wrapper"><!--wrapper-->

                                            <h1 class="main-title"><a data-content-id="<?= $t['id'] ?>"
                                                                      data-content="<?= $t['title'] ?>"
                                                                      class="product_btn_cls"
                                                                      href="javascript:;"><?= $t['title'] ?></a></h1>
                                            <span class="subtitle"><?= $t['price_description'] ?></span>

                                            <div class="rtng-block wrapper"><!--rtng-block-->
                                                <div class="rateyo-readonly-widg"></div>
                                            </div><!--/. rtng-block-->

                                            <span class="product-price">Rs <?= $t['price'] ?></span>

                                        </div><!--/. wrapper-->

                                    </div><!--/. product-list-detail-->

                                </div><!--/. row-->

                            </div><!--/. wrapper-->

                        </div><!--/. each-product-list-->
                    <?php endforeach; ?>
                </div>   <!--/. row-->
                <?php if (empty($items['rows'])): ?>
                    <p>No items found</p>
                <?php endif; ?>
            </div>   <!--/. products-listings-area-->


        </div><!--/. product-list-block-->
        <?php
        if (isset($items) && isset($items['pagination'])) {
            $dataArr = [
                'pagination' => $items['pagination']
            ];
            $this->view('pagination', $dataArr);
        }

        ?>
    </div>


</div><!--/. container-->

