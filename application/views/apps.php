<div class="page-content-wrap bg-light">
    <!-- page content holder -->
    <div class="page-content-holder no-padding">
        <!-- page title -->
        <div class="page-title">
            <h1>Apps Listing</h1>
            <!-- breadcrumbs -->
            <ul class="breadcrumb">
                <li><a href="<?=base_url(); ?>">Home</a></li>
                <li><a href="javascript:;">Apps</a></li>
            </ul>
            <!-- ./breadcrumbs -->
        </div>
        <!-- ./page title -->
    </div>
    <!-- ./page content holder -->
</div>
<!-- page content wrapper -->
<div class="page-content-wrap">
    <!-- page content holder -->
    <div class="page-content-holder padding-v-30">

        <div class="block-heading this-animate" data-animate="fadeInLeft">
            <div class="block-heading-text">
            </div>
        </div>

        <div class="row">
            <div class="col-md-9">

                <div class="row">
                    <?php

                    if (isset($projects)) {
                    foreach ($projects as $project) {
                    ?>
                    <div class="col-md-6">

                        <div class="blog-item this-animate" data-animate="fadeInUp">
                            <div class="blog-media">
                                <img src="<?php echo isset($project['thumbnail']) ? $project['thumbnail'] : ""; ?>" class="img-responsive"/>
                            </div>
                            <div class="blog-data">
                                <h5><a href="<?php echo base_url() ?>app/view/<?= $project['id']; ?>"><?= $project['name']; ?></a></h5>
                                <p><?php
                                    $output = strip_tags($project['description']);
                                    if(strlen($output)>110){
                                        echo substr($output,0,110);
                                        echo "...";
                                    }else{
                                        echo $output;
                                    }
                                    ?>
                                </p>
                            </div>
                        </div>

                    </div>
                    <?php }
                    }
                    ?>
                </div>

            </div>

        </div>

    </div>
    <!-- ./page content holder -->
</div>
<!-- ./page content wrapper -->