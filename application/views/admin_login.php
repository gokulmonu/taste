<script type="text/javascript" src=" <?php echo base_url() ?>bootstrap/js/admin/plugins/jquery/jquery.min.js"></script>

<script>
	function validateLogin () {
		var username = $('#username').val();
		var password = $('#password').val();
		var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/rn/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}

// Encode the String
		var encodedString = btoa(password);
		$.ajax({
			url: '<?php echo base_url()?>login/validate',
			type: 'POST',
			data:{
				'username':username,
				'password':encodedString

			},
			success: function (data) {
				$('#username_error').html('');
				$('#password_error').html('');
				if(!data.success){
					for(key in data){
						$('#'+data[key]['field_id']).html(data[key]['label']);
					}
				}
				if(typeof  data.url != "undefined"){
					if(data.url.success){

						window.location.href ='<?php if(isset($redirect)){
                        echo base64_decode($redirect);
                        }else{
                        echo base_url();
                        } ?>';

					}else{
						alert('Username or password does not match');
					}
				}


			},
			error: function (e) {
				//called when there is an error
				//console.log(e.message);
			}
		});
	}
	function openPage(pagenum) {
		currentPage =pagenum;
		search();
	}
</script>
<?php
$this->view('message/header');
?>
<!DOCTYPE html>
<html lang="en" class="body-full-height">
<head>
	<!-- META SECTION -->
	<title>Atlant - Responsive Bootstrap Admin Template</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<link rel="icon" href="favicon.ico" type="image/x-icon" />
	<!-- END META SECTION -->

	<!-- CSS INCLUDE -->
	<link rel="stylesheet" type="text/css" id="theme" href="css/theme-default.css"/>
	<!-- EOF CSS INCLUDE -->
</head>
<body>

<div class="login-container">

	<div class="login-box animated fadeInDown">
		<div class="login-logo"></div>
		<div class="login-body">
			<div class="login-title"><strong>Welcome</strong>, Please login</div>
			<form action="javascript:;" class="form-horizontal">
				<div class="form-group">
					<div class="col-md-12">
						<input type="text" class="form-control" placeholder="Username" id="username"/>
						<span style="color: white" class="error-msg" id="username_error"></span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<input type="password" class="form-control" placeholder="Password" id="password"/>
						<span style="color: white" class="error-msg" id="password_error"></span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-6">

					</div>
					<div class="col-md-6">
						<button class="btn btn-info btn-block" onclick="validateLogin()">Log In</button>
					</div>
				</div>
			</form>
		</div>
		<div class="login-footer">
			<div class="pull-left">
				&copy; 2014 Tokup
			</div>
			<div class="pull-right">
			</div>
		</div>
	</div>

</div>

</body>
</html>
