<section class="wrp innerpage-content-wrp"><!--innerpage-content-wrp-->

<div class="container"><!--container-->

    <div class="careers-detail-wrp wrp"><!--careers-detail-wrp-->

        <div class="wrp careers-detail-block"><!--careers-detail-block-->

            <div class="wrp careers-main-contents"><!--careers-main-contents-->

                <h2 class="section-title">careers</h2>

                <div class="wrp careers-descrip"><!--careers-descrip-->
                    <p>Offers high quality and professional IT solutions and services to meet the evolving needs of businesses across the globe.</p>

                    <p>We use PHP Language in all the web development because PHP is a open source server side scripting language which can be easily embedded into the HTML and CSS. PHP installation and configuration is very easy so it doesn’t make any problem in the PHP Development. </p>

                    <p>PHP provides a high security which doesn’t require big changes to be done during the project development that why it is chosen by the many developers and the companies.  PHP can run on all platforms of the operating system and it gives you the feature that you can deploy the coding of one operating system to another also which doesn’t make any changes to the project.</p>

                    <div class="sub-section-area wrp"><!--sub-section-area-->

                        <h3 class="subtitle">Currently we have openings for </h3>

                        <ul class="sublist-block">
                            <?php
                            if(isset($rows) && count($rows)) {
                                foreach ($rows as $values) {
                                    ?>
                                    <li>
                                        <span><?= $values['title'] ?>	<?= $values['experience'] ?> years
                                        <br>
                                            <p><?= $values['description'] ?></p>
                                        </span>
                                    </li>
                                    <?php
                                }
                            }
                            ?>

                        </ul>

                    </div><!--/. sub-section-area-->

                    <p>Send us your CV to <span class="mail-link">ezoneinfotvm@gmail.com</span></p>



                </div><!--/. careers-descrip-->

            </div><!--/. careers-main-contents-->

        </div><!--/. careers-detail-block-->


    </div><!--/. careers-detail-wrp-->

  </div><!--/. container-->

</section><!--/. innerpage-content-wrp-->