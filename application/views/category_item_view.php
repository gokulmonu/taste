<script>

    $(document).ready(function(){

        $('.tooltip-add').attr('data-toggle', 'tooltip');
        $('.tooltip-add').attr('data-placement', 'top');
        $('[data-toggle="tooltip"]').tooltip();


        $(".rateyo-readonly-widg").rateYo({
            rating: 4.2,
            readOnly: true,
            starWidth: "15px"
        });

        $(window).load(function(){
            $('#carousel').flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: false,
                slideshow: false,
                itemWidth: 210,
                itemMargin: 5,
                asNavFor: '#slider'
            });

            $('#slider').flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: false,
                slideshow: false,
                sync: "#carousel",
                start: function(slider){
                    $('body').removeClass('loading');
                }
            });
        });


    });


</script>
<div class="wrapper page-banner-wrapper"><!--page-banner-wrapper-->

    <div class="container"><!--container-->

        <div class="wrapper page-banner-block"><!--page-banner-block-->

            <h1>product detail</h1>

        </div><!--/. page-banner-block-->

    </div><!--/. container-->

</div><!--/. page-banner-wrapper-->

<div class="wrapper product-detail-wrapper product-wrapper"><!--product-wrapper-->

    <div class="container"><!--container-->

        <div class="wrapper product-detail-block"><!--product-detail-block-->

            <div class="row"><!--row-->

                <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 product-img-slider"><!--product-img-slider-->

                    <div class="wrapper"><!--wrapper-->

                        <div id="slider" class="flexslider">

                            <ul class="slides">
                                <?php
                                if (isset($images) && count($images)) {
                                    foreach ($images as $value) {
                                        ?>

                                        <li>
                                            <img src="<?= base_url() ?><?php echo $value['thumbnail'] ?>"/>
                                        </li>
                                        <?php
                                    }
                                }

                                ?>
                            </ul>
                        </div>
                        <div id="carousel" class="flexslider">
                            <ul class="slides">
                                <?php
                                if (isset($images) && count($images)) {
                                    foreach ($images as $value) {
                                        ?>

                                        <li>
                                            <img src="<?= base_url() ?><?php echo $value['thumbnail'] ?>"/>
                                        </li>
                                        <?php
                                    }
                                }

                                ?>
                            </ul>
                        </div>


                    </div><!--/. wrapper-->

                </div><!--/. product-img-slider-->


                <div class="col-lg-7 col-md-6 col-sm-12 col-xs-12 product-detail-contents"><!--product-detail-contents-->

                    <div class="wrapper"><!--wrapper-->

                        <h1 class="main-title"><?php echo $title ?></h1>
                        <span class="subtitle"><?php echo $price_description ?></span>

                        <div class="rtng-block wrapper"><!--rtng-block-->
                            <div class="rateyo-readonly-widg"></div>
                        </div><!--/. rtng-block-->

                        <div class="wrapper product-price-full"><!--product-price-full-->
                            <?php
                            $per = $price*12/100;
                            $dummyPrice = $per+$price;
                            ?>

                            <h3><em>Rs <?= $dummyPrice ?></em> <span>Rs <?php echo $price ?></span></h3>

                        </div><!--/. product-price-full-->

                        <div class="wrapper product-description"><!--product-description-->
                            <?=$description ?>
                        </div><!--/. product-description-->

                        <div class="wrapper stock-metas"><!--stock-metas-->

                            <ul>

                                <li><label>availability : </label> <span class="in-stock">in stock</span></li>

                                <li><label>category : </label>

                                    <ul class="curnt-category-list">
                                        <li><a href="javascript:;"><?= isset($category_name)?$category_name:"" ?></a></li>
                                    </ul>

                                </li>
                                <li><label>Tags : </label>

                                    <ul class="curnt-category-list">
                                        <li><a href="javascript:;"><?php echo isset($_tags) ? $_tags : ""; ?></a></li>
                                    </ul>

                                </li>
                            </ul>

                        </div><!--/. stock-metas-->

                        <div class="wrapper quantity-contrl"><!--quantity-contrl-->

                            <div class="row"><!--row-->

                              <!--  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

                                    <div class="form-group wrapper">

                                        <form>

                                            <div class="qty-cntrl-block">
                                                <input type='button' value='-' class='qty-c qtyminus' field='quantity' />
                                                <input type='text' class="form-control qty-txt" name='quantity' value='0' class='qty' />
                                                <input type='button' value='+' class='qty-c qtyplus' field='quantity' />
                                            </div>


                                        </form>

                                    </div>

                                </div>-->
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"><!--col-->

                                    <button type="submit" class="page-btn ordr-submit"><i class="fa fa-shopping-basket"></i> order now</button>

                                </div><!--/. col-->

                            </div>	<!--/. row-->

                        </div><!--/. quantity-contrl-->

                    </div><!--/. wrapper-->

                </div><!--/. product-detail-contents-->

            </div><!--/. row-->



        </div><!--/. product-detail-block-->

    </div><!--/. container-->

</div><!--/. product-wrapper-->