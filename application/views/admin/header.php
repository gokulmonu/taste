<head>
    <!-- META SECTION -->
    <title>My Company - Admin</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <!-- END META SECTION -->

    <!-- CSS INCLUDE -->
    <link rel="stylesheet" type="text/css" id="theme" href="<?php echo base_url('bootstrap/css/admin/theme-night.css'); ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>bootstrap/css/admin/dropzone/dropzone.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('bootstrap/js/admin/choosen/chosen.css');?>"/>
    <!-- EOF CSS INCLUDE -->



    <!-- START SCRIPTS -->
    <!-- START PLUGINS -->
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/plugins/jquery/jquery.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/plugins/jquery/jquery-ui.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/plugins/bootstrap/bootstrap.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/plugins/smartwizard/jquery.smartWizard-2.0.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/plugins/summernote/summernote.js');?>"></script>
    <!-- END PLUGINS -->

    <!-- START THIS PAGE PLUGINS-->

    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/plugins/datatables/jquery.dataTables.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/plugins/tableexport/tableExport.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/plugins/tableexport/jquery.base64.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/plugins/tableexport/html2canvas.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/plugins/tableexport/jspdf/libs/sprintf.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/plugins/tableexport/jspdf/jspdf.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/plugins/tableexport/jspdf/libs/base64.js');?>"></script>

    <script type='text/javascript' src="<?php echo base_url('bootstrap/js/admin/dropzone/dropzone.js');?>"></script>
    <script type='text/javascript' src="<?php echo base_url('bootstrap/js/admin/plugins/icheck/icheck.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js');?>"></script>

    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/plugins/morris/raphael-min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/plugins/morris/morris.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/plugins/rickshaw/d3.v3.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/plugins/rickshaw/rickshaw.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js');?>"></script>
    <script type='text/javascript' src="<?php echo base_url('bootstrap/js/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js');?>"></script>
    <script type='text/javascript' src="<?php echo base_url('bootstrap/js/admin/plugins/bootstrap/bootstrap-datepicker.js');?>"></script>
    <script type='text/javascript' src="<?php echo base_url('bootstrap/js/admin/plugins/bootstrap/bootstrap-timepicker.min.js');?>"></script>
    <script type='text/javascript' src="<?php echo base_url('bootstrap/js/admin/plugins/bootstrap/bootstrap-select.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/plugins/owl/owl.carousel.min.js');?>"></script>

    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/plugins/moment.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/plugins/daterangepicker/daterangepicker.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/plugins/blueimp/jquery.blueimp-gallery.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/faq.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/tagsinput/jquery.tagsinput.min.js');?>"></script>


    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/choosen/chosen.jquery.js');?>"></script>
    <!-- END THIS PAGE PLUGINS-->

    <!-- START TEMPLATE -->
    <!--<script type="text/javascript" src="<?php /*echo base_url('bootstrap/js/admin/settings.js');*/?>"></script>
-->
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/plugins.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/admin/actions.js');?>"></script>

   <!-- <script type="text/javascript" src="<?php /*echo base_url('bootstrap/js/admin/demo_dashboard.js');*/?>"></script>
   --> <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->

</head>

<body>
<?php
$userData = $this->core_lib->getLoggedUserData();
?>
<!-- START PAGE CONTAINER -->
<div class="page-container">
    <!-- START PAGE SIDEBAR -->
    <div class="page-sidebar">
        <!-- START X-NAVIGATION -->
        <ul class="x-navigation">
            <li class="xn-logo">
                <a href="#" class="profile-mini">
                    <img src="<?php echo base_url('uploads'); ?>/<?= isset($userData['file_ref'])?$userData['file_ref']:""; ?>" alt="Name"/>
                </a>
                <a href="#" class="x-navigation-control"></a>
            </li>
            <li class="xn-profile">
                <a href="#" class="profile-mini">
                   <img src="<?php echo base_url('uploads'); ?>/<?= isset($userData['file_ref'])?$userData['file_ref']:""; ?>" alt="Name"/></a>
                <div class="profile">
                    <div class="profile-image">
                        <img src="<?php echo base_url('uploads'); ?>/<?= isset($userData['file_ref'])?$userData['file_ref']:""; ?>" alt="Name"/>
                    </div>
                    <div class="profile-data">
                        <div class="profile-data-name"><?= isset($userData['first_name'])?$userData['first_name']:""; ?> <?=isset($userData['last_name'])?$userData['last_name']:""; ?></div>
                        <div class="profile-data-title"><?=isset($userData['position'])?$userData['position']:""; ?></div>
                    </div>
                   <!-- <div class="profile-controls">
                        <a href="pages-profile.html" class="profile-control-left"><span class="fa fa-info"></span></a>
                        <a href="pages-messages.html" class="profile-control-right"><span class="fa fa-envelope"></span></a>
                    </div>-->
                </div>
            </li>
            <li class="xn-title">Navigation</li>
            <li class="active">
                <a href="<?= base_url() ?>admin/dashboard"><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span></a>
            </li>
            <li>
                <a href="<?= base_url() ?>admin/my-profile"><span class="fa fa-user"></span> <span class="xn-text">My Profile</span></a>
            </li>

            <!--<li class="xn-openable">
                <a href="#"><span class="fa fa-user"></span> <span class="xn-text">Apps</span></a>
                <ul>
                    <li><a href="<?/*= base_url() */?>admin/project/add"><span class="fa fa-plus"></span>Add New</a></li>
                    <li><a href="<?/*= base_url() */?>admin/projects"><span class="fa fa-list"></span>List All</a></li>
                </ul>
            </li>-->
            <!--<li class="xn-openable">
                <a href="#"><span class="fa fa-suitcase"></span> <span class="xn-text">Careers</span></a>
                <ul>
                    <li><a href="<?/*= base_url() */?>admin/career/add"><span class="fa fa-plus"></span>Add New</a></li>
                    <li><a href="<?/*= base_url() */?>admin/careers"><span class="fa fa-list"></span>List All</a></li>
                </ul>
            </li>-->
            <li class="xn-openable">
                <a href="#"><span class="fa fa-group"></span> <span class="xn-text">Our Team</span></a>
                <ul>
                    <li><a href="<?= base_url() ?>admin/member/add"><span class="fa fa-plus"></span>Add New</a></li>
                    <li><a href="<?= base_url() ?>admin/members"><span class="fa fa-list"></span>List All</a></li>
                </ul>
            </li>
            <!--<li class="xn-openable">
                <a href="#"><span class="fa fa-dollar"></span> <span class="xn-text">Rates</span></a>
                <ul>
                    <li><a href="<?/*= base_url() */?>admin/rate/add"><span class="fa fa-plus"></span>Add New</a></li>
                    <li><a href="<?/*= base_url() */?>admin/rates"><span class="fa fa-list"></span>Grid View</a></li>
                    <li><a href="<?/*= base_url() */?>admin/rates/list"><span class="fa fa-list"></span>List All</a></li>
                </ul>
            </li>-->
           <!-- <li class="xn">
                <a href="<?/*= base_url() */?>admin/messages"><span class="fa fa-comment"></span> <span class="xn-text">Messages</span></a>
            </li>-->
            <li class="xn">
                <a href="<?= base_url() ?>admin/enquiry"><span class="fa fa-envelope-o"></span> <span class="xn-text">Enquiry</span></a>
            </li>
            <!--<li class="xn-openable">
                <a href="#"><span class="fa fa-sign-in"></span> <span class="xn-text">Registration</span></a>
                <ul>
                    <li><a href="<?/*= base_url() */?>admin/reseller"><span class="fa fa-list"></span>Reseller</a></li>
                    <li><a href="<?/*= base_url() */?>admin/user"><span class="fa fa-list"></span>User</a></li>
                </ul>
            </li>-->
            <li class="xn-openable">
                <a href="#"><span class="fa fa-sign-in"></span> <span class="xn-text">Category</span></a>
                <ul>
                    <li><a href="<?= base_url() ?>category"><span class="fa fa-list"></span>List</a></li>
                    <li><a href="<?= base_url() ?>category/add/0"><span class="fa fa-plus"></span>Add</a></li>
                </ul>
            </li>
            <li class="xn-openable">
                <a href="#"><span class="fa fa-sign-in"></span> <span class="xn-text">Slider</span></a>
                <ul>
                    <li><a href="<?= base_url() ?>slider"><span class="fa fa-list"></span>List</a></li>
                    <li><a href="<?= base_url() ?>slider/add"><span class="fa fa-plus"></span>Add</a></li>
                </ul>
            </li>
            <li class="xn">
                <a href="<?= base_url(); ?>invoice"><span class="fa fa-file-pdf-o"></span> <span class="xn-text">Invoice</span></a>
            </li>
        </ul>

        <!-- END X-NAVIGATION -->
    </div>
    <!-- END PAGE SIDEBAR -->


    <!-- PAGE CONTENT -->
<div class="page-content">

    <!-- START X-NAVIGATION VERTICAL -->
    <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
        <!-- TOGGLE NAVIGATION -->
        <li class="xn-icon-button">
            <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
        </li>
        <!-- END TOGGLE NAVIGATION -->
        <!-- SEARCH -->
        <li class="xn-search">
            <form role="form">
                <input type="text" name="search" placeholder="Search..."/>
            </form>
        </li>
        <!-- END SEARCH -->
        <!-- SIGN OUT -->
        <li class="xn-icon-button pull-right">
            <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>
        </li>
        <!-- END SIGN OUT -->
        <!-- MESSAGES -->
        <?php
        $unreadMessages = $this->core_lib->getUnreadMessages();

        ?>
        <li class="xn-icon-button pull-right">
            <a href="#"><span class="fa fa-comments"></span></a>
            <div class="informer informer-danger"><?php echo count($unreadMessages) ?></div>
            <div class="panel panel-primary animated zoomIn xn-drop-left xn-panel-dragging">
                <div class="panel-heading">
                    <h3 class="panel-title"><span class="fa fa-comments"></span> Messages</h3>
                    <div class="pull-right">
                        <span class="label label-danger"><?php echo count($unreadMessages) ?> new</span>
                    </div>
                </div>
                <div class="panel-body list-group list-group-contacts scroll" style="height: 200px;">
                    <?php
                    if($unreadMessages){
                       foreach($unreadMessages as $msg){
                           ?>
                           <a href="<?= base_url() ?>admin/message/view/<?= $msg['from_id']?>" class="list-group-item">
                               <img src="<?= base_url() ?>uploads/<?= isset($msg['from_file_ref'])?$msg['from_file_ref']:""; ?>" class="pull-left" alt="John Doe"/>
                               <span class="contacts-title"><?= $msg['from_first_name'] ?></span>
                               <p><?= substr($msg['message'],0,20)?></p>
                           </a>
                    <?php
                       }
                    }
                    ?>
                </div>
                <div class="panel-footer text-center">
                    <a href="<?= base_url() ?>admin/messages">Show all messages</a>
                </div>
            </div>
        </li>
        <!-- END MESSAGES -->
        <!-- TASKS -->
        <?php
        $activeEnquiry = $this->core_lib->getActiveEnquiry();
        ?>
        <li class="xn-icon-button pull-right">
            <a href="#"><span class="fa fa-tasks"></span></a>
            <div class="informer informer-warning"><?php echo count($activeEnquiry['rows'])?></div>
            <div class="panel panel-primary animated zoomIn xn-drop-left xn-panel-dragging">
                <div class="panel-heading">
                    <h3 class="panel-title"><span class="fa fa-envelope-o"></span>Enquiry</h3>
                    <div class="pull-right">
                        <span class="label label-warning"><?php echo count($activeEnquiry['rows'])?> active</span>
                    </div>
                </div>
                <div class="panel-body list-group scroll" style="height: 200px;">
                    <?php
                    if(count($activeEnquiry['rows'])){
                        foreach($activeEnquiry['rows'] as $items){
                            ?>
                            <a class="list-group-item" href="<?= base_url() ?>admin/enquiry/view/<?= $items['id']?>">
                                <strong><?= $items['name']?>, <?= $items['subject']?></strong>

                                <small class="text-muted"><?= $items['email']?>, <?= $items['phone']?></small>
                            </a>
                    <?php
                        }
                    }
                    ?>

                </div>
                <div class="panel-footer text-center">
                    <a href="<?= base_url() ?>admin/enquiry">Show all Enquiry</a>
                </div>
            </div>
        </li>
        <!-- END TASKS -->
    </ul>
    <!-- END X-NAVIGATION VERTICAL -->

    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                <div class="mb-content">
                    <p>Are you sure you want to log out?</p>
                    <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <a href="<?php echo base_url()?>logout" class="btn btn-success btn-lg">Yes</a>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MESSAGE BOX-->


    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">
        <ul class="breadcrumb">
            <?php
           /* if(isset($breadcrumbs)){
                foreach($breadcrumbs as $bKey => $bValue){
                    */?><!--
                    <li>
                        <a href="<?php /*echo base_url('admin/'.$bKey)*/?>"><?/*= $bValue; */?></a>
                    </li>
                    --><?php
/*                }
            }*/
            ?>
        </ul>

        <div class="page-title">
            <h2>
                <span class="<?php if(isset($page_header) && isset($page_header['icon'])){
                    echo $page_header['icon'];
                } ?>"></span>
                <?php if(isset($page_header) && isset($page_header['title'])){
                    echo $page_header['title'];
                } ?>
            </h2>
        </div>