 <!-- START RESPONSIVE TABLES -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-body panel-body-table">

                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped table-actions">
                                            <thead>

                                            <tr>
                                                <th width="50">id</th>
                                                <th width="60">Name</th>
                                                <th width="100">Subject</th>
                                                <th width="150">Email</th>
                                                <th width="100">actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            if(isset($data['rows']) && count($data['rows'])){
                                                foreach ($data['rows'] as $value){
                                                    ?>
                                            <tr>
                                                <td class="text-center"><?= $value['id'] ?></td>
                                                <td><strong><?php echo $value['name'] ?></strong></td>
                                                <td><?= $value['subject'] ?></td>
                                                <td><?= $value['email'] ?></td>
                                                <td>
                                                    <a href="<?php echo base_url()?>admin/enquiry/view/<?php echo  $value['id'] ?>" class="btn btn-default btn-rounded btn-sm"><span class="fa fa-eye"></span></a>
                                                    <a href="javascript:;" class="btn btn-danger btn-rounded btn-sm" title="delete" onclick="deleteEnquiry('<?php echo $value['id'] ?>')"><span class="fa fa-times"></span></a>
                                                </td>
                                            </tr>
                                            <?php
                                            }
                                            }else{
                                                ?>
                                                <td>No results Found</td>
                                                <?php
                                            }

                                            ?>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- END RESPONSIVE TABLES -->

<?php
if(isset($data['pagination'])){
    $dataArr = [
        'pagination'=>$data['pagination']
    ];
    $this->view('pagination',$dataArr);
}

?>
