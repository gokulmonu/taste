
<div class="col-md-10">
    <table cellpadding="0" class="table table-bordered table-striped">
        <tr>
            <th width="10%">
                Name
            </th>
            <td>
                <?= isset($name)?$name:""; ?>
            </td>
        </tr>
        <tr>
            <th width="10%">
                Email
            </th>
            <td>
                <?= isset($email)?$email:""; ?>
            </td>
        </tr>
        <tr>
            <th width="10%">
                Subject
            </th>
            <td>
                <?= isset($subject)?$subject:""; ?>
            </td>
        </tr>
        <tr>
            <th width="10%">
                Phone
            </th>
            <td>
                <?= isset($phone)?$phone:""; ?>
            </td>
        </tr>
        <tr>
            <th width="10%">
                Message
            </th>
            <td>
                <?= isset($message)?$message:""; ?>
            </td>
        </tr>
    </table>
</div>
