
<script>
    $(function(){
        $('#btn_submit').on('click',function(event){
            event.preventDefault();
            var formData = getCareerFormData();
            $.ajax({
                url: '<?php echo base_url()?>admin/career/validate',
                type: 'POST',
                data:formData,
                async : false,
                cache : false,
                contentType : false,
                processData : false,
                success: function (data) {
                    $('.help-block').html('');
                    if(!data.success){
                        for(key in data){
                            $('#'+data[key]['field_id']).html(data[key]['label']);
                        }
                    }else{
                        location.href="<?php echo base_url()?>admin/careers";
                    }



                },
                error: function (e) {
                    //called when there is an error
                    //console.log(e.message);
                }
            });
        });

        function getCareerFormData()
        {
            var formElement = new FormData();
            formElement.append('id',$('#id').val());
            formElement.append('title',$('#title').val());
            formElement.append('experience',$('#experience').val());
            formElement.append('salary',$('#salary').val());
            formElement.append('description',$('#description').val());
            return formElement;
        }

    });
</script>
<div class="col-md-12">

    <form class="form-horizontal">
        <input type="hidden" id="id" value="<?= isset($id)?$id:0; ?>">
        <div class="panel panel-default">
            <div class="panel-body">

                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">Title</label>
                    <div class="col-md-6 col-xs-12">
                        <div class="input-group">
                            <span class="input-group-addon"><span style="line-height: 28px;" class="fa fa-pencil"></span></span>
                            <input type="text" id="title" value="<?= isset($title)?$title:""; ?>" class="form-control">
                        </div>
                        <span id="title_error" class="help-block"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">Experience</label>
                    <div class="col-md-6 col-xs-12">
                        <div class="input-group">
                            <span class="input-group-addon"><span style="line-height: 28px;" class="fa fa-star-o"></span></span>
                            <input type="text" id="experience" value="<?= isset($experience)?$experience:""; ?>"  class="form-control">
                        </div>
                        <span id="experience_error" class="help-block"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">Salary</label>
                    <div class="col-md-6 col-xs-12">
                        <div class="input-group">
                            <span class="input-group-addon"><span style="line-height: 28px;" class="fa fa-money"></span></span>
                            <input type="text" id="salary" value="<?= isset($salary)?$salary:""; ?>"  class="form-control">
                        </div>
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">Description</label>
                    <div class="col-md-6 col-xs-12">
                        <div class="input-group">
                            <span class="input-group-addon"><span style="line-height: 28px;" class="fa fa-file-text"></span></span>
                            <textarea id="description"  class="form-control"><?= isset($description)?$description:""; ?></textarea>
                        </div>
                        <span class="help-block"></span>
                    </div>
                </div>


            </div>
            <div class="panel-footer">
                <button class="btn btn-default">Clear Form</button>
                <button id="btn_submit" class="btn btn-primary pull-right">Submit</button>
            </div>
        </div>
    </form>

</div>