<script>

    function deleteCareer(id)
    {
        $.ajax({
            url: '<?php echo base_url()?>admin/career/delete',
            type: 'POST',
            data:{'id':id},
            success: function (data) {
                location.href="<?php echo base_url()?>admin/careers";
            },
            error: function (e) {
                //called when there is an error
                //console.log(e.message);
            }
        });
    }
</script>
<div class="col-md-8">

    <div class="panel panel-default">
        <div class="panel-body">
            <h3 class="push-down-0">Current Openings </h3>
        </div>
        <div class="panel-body faq">
            <?php
            if(isset($list) && !empty($list)){
                foreach($list as $value){
                    ?>
                    <div class="faq-item">
                        <div class="faq-title"><span class="fa fa-angle-down"></span><?= isset($value['title'])?$value['title'] :""?></div>
                        <div class="faq-text">
                            <h5><?= isset($value['title'])?$value['title'] :""?> - <?= isset($value['salary'])?$value['salary'] :""?>:-Salary</h5>
                            <p><?= isset($value['description'])?$value['description'] :""?></p>
                            <button class="btn btn-danger" onclick="deleteCareer('<?= isset($value['id'])?$value['id'] : 0?>')" type="button">Delete</button>
                            <a class="btn btn-info" href="<?php echo base_url('admin/career/edit') ?>/<?= isset($value['id'])?$value['id'] :0?>">Edit</a>
                        </div>

                    </div>

                    <?php
                }
            }else{
                echo "No careers Found";
            }
            ?>

        </div>
    </div>

</div>