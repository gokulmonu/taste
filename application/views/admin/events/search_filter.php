<script>
    //var currentPage = 1;
    $(function () {
       $('#phrase').on('keyup',function () {
           setTimeout(search(1),1000);
        });

    });
    function search (curpage) {
        var phrase = $('#phrase').val();
        $.ajax({
            url: '<?php echo base_url()?>admin/event/search',
            type: 'POST',
            dataType: "html",
            data:{
                'phrase':phrase,
                'page':curpage

            },
            success: function (data) {
                $('#event_result_container').html(data);
            },
            error: function (e) {
                //called when there is an error
                //console.log(e.message);
            }
        });
    }
    function deleteEvent(eventId) {
        if(confirm("Are you sure want to delete this Event?")){
            var postData = {};
            postData['event_id'] = eventId;
            $.ajax({
                url: '<?php echo base_url()?>admin/event/delete',
                type: 'POST',
                data: postData,
                success: function (data) {
                    location.reload();
                },
                error: function (e) {
                    //called when there is an error
                    //console.log(e.message);
                }
            });
        }else{
            return false;
        }

    }
    function openPage(pagenum) {
        search(pagenum);
    }
</script>
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-6">
            <!-- START DEFAULT BUTTONS -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Options</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <a href="<?php echo base_url()?>admin/event/add" class="btn btn-primary">Add Event</a>

                    </div>

                    <div class="form-group">

                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-search"></span></span>
                                <input type="text" class="form-control" id="phrase" placeholder="Search by phrase"  />
                            </div>

                    </div>

                </div>

            </div>

        </div>
    </div>
</div>
<!-- END DEFAULT BUTTONS -->