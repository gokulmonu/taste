<script>
    Dropzone.autoDiscover = false;
    $(function () {
        $("div#drop_zone").dropzone({
            url: '<?php echo base_url()?>admin/event/uploadEventImage',
            init :function(){
                this.on('sending',function(file,xhr,formData){
                    formData.append('event_id','<?= isset($id)?$id:""; ?>');
                })
            },
            success: function (file, response) {
                if(response.error){
                    $('#upload_error').html(response.error);
                    $('#upload_error_container').iziModal('open');
                }else{
                    reloadImages();
                }
                //this.removeFile(file);

            }
        });

        function reloadImages() {
            $body.addClass("loading");
            setTimeout(loadImages,3000);
        }

        function loadImages() {
            var id = $('#id').val();
            $.ajax({
                url: '<?php echo base_url()?>admin/event/getEventImageForRender',
                type: 'POST',
                dataType: "html",
                data:{
                    id:id
                },
                success: function (data) {
                    $body.removeClass("loading");
                    $('#image_container').html(data);
                },
                error: function (e) {
                    //called when there is an error
                    //console.log(e.message);
                }
            });
        }

    });
</script>
    <div class="panel panel-default tabs">
    <ul class="nav nav-tabs" role="tablist">
        <li class="active"><a href="#tab-first" role="tab" data-toggle="tab">Event Details</a></li>
        <li><a href="#tab-second" role="tab" data-toggle="tab">Description</a></li>
        <li><a href="#tab-third" role="tab" data-toggle="tab">Images</a></li>
    </ul>
    <div class="panel-body tab-content">
        <div class="tab-pane active" id="tab-first">
            <table class="table table-bordered table-striped table-actions">
                <tr>
                    <th width="100">Name</th>
                    <td><?= isset($name)?$name:""; ?></td>
                </tr><tr>
                    <th width="100">Title</th>
                    <td><?= isset($title)?$title:""; ?></td>
                </tr>
                </table>
        </div>
        <div class="tab-pane" id="tab-second">
            <div>
                <?= isset($description)?$description:""; ?>
            </div>

        </div>
        <div class="tab-pane" id="tab-third">
            <div id="drop_zone" class="dropzone">

            </div>
        </div>
    </div>
</div>