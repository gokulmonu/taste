<script>
    //var currentPage = 1;
    $(function () {
       $('#phrase').on('keyup',function () {
           setTimeout(search(1),1000);
        });

    });
    function search (curpage) {
        var phrase = $('#phrase').val();
        $.ajax({
            url: '<?php echo base_url()?>admin/project/search',
            type: 'POST',
            dataType: "html",
            data:{
                'phrase':phrase,
                'page':curpage

            },
            success: function (data) {
                $('#project_result_container').html(data);
            },
            error: function (e) {
                //called when there is an error
                //console.log(e.message);
            }
        });
    }
    function deleteProject(projectId) {
        if(confirm("Are you sure want to delete this App?")){
            var postData = {};
            postData['project_id'] = projectId;
            $.ajax({
                url: '<?php echo base_url()?>admin/project/delete',
                type: 'POST',
                data: postData,
                success: function (data) {
                    location.reload();
                },
                error: function (e) {
                    //called when there is an error
                    //console.log(e.message);
                }
            });
        }else{
            return false;
        }

    }
    function openPage(pagenum) {
        search(pagenum);
    }
</script>
    <div class="row">
        <div class="col-md-6">
            <!-- START DEFAULT BUTTONS -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Options</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span style="line-height: 28px;" class="fa fa-angle-down"></span></a></li>
                        <li><a href="#" class="panel-remove"><span style="line-height: 28px;" class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <a href="<?php echo base_url()?>admin/project/add" class="btn btn-primary">Add App</a>

                    </div>

                    <div class="form-group">

                            <div class="input-group">
                                <span class="input-group-addon"><span style="line-height: 28px;" class="fa fa-search"></span></span>
                                <input type="text" class="form-control" id="phrase" placeholder="Search by phrase"  />
                            </div>

                    </div>

                </div>

            </div>

        </div>
    </div>
<!-- END DEFAULT BUTTONS -->