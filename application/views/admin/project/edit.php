<script>
    $(function () {
        $("#project_form").submit(function( event ) {
            event.preventDefault();
            var formData = getFormData();
            $('#error_container').hide();
            $.ajax({
                url: '<?php echo base_url()?>admin/project/validate',
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify( formData),
                success: function (data) {
                    $('#product_name_error').html('');
                    if(data.success){
                        window.location.href ='<?php echo base_url() ?>admin/projects';
                    }

                    if(!data.success){
                        $('#error_container').show();
                    }

                },
                error: function (e) {
                }
            });

        });

        function getFormData() {

            var data = {};
            data['id'] = $('#id').val();
            data['name'] = $('#name').val();
            data['title'] = $('#title').val();
            data['url'] = $('#url').val();
            data['description'] = $('#description').val();
            return data;
        }
    });

</script>

<!-- START WIZARD WITH SUBMIT BUTTON -->
<div class="block">
    <div class="alert alert-danger" role="alert" id="error_container" style="display: none">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <span id="error_msg"> Please Enter App Name</span>
    </div>
    <h4>App Add/Edit</h4>
    <form role="form" class="form-horizontal" id="project_form">
        <div class="wizard show-submit">
            <ul>
                <li>
                    <a href="#step-5">
                        <span class="stepNumber">1</span>
                        <span class="stepDesc">App<br /><small>Basic Data</small></span>
                    </a>
                </li>
                <li>
                    <a href="#step-6">
                        <span class="stepNumber">2</span>
                        <span class="stepDesc">App Description<br /><small>Information</small></span>
                    </a>
                </li>
            </ul>
            <div id="step-5">
                <input type="hidden" id="id" value="<?php echo isset($id)?$id:0  ?>">
                <div class="form-group">
                    <label class="col-md-2 control-label">Name*</label>

                    <div class="col-md-6 col-xs-12">
                        <input type="text"  id="name" placeholder="App name" class="form-control" value="<?php echo isset($name)?$name:""; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">URL</label>
                    <div class="col-md-6 col-xs-12">
                        <input type="text"  id="url" placeholder="App URL" class="form-control" value="<?php echo isset($url)?$url:""; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Title</label>
                    <div class="col-md-6 col-xs-12">
                        <textarea class="form-control" rows="5" id="title" placeholder="Something about App"><?php echo isset($title)?$title:""; ?></textarea>
                    </div>
                </div>

            </div>
            <div id="step-6">

                <div class="block">
                    <h4>App Description</h4>


                                <textarea class="summernote" id="description">
                                    <?php echo isset($description)?$description:""; ?>
                                </textarea>
                </div>


            </div>
        </div>
    </form>
</div>
<!-- END WIZARD WITH SUBMIT BUTTON -->
