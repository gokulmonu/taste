<script>
    Dropzone.autoDiscover = false;
    $(function () {
        $("div#drop_zone").dropzone({
            url: '<?php echo base_url()?>admin/project/uploadProjectImage',
            init :function(){
                this.on('sending',function(file,xhr,formData){
                    formData.append('project_id','<?= isset($id)?$id:""; ?>');
                })
            },
            success: function (file, response) {
                if(response.error){
                    $('#upload_error').html(response.error);
                    $('#upload_error_container').iziModal('open');
                }else{
                    reloadImages();
                }
                //this.removeFile(file);

            }
        });


    });
    function reloadImages() {
        setTimeout(loadImages,3000);
    }

    function loadImages() {
        var id = '<?= isset($id)?$id:0; ?>';
        $.ajax({
            url: '<?php echo base_url()?>admin/project/getProjectImageForRender',
            type: 'POST',
            dataType: "html",
            data:{
                id:id
            },
            success: function (data) {
                $('#image_container').html(data);
            },
            error: function (e) {
                //called when there is an error
                //console.log(e.message);
            }
        });
    }
    function removeImage(fileId) {
        var postData = {};
        postData['file_id'] = fileId;
        $.ajax({
            url: '<?php echo base_url()?>admin/project/removeImage',
            type: 'POST',
            data: postData,
            success: function (data) {

                reloadImages();
            },
            error: function (e) {
                //called when there is an error
                //console.log(e.message);
            }
        });
    }
</script>
    <div class="panel panel-default tabs">
    <ul class="nav nav-tabs" role="tablist">
        <li class="active"><a href="#tab-first" role="tab" data-toggle="tab">Event Details</a></li>
        <li><a href="#tab-second" role="tab" data-toggle="tab">Description</a></li>
        <li><a href="#tab-third" role="tab" data-toggle="tab">Images</a></li>
    </ul>
    <div class="panel-body tab-content">
        <div class="tab-pane active" id="tab-first">
            <table class="table table-bordered table-striped table-actions">
                <tr>
                    <th width="100">Name</th>
                    <td><?= isset($name)?$name:""; ?></td>
                </tr><tr>
                    <th width="100">Title</th>
                    <td><?= isset($title)?$title:""; ?></td>
                </tr>
                </table>
        </div>
        <div class="tab-pane" id="tab-second">
            <div>
                <?= isset($description)?$description:""; ?>
            </div>

        </div>
        <div class="tab-pane" id="tab-third">
            <div id="drop_zone" class="dropzone" style="min-height: 180px;">

            </div>
            <div id="image_container">
                <?php
                $this->view('admin/project/image_list');
                ?>
            </div>
        </div>
    </div>
</div>