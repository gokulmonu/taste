<script>
    $(function(){
        reloadConversations();
       setInterval(reloadConversations,10000);
       setInterval(updateLastSeen,50000);

        function reloadConversations()
        {
            $.ajax({
                url: '<?php echo base_url()?>admin/get-all-conversations',
                type: 'POST',
                contentType: 'application/json',
                data: {},
                success: function (json) {
                    if(json.success){
                        if(json.result){

                            for(key in json.result){
                                var sortOrder = json.result[key]['messages'][0]['id'];
                               if($('#chat_user_'+key).length){
                                   if(json.result[key]['messages']){
                                       $('#chat_user_'+key).find('p').html(json.result[key]['messages'][0]['message']);
                                       if(json.result[key]['unread_msg_count']>= 1){
                                           $('#chat_user_'+key).find('.msg_count').html(json.result[key]['unread_msg_count']);
                                           $('#chat_user_'+key).removeClass('active').addClass('active');
                                       }else{
                                           $('#chat_user_'+key).find('.msg_count').html("");
                                           $('#chat_user_'+key).removeClass('active');
                                       }
                                   }

                                   $('#chat_user_'+key).attr('data-sort',sortOrder);
                               }else{
                                  var aTag = document.createElement('a');
                                   aTag.setAttribute('id','chat_user_'+key);
                                   aTag.setAttribute('class','list-group-item active msg_cls');
                                   aTag.setAttribute('data-content',key);
                                   aTag.setAttribute('data-sort',sortOrder);
                                   aTag.setAttribute('href','<?= base_url() ?>admin/message/view/'+key);

                                   var div = document.createElement('div');
                                   div.setAttribute('class','list-group-status status-'+json.result[key]['user_data']['status']);
                                   aTag.appendChild(div);
                                   var img = document.createElement('img');
                                   img.setAttribute('class','pull-left');
                                   img.setAttribute('src','<?= base_url() ?>uploads/'+json.result[key]['user_data']['file_ref']);
                                   aTag.appendChild(img);
                                   var span = document.createElement('div');
                                   span.setAttribute('class','contacts-title');
                                   span.innerHTML = json.result[key]['user_data']['first_name'];
                                   aTag.appendChild(span);

                                   var spanInner = document.createElement('span');
                                   spanInner.setAttribute('class','label label-danger');
                                   spanInner.innerHTML = +json.result[key]['unread_msg_count'];

                                   span.appendChild(spanInner);

                                   var p = document.createElement('p');
                                   p.innerHTML = json.result[key]['messages'][0]['message'];
                                   aTag.appendChild(p);
                                   $('#message_container').append(aTag);
                               }
                                //sortOrder++
                            }
                           resortMessages();
                        }
                    }

                },
                error: function (e) {
                }
            });

        }
    });

    function resortMessages()
    {
        var divList = $(".msg_cls");
        divList.sort(function(a, b){
            return parseInt($(a).attr('data-sort'))<parseInt($(b).attr('data-sort'))
        });
        $("#message_container").html(divList);
    }

    function updateLastSeen()
    {
        $.ajax({
            url: '<?php echo base_url()?>update/last-seen'
        });
    }
</script>


<div class="col-md-8">
<div class="content-frame-right" style="height: 611px;">

    <div id="message_container" class="list-group list-group-contacts border-bottom push-down-10">
        <?php
        if(isset($list) && !empty($list)){
            foreach($list as $key => $value){
                ?>
                <a data-sort="<?= isset($value['messages'][0]['id'])?$value['messages'][0]['id']:"" ?>" data-content="<?= isset($value['user_data']['id'])?$value['user_data']['id']:"";?>" class="list-group-item msg_cls" href="<?= base_url() ?>admin/message/view/<?= isset($value['user_data']['id'])?$value['user_data']['id']:"";?>" id="chat_user_u_<?= isset($value['user_data']['id'])?$value['user_data']['id']:"";?>">
                    <?php
                    $status = "away";
                    $lastSeen = isset($value['user_data']['last_seen'])?$value['user_data']['last_seen']:"";
                    if($lastSeen){
                        date_default_timezone_set("Asia/Kolkata");
                       $currentDateTime = date('Y-m-d H:i:s');
                        $lastSeenAdded = strtotime($lastSeen) + 60;
                        $currentStr = strtotime($currentDateTime);
                        if($lastSeenAdded >$currentStr){
                            $status = "online";
                        }

                    }
                    ?>
                    <div class="list-group-status status-<?php echo $status?>"></div>
                    <img alt="Dmitry Ivaniuk" class="pull-left" src="<?= base_url() ?>uploads/<?= isset($value['user_data']['file_ref'])?$value['user_data']['file_ref']:""; ?>">
                    <div class="contacts-title"><?= isset($value['user_data']['first_name'])?$value['user_data']['first_name']:"" ?>
                        <span class="label label-danger msg_count"><?= isset($value['unread_msg_count'])?$value['unread_msg_count']:"" ?></span>
                    </div>
                    <p><?= isset($value['messages'][0]['message'])?$value['messages'][0]['message']:"" ?></p>
                </a>
        <?php
            }
        }

        ?>

    </div>

</div>
</div>
