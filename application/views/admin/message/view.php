<script>
    var userProfileUrl = '<?= isset($user_details['file_ref'])?$user_details['file_ref']:""?>';
    var userName = '<?= isset($user_details['first_name'])?$user_details['first_name']:"" ?>';
    var userId = '<?= isset($user_details['id'])?$user_details['id']:0 ?>';
    var currentPage = 1;
    updateMessagesStatus();
    $(function () {
        reloadMessages();
        setInterval(reloadMessages,10000);
        setInterval(updateLastSeen,10000);

        $('#send_msg_btn').on('click',function(){
            var postData = {};
            postData['to_id'] = '<?= isset($user_id)?$user_id:"";?>';
            postData['msg'] = $('#msg').val();
            $.ajax({
                url: '<?php echo base_url()?>message/send',
                type: 'POST',
                data: postData,
                success: function (json) {
                    $('#msg').val("");
                    reloadMessages();
                }
            });
        });

        function reloadMessages() {
            var postData = {};
            postData['id'] = '<?= isset($user_id)?$user_id:"";?>';
            postData['page'] = currentPage;
            $.ajax({
                url: '<?php echo base_url()?>admin/get-message',
                type: 'POST',
                data: postData,
                success: function (json) {
                    if (json.success) {
                        if (json.result) {
                            for (key in json.result) {
                                if(!$('#msg_'+json.result[key]['id']).length){
                                    var div = document.createElement('div');
                                    if (json.result[key]['to_id'] == userId) {
                                        div.setAttribute('class', 'item in item-visible msg_cls');
                                    } else {
                                        div.setAttribute('class', 'item item-visible msg_cls');
                                    }
                                    div.setAttribute('id', 'msg_'+json.result[key]['id']);
                                    div.setAttribute('data-content',json.result[key]['id']);
                                    var innerDiv = document.createElement('div');
                                    innerDiv.setAttribute('class', 'image');

                                    var img = document.createElement('img');
                                    if (json.result[key]['to_id'] == userId) {
                                        img.setAttribute('src', '<?= base_url() ?>uploads/' + json.result[key]['from_file_ref']);
                                    } else {
                                        img.setAttribute('src', '<?= base_url() ?>uploads/' + userProfileUrl);
                                    }
                                    innerDiv.appendChild(img);

                                    var textDiv = document.createElement('div');
                                    textDiv.setAttribute('class', 'text');

                                    var titleDiv = document.createElement('div');
                                    titleDiv.setAttribute('class', 'heading');

                                    var aTag = document.createElement('a');
                                    aTag.setAttribute('href', '#');
                                    var aTextElem;
                                    if (json.result[key]['to_id'] == userId) {
                                        aTextElem = document.createTextNode(json.result[key]['from_first_name']);

                                    } else {
                                        aTextElem = document.createTextNode(userName);

                                    }
                                    aTag.appendChild(aTextElem);

                                    var created = json.result[key]['msg_created'];
                                    var span = document.createElement('span');
                                    span.setAttribute('class', 'date');
                                    span.innerHTML = created;

                                    titleDiv.appendChild(aTag);
                                    titleDiv.appendChild(span);

                                    textDiv.appendChild(titleDiv);
                                    var textNode = document.createTextNode(json.result[key]['message']);
                                    textDiv.appendChild(textNode);

                                    div.appendChild(innerDiv);
                                    div.appendChild(textDiv);

                                    $('#message_container').append(div);
                                    resortMessages();

                                }

                            }
                        }
                        updateMessagesStatus();
                    }

                },
                error: function (e) {
                }
            });

        }

        function resortMessages()
        {
            var divList = $(".msg_cls");
            divList.sort(function(a, b){
                return $(a).attr('data-content')-$(b).attr('data-content')
            });
            $("#message_container").html(divList);
            scrollToBottom();
        }

        function updateLastSeen()
        {
            $.ajax({
                url: '<?php echo base_url()?>update/last-seen'
            });
        }

    });
    function updateMessagesStatus()
    {
        var postData = {};
        postData['id'] = '<?= isset($user_id)?$user_id:"";?>';
        $.ajax({
            url: '<?php echo base_url()?>update/message-status',
            type: 'POST',
            data: postData,
            success: function (json) {

            }
        });
    }
    function scrollToBottom()
    {
        $(window).scrollTop($('#msg_text_container').offset().top);
    }
</script>

<div class="col-md-8">

	<div class="message-wrapper">
    	
        <div class="messages messages-img" id="message_container">

    </div>
    <div class="panel panel-default push-up-10 send-msg">
        <div class="panel-body panel-body-search">
            <div class="input-group">
                <div class="input-group-btn">

                </div>
                <input type="text" placeholder="Your message..." id="msg" class="form-control">

                <div class="input-group-btn" id="msg_text_container">
                    <button class="btn btn-default" id="send_msg_btn">Send</button>
                </div>
            </div>
        </div>
    </div>
        
        
    </div>

    
</div>
