<script>
    function deleteMember(memberId) {
        if(confirm("Are you sure want to delete this member?")){
            var postData = {};
            postData['member_id'] = memberId;
            $.ajax({
                url: '<?php echo base_url()?>admin/member/delete',
                type: 'POST',
                data: postData,
                success: function (data) {
                     location.reload();
                },
                error: function (e) {
                    //called when there is an error
                    //console.log(e.message);
                }
            });
        }else{
            return false;
        }

    }
</script>
<div class="row">
    <?php
    if (isset($list)) {
        foreach ($list as $value) {
            ?>
            <div class="col-md-3">
                <!-- CONTACT ITEM -->
                <div class="panel panel-default">
                    <div class="panel-body profile">
                        <div class="profile-image">
                            <img alt="Nadia Ali" src="<?= base_url() ?>uploads/<?= isset($value['file_ref'])?$value['file_ref']:""; ?>">
                        </div>
                        <div class="profile-data">
                            <div class="profile-data-name"><?= isset($value['name'])?$value['name']:""; ?></div>
                            <div class="profile-data-title"><?= isset($value['position'])?$value['position']:""; ?></div>
                        </div>
                        <div class="profile-controls">
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="contact-info">
                            <p>
                                <small>Mobile</small>
                                <br><?= isset($value['phone'])?$value['phone']:""; ?>
                            </p>
                            <p>
                                <small>Email</small>
                                <br><?= isset($value['email'])?$value['email']:""; ?>
                            </p>

                        </div>
                    </div>
                    <a class="btn btn-info" href="<?php echo base_url('admin/member/edit') ?>/<?= isset($value['id'])?$value['id'] :0?>">Edit</a>
                    <a class="btn btn-warning" onclick="deleteMember('<?= isset($value['id'])?$value['id'] :0?>')" href="javascript:;">Delete</a>

                </div>
                <!-- END CONTACT ITEM -->

            </div>
            <?php
        }
    }

    ?>

</div>
