
<script>
    $(function(){
       $('#btn_submit').on('click',function(event){
           event.preventDefault();
           var formData = getMemberFormData();
           $.ajax({
               url: '<?php echo base_url()?>admin/member/validate',
               type: 'POST',
               data:formData,
               async : false,
               cache : false,
               contentType : false,
               processData : false,
               success: function (data) {
                   $('.help-block').html('');
                   if(!data.success){
                       for(key in data){
                           $('#'+data[key]['field_id']).html(data[key]['label']);
                       }
                   }else{
                    location.href="<?php echo base_url()?>admin/members";
                   }



               },
               error: function (e) {
                   //called when there is an error
                   //console.log(e.message);
               }
           });
       });

        function getMemberFormData()
        {
            var formElement = new FormData();
            formElement.append('id',$('#id').val());
            formElement.append('position',$('#position').val());
            formElement.append('name',$('#name').val());
            formElement.append('phone',$('#phone').val());
            formElement.append('email',$('#email').val());
            formElement.append('file',$('#filename')[0].files[0]);
            return formElement;
        }
    });
</script>
<div class="col-md-12">

    <form class="form-horizontal">
        <input type="hidden" id="id" value="<?= isset($id)?$id:0; ?>">
        <div class="panel panel-default">
            <div class="panel-body">

                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">Name</label>
                    <div class="col-md-6 col-xs-12">
                        <div class="input-group">
                            <span class="input-group-addon"><span style="line-height: 28px;" class="fa fa-user"></span></span>
                            <input type="text" id="name" value="<?= isset($name)?$name:""; ?>" class="form-control">
                        </div>
                        <span id="name_error" class="help-block"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">Email</label>
                    <div class="col-md-6 col-xs-12">
                        <div class="input-group">
                            <span class="input-group-addon"><span style="line-height: 28px;" class="fa fa-envelope"></span></span>
                            <input type="text" id="email" value="<?= isset($email)?$email:""; ?>"  class="form-control">
                        </div>
                        <span id="email_error" class="help-block"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">Phone</label>
                    <div class="col-md-6 col-xs-12">
                        <div class="input-group">
                            <span class="input-group-addon"><span style="line-height: 28px;" class="fa fa-phone"></span></span>
                            <input type="text" id="phone" value="<?= isset($phone)?$phone:""; ?>"  class="form-control">
                        </div>
                        <span id="phone_error" class="help-block"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">Position</label>
                    <div class="col-md-6 col-xs-12">
                        <div class="input-group">
                            <span class="input-group-addon"><span style="line-height: 28px;" class="fa fa-info"></span></span>
                            <input type="text" id="position" value="<?= isset($position)?$position:""; ?>"  class="form-control">
                        </div>
                        <span class="help-block"></span>
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">File</label>
                    <div class="col-md-6 col-xs-12">
                       <span>Browse file</span><input type="file" title="Browse file" id="filename" name="filename">
                        <span class="help-block">Input type file</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">&nbsp;</label>
                    <div class="col-md-6 col-xs-12">
                    <div class="gallery">
                        <a data-gallery="" title="Profile" href="<?= base_url() ?>uploads/<?= isset($file_ref)?$file_ref:""; ?>" class="gallery-item">
                            <div class="image">
                                <img alt="Profile Image" src="<?= base_url() ?>uploads/<?= isset($file_ref)?$file_ref:""; ?>">
                            </div>
                        </a>
                    </div>
                    </div>
                    </div>

            </div>
            <div class="panel-footer">
                <button class="btn btn-default">Clear Form</button>
                <button id="btn_submit" class="btn btn-primary pull-right">Submit</button>
            </div>
        </div>
    </form>

</div>