<script>

    function deleteRate(id)
    {
        $.ajax({
            url: '<?php echo base_url()?>admin/rate/delete',
            type: 'POST',
            data:{'id':id},
            success: function (data) {
                location.href="<?php echo base_url()?>admin/rates";
            },
            error: function (e) {
                //called when there is an error
                //console.log(e.message);
            }
        });
    }
</script>
<div class="col-md-12">
    <?php
    if(isset($list) && !empty($list)) {
        foreach ($list as $value) {
            ?>
            <div class="col-md-4">

                <div class="panel panel-success push-up-20">
                    <div class="panel-body panel-body-pricing">
                        <h2><?= isset($value['title'])?$value['title'] :""?><br>
                            <small>$<?= isset($value['price'])?$value['price'] :""?>/Per <?= isset($value['type'])?$value['type'] :""?></small>
                        </h2>
                        <div style="width: 100px;" class="pull-right">
                            <button class="btn btn-info btn-block">
                                <?php
                                if(isset($value['active']) && $value['active'] == 1)
                                {
                                    ?>Active
                                <?php
                                }else{
                                    ?>Disabled
                                <?php
                                } ?>
                            </button>
                        </div>
                        <?php
                        if(isset($value['country_code'])){
                            ?>
                            <img border="1" alt="<?php echo $value['country_name'] ?>" src="http://images.nationmaster.com/thumb.php?path=images/nm/flags/<?php echo $value['country_code'] ?>-flag.gif">

                            <?php
                        }
                        ?>
                        <p style="font-weight: bold"><?= isset($value['start_date'])?$value['start_date'] :""?> to <?= isset($value['finish_date'])?$value['finish_date'] :""?></p>
                        <p><span class="fa fa-caret-right"></span><?= isset($value['description'])?nl2br($value['description']) :""?></p>

                        <p class="text-muted">For individuals</p>
                    </div>
                    <div class="panel-footer">
                        <a class="btn btn-info btn-block" href="<?php echo base_url('admin/rate/edit') ?>/<?= isset($value['id'])?$value['id'] :0?>">Edit</a>
                        <button onclick="deleteRate('<?= isset($value['id'])?$value['id'] : 0?>')" class="btn btn-danger btn-block">Delete Plan</button>
                    </div>
                </div>

            </div>

            <?php
        }
    }
?>
</div>