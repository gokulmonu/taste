<script>

    function deleteRate(id) {
        $.ajax({
            url: '<?php echo base_url()?>admin/rate/delete',
            type: 'POST',
            data: {'id': id},
            success: function (data) {
                location.href = "<?php echo base_url()?>admin/rates/list";
            },
            error: function (e) {
                //called when there is an error
                //console.log(e.message);
            }
        });
    }
</script>
<div class="col-md-12">
    <a href="<?= base_url('admin/rates/list')?>" class="btn btn-default"><i class="fa fa-arrow-left"></i> Go Back</a>
    <a href="<?php echo base_url('admin/rate/edit') ?>/<?= isset($id) ? $id : 0 ?>" class="btn btn-default"><i class="fa fa-pencil"></i>Edit</a>
    <a href="javascript:;" onclick="deleteRate('<?= isset($id) ? $id : 0 ?>')" class="btn btn-default"><i class="fa fa-trash-o"></i>Delete</a>
</div>


<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading ui-draggable-handle">
        </div>
        <div class="panel-body">
            <table class="table table-bordered">
                <tr>
                    <th width="20%">
                        Name
                    </th>
                    <td>
                        <?= isset($title)?$title:"" ?>
                    </td>
                </tr>
                <tr>
                    <th width="20%">
                        Active
                    </th>
                    <td>
                        <?= $active==1?"Yes":"No" ?>
                    </td>
                </tr>
                <tr>
                    <th width="20%">
                        Country
                    </th>
                    <td>
                        <img border="1" alt="" src="http://images.nationmaster.com/thumb.php?path=images/nm/flags/<?php echo isset($country_code)?$country_code:""; ?>-flag.gif">
                        <br><br>
                        <?= isset($country_name)?$country_name:"" ?>
                    </td>
                </tr>
                <tr>
                    <th width="20%">
                        Type
                    </th>
                    <td>
                        <?= isset($type)?$type:"" ?>
                    </td>
                </tr>
                <tr>
                    <th width="20%">
                        Price
                    </th>
                    <td>
                        $<?= isset($price)?$price:"" ?>
                    </td>
                </tr>
                <tr>
                    <th width="20%">
                        Start Date
                    </th>
                    <td>
                        <?= isset($start_date)?$start_date:"" ?>
                    </td>
                </tr>
                <tr>
                    <th width="20%">
                        Finish Date
                    </th>
                    <td>
                        <?= isset($finish_date)?$finish_date:"" ?>
                    </td>
                </tr>
                <tr>
                    <th width="20%">
                        Description
                    </th>
                    <td>
                        <?= isset($description)?nl2br($description):"" ?>
                    </td>
                </tr>

            </table>
        </div>
    </div>
</div>

