
<script>
    $(function(){
        $('#btn_submit').on('click',function(event){
            event.preventDefault();
            var formData = getRateFormData();
            $.ajax({
                url: '<?php echo base_url()?>admin/rate/validate',
                type: 'POST',
                data:formData,
                async : false,
                cache : false,
                contentType : false,
                processData : false,
                success: function (data) {
                    $('.help-block').html('');
                    if(!data.success){
                        for(key in data){
                            $('#'+data[key]['field_id']).html(data[key]['label']);
                        }
                    }else{
                        location.href="<?php echo base_url()?>admin/rates";
                    }



                },
                error: function (e) {
                    //called when there is an error
                    //console.log(e.message);
                }
            });
        });

        function getRateFormData()
        {
            var formElement = new FormData();
            formElement.append('id',$('#id').val());
            formElement.append('title',$('#title').val());
            if($('#active').is(':checked')){
                formElement.append('active',1);
            }else{
                formElement.append('active',0);
            }
            formElement.append('type',$('#type').val());
            formElement.append('price',$('#price').val());
            formElement.append('country',$('#country').val());
            formElement.append('start_date',$('#start_date').val());
            formElement.append('finish_date',$('#finish_date').val());
            formElement.append('description',$('#description').val());
            return formElement;
        }

    });
</script>
<div class="col-md-12">

    <form class="form-horizontal">
        <input type="hidden" id="id" value="<?= isset($id)?$id:0; ?>">
        <div class="panel panel-default">
            <div class="panel-body">

                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">Active</label>
                    <div class="col-md-6 col-xs-12">
                        <label class="switch">
                            <input type="checkbox" id="active" <?php if(isset($active) && $active){?>checked="checked"<?php } ?> value="" class="switch">
                            <span></span>
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">Title</label>
                    <div class="col-md-6 col-xs-12">
                        <div class="input-group">
                            <span class="input-group-addon"><span style="line-height: 28px;" class="fa fa-pencil"></span></span>
                            <input type="text" id="title" value="<?= isset($title)?$title:""; ?>" class="form-control">
                        </div>
                        <span id="title_error" class="help-block"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">Type</label>
                    <div class="col-md-6 col-xs-12">
                        <div class="input-group">
                            <span class="input-group-addon"><span style="line-height: 28px;" class="fa fa-star-o"></span></span>
                            <select id="type" class="form-control">
                                <option value="Year" <?php if(isset($type) && $type == "Year"){?>selected<?php } ?>>Year</option>
                                <option value="Month" <?php if(isset($type) && $type == "Month"){?>selected<?php } ?>>Month</option>
                                <option value="Week" <?php if(isset($type) && $type == "Week"){?>selected<?php } ?>>Week</option>
                                <option value="Day" <?php if(isset($type) && $type == "Day"){?>selected<?php } ?>>Day</option>
                            </select>
                        </div>
                        <span id="type_error" class="help-block"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">Price</label>
                    <div class="col-md-6 col-xs-12">
                        <div class="input-group">
                            <span class="input-group-addon"><span style="line-height: 28px;" class="fa fa-money"></span></span>
                            <input type="text" id="price" value="<?= isset($price)?$price:""; ?>"  class="form-control">
                        </div>
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">Country</label>
                    <div class="col-md-6 col-xs-12">
                        <select id="country" class="form-control select" data-live-search="true">
                            <?php
                            if(isset($country_list)){
                                foreach($country_list as $c){
                                    ?>
                                    <option <?php
                                    if(isset($country) && $country == $c['id']){
                                        ?>
                                        selected="selected"
                                        <?php
                                    }
                                    ?>
                                        value="<?php echo $c['id'] ?>"><?php echo $c['name'] ?></option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">Start Date</label>
                    <div class="col-md-6 col-xs-12">
                        <div class="input-group">
                        <span class="input-group-addon"><span style="line-height: 28px;" class="fa fa-calendar"></span></span>
                        <input type="text" id="start_date" value="<?= isset($start_date)?$start_date:""; ?>" class="form-control datepicker">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">Finish Date</label>
                    <div class="col-md-6 col-xs-12">
                        <div class="input-group">
                        <span class="input-group-addon"><span style="line-height: 28px;" class="fa fa-calendar"></span></span>
                        <input type="text" id="finish_date" value="<?= isset($finish_date)?$finish_date:""; ?>" class="form-control datepicker">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">Description</label>
                    <div class="col-md-6 col-xs-12">
                        <div class="input-group">
                            <span class="input-group-addon"><span style="line-height: 28px;" class="fa fa-file-text"></span></span>
                            <textarea id="description"  class="form-control"><?= isset($description)?$description:""; ?></textarea>
                        </div>
                        <span class="help-block"></span>
                    </div>
                </div>


            </div>
            <div class="panel-footer">
                <button class="btn btn-default">Clear Form</button>
                <button id="btn_submit" class="btn btn-primary pull-right">Submit</button>
            </div>
        </div>
    </form>

</div>