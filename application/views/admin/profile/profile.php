
<script>
    $(function(){
       $('#btn_submit').on('click',function(event){
           event.preventDefault();
           var formData = getMemberFormData();
           $.ajax({
               url: '<?php echo base_url()?>admin/profile/validate',
               type: 'POST',
               data:formData,
               async : false,
               cache : false,
               contentType : false,
               processData : false,
               success: function (data) {
                   $('.help-block').html('');
                   if(!data.success){
                       for(key in data){
                           $('#'+data[key]['field_id']).html(data[key]['label']);
                       }
                   }else{
                    location.reload();
                   }



               },
               error: function (e) {
                   //called when there is an error
                   //console.log(e.message);
               }
           });
       });

        function getMemberFormData()
        {
            var formElement = new FormData();
            formElement.append('id',$('#id').val());
            formElement.append('position',$('#position').val());
            formElement.append('first_name',$('#first_name').val());
            formElement.append('last_name',$('#last_name').val());
            formElement.append('file',$('#filename')[0].files[0]);
            return formElement;
        }
    });
</script>
<div class="col-md-12">

    <form class="form-horizontal">
        <input type="hidden" id="id" value="<?= isset($id)?$id:0; ?>">
        <div class="panel panel-default">
            <div class="panel-body">

                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">First Name</label>
                    <div class="col-md-6 col-xs-12">
                        <div class="input-group">
                            <span class="input-group-addon"><span style="line-height: 28px;" class="fa fa-pencil"></span></span>
                            <input type="text" id="first_name" value="<?= isset($first_name)?$first_name:""; ?>" class="form-control">
                        </div>
                        <span id="first_name_error" class="help-block"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">Last Name</label>
                    <div class="col-md-6 col-xs-12">
                        <div class="input-group">
                            <span class="input-group-addon"><span style="line-height: 28px;" class="fa fa-pencil"></span></span>
                            <input type="text" id="last_name" value="<?= isset($last_name)?$last_name:""; ?>"  class="form-control">
                        </div>
                        <span id="last_name_error" class="help-block"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">Position</label>
                    <div class="col-md-6 col-xs-12">
                        <div class="input-group">
                            <span class="input-group-addon"><span style="line-height: 28px;" class="fa fa-info"></span></span>
                            <input type="text" id="position" value="<?= isset($position)?$position:""; ?>"  class="form-control">
                        </div>
                        <span class="help-block"></span>
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">File</label>
                    <div class="col-md-6 col-xs-12">
                       <span>Browse file</span><input type="file" title="Browse file" id="filename" name="filename">
                        <span class="help-block">Input type file</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">&nbsp;</label>
                    <div class="col-md-6 col-xs-12">
                    <div class="gallery">
                        <a data-gallery="" title="Profile" href="<?= base_url() ?>uploads/<?= isset($file_ref)?$file_ref:""; ?>" class="gallery-item">
                            <div class="image">
                                <img alt="Profile Image" src="<?= base_url() ?>uploads/<?= isset($file_ref)?$file_ref:""; ?>">
                            </div>
                        </a>
                    </div>
                    </div>
                    </div>

            </div>
            <div class="panel-footer">
                <button class="btn btn-default">Clear Form</button>
                <button id="btn_submit" class="btn btn-primary pull-right">Submit</button>
            </div>
        </div>
    </form>

</div>