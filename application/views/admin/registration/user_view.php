<script>

    function deleteUser(id) {
        $.ajax({
            url: '<?php echo base_url()?>admin/user/delete',
            type: 'POST',
            data: {'id': id},
            success: function (data) {
                location.href = "<?php echo base_url()?>admin/user";
            },
            error: function (e) {
                //called when there is an error
                //console.log(e.message);
            }
        });
    }
</script>
<div class="col-md-12">
    <a href="<?= base_url('admin/user')?>" class="btn btn-default"><i class="fa fa-arrow-left"></i> Go Back</a>
    <a href="javascript:;" onclick="deleteUser('<?= isset($tokup_user_id) ? $tokup_user_id : 0 ?>')" class="btn btn-default"><i class="fa fa-trash-o"></i>Delete</a>
</div>


<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading ui-draggable-handle">
        </div>
        <div class="panel-body">
            <table class="table table-bordered">
                <tr>
                    <th width="20%">
                        Name
                    </th>
                    <td>
                        <?php echo $first_name." ".$last_name; ?>
                    </td>
                </tr>
                <tr>
                    <th width="20%">
                        Username
                    </th>
                    <td>
                        <?php echo $username; ?>
                    </td>
                </tr>
                <tr>
                    <th width="20%">
                        Password
                    </th>
                    <td>
                        <?php echo $password; ?>
                    </td>
                </tr>
                <tr>
                    <th width="20%">
                        Email
                    </th>
                    <td>
                        <?= $email ?>
                    </td>
                </tr>

                <tr>
                    <th width="20%">
                        Phone
                    </th>
                    <td>
                        <?= isset($phone)?$phone:"" ?>
                    </td>
                </tr>
                <tr>
                    <th width="20%">
                        Address
                    </th>
                    <td>
                        <?= isset($street_address_1)?$street_address_1:"" ?>
                    </td>
                </tr>
                <tr>
                    <th width="20%">
                        Message
                    </th>
                    <td>
                        <?= isset($message)?nl2br($message):"" ?>
                    </td>
                </tr>

            </table>
        </div>
    </div>
</div>

