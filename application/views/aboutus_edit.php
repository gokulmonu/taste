
<script>

    $(function () {

        $("#add_member_container").iziModal({
            title: 'Add Members',
            subtitle: 'You can add a new member from here',
            headerColor: '#0caad1',
            width: 600,
            autoOpen: 0,
            fullscreen:true,
            onClosed: function(){
                history.pushState('', document.title, window.location.pathname);

            }
        });

        $("#about_form").submit(function( event ) {
            event.preventDefault();
            var formData = getFormData();
            $.ajax({
                url: '<?php echo base_url()?>aboutus/validate',
                type: 'POST',
                data:  formData,
                success: function (data) {
                    $('#product_name_error').html('');
                    if(data.success){
                        alert('successfully updated');
                        window.location.href ='<?php echo base_url() ?>';
                    }

                    if(!data.success){
                        for(key in data){
                            $('#'+data[key]['field_id']).html(data[key]['label']);
                        }
                    }

                },
                error: function (e) {
                    //called when there is an error
                    //console.log(e.message);
                }
            });

        });

        $("#add_member_form").submit(function( event ) {
            event.preventDefault();
            var formData = getMemberFormData();
            $.ajax({
                url: '<?php echo base_url()?>member/validate',
                type: 'POST',
                data:  formData,
                async : false,
                cache : false,
                contentType : false,
                processData : false,
                success: function (data) {
                    $('#product_name_error').html('');
                    if(data.success){
                        $('#add_member_container').iziModal('close');
                        alert('successfully updated');
                        location.reload();

                    }

                    if(!data.success){
                        for(key in data){
                            $('#'+data[key]['field_id']).html(data[key]['label']);
                        }
                    }

                },
                error: function (e) {
                    //called when there is an error
                    //console.log(e.message);
                }
            });

        });
        $('#add_members').on('click', function (event) {
            event.preventDefault();
            $('#add_member_container').iziModal('open');
        });

        function getMemberFormData()
        {
            var formElement = new FormData();
            formElement.append('member_name',$('#member_name').val());
            formElement.append('member_position',$('#member_position').val());
            formElement.append('member_email',$('#member_email').val());
            formElement.append('member_phone',$('#member_phone').val());
            formElement.append('file',$('#member_photo')[0].files[0]);
            return formElement;
        }
        function getFormData() {

            var data = {};
            data['id'] = $('#id').val();
            data['introduction'] = $('#introduction').val();
            data['mission'] = $('#mission').val();
            data['objectives'] = $('#objectives').val();
            data['principles'] = $('#principles').val();
            return data;
        }

    });

    function deleteMember(memberId) {
        if(confirm("Are you sure want to delete this member?")){
            var postData = {};
            postData['member_id'] = memberId;
            $.ajax({
                url: '<?php echo base_url()?>member/delete',
                type: 'POST',
                data: postData,
                success: function (data) {
                   // location.reload();
                },
                error: function (e) {
                    //called when there is an error
                    //console.log(e.message);
                }
            });
        }else{
            return false;
        }

    }
</script>


<section class="s-wrp s-hi-pad"><!-- section wrp-->
    <div class="s-container"><!-- s-container-->

        <div class="s-wrp"><!--s-wrp-->

            <div class="s-row"><!--s row-->

                <div class="s-col-lg-12 s-col-md-12 s-col-sm-12 s-col-xs-12"><!--s col-->

                    <article class="s-wrp edit-block">

                        <h2 class="main-title">Add / Edit About Us</h2>

                    </article>
                </div>
                <!--/. s col-->

            </div>
            <!--/. s row-->

        </div>
        <!--/. s-wrp-->
    </div>
    <!--/. s-container-->
</section>
<!--/. section wrp-->


<section class="s-wrp"><!-- section wrp-->
    <div class="s-container"><!-- s-container-->

        <div class="s-wrp"><!--s-wrp-->

            <div class="s-row"><!--s row-->

                <div class="s-col-lg-12 s-col-md-12 s-col-sm-12 s-col-xs-12"><!--s col-->


                    <form id="about_form" class="custom-form">
                        <input type="hidden" id="id" value="<?php if(isset($data[0]['id']))echo $data[0]['id']; ?>">
                        <div class="s-form-group">
                            <label for="title">Introduction:</label>
                            <textarea id="introduction" name="introduction" placeholder="Enter Introduction"><?php if(isset($data[0]['introduction']))echo $data[0]['introduction']; ?></textarea>
                            <span class="error-msg" id="introduction_error"></span>
                        </div>

                        <div class="s-form-group">
                            <label for="mission">Mission:</label>
                            <textarea id="mission"  placeholder="Enter  Mission" name="mission"><?php if(isset($data[0]['mission']))echo $data[0]['mission']; ?></textarea>
                            <span class="error-msg" id="mission_error"></span>
                        </div>

                        <div class="s-form-group">
                            <label for="objectives">Objectives:</label>
                            <textarea id="objectives" name="objectives" placeholder="Enter Objectives"><?php if(isset($data[0]['objectives']))echo $data[0]['objectives']; ?></textarea>
                            <span class="error-msg" id="objectives_error"></span>
                        </div>

                        <div class="s-form-group">
                            <label for="principles">Principles:</label>
                            <textarea  id="principles" name="principles" placeholder="Enter Principles" ><?php if(isset($data[0]['principles']))echo $data[0]['principles']; ?></textarea>
                            <span class="error-msg" id="experiance_error"></span>
                        </div>

                        <div class="s-form-group s-wrp">
                            <input type="submit" class="pg-btn" value="Save">
                        </div>
                    </form>


                </div>
                <!--/. s col-->

            </div>
            <!--/. s row-->

        </div>
        <!--/. s-wrp-->
    </div>
    <!--/. s-container-->
</section>
<!--/. section wrp-->
<section class="s-wrp">
    <div class="s-container"><!-- s-container-->
        <div class="s-row"><!--s row-->
            <div class="s-col-lg-12 s-col-md-12 s-col-sm-12 s-col-xs-12"><!--s col-->
                <div class="s-wrp button-sec">
                    <a class="page-btn " href="javascript:;" id="add_members"><i class="fa fa-plus"></i>Add Members</a>
                </div>
            </div>
        </div>
        <div class="s-row"><!--s row-->
            <div class="tbl-box f-wrp"><!--tbl box-->

                <div class="scroll-div">

                    <table class="table-responsive normal-table custom-table">
                        <thead>
                        <tr>
                            <th>NAME</th>
                            <th>POSITION</th>
                            <th>EMAIL</th>
                            <th>ACTION</th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(isset($members) && count($members)){
                            foreach ($members as $value){
                                ?>
                                <tr>
                                    <td><?php echo $value['name'] ?></td>
                                    <td><?php echo $value['position'] ?></td>
                                    <td><?php echo $value['email'] ?></td>
                                    <td>
                                        <a class="action-btn" href="<?php echo base_url()?>member/edit/<?php echo  $value['id'] ?>" title="edit details"><i class="fa fa-edit"></i></a>
                                        <a class="action-btn" href="javascript:;" title="delete" onclick="deleteMember('<?php echo $value['id'] ?>')"><i class="fa fa-trash"></i></a>
                                    </td>


                                </tr>
                                <?php
                            }
                        }else{
                            ?>
                            <td>No results Found</td>
                            <?php
                        }

                        ?>

                        </tbody>

                    </table>

                </div>

            </div>

        </div>
    </div>
</section>
<div id="add_member_container">
    <section class="s-wrp"><!-- section wrp-->
        <div class="s-container"><!-- s-container-->

            <div class="s-wrp"><!--s-wrp-->

                <div class="s-row"><!--s row-->

                    <div class="s-col-lg-12 s-col-md-12 s-col-sm-12 s-col-xs-12"><!--s col-->


                        <form id="add_member_form" class="custom-form" enctype="multipart/form-data" accept-charset="utf-8">
                             <div class="s-form-group">
                                <label for="member_name">Name:</label>
                                <input type="text" id="member_name" name="member_name" placeholder="Enter Name"/>
                                <span class="error-msg" id="member_name_error"></span>
                            </div>
                            <div class="s-form-group">
                                <label for="member_position">Position:</label>
                                <input type="text" id="member_position" name="member_position" placeholder="Enter position"/>
                                <span class="error-msg" id="member_position_error"></span>
                            </div>
                            <div class="s-form-group">
                                <label for="member_email">Email:</label>
                                <input type="text" id="member_email" name="member_email" placeholder="Enter Email"/>
                                <span class="error-msg" id="member_email_error"></span>
                            </div>
                            <div class="s-form-group">
                                <label for="member_phone">Phone:</label>
                                <input type="text" id="member_phone" name="member_phone" placeholder="Enter Phone Number"/>
                                <span class="error-msg" id="member_phone_error"></span>
                            </div>
                            <div class="s-form-group">
                                <label for="member_photo">Image:</label>
                                <input type="file" id="member_photo" name="member_photo"/>
                                <span class="error-msg" id="member_photo_error"></span>
                            </div>

                            <div class="s-form-group s-wrp">
                                <input type="submit" class="pg-btn" value="Save">
                            </div>

                        </form>


                    </div>
                    <!--/. s col-->

                </div>
                <!--/. s row-->

            </div>
            <!--/. s-wrp-->
        </div>
        <!--/. s-container-->
    </section>

</div>
