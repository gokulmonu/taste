
<script>
    jQuery(function(){
        $('#links').on('click',function(event){
            event = event || window.event;
            var target = event.target || event.srcElement;
            var link = target.src ? target.parentNode : target;
            var options = {index: link, event: event,onclosed: function(){
                setTimeout(function(){
                    $("body").css("overflow","");
                },200);
            }};
            var links = this.getElementsByTagName('a');
            blueimp.Gallery(links, options);
        })
    });
</script>

<div class="page-content-wrap bg-light">
    <!-- page content holder -->
    <div class="page-content-holder no-padding">
        <!-- page title -->
        <div class="page-title">
            <h1>Apps Listing</h1>
            <!-- breadcrumbs -->
            <ul class="breadcrumb">
                <li><a href="<?=base_url(); ?>">Home</a></li>
                <li><a href="<?=base_url(); ?>apps">Apps</a></li>
                <li><a href="javascript:;">Apps</a></li>
            </ul>
            <!-- ./breadcrumbs -->
        </div>
        <!-- ./page title -->
    </div>
    <!-- ./page content holder -->
</div>
<!-- page content wrapper -->
<div class="page-content-wrap">
    <!-- page content holder -->
    <div class="page-content-holder padding-v-30">

        <div class="row">
            <div class="col-md-9">
                <div id="links" class="gallery">
                    <?php
                    if(isset($images) &&  count($images) ){
                        foreach($images as $value){
                            ?>

                            <a data-gallery="" title="<?= $value['file_name'] ?>" href="<?php echo $value['thumbnail'] ?>" class="gallery-item">
                                <div class="image">
                                    <img alt="<?= $value['file_name'] ?>" src="<?php echo $value['thumbnail'] ?>">
                                </div>
                            </a>
                            <?php
                        }
                    }

                    ?>
                </div>
                <div class="blog-content">
                    <h2><?= isset($name)?$name:""; ?></h2>
                <p><?= isset($description)?$description:""; ?></p>
                </div>
            </div>
            <div class="col-md-3">

                <div class="text-column this-animate" data-animate="fadeInRight">
                    <a href="<?= isset($url)?$url:""; ?>">
                    <img width="90%" height="90%" src="<?php echo base_url('bootstrap/images/web/get-it-on-google-play.png');?>" />
                    </a>
                </div>
            </div>
        </div>

    </div>
    <!-- ./page content holder -->

    <!-- BLUEIMP GALLERY -->
    <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
        <div class="slides"></div>
        <h3 class="title"></h3>
        <a class="prev">‹</a>
        <a class="next">›</a>
        <a class="close">×</a>
        <a class="play-pause"></a>
        <ol class="indicator"></ol>
    </div>
    <!-- END BLUEIMP GALLERY -->
</div>
<!-- ./page content wrapper -->