<?php

class Invoice_model extends MY_Model
{
    function __construct()
    {
        //$this->CI = & get_instance();
        parent::__construct();
        $this->load->library('Base', 'base');

    }

    protected $tableName = "sb_invoice";

    public $category = [];
    public $formFields = [
        'id' => 'integer',
        'total_price' => 'decimal',
        'status' => 'integer',//0-pending,1-accepted,2-completed
        'customer_name' => 'string',
        'customer_address' =>'text',
        'gst' => 'string',
        'date'=> 'date'
    ];

    public function add($dataArr)

    {
        $resultArr = $this->base->add($dataArr, $this->formFields);
        if ($resultArr) {
            $this->db->insert($this->tableName, $resultArr);
            return $this->db->insert_id();
        } else {
            return false;
        }

    }

    public function update($dataArr, $whereArr)
    {
        $whereArrSanitize = $this->base->validate($whereArr, $this->formFields);
        $resultArr = $this->base->update($dataArr, $this->formFields);
        if (count($resultArr) && count($whereArrSanitize)) {
            $this->db->update($this->tableName, $resultArr, $whereArrSanitize);
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    public function delete($whereArr, $strongDelete = false)
    {
        $whereArrSanitize = $this->base->validate($whereArr, $this->formFields);
        if (isset($whereArrSanitize) && !empty($whereArrSanitize)) {
            if ($strongDelete) {
                $this->db->delete($this->tableName, $whereArrSanitize);
            } else {
                $resultArr = $this->base->delete();
                $this->db->update($this->tableName, $resultArr, $whereArrSanitize);
            }
        }
    }

    public function getSearchQuery($params = [],$startDate = "",$finishDate = "")
    {
        $sql = "SELECT * FROM $this->tableName c where c.is_deleted is null";
        if (isset($params['phrase']) && !empty($params['phrase'])) {
            $sql.= " and c.customer_name like '%".$params['phrase'] ."%'";
            $sql.= " or c.customer_address like '%".$params['phrase'] ."%'";
        }
        if($startDate){
            $sql.= " and c.date>='$startDate'";
        }
        if($finishDate){
            $sql.= " and c.date<='$finishDate'";
        }
        return $sql;


    }

}