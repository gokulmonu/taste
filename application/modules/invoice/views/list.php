<style>
    .modal-dialog{
        width:1000px;
    }
</style>
<script>
    jQuery(function () {
        <?php if(isset($_graph) && !empty($_graph)): ?>
        Morris.Line({
            element: 'morris-line-example',
            data: <?php echo $_graph ?>,
            xkey: 'y',
            ykeys: ['a'],
            labels: ['Sale Value'],
            resize: true,
            lineColors: ['#33414E']
        });
        <?php endif; ?>
        jQuery('#search_btn').on('click', function () {
            var startDate = jQuery('#start_date').val();
            var finishDate = jQuery('#finish_date').val();
            location.href = "<?php echo base_url()?>invoice?start_date=" + startDate + "&finish_date=" + finishDate;
        });
       jQuery('#btn_form_add').on('click',function () {
           jQuery.ajax({
               url: '<?php echo base_url()?>invoice/add',
               type: 'POST',
               dataType: "html",
               data:{
                   'id':""
               },
               success: function (data) {
                   jQuery('#invoice_form_container').html(data);
                   addListner();
               },
               error: function (e) {
                   //called when there is an error
                   //console.log(e.message);
               }
           });
       });
        jQuery('.invoice_edit_btn').on('click',function () {
            jQuery('#btn_form_add').trigger('click');
            editInvoice(jQuery(this).attr('data-content'));
        });

        function editInvoice(id)
        {
            jQuery.ajax({
                url: '<?php echo base_url()?>invoice/edit/'+id,
                type: 'POST',
                dataType: "html",
                data:{
                    'id':""
                },
                success: function (data) {
                    jQuery('#invoice_form_container').html(data);
                    addListner();
                },
                error: function (e) {
                    //called when there is an error
                    //console.log(e.message);
                }
            });
        }

        function addListner() {
            jQuery('#btn_new_row').on('click', function () {
                var cloneContent = jQuery('#clone_container').html();
                //cloneContent.find('.name_input_cls').chosen({width: "95%"});
                jQuery('#invoice_item_container').append(cloneContent);
                jQuery('#invoice_item_container').find('tr:last').find('.name_input_cls').chosen({width: "95%"});
               // jQuery(".chosen-select").chosen({width: "95%"});
                addCloseEvent();
            });
            addCloseEvent();
            addSubmitEvent();
        }

        function addCloseEvent() {
            jQuery('.remove_btn_cls').on('click', function () {
                jQuery(this).closest('tr.item_cls').remove();
                updatePrice();
            });
              jQuery('.item_input_cls').on('change', function () {
                  updatePrice();

            });
            jQuery('.quantity_input_cls').on('change', function () {
                  updatePrice();
            });
            jQuery('.quantity_input_cls').on('keyup', function () {
                  updatePrice();
            });
            jQuery('.price_input_cls').on('keyup', function () {
                  updatePrice();
            });


        }

        function addSubmitEvent() {
            $("#btn_submit").click(function (event) {
                event.preventDefault();
                var formData = getFormData();
                $('#error_container').hide();
                $.ajax({
                    url: '<?php echo base_url()?>invoice/validate',
                    type: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify(formData),
                    success: function (data) {
                        if (data.success) {
                            location.reload();
                              }

                        if (!data.success) {
                            var alertMsg = "";
                            for (var key in data) {
                                if (data.hasOwnProperty(key)) {
                                    if (typeof data[key]['label'] !== "undefined") {
                                        alertMsg += data[key]['label'] + "<br>";
                                    }
                                }

                            }
                            $('#error_container').html(alertMsg).show();
                        }

                    },
                    error: function (e) {
                    }
                });

            });
        }

        function updatePrice() {
            var totalPrice = 0;
            jQuery('#invoice_item_container').find('tr').each(function () {
                var price = jQuery(this).find('.item_input_cls option:selected').attr('data-content');
                var quantity = jQuery(this).find('.quantity_input_cls').val();
                var total = price * quantity;
                totalPrice += total;
                jQuery(this).find('.price_input_cls').val(total);
            });
            var gst = (12 * totalPrice) / 100;
            totalPrice += gst;
            jQuery('#total_cost').html(totalPrice);
        }

        function getFormData() {

            var data = {};
            data['id'] = $('#id').val();
            data['customer_name'] = $('#customer_name').val();
            data['customer_address'] = $('#customer_address').val();
            data['date'] = $('#invoice_date').val();
            data['total_price'] = jQuery('#total_cost').html();
            var dataArr = [];
            jQuery('#invoice_item_container').find('tr').each(function () {
                var temp = {};
                temp['id'] = jQuery(this).find('.invoice_item_id').val();
                temp['category_item_id'] = jQuery(this).find('.item_input_cls').val();
                temp['quantity'] = jQuery(this).find('.quantity_input_cls').val();
                temp['price'] = jQuery(this).find('.price_input_cls').val();
                dataArr.push(temp);
            });
            data['invoice_items'] = JSON.stringify(dataArr);
            return data;
        }

    });

</script>
<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="col-md-6 col-xs-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Toolbar</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span style="line-height: 29px;"
                                                                     class="fa fa-angle-down"></span></a></li>
                        <li><a href="#" class="panel-remove"><span style="line-height: 29px;"
                                                                   class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <button id="btn_form_add" class="btn btn-default" data-toggle="modal" data-target="#modal_basic">
                            <i class="glyphicon fa fa-plus"></i>Add Invoice
                        </button>

                    </div>

                    <div class="col-md-6 col-xs-6">
                        <div class="form-group">
                            <div class="input-group">
                            <span class="input-group-addon"><span style="line-height:2"
                                                                  class="fa fa-calendar"></span></span>
                                <input id="start_date" type="text" class="form-control datepicker"
                                       value="<?=isset($_start_date)?$_start_date:"" ?>"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <div class="form-group">
                            <div class="input-group">
                            <span class="input-group-addon"><span style="line-height:2"
                                                                  class="fa fa-calendar"></span></span>
                                <input id="finish_date" type="text" class="form-control datepicker"
                                       value="<?=isset($_finish_date)?$_finish_date:"" ?>"/>
                            </div>
                        </div>
                        <button id="search_btn" class="btn btn-success pull-right">
                            <i class="glyphicon fa fa-search"></i>Search
                        </button>
                    </div>

                </div>
                <div class="pull-left">

                    <div class="widget widget-primary">
                        <div class="widget-title">TOTAL</div>
                        <div class="widget-subtitle">Sales</div>
                        <div class="widget-int">Rs <span data-to="1564" data-toggle="counter"><?=$_total ?></span></div>
                    </div>

                </div>
            </div>

        </div>
        <!-- END DEFAULT BUTTONS -->
        <div class="col-md-6 col-xs-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Sales Chart</h3>
                </div>
                <div class="panel-body">
                    <div id="morris-line-example" style="height: 300px;"></div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">

        <div class="btn-group pull-right">
            <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i
                    class="fa fa-bars"></i> Export
                Data
            </button>
            <ul class="dropdown-menu">
                <li><a href="#"
                       onClick="$('#customers2').tableExport({type:'csv',escape:'false'});"><img
                            src='<?= base_url() ?>bootstrap/images/admin/icons/csv.png' width="24"/>
                        CSV</a></li>
                <li><a href="#"
                       onClick="$('#customers2').tableExport({type:'txt',escape:'false'});"><img
                            src='<?= base_url() ?>bootstrap/images/admin/icons/txt.png' width="24"/>
                        TXT</a></li>
                <li class="divider"></li>
                <li><a href="#"
                       onClick="$('#customers2').tableExport({type:'excel',escape:'false'});"><img
                            src='<?= base_url() ?>bootstrap/images/admin/icons/xls.png' width="24"/>
                        XLS</a></li>
                <li><a href="#"
                       onClick="$('#customers2').tableExport({type:'doc',escape:'false'});"><img
                            src='<?= base_url() ?>bootstrap/images/admin/icons/word.png'
                            width="24"/> Word</a></li>
                <li><a href="#"
                       onClick="$('#customers2').tableExport({type:'powerpoint',escape:'false'});"><img
                            src='<?= base_url() ?>bootstrap/images/admin/icons/ppt.png' width="24"/>
                        PowerPoint</a>
                </li>
                <li class="divider"></li>
                <li><a href="#"
                       onClick="$('#customers2').tableExport({type:'png',escape:'false'});"><img
                            src='<?= base_url() ?>bootstrap/images/admin/icons/png.png' width="24"/>
                        PNG</a></li>
                <li><a href="#"
                       onClick="$('#customers2').tableExport({type:'pdf',escape:'false'});"><img
                            src='<?= base_url() ?>bootstrap/images/admin/icons/pdf.png' width="24"/>
                        PDF</a></li>
            </ul>
        </div>

    </div>
    <div class="panel-body">
        <table id="customers2" class="table datatable">
            <thead>
            <tr>
                <th>Name</th>
                <th>Address</th>
                <th>Date</th>
                <th>Total Price</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>

            <?php
            if (isset($data['rows']) && count($data['rows'])) {
                foreach ($data['rows'] as $value) {
                    ?>
                    <tr>
                        <td>
                            <?= $value['customer_name'] ?></td>
                        <td><?= $value['customer_address'] ?></td>
                        <td><?= $value['date'] ?></td>
                        <td><?= $value['total_price'] ?></td>
                        <td>
                            <a style="padding: 3px 1px;" class="btn invoice_edit_btn" data-content="<?= isset($value['id']) ? $value['id'] : 0 ?>"
                               href="javascript:;" ><i
                                    class="fa fa-pencil"></i></a>
                            <a style="padding: 3px 1px;" class="btn"
                               href="<?php echo base_url('invoice/print') ?>/<?= isset($value['id']) ? $value['id'] : 0 ?>"><i
                                    class="fa fa-print"></i></a>
                        </td>
                    </tr>

                    <?php
                }
            }
            ?>


            </tbody>
        </table>

    </div>
</div>

<!-- MODALS -->
<div class="modal" id="modal_basic" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="defModalHead">Add Invoice</h4>
            </div>
            <div class="modal-body" id="invoice_form_container" style="height: 400px;overflow-y:scroll">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="close_modal" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
