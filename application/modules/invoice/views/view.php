<script>
    function deleteSlider(sliderId) {
        if (confirm("Are you sure want to delete this slider?")) {
            var postData = {};
            postData['id'] = sliderId;
            $.ajax({
                url: '<?php echo base_url()?>slider/delete',
                type: 'POST',
                data: postData,
                success: function (data) {
                    window.location.href = '<?php  echo base_url() . "slider"?>';
                },
                error: function (e) {
                    //called when there is an error
                    //console.log(e.message);
                }
            });
        } else {
            return false;
        }

    }
</script>

<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="col-md-6 col-xs-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Toolbar</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span style="line-height: 29px;"
                                                                     class="fa fa-angle-down"></span></a></li>
                        <li><a href="#" class="panel-remove"><span style="line-height: 29px;"
                                                                   class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="form-group">

                        <button
                            onclick="location.href='<?php echo base_url() ?>slider/edit/<?php echo $details['id'] ?>';"
                            class="btn btn-info"><i class="glyphicon fa fa-edit"></i> Edit
                        </button>
                        <button onclick="deleteSlider('<?php echo $details['id'] ?>')"
                                class="btn btn-info"><i class="glyphicon fa fa-trash-o"></i> Delete
                        </button>
                        <button onclick="location.href='<?php echo base_url() ?>slider';" class="btn btn-info"><i
                                class="glyphicon fa fa-chevron-circle-left"></i> Back
                        </button>
                    </div>

                </div>
            </div>
        </div>
        <!-- END DEFAULT BUTTONS -->
        <!-- START TABS -->
        <div class="panel panel-default tabs">
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#tab-first" role="tab" data-toggle="tab">Details</a></li>
            </ul>
            <div class="panel-body tab-content">
                <div class="tab-pane active" id="tab-first">
                    <p>
                        <span><b>Name: </b><?php echo $details['name'] ?></span><br>
                        <span><b>URL: </b><?php echo $details['url'] ?></span><br>
                        <span><b>Active: </b><?php echo ($details['active']) == 1 ? "yes" : "no" ?></span><br>
                        <span><b>Description: </b><?php echo $details['description'] ?></span><br>
                    </p>
                    <?php if(isset($details['file_ref'])): ?>


                            <div class="gallery">
                                <a data-gallery="" title="Profile" href="<?= base_url() ?>uploads/<?= isset($details['file_ref'])?$details['file_ref']:""; ?>" class="gallery-item">
                                    <div class="image">
                                        <img alt="Profile Image" src="<?= base_url() ?>uploads/<?= isset($details['file_ref'])?$details['file_ref']:""; ?>">
                                    </div>
                                </a>
                            </div>

                    <?php endif; ?>
                </div>
            </div>
        </div>
        <!-- END TABS -->
    </div>
</div>