<script>
    jQuery(function(){
        jQuery(".chosen-select").chosen({width: "95%"});
        jQuery('#invoice_date').datepicker({format:'yyyy-mm-dd'});
    });
</script>
<div class="panel-body" xmlns="http://www.w3.org/1999/html">

    <div class="row">

        <div class="col-md-6">
            <input type="hidden" value="<?= isset($id)?$id:""; ?>" id="id"/>
            <div class="form-group">
                <label class="col-md-3 control-label">Name</label>
                <div class="col-md-9">
                    <div class="input-group">
                        <span class="input-group-addon"><span style="line-height:2" class="fa fa-user"></span></span>
                        <input id="customer_name" value="<?= isset($customer_name)?$customer_name:""; ?>" type="text" class="form-control"/>
                    </div>
                    <span class="help-block"></span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label">Address</label>
                <div class="col-md-9 col-xs-12">
                    <textarea id="customer_address" class="form-control" rows="3"><?= isset($customer_address)?$customer_address:""; ?></textarea>
                    <span class="help-block"></span>
                </div>
            </div>

        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-3 control-label">Invoice ID</label>
                <div class="col-md-9">
                    <div class="input-group">
                        <span class="input-group-addon"><span style="line-height:2" class="fa fa-pencil"></span></span>
                        <input value="<?= isset($id)?$id:""; ?>" type="text" class="form-control"/>
                    </div>
                    <span class="help-block"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Date</label>
                <div class="col-md-9">
                    <div class="input-group">
                        <span class="input-group-addon"><span style="line-height:2" class="fa fa-calendar"></span></span>
                        <input id="invoice_date" type="text" class="form-control datepicker" value="<?= isset($date)?$date:""; ?>"/>
                    </div>
                    <span class="help-block"></span>
                </div>
            </div>

        </div>
        <div class="col-md-12">
            <table class="table">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody id="invoice_item_container">

                <?php if(isset($invoice_items) && !empty($invoice_items)):?>
                    <?php foreach ($invoice_items as $t) : ?>
                        <tr class="item_cls">
                            <input type="hidden" value="<?=$t['invoice_item_id'] ?>" class="invoice_item_id"/>
                            <td class="name_cls">
                                <select class="chosen-select item_input_cls" >
                                    <option data-content="" value="">select</option>
                                    <?php foreach ($items as $itm): ?>
                                        <option <?php
                                        if($itm['id'] == $t['category_item_id']){
                                            ?>
                                            selected="selected"
                                            <?php
                                        }

                                        ?>  data-content="<?=$itm['price'] ?>" value="<?=$itm['id'] ?>"><?=$itm['title'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </td>
                            <td class="quantity_cls">
                                <input type="number" placeholder="Quantity" class="form-control quantity_input_cls"
                                       value="<?=$t['invoice_quantity'] ?>"/>
                            </td>
                            <td class="price_cls">
                                <input type="text" placeholder="Price" class="form-control price_input_cls"
                                       value="<?=$t['invoice_price'] ?>"/>
                            </td>
                            <td class="remove_cls">
                                <button class="btn btn-warning remove_btn_cls">Remove</button>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                <?php endif; ?>
                <?php if(empty($invoice_items)): ?>
                <tr class="item_cls">
                    <input type="hidden" value="" class="invoice_item_id"/>
                    <td class="name_cls">
                        <select class="chosen-select item_input_cls" >
                            <option data-content="" value="">select</option>
                            <?php foreach ($items as $itm): ?>
                            <option data-content="<?=$itm['price'] ?>" value="<?=$itm['id'] ?>"><?=$itm['title'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                    <td class="quantity_cls">
                        <input type="number" placeholder="Quantity" class="form-control quantity_input_cls"
                               value=""/>
                    </td>
                    <td class="price_cls">
                        <input type="text" placeholder="Price" class="form-control price_input_cls"
                               value=""/>
                    </td>
                    <td class="remove_cls">
                        <button class="btn btn-warning remove_btn_cls">Remove</button>
                    </td>
                </tr>
                <?php endif; ?>
                </tbody>
            </table>
            <div style="display: none">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Quantity</th>
                        <th>Price</th>
                    </tr>
                    </thead>
                    <tbody id="clone_container">
                    <tr class="item_cls">
                        <input type="hidden" value="" class="invoice_item_id"/>
                        <td class="name_cls">
                            <select class="name_input_cls item_input_cls" >
                                <option data-content="" value="">select</option>
                                <?php foreach ($items as $itm): ?>
                                    <option data-content="<?=$itm['price'] ?>" value="<?=$itm['id'] ?>"><?=$itm['title'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                        <td class="quantity_cls">
                            <input type="number" placeholder="Quantity" class="form-control quantity_input_cls"
                                   value=""/>
                        </td>
                        <td class="price_cls">
                            <input type="text" placeholder="Price" class="form-control price_input_cls"
                                   value=""/>
                        </td>
                        <td class="remove_cls">
                            <button class="btn btn-warning remove_btn_cls">Remove</button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <button id="btn_new_row" class="btn btn-success pull-left">New Row</button>
            <div class="pull-right">
                <span style="font-weight: bold">GST: 12%</span><br><br>
                <span style="font-weight: bold">Total: Rs</span><span id="total_cost">
                    <?php if(isset($total_price)): ?>
                        <?= $total_price ?>
                    <?php else: ?>
                    0.00
                    <?php endif;?>
                </span>
            </div>
        </div>

    </div>
    <div class="panel-footer">
        <button id="btn_submit" class="btn btn-primary pull-right">Submit</button>
    </div>
</div>
