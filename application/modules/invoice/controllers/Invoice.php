<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

include_once APPPATH . '/third_party/phpwkhtmltopdf/src/Pdf.php';
use mikehaertl\wkhtmlto\Pdf;

class Invoice extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('core_lib');
        $this->load->library('form');
    }

    public function listing()
    {
        $startDate = isset($_REQUEST['start_date'])?$_REQUEST['start_date']:"";
        $finishDate = isset($_REQUEST['finish_date'])?$_REQUEST['finish_date']:"";
        $result = $this->invoiceSearch();
        $total=0.00;
        if (isset($result['rows']) && !empty($result['rows'])) {
            foreach ($result['rows'] as $r) {
                $total += $r['total_price'];
            }

        }
        $resultArr['data'] = $result;
        $resultArr['_start_date'] = $startDate;
        $resultArr['_finish_date'] = $finishDate;
        $resultArr['_total'] = $total;
        $graphData = $this->getGraphData();

        $resultArr['_graph'] = json_encode($graphData);
        //echo '<pre>';print_r($resultArr['_graph'] );die();
        $pageTitle = [];
        $pageTitle['title'] = "Invoice List";
        $pageTitle['icon'] = "fa fa-list";
        $resultArr['page_header'] = $pageTitle;
        $this->load->moduleTemplate('invoice/list', $resultArr);
    }

    protected function invoiceSearch()
    {
        $postData = [
            'page' => $this->input->post('page'),
            'phrase' => $this->input->post('phrase')
        ];
        $startDate = isset($_REQUEST['start_date'])?$_REQUEST['start_date']:"";
        $finishDate = isset($_REQUEST['finish_date'])?$_REQUEST['finish_date']:"";
        $this->load->model('invoice/invoice_model');
        $query = $this->invoice_model->getSearchQuery($postData,$startDate,$finishDate);
        $result = $this->invoice_model->getListByQuery($query, 1, 9999);
        return $result;
    }

    private function getGraphData()
    {
        $months = [];
        $date = date('Y-m-d', strtotime("first day of this month"));
        $months[] = $date;
        $months[] = date('Y-m-d', strtotime($date . " -1 month"));
        $months[] = date('Y-m-d', strtotime($date . " -2 month"));
        $months[] = date('Y-m-d', strtotime($date . " -3 month"));
        $months[] = date('Y-m-d', strtotime($date . " -4 month"));
        $months[] = date('Y-m-d', strtotime($date . " -5 month"));
        $results = [];
        $arr = [];
        foreach ($months as $m) {
            $this->load->model('invoice/invoice_model');
            $lastDay = date('Y-m-t', strtotime($m));
            $query = $this->invoice_model->getSearchQuery([], $m, $lastDay);
            $result = $this->invoice_model->getListByQuery($query, 1, 9999);
            $total = 0.00;
            if (isset($result['rows']) && !empty($result['rows'])) {
                foreach ($result['rows'] as $r) {
                    $total += $r['total_price'];
                }

            }
            $results[date('Y-m-d', strtotime($m))] = $total;
        }
        foreach ($results as $k => $v) {
            $arr[] = ["y" => $k, "a" => $v];
        }
        return $arr;
    }


    public function add()
    {
        $invoiceDetails = [];
        $mode = $this->uri->segment(2);
        if ($mode == "edit") {
            $id = $this->uri->segment(3);
            if ($id) {
                $invoiceDetails = $this->getInvoiceData($id);
            }
        }
        $this->load->model('category/category_item_model');
        $items = $this->category_item_model->getAll();
        $invoiceDetails['items'] = $items;
        $this->load->view('invoice/edit', $invoiceDetails);
    }

    public function getInvoiceData($invoiceId)
    {
        $this->load->model('invoice/invoice_model');
        $invoiceDetails = $this->invoice_model->getData($invoiceId);
        $this->load->model('invoice/invoice_item_model');
        $invoiceDetails['invoice_items'] = $this->invoice_item_model->getInvoiceItems($invoiceId);
        return $invoiceDetails;
    }

    public function validate()
    {
        $postData = json_decode(file_get_contents('php://input'), true);
        $invoiceItems = json_decode($postData['invoice_items'], true);
        $this->form->setRules($postData['customer_name'], 'customer_name', 'Please enter customer name');
        $validationArr = $this->form->run();
        if (is_array($validationArr) && count($validationArr) && $validationArr != false) {
            header('Content-Type: application/json');
            echo json_encode($validationArr);
        } else {
            $postData['gst'] = 12;
            $this->load->model('invoice/invoice_model');
            if (isset($postData['id']) && !empty($postData['id'])) {
                $temp = [
                    'id' => $postData['id']
                ];
                //echo '<pre>';print_r($postData);die();
                $this->invoice_model->update($postData, $temp);
            } else {
                $invoiceId = $this->invoice_model->add($postData);

            }
            if (!isset($invoiceId) && empty($invoiceId)) $invoiceId = $postData['id'];
            if ($invoiceId) {
                $newIds = [];
                $existingIds = [];
                $this->load->model('invoice/invoice_item_model');
                $itemsList = $this->invoice_item_model->getInvoiceItems($invoiceId);
                if ($itemsList) {
                    $existingIds = array_filter(array_column($itemsList, 'category_item_id'));
                }
                foreach ($invoiceItems as $item) {
                    if (isset($item['category_item_id']) && !empty($item['category_item_id'])) {
                        $newIds[] = $item['category_item_id'];
                    }
                    $item['invoice_id'] = $invoiceId;
                    $this->load->model('invoice/invoice_item_model');
                    if (isset($item['id']) && !empty($item['id'])) {
                        $temp = [
                            'id' => $item['id']
                        ];
                        $this->invoice_item_model->update($item, $temp);
                    } else {
                        $this->invoice_item_model->add($item);
                    }

                }
                $deletedItems = array_diff($existingIds, $newIds);
                if ($deletedItems) {
                    $this->load->model('invoice/invoice_item_model');
                    foreach ($deletedItems as $c) {
                        $del = $this->invoice_item_model->loadByFields(['category_item_id' => $c, 'invoice_id' => $invoiceId]);
                        if ($del) {
                            $this->invoice_item_model->delete(['id' => $del['id']]);
                        }
                    }

                }
            }
            $result = ['id' => $invoiceId, 'success' => true];
            header('Content-Type: application/json');
            echo json_encode($result);
        }


    }

    public function makePdf()
    {
        $id = $this->uri->segment(3);
        $content = $this->load->view('about_us', [], true);
        $pdf = new Pdf([
            'ignoreWarnings' => true,
        ]);
        $pdf->addPage("<html>" . $content . "</html>");
        $pdf->binary = 'C:\Program Files\wkhtmltopdf\bin\wkhtmltopdf.exe';

        if (!$pdf->saveAs('C:\xampp\htdocs\taste\temp123.pdf')) {
            echo $pdf->getError();
        }
        //echo '<pre>';print_r($content);die();
        if ($id) {
            $this->load->model('invoice/invoice_model');
            $invoiceDetails = $this->invoice_model->getData($id);
            $outputData = [
                'details' => $invoiceDetails
            ];
            $bc = [];
            $bc['category'] = "Invoice";
            $bc['category/view/' . $id] = "View";
            $outputData['breadcrumbs'] = $bc;

            $pageTitle = [];
            $pageTitle['title'] = "Invoice View";
            $pageTitle['icon'] = "fa fa-list";
            $outputData['page_header'] = $pageTitle;
            $this->load->moduleTemplate('invoice/view', $outputData);
        }
    }

    public function delete()
    {
        $invoiceId = $this->input->post('id');
        if ($invoiceId) {
            $this->load->model('invoice/invoice_model', 'invoice_model');
            $this->invoice_model->delete(['id' => $invoiceId]);
        }
    }


}