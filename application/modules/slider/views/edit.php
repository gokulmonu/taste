<script>
    $(function () {
        $("#slider_form").submit(function (event) {
            event.preventDefault();
            var formData = getFormData();
            $.ajax({
                url: '<?php echo base_url()?>slider/validate',
                type: 'POST',
                data: formData,
                async : false,
                cache : false,
                contentType : false,
                processData : false,
                success: function (data) {
                    $('.help-block').html('');
                    if (data.success) {
                        if (data.id) {
                            window.location.href = '<?php echo base_url() ?>slider/view/' + data.id;
                        }
                    } else {
                        for(key in data){
                            $('#'+data[key]['field_id']+"_error").html(data[key]['label']);
                        }
                    }

                },
                error: function (e) {
                    //called when there is an error
                    //console.log(e.message);
                }
            });

        });


        function getFormData() {

            var formElement = new FormData();
            formElement.append('id', $('#id').val());
            formElement.append('active', $('#active').is(':checked'));
            formElement.append('name', $('#name').val());
            formElement.append('url', $('#url').val());
            formElement.append('description', $('#description').val());
            formElement.append('file', $('#filename')[0].files[0]);
            return formElement;
        }

    });
</script>
<div class="col-md-12">

    <form class="form-horizontal" id="slider_form">
        <input type="hidden" id="id" value="<?php echo isset($id) ? $id : false ?>">
        <div class="panel panel-default">
            <div class="panel-body">

                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">Active</label>
                    <div class="col-md-6 col-xs-12">
                        <label class="switch">
                            <input type="checkbox" id="active"
                                   <?php if (isset($active) && $active){ ?>checked="checked"<?php } ?> value=""
                                   class="switch">
                            <span></span>
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">Title</label>
                    <div class="col-md-6 col-xs-12">
                        <div class="input-group">
                            <span class="input-group-addon"><span style="line-height: 28px;"
                                                                  class="fa fa-pencil"></span></span>
                            <input type="text" id="name" value="<?php echo isset($name) ? $name : false ?>"
                                   class="form-control">
                        </div>
                        <span id="name_error" class="help-block"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">URL</label>
                    <div class="col-md-6 col-xs-12">
                        <textarea class="form-control" rows="3" id="url"
                                  placeholder=""><?php echo isset($url) ? $url : ""; ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">Description</label>
                    <div class="col-md-6 col-xs-12">
                        <textarea class="form-control" rows="5" id="description"
                                  placeholder=""><?php echo isset($description) ? $description : ""; ?></textarea>
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">File</label>
                    <div class="col-md-6 col-xs-12">
                        <span>Browse file</span><input type="file" title="Browse file" id="filename" name="filename">
                        <span class="help-block">Input type file</span>
                    </div>
                </div>
                <?php if(isset($file_ref)): ?>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">&nbsp;</label>
                        <div class="col-md-6 col-xs-12">
                            <div class="gallery">
                                <a data-gallery="" title="Profile" href="<?= base_url() ?>uploads/<?= isset($file_ref)?$file_ref:""; ?>" class="gallery-item">
                                    <div class="image">
                                        <img alt="Profile Image" src="<?= base_url() ?>uploads/<?= isset($file_ref)?$file_ref:""; ?>">
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
            <div class="panel-footer">
                <button id="btn_submit" class="btn btn-primary pull-right">Submit</button>
            </div>
        </div>
    </form>

</div>
