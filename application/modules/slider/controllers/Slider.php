<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Slider extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('core_lib');
        $this->load->library('form');
    }

    public function listing()
    {

        $result = $this->sliderSearch();
        $resultArr['data'] = $result;
        $pageTitle = [];
        $pageTitle['title'] = "Slider List";
        $pageTitle['icon'] = "fa fa-list";
        $resultArr['page_header'] = $pageTitle;
        $this->load->moduleTemplate('slider/list', $resultArr);
    }

    protected function sliderSearch()
    {
        $postData = [
            'page' => $this->input->post('page'),
            'phrase' => $this->input->post('phrase')
        ];
        $this->load->model('slider/slider_model');
        $query = $this->slider_model->getSearchQuery($postData);
        $result = $this->slider_model->getListByQuery($query, 1, 9999);
        return $result;
    }

    public function add()
    {
        $sliderDetails = [];
        $mode = $this->uri->segment(2);
        if ($mode == "edit") {
            $sliderId = $this->uri->segment(3);
            if ($sliderId) {
                $this->load->model('slider/slider_model');
                $sliderDetails = $this->slider_model->getData($sliderId);
            }
        }
        $bc = [];
        $bc['category'] = "Slider";
        $sliderDetails['breadcrumbs'] = $bc;

        $pageTitle = [];
        $pageTitle['title'] = "Slider Edit/Add";
        $pageTitle['icon'] = "fa fa-tags";
        $sliderDetails['page_header'] = $pageTitle;
        $this->load->moduleTemplate('slider/edit', $sliderDetails);
    }

    public function validate()
    {
        $postData = [
            'id' => $this->input->post('id'),
            'name' => $this->input->post('name'),
            'description' => $this->input->post('description'),
            'url' => $this->input->post('url'),
            'active' => $this->input->post('active') == "true" ? 1 : 0,
        ];

        $this->form->setRules($postData['name'], 'name', 'Please enter slider name');
        $validationArr = $this->form->run();
        if (is_array($validationArr) && count($validationArr) && $validationArr != false) {
            header('Content-Type: application/json');
            echo json_encode($validationArr);
        } else {
            $this->load->library('upload_lib');
            $resultArr = $this->upload_lib->doUpload();
            if (count($resultArr) && !isset($resultArr['error'])) {
                $this->load->model('file_model', 'file_model');
                $tempData = [
                    'file_path' => $resultArr['upload_data']['full_path'],
                    'file_name' => $resultArr['upload_data']['file_name'],
                    'file_extension' => $resultArr['upload_data']['file_type'],
                ];
                $imgId = $this->file_model->add($tempData);
                if ($imgId) {
                    $postData['file_ref'] = $tempData['file_name'];
                }
            }

            $this->load->model('slider/slider_model');
            if (isset($postData['id']) && !empty($postData['id'])) {
                $temp = [
                    'id' => $postData['id']
                ];
                $this->slider_model->update($postData, $temp);
            } else {
                $sliderId = $this->slider_model->add($postData);

            }
            if (!isset($sliderId) && empty($sliderId)) $sliderId = $postData['id'];
            $result = ['id' => $sliderId, 'success' => true];
            header('Content-Type: application/json');
            echo json_encode($result);
        }


    }

    public function itemView()
    {
        $id = $this->uri->segment(3);
        if ($id) {
            $this->load->model('slider/slider_model');
            $sliderDetails = $this->slider_model->getData($id);
            $outputData = [
                'details' => $sliderDetails
            ];
            $bc = [];
            $bc['category'] = "Slider";
            $bc['category/view/' . $id] = "View";
            $outputData['breadcrumbs'] = $bc;
            //echo '<pre>';print_r($outputData);die();
            $pageTitle = [];
            $pageTitle['title'] = "Slider View";
            $pageTitle['icon'] = "fa fa-list";
            $outputData['page_header'] = $pageTitle;
            $this->load->moduleTemplate('slider/view', $outputData);
        }
    }

    public function delete()
    {
        $sliderId = $this->input->post('id');
        if ($sliderId) {
            $this->load->model('slider/slider_model', 'slider_model');
            $this->slider_model->delete(['id' => $sliderId]);
        }
    }


}