<?php

class Category_item_model extends MY_Model
{
    function __construct()
    {
        //$this->CI = & get_instance();
        parent::__construct();
        $this->load->library('Base', 'base');

    }

    protected $tableName = "sb_category_item";

    public $formFields = [
        'id' => 'integer',
        'active' => 'integer',
        'deal_of_day' => 'integer',
        'title' => 'string',
        'price' => 'string',
        'description' => 'text',
        'price_description' => 'text',
        'category_id' => 'integer',
    ];


    public function add($dataArr)

    {
        $resultArr = $this->base->add($dataArr, $this->formFields);
        if ($resultArr) {
            $this->db->insert($this->tableName, $resultArr);
            return $this->db->insert_id();
        } else {
            return false;
        }

    }

    public function update($dataArr, $whereArr)
    {
        $whereArrSanitize = $this->base->validate($whereArr, $this->formFields);
        $resultArr = $this->base->update($dataArr, $this->formFields);
        if (count($resultArr) && count($whereArrSanitize)) {
            $this->db->update($this->tableName, $resultArr, $whereArrSanitize);
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    public function delete($whereArr, $strongDelete = false)
    {
        $whereArrSanitize = $this->base->validate($whereArr, $this->formFields);
        if (isset($whereArrSanitize) && !empty($whereArrSanitize)) {
            if ($strongDelete) {
                $this->db->delete($this->tableName, $whereArrSanitize);
            } else {
                $resultArr = $this->base->delete();
                $this->db->update($this->tableName, $resultArr, $whereArrSanitize);
            }
        }
    }

    public function getLatestJobs($limit = 5)
    {
        $this->db->select('*')
            ->from($this->tableName)
            ->where('is_deleted', NULL, false)
            ->order_by('created', 'DESC');
        $this->db->limit($limit);
        $q = $this->db->get();
        return $q->result_array();
    }

    public function getSearchQuery($params = [], $categoryIds = [])
    {
        $sql = "SELECT e.*,c.name as category_name FROM $this->tableName e LEFT JOIN sb_category c on e.category_id=c.id where e.is_deleted is null";

        if (isset($params['phrase']) && !empty($params['phrase'])) {
            $sql .= " and ( c.name like '%" . $params['phrase'] . "%' or e.title like '%" . $params['phrase'] . "%'";
            $sql .= " or e.description like '%" . $params['phrase'] . "%'";
            $sql .= " or e.price_description like '%" . $params['phrase'] . "%')";
        }
        if (isset($params['active']) && !empty($params['active'])) {
            $sql.= " and e.active=1";
        }
        if (isset($params['deal_of_day']) && !empty($params['deal_of_day'])) {
            $sql.= " and e.deal_of_day=1";
        }
        if (is_array($categoryIds) && count($categoryIds)) {
            $ids = join("','", $categoryIds);
            $sql .= " and (e.category_id IN ('$ids'))";
        }
        return $sql;
    }

}