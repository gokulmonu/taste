<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
$route['default_controller'] = "category";
$route['404_override'] = '';


/*category routes*/
$route['category/add/:num'] = "category/Category/add";
$route['category/edit/:num'] = "category/Category/add";
$route['category/validate'] = "category/Category/validate";

$route['category'] = "category/Category/listing";
$route['category/view/:num'] = "category/Category/itemView";
$route['category/getTree'] = "category/Category/categoryTree";
$route['category/delete'] = "category/Category/delete";

//Item Routes
$route['category/item/add/:num'] = "category/Category/itemAdd";
$route['category/item/edit/:num'] = "category/Category/itemAdd";
$route['category/item/validate'] = "category/Category/itemValidate";
$route['category/item/upload/images'] = "category/Category/uploadItemImage";
$route['category/item/remove-image'] = "category/Category/removeImage";
$route['category/item/delete'] = "category/Category/itemDelete";
$route['category/item/getimages'] = "category/Category/getItemImageForRender";
$route['category/item/view/(:num)'] = "category/Category/categoryItemView/$1";


//Website routes
$route['category/view/(:num)/(:any)'] = "category/Website/viewCategory";//category/view/categoryid/categoryname
$route['category/view/search/(:num)/(:num)/(:any)'] = "category/Website/categoryPageSearch";//category/view/search/categoryid/pagenum/categoryname?phrase

$route['category/product/view/(:num)/(:any)'] = "category/Website/viewProduct";//category/product/view/productid/productname
