<script>
    Dropzone.autoDiscover = false;
    $(function () {
        $("div#drop_zone").dropzone({
            url: '<?php echo base_url()?>category/item/upload/images',
            init :function(){
                this.on('sending',function(file,xhr,formData){
                    formData.append('item_id','<?= isset($id)?$id:""; ?>');
                })
            },
            success: function (file, response) {
                if(response.error){
                    $('#upload_error').html(response.error);
                    $('#upload_error_container').iziModal('open');
                }else{
                    reloadImages();
                }
                //this.removeFile(file);

            }
        });


    });
    function reloadImages() {
        setTimeout(loadImages,3000);
    }

    function loadImages() {
        var id = '<?= isset($id)?$id:0; ?>';
        $.ajax({
            url: '<?php echo base_url()?>category/item/getimages',
            type: 'POST',
            dataType: "html",
            data:{
                id:id
            },
            success: function (data) {
                $('#image_container').html(data);
            },
            error: function (e) {
                //called when there is an error
                //console.log(e.message);
            }
        });
    }
    function removeImage(fileId) {
        var postData = {};
        postData['file_id'] = fileId;
        $.ajax({
            url: '<?php echo base_url()?>category/item/remove-image',
            type: 'POST',
            data: postData,
            success: function (data) {

                reloadImages();
            },
            error: function (e) {
                //called when there is an error
                //console.log(e.message);
            }
        });
    }

    function deleteCategoryItem(itemId) {
        if (confirm("Are you sure want to delete this item?")) {
            var postData = {};
            postData['id'] = itemId;
            $.ajax({
                url: '<?php echo base_url()?>category/item/delete',
                type: 'POST',
                data: postData,
                success: function (data) {
                    window.location.href = '<?php  echo base_url() . "category/view/"?><?php echo $category_id ?>';
                },
                error: function (e) {
                    //called when there is an error
                    //console.log(e.message);
                }
            });
        } else {
            return false;
        }

    }
</script>
<div class="col-md-6 col-xs-6">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Toolbar</h3>
            <ul class="panel-controls">
                <li><a href="#" class="panel-collapse"><span style="line-height: 29px;"
                                                             class="fa fa-angle-down"></span></a></li>
                <li><a href="#" class="panel-remove"><span style="line-height: 29px;"
                                                           class="fa fa-times"></span></a></li>
            </ul>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <button
                    onclick="location.href='<?php echo base_url() ?>category/item/edit/<?php echo $id ?>';"
                    class="btn btn-info"><i class="glyphicon fa fa-edit"></i> Edit
                </button>

                <button onclick="deleteCategoryItem('<?php echo $id ?>')"
                        class="btn btn-info"><i class="glyphicon fa fa-trash-o"></i> Delete
                </button>
                <?php if ($category_id) {
                    ?>
                    <button
                        onclick="location.href='<?php echo base_url() ?>category/view/<?php echo $category_id ?>';"
                        class="btn btn-info"><i class="glyphicon fa fa-chevron-circle-left"></i> Back To Category
                    </button>
                    <?php
                }
                ?>
            </div>

        </div>
    </div>
</div>
    <div class="panel panel-default tabs">
    <ul class="nav nav-tabs" role="tablist">
        <li class="active"><a href="#tab-first" role="tab" data-toggle="tab">Item Details</a></li>
        <li><a href="#tab-second" role="tab" data-toggle="tab">Description</a></li>
        <li><a href="#tab-third" role="tab" data-toggle="tab">Images</a></li>
    </ul>
    <div class="panel-body tab-content">
        <div class="tab-pane active" id="tab-first">
            <table class="table table-bordered table-striped table-actions">
              <tr>
                    <th width="100">Title</th>
                    <td><?= isset($title)?$title:""; ?></td>
                </tr>
                <tr>
                    <th width="100">Active</th>
                    <td><?= ($active)== 1?"Yes":"No"; ?></td>
                </tr>
                <tr>
                    <th width="100">Deal Of the Day</th>
                    <td><?= ($deal_of_day)== 1?"Yes":"No"; ?></td>
                </tr>
                <tr>
                    <th width="100">Price</th>
                    <td><?= isset($price)?$price:""; ?></td>
                </tr> <tr>
                    <th width="100">Price Description</th>
                    <td><?= isset($price_description)?$price_description:""; ?></td>
                </tr>
                </table>
        </div>
        <div class="tab-pane" id="tab-second">
            <div>
                <?= isset($description)?$description:""; ?>
            </div>

        </div>
        <div class="tab-pane" id="tab-third">
            <div id="drop_zone" class="dropzone" style="min-height: 180px;">

            </div>
            <div id="image_container">
                <?php
                $this->view('category/item/image_list');
                ?>
            </div>
        </div>
    </div>
</div>