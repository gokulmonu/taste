<script>
    $(function () {
        $("#item_form").submit(function (event) {
            event.preventDefault();
            var formData = getFormData();
            $('#error_container').hide();
            $.ajax({
                url: '<?php echo base_url()?>category/item/validate',
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(formData),
                success: function (data) {
                    if (data.success) {
                        window.location.href = '<?php echo base_url() ?>category/view/<?php echo isset($category_id) ? $category_id : 0 ?>';
                    }

                    if (!data.success) {
                        var alertMsg = "";
                        for (var key in data) {
                            if (data.hasOwnProperty(key)) {
                                if (typeof data[key]['label'] !== "undefined") {
                                    alertMsg += data[key]['label'] + "<br>";
                                }
                            }

                        }
                        $('#error_container').html(alertMsg).show();
                    }

                },
                error: function (e) {
                }
            });

        });

        function getFormData() {

            var data = {};
            data['id'] = $('#id').val();
            data['title'] = $('#job_title').val();
            data['active'] = $('#active').is(':checked');
            data['deal_of_day'] = $('#deal_of_day').is(':checked');
            data['tags'] = $('#tags').val();
            data['price'] = $('#price').val();
            data['price_description'] = $('#price_description').val();
            data['description'] = $('#description').val();
            data['category_id'] = $('#category_id').val();
            return data;
        }
    });

</script>

<!-- START WIZARD WITH SUBMIT BUTTON -->
<div class="block">
    <div class="alert alert-danger" role="alert" id="error_container" style="display: none">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span
                class="sr-only">Close</span></button>
        <span id="error_msg"></span>
    </div>
    <h4>Item Add/Edit</h4>
    <form role="form" class="form-horizontal" id="item_form">
        <div class="wizard show-submit">
            <ul>
                <li>
                    <a href="#step-5">
                        <span class="stepNumber">1</span>
                        <span class="stepDesc">Item<br/><small>Specify details of the item</small></span>
                    </a>
                </li>
                <li>
                    <a href="#step-6">
                        <span class="stepNumber">2</span>
                        <span class="stepDesc">Description<br/><small>Specify the description of this item</small></span>
                    </a>
                </li>
            </ul>
            <div id="step-5">
                <input type="hidden" id="id" value="<?php echo isset($id) ? $id : 0 ?>">
                <input type="hidden" id="category_id" value="<?php echo isset($category_id) ? $category_id : 0 ?>">
                <div class="form-group">
                    <label class="col-md-2 control-label">Active</label>
                    <div class="col-md-6 col-xs-12">
                        <label class="switch">
                            <input type="checkbox" id="active"
                                   <?php if (isset($active) && $active){ ?>checked="checked"<?php } ?> value=""
                                   class="switch">
                            <span></span>
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Deal Of The Day</label>
                    <div class="col-md-6 col-xs-12">
                        <label class="switch">
                            <input type="checkbox" id="deal_of_day"
                                   <?php if (isset($deal_of_day) && $deal_of_day){ ?>checked="checked"<?php } ?> value=""
                                   class="switch">
                            <span></span>
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Title*</label>

                    <div class="col-md-6 col-xs-12">
                        <input type="text" id="job_title" placeholder="Item Title" class="form-control"
                               value="<?php echo isset($title) ? $title : ""; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Price</label>

                    <div class="col-md-6 col-xs-12">
                        <input type="text" id="price" placeholder="Item Price" class="form-control"
                               value="<?php echo isset($price) ? $price : ""; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Keywords</label>
                    <div class="col-md-6 col-xs-12">
                        <input type="text" class="tagsinput" id="tags"
                               value="<?php echo isset($_tags) ? $_tags : ""; ?>"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Price Description</label>
                    <div class="col-md-6 col-xs-12">
                        <textarea class="form-control" rows="5" id="price_description"
                                  placeholder="Specify price description like today's offer etc"><?php echo isset($price_description) ? $price_description : ""; ?></textarea>
                    </div>
                </div>
            </div>
            <div id="step-6">
                <div class="block">
                    <h6>Description</h6>
                    <textarea class="summernote" id="description">
                                    <?php echo isset($description) ? $description : ""; ?>
                    </textarea>
                </div>


            </div>
        </div>
    </form>
</div>
<!-- END WIZARD WITH SUBMIT BUTTON -->
