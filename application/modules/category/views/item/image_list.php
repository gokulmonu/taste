
<script>
    jQuery(function(){
       $('#links').on('click',function(event){
           event = event || window.event;
           var target = event.target || event.srcElement;
           var link = target.src ? target.parentNode : target;
           var options = {index: link, event: event,onclosed: function(){
               setTimeout(function(){
                   $("body").css("overflow","");
               },200);
           }};
           var links = this.getElementsByTagName('a');
           blueimp.Gallery(links, options);
       })
    });
</script>

<div id="links" class="gallery">
    <?php
    if(isset($images) &&  count($images) ){
    foreach($images as $value){
        ?>

        <a data-gallery="" title="<?= $value['file_name'] ?>" href="<?=base_url() ?><?php echo $value['thumbnail'] ?>" class="gallery-item">
            <div class="image">
                <img alt="<?= $value['file_name'] ?>" src="<?=base_url() ?><?php echo $value['thumbnail'] ?>">
                <ul class="gallery-item-controls">
                    <li><span  onclick="removeImage('<?php echo $value['id'] ?>')" class="file_btn"><i class="fa fa-times"></i></span></li>
                </ul>
            </div>
            <div class="meta">
                <strong><?= $value['file_name'] ?></strong>
                <span></span>
            </div>
        </a>
    <?php
    }
    }

    ?>

</div>
<!-- BLUEIMP GALLERY -->
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
</div>
<!-- END BLUEIMP GALLERY -->
