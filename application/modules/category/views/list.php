<script>
    function deleteCategory(categoryId) {
        if (confirm("Are you sure want to delete this category and its subcategory?")) {
            var postData = {};
            postData['category_id'] = categoryId;
            $.ajax({
                url: '<?php echo base_url()?>category/delete',
                type: 'POST',
                data: postData,
                success: function (data) {
                    window.location.href = '<?php  echo base_url() . "category/"?>';
                },
                error: function (e) {
                    //called when there is an error
                    //console.log(e.message);
                }
            });
        } else {
            return false;
        }

    }
</script>

<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="col-md-6 col-xs-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Toolbar</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span style="line-height: 29px;"
                                                                     class="fa fa-angle-down"></span></a></li>
                        <li><a href="#" class="panel-remove"><span style="line-height: 29px;"
                                                                   class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <button
                            onclick="location.href='<?php echo base_url() ?>category/add/0';"
                            class="btn btn-info"><i class="glyphicon fa fa-plus"></i> Add
                        </button>
                    </div>

                </div>
            </div>
        </div>
        <!-- END DEFAULT BUTTONS -->

        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="btn-group pull-right">
                    <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i
                            class="fa fa-bars"></i> Export
                        Data
                    </button>
                    <ul class="dropdown-menu">
                        <li><a href="#"
                               onClick="$('#customers2').tableExport({type:'csv',escape:'false'});"><img
                                    src='<?= base_url() ?>bootstrap/images/admin/icons/csv.png' width="24"/>
                                CSV</a></li>
                        <li><a href="#"
                               onClick="$('#customers2').tableExport({type:'txt',escape:'false'});"><img
                                    src='<?= base_url() ?>bootstrap/images/admin/icons/txt.png' width="24"/>
                                TXT</a></li>
                        <li class="divider"></li>
                        <li><a href="#"
                               onClick="$('#customers2').tableExport({type:'excel',escape:'false'});"><img
                                    src='<?= base_url() ?>bootstrap/images/admin/icons/xls.png' width="24"/>
                                XLS</a></li>
                        <li><a href="#"
                               onClick="$('#customers2').tableExport({type:'doc',escape:'false'});"><img
                                    src='<?= base_url() ?>bootstrap/images/admin/icons/word.png'
                                    width="24"/> Word</a></li>
                        <li><a href="#"
                               onClick="$('#customers2').tableExport({type:'powerpoint',escape:'false'});"><img
                                    src='<?= base_url() ?>bootstrap/images/admin/icons/ppt.png' width="24"/>
                                PowerPoint</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="#"
                               onClick="$('#customers2').tableExport({type:'png',escape:'false'});"><img
                                    src='<?= base_url() ?>bootstrap/images/admin/icons/png.png' width="24"/>
                                PNG</a></li>
                        <li><a href="#"
                               onClick="$('#customers2').tableExport({type:'pdf',escape:'false'});"><img
                                    src='<?= base_url() ?>bootstrap/images/admin/icons/pdf.png' width="24"/>
                                PDF</a></li>
                    </ul>
                </div>

            </div>
            <div class="panel-body">
                <table id="customers2" class="table datatable">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Active</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php
                    if (isset($data['rows']) && count($data['rows'])) {
                        foreach ($data['rows'] as $value) {
                            ?>
                            <tr>
                                <td>
                                    <?= $value['name'] ?></td>
                                <td><?php echo ($value['active']) == 1 ? "yes" : "no" ?></td>
                                <td>
                                    <a style="padding: 3px 1px;" class="btn"
                                       href="<?php echo base_url('category/edit') ?>/<?= isset($value['id']) ? $value['id'] : 0 ?>"><i
                                            class="fa fa-pencil"></i></a>
                                    <a style="padding: 3px 1px;" class="btn"
                                       href="<?php echo base_url('category/view') ?>/<?= isset($value['id']) ? $value['id'] : 0 ?>"><i
                                            class="fa fa-eye"></i></a>
                                    <a onclick="deleteCategory('<?php echo $value['id'] ?>')" style="padding: 3px 1px;" class="btn"
                                       href="javascript:;"><i
                                            class="fa fa-trash-o"></i></a>
                                </td>
                            </tr>

                            <?php
                        }
                    }
                    ?>


                    </tbody>
                </table>

            </div>
        </div>

    </div>
</div>