<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Website extends MY_Controller
{
    public function __construct()
    {
        parent::__construct("website");
        $this->load->library('form');
    }

    public function viewCategory()
    {
        $categoryId = $this->uri->segment(3);
        $this->load->model('category/category_model');
        $categoryDetails = $this->category_model->getData($categoryId);

        $this->load->model('category/category_item_model');
        $query = $this->category_item_model->getSearchQuery(['active' => 1],[$categoryId]);
        $items = $this->category_item_model->getListByQuery($query, 1, 1);
        if ($items['rows']) {
            foreach ($items['rows'] as $key => $value) {
                $this->load->model('category/category_item_image_model');
                $itemImages = $this->category_item_image_model->getItemImages($value['id']);
                if (count($itemImages)) {
                    $image = current($itemImages);
                    if (strpos($image['file_path'], "uploads/") !== false) {
                        $loc = substr($image['file_path'], strpos($image['file_path'], "uploads/"));
                    }
                    $path = base_url();
                    $name = substr($image['file_name'], 0, strrpos($image['file_name'], '.'));
                    $ext = substr($image['file_name'], strrpos($image['file_name'], '.'));
                    if (isset($loc)) {
                        $thumbPath = $loc;
                    } else {
                        $thumbPath = $path . 'uploads/' . $name . $ext;

                    }
                    $items['rows'][$key]['thumbnail'] = $thumbPath;

                }
            }

        }
        $this->load->template('category_view', ['items' => $items,'data'=>$categoryDetails]);
    }

    public function categoryPageSearch()
    {
        $phrase = $this->input->get('phrase');
        $categoryId = $this->uri->segment(4);
        $page = $this->uri->segment(5);
        $this->load->model('category/category_model');
        $categoryDetails = $this->category_model->getData($categoryId);

        $this->load->model('category/category_item_model');
        $query = $this->category_item_model->getSearchQuery(['active' => 1,'phrase'=>$phrase],[$categoryId]);
        $items = $this->category_item_model->getListByQuery($query,$page, 1);
        //echo '<pre>';print_r($phrase);die();
        if ($items['rows']) {
            foreach ($items['rows'] as $key => $value) {
                $this->load->model('category/category_item_image_model');
                $itemImages = $this->category_item_image_model->getItemImages($value['id']);
                if (count($itemImages)) {
                    $image = current($itemImages);
                    if (strpos($image['file_path'], "uploads/") !== false) {
                        $loc = substr($image['file_path'], strpos($image['file_path'], "uploads/"));
                    }
                    $path = base_url();
                    $name = substr($image['file_name'], 0, strrpos($image['file_name'], '.'));
                    $ext = substr($image['file_name'], strrpos($image['file_name'], '.'));
                    if (isset($loc)) {
                        $thumbPath = $loc;
                    } else {
                        $thumbPath = $path . 'uploads/' . $name . $ext;

                    }
                    $items['rows'][$key]['thumbnail'] = $thumbPath;

                }else{
                    $items['rows'][$key]['thumbnail'] = "";
                }
            }

        }
        //echo '<pre>';print_r($items);die();
        $this->load->template('category_view', ['items' => $items,'data'=>$categoryDetails,'phrase'=>$phrase]);
    }

    public function viewProduct()
    {
        $productId = $this->uri->segment(4);
        $this->load->model('category/category_item_model');
        $categoryItemData = $this->category_item_model->getData($productId);
        $categoryItemData['images'] = $this->getItemImages($productId);
        $this->load->model('tag_model', 'tag_model');
        $tags = $this->tag_model->listByFields(['object_ref' => 'sb_category_item', 'object_id' => $productId]);
        if ($tags) {
            $tag = implode(', ', array_column($tags, 'name'));
            $categoryItemData['_tags'] = $tag;
        }
        if (isset($categoryItemData['category_id'])) {
            $this->load->model('category/category_model');
            $categoryDetails = $this->category_model->getData($categoryItemData['category_id']);
            $categoryItemData['category_name'] = isset($categoryDetails['name']) ? $categoryDetails['name'] : "";
        }

        $this->load->template('category_item_view', $categoryItemData);
    }

    public function getItemImages($itemId = 0)
    {
        $imagesList = [];
        if (!empty($itemId)) {
            $this->load->model('category_item_image_model', 'category_item_image_model');
            $imagesList = $this->category_item_image_model->getItemImages($itemId);
            if (count($imagesList)) {
                foreach ($imagesList as $key => $image) {
                    if (strpos($image['file_path'], "uploads/") !== false) {
                        $loc = substr($image['file_path'], strpos($image['file_path'], "uploads/"));
                    }
                    $path = base_url();
                    $name = substr($image['file_name'], 0, strrpos($image['file_name'], '.'));
                    $ext = substr($image['file_name'], strrpos($image['file_name'], '.'));
                    if(isset($loc)){
                        $thumbPath = $loc;

                    }else{
                        $thumbPath = $path . 'uploads/' . $name . $ext;

                    }
                    $imagesList[$key]['thumbnail'] = $thumbPath;
                }

            }
        }
        return $imagesList;

    }
}