<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Category extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('core_lib');
        $this->load->library('form');
    }

    public function add()
    {
        $categoryDetails = [];
        $mode = $this->uri->segment(2);
        if ($mode == "edit") {
            $categoryId = $this->uri->segment(3);
            if ($categoryId) {
                $this->load->model('category/category_model');
                $categoryDetails = $this->category_model->getData($categoryId);
            }
        } else {
            $parentId = $this->uri->segment(3);
            $categoryDetails = [
                'parent_id' => $parentId
            ];

        }
        $bc = [];
        $bc['category'] = "Category";
        $categoryDetails['breadcrumbs'] = $bc;

        $pageTitle = [];
        $pageTitle['title'] = "Category Add";
        $pageTitle['icon'] = "fa fa-tags";
        $categoryDetails['page_header'] = $pageTitle;
        $this->load->moduleTemplate('category/edit', $categoryDetails);
    }

    public function itemAdd()
    {
        $categoryItemDetails = [];
        $mode = $this->uri->segment(3);
        if ($mode == "edit") {
            $categoryItemId = $this->uri->segment(4);
            if ($categoryItemId) {
                $this->load->model('category/category_item_model');
                $categoryItemDetails = $this->category_item_model->getData($categoryItemId);

                $this->load->model('tag_model', 'tag_model');
                $tags = $this->tag_model->listByFields(['object_ref' => 'sb_category_item', 'object_id' => $categoryItemId]);
                if ($tags) {
                    $tag = implode(',', array_column($tags, 'name'));
                    $categoryItemDetails['_tags'] = $tag;
                }
            }

        } else {
            $parentId = $this->uri->segment(4);
            $categoryItemDetails = [
                'category_id' => $parentId
            ];

        }
        $bc = [];
        $bc['category'] = "Category";
        $categoryDetails['breadcrumbs'] = $bc;

        $pageTitle = [];
        $pageTitle['title'] = "Category Item Add/Edit";
        $pageTitle['icon'] = "fa fa-tags";
        $categoryItemDetails['page_header'] = $pageTitle;
        $this->load->moduleTemplate('category/item/edit', $categoryItemDetails);
    }

    public function itemValidate()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        $this->form->setRules($data['title'], 'title_error', 'Please Enter Item Title');
        $validationArr = $this->form->run();
        if (is_array($validationArr) && count($validationArr) && $validationArr != false) {
            header('Content-Type: application/json');
            echo json_encode($validationArr);
        } else {
            $this->load->model('category/category_item_model', 'category_item_model');
            if (isset($data['id']) && !empty($data['id'])) {
                $temp = [
                    'id' => $data['id']
                ];
                $this->category_item_model->update($data, $temp);
            } else {
                $itemId = $this->category_item_model->add($data);

            }
            if (!isset($itemId) && empty($itemId)) $itemId = $data['id'];
            $this->addTags($data['tags'], $itemId);
            $result = ['id' => $itemId, 'success' => true];
            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    private function addTags($tags, $objectId)
    {
        $tagsList = explode(',', $tags);
        if ($tagsList) {
            $this->load->model('tag_model', 'tag_model');
            foreach ($tagsList as $tag) {
                if (!$this->tag_model->loadByFields(['name' => $tag, 'object_ref' => 'sb_category_item'])) {
                    $this->tag_model->add(['name' => $tag, 'object_ref' => 'sb_category_item', 'object_id' => $objectId]);
                }
            }
        } else {
            $removedTags = $this->tag_model->listByFields(['object_ref' => 'sb_category_item', 'object_id' => $objectId]);
            if ($removedTags) {
                foreach ($removedTags as $rm) {
                    $this->tag_model->delete(['id' => $rm['id']]);
                }

            }
        }
    }

    public function validate()
    {
        $postData = [
            'id' => $this->input->post('id'),
            'name' => $this->input->post('name'),
            'parent_id' => $this->input->post('parent_id'),
            'description' => $this->input->post('description'),
            'active' => $this->input->post('active') == "true" ? 1 : 0,
        ];
        //echo '<pre>';var_dump($postData);die();
        $this->form->setRules($postData['name'], 'name', 'Please Enter Category Name');
        $validationArr = $this->form->run();
        if (is_array($validationArr) && count($validationArr) && $validationArr != false) {
            header('Content-Type: application/json');
            echo json_encode($validationArr);
        } else {
            $this->load->library('upload_lib');
            $ud = uniqid(session_id());
            $resultArr = $this->upload_lib->doUpload(false, "uploads/".$ud);
            if(count($resultArr) && !isset($resultArr['error'])){
                $this->load->model('file_model','file_model');
                $tempData = [
                    'file_path' =>$resultArr['upload_data']['full_path'],
                    'file_name' =>$resultArr['upload_data']['file_name'],
                    'file_extension' =>$resultArr['upload_data']['file_type'],
                ];
                $imgId = $this->file_model->add($tempData);
                if($imgId){
                    $postData['file_ref'] = "uploads/" . $ud . "/" . $tempData['file_name'];
                }
            }

            $this->load->model('category/category_model');
            if (isset($postData['id']) && !empty($postData['id'])) {
                $temp = [
                    'id' => $postData['id']
                ];
                $this->category_model->update($postData, $temp);
            } else {
                $categoryId = $this->category_model->add($postData);

            }
            if (!isset($categoryId) && empty($categoryId)) $categoryId = $postData['id'];
            $result = ['id' => $categoryId, 'success' => true];
            header('Content-Type: application/json');
            echo json_encode($result);
        }


    }

    public function itemView()
    {
        $id = $this->uri->segment(3);
        if ($id) {
            $this->load->model('category/category_model');
            $categoryDetails = $this->category_model->getData($id);
            $parentId = $categoryDetails['id'];
            $postData = [
                'parent_id' => $parentId
            ];
            $query = $this->category_model->getSearchQuery($postData);
            $result = $this->category_model->getListByQuery($query, 1, 9999);
            $outputData = [
                'details' => $categoryDetails,
                'data' => $result
            ];
            $result = $this->getItems($id);
            $outputData['item'] = $result;
            $bc = [];
            $bc['category'] = "Category";
            $bc['category/view/' . $id] = "View";
            $outputData['breadcrumbs'] = $bc;
            //echo '<pre>';print_r($outputData);die();
            $pageTitle = [];
            $pageTitle['title'] = "Category View";
            $pageTitle['icon'] = "fa fa-list";
            $outputData['page_header'] = $pageTitle;
            $this->load->moduleTemplate('category/view', $outputData);
        }
    }

    public function categoryItemView($itemId)
    {
        $this->load->model('category/category_item_model');
        $categoryItemData = $this->category_item_model->getData($itemId);
        $categoryItemData['images'] = $this->getItemImages($itemId);
        $pageTitle = [];
        $pageTitle['title'] = "Category Item View";
        $pageTitle['icon'] = "fa fa-tags";
        $categoryItemData['page_header'] = $pageTitle;
        $this->load->moduleTemplate('category/item/view', $categoryItemData);
    }


    public function uploadItemImage()
    {
        $itemId = $this->input->post('item_id');
        $this->load->library('upload_lib');
        $resultArr = $this->upload_lib->doUpload(false, "uploads/".uniqid(session_id()));
        if (count($resultArr) && !isset($resultArr['error'])) {
            $this->load->model('file_model', 'file_model');
            $tempData = [
                'file_path' => $resultArr['upload_data']['full_path'],
                'file_name' => $resultArr['upload_data']['file_name'],
                'file_extension' => $resultArr['upload_data']['file_type'],
            ];
            $imgId = $this->file_model->add($tempData);
            if ($imgId) {
                $fileDetails['item_id'] = $itemId;
                $fileDetails['file_id'] = $imgId;
                $this->load->model('category_item_image_model', 'category_item_image_model');
                $this->category_item_image_model->add($fileDetails);

            }
        } elseif (isset($resultArr['error'])) {
            header('Content-Type: application/json');
            echo json_encode($resultArr);
        }
    }

    public function getItemImageForRender()
    {
        $itemId = $this->input->post('id');
        $imagesList = $this->getItemImages($itemId);
        $data['images'] = $imagesList;
        $this->load->view('category/item/image_list', $data);
    }

    public function getItemImages($itemId = 0)
    {
        $imagesList = [];
        if (!empty($itemId)) {
            $this->load->model('category_item_image_model', 'category_item_image_model');
            $imagesList = $this->category_item_image_model->getItemImages($itemId);
            if (count($imagesList)) {
                foreach ($imagesList as $key => $image) {
                    if (strpos($image['file_path'], "uploads/") !== false) {
                        $loc = substr($image['file_path'], strpos($image['file_path'], "uploads/"));
                    }
                    $path = base_url();
                    $name = substr($image['file_name'], 0, strrpos($image['file_name'], '.'));
                    $ext = substr($image['file_name'], strrpos($image['file_name'], '.'));
                    if(isset($loc)){
                        $thumbPath = $loc;

                    }else{
                        $thumbPath = $path . 'uploads/' . $name . $ext;

                    }
                    $imagesList[$key]['thumbnail'] = $thumbPath;
                }

            }
        }
        //echo '<pre>';print_r($imagesList);die();
        return $imagesList;

    }

    public function removeImage()
    {
        $fileId = $this->input->post('file_id');
        if ($fileId) {
            $this->load->model('file_model', 'file_model');
            $fileDetails = $this->file_model->getData($fileId);
            $temp = [
                'id' => $fileId
            ];
            $this->file_model->delete($temp);
            $path = $fileDetails['file_path'];
            $fileUrl = substr($path, 0, strrpos($path, '/'));
            $name = substr($fileDetails['file_name'], 0, strrpos($fileDetails['file_name'], '.'));
            $thumbPath = $fileUrl . '/thumbnail/' . $name . '_thumb.png';
            unlink($fileDetails['file_path']);
            unlink($thumbPath);
        }

        if ($fileId) {
            $this->load->model('category_item_image_model', 'category_item_image_model');
            $temp = [
                'file_id' => $fileId
            ];
            $this->category_item_image_model->delete($temp);
        }
    }


    private function getItems($categoryId = 0)
    {
        $this->load->model('category/category_item_model');
        $query = $this->category_item_model->getSearchQuery([], [$categoryId]);
        $result = $this->category_item_model->getListByQuery($query, 1, 9999);
        return $result;
    }

    public function listing()
    {
        $result = $this->categorySearch();
        $resultArr['data'] = $result;
        $pageTitle = [];
        $pageTitle['title'] = "Category List";
        $pageTitle['icon'] = "fa fa-list";
        $resultArr['page_header'] = $pageTitle;
        $this->load->moduleTemplate('category/list', $resultArr);
    }

    protected function categorySearch()
    {
        $postData = [
            'page' => $this->input->post('page'),
            'phrase' => $this->input->post('phrase'),
            'parent_id' => $this->input->post('parent_id'),
        ];
        if (empty($postData['parent_id'])) $postData['parent_id'] = 0;
        $this->load->model('category/category_model');
        $query = $this->category_model->getSearchQuery($postData);
        $result = $this->category_model->getListByQuery($query, 1, 9999);
        return $result;
    }

    public function delete()
    {
        $categoryId = $this->input->post('category_id');
        if ($categoryId) {
            $this->load->model('category/category_model', 'category_model');
            $categoryIds = $this->category_model->categories($categoryId);
            if ($categoryIds) {
                $this->category_model->deleteCategories($categoryIds);
            }
        }
    }
    public function itemDelete()
    {
        $categoryItemId = $this->input->post('id');
        if ($categoryItemId) {
            $this->load->model('category/category_item_model', 'category_item_model');
            $this->category_item_model->delete(['id' => $categoryItemId]);
        }
    }


}