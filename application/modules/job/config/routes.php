<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
$route['default_controller'] = "job";
$route['404_override'] = '';


$route['job/list'] = "job/Job/listing";
$route['job/add'] = "job/Job/add";
$route['job/validate'] = "job/Job/validate";
$route['job/delete'] = "job/Job/delete";
$route['job/edit/(:num)'] = "job/Job/add";
$route['job/view/(:num)'] = "job/Job/itemView";
$route['job/submitted/list'] = "job/Job/jobApplied";
$route['job/download/resume'] = "job/Job/downloadFile";

//Website Routes
$route['job/apply'] = "job/Website/applyJob";
$route['job/search'] = "job/Website/jobList";
$route['job/search/(:num)/(:any)'] = "job/Website/jobList";
$route['job/search/(:num)'] = "job/Website/jobList";
$route['job/apply/validate'] = "job/Website/validateResume";
$route['job/(:any)/view/(:num)'] = "job/Website/viewJob";
