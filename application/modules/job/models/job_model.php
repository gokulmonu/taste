<?php

class Job_model extends MY_Model
{
    function __construct()
    {
        //$this->CI = & get_instance();
        parent::__construct();
        $this->load->library('Base', 'base');

    }

    protected $tableName = "sb_job";

    public $formFields = [
        'id' => 'integer',
        'title' => 'string',
        'min_work_exp' => 'integer',
        'max_work_exp' => 'integer',
        'price_type' => 'string',
        'min_annual_ctc' => 'string',
        'max_annual_ctc' => 'string',
        'other_salary_details' => 'text',
        'location' => 'text',
        'employment_type' => 'string',
        'industry_id' => 'integer',
        'candidate_profile' => 'text',
        'company_name' => 'string',
        'company_website' => 'text',
        'contact_email' => 'string',
        'contact_person' => 'string',
        'contact_number' => 'string',
        'about_company' => 'text',
        'no_of_vacancies' => 'integer'
    ];


    public function add($dataArr)

    {
        $resultArr = $this->base->add($dataArr, $this->formFields);
        if ($resultArr) {
            $this->db->insert($this->tableName, $resultArr);
            return $this->db->insert_id();
        } else {
            return false;
        }

    }

    public function update($dataArr, $whereArr)
    {
        $whereArrSanitize = $this->base->validate($whereArr, $this->formFields);
        $resultArr = $this->base->update($dataArr, $this->formFields);
        if (count($resultArr) && count($whereArrSanitize)) {
            $this->db->update($this->tableName, $resultArr, $whereArrSanitize);
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    public function delete($whereArr, $strongDelete = false)
    {
        $whereArrSanitize = $this->base->validate($whereArr, $this->formFields);
        if (isset($whereArrSanitize) && !empty($whereArrSanitize)) {
            if ($strongDelete) {
                $this->db->delete($this->tableName, $whereArrSanitize);
            } else {
                $resultArr = $this->base->delete();
                $this->db->update($this->tableName, $resultArr, $whereArrSanitize);
            }
        }
    }

    public function getLatestJobs($limit = 5)
    {
        $this->db->select('*')
            ->from($this->tableName)
            ->where('is_deleted', NULL, false)
            ->order_by('created', 'DESC');
        $this->db->limit($limit);
        $q = $this->db->get();
        return $q->result_array();
    }

    public function getSearchQuery($params = [],$jobIds = [])
    {
        $sql = "SELECT e.*,c.name as industry_name FROM $this->tableName e LEFT JOIN sb_category c on e.industry_id=c.id where e.is_deleted is null";

        if (isset($params['phrase']) && !empty($params['phrase'])) {
            $sql.= " and ( c.name like '%".$params['phrase'] ."%' or e.title like '%".$params['phrase'] ."%'" ;
            $sql.= " or e.location like '%".$params['phrase'] ."%' or e.company_name like '%".$params['phrase'] ."%'" ;
            $sql.= " or e.candidate_profile like '%".$params['phrase'] ."%')" ;
        }
        if (isset($jobIds) && is_array($jobIds) && count($jobIds)) {
            $ids = join("','", $jobIds);
            $sql .= " or (e.id IN ('$ids') and e.is_deleted is null)";
        }
        return $sql;


    }

}