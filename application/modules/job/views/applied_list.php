<script>
    function deleteJob(jobId) {
        if (confirm("Are you sure want to delete this job?")) {
            var postData = {};
            postData['job_id'] = jobId;
            $.ajax({
                url: '<?php echo base_url()?>job/delete',
                type: 'POST',
                data: postData,
                success: function (data) {
                    window.location.href = '<?php  echo base_url() . "job/list"?>';
                },
                error: function (e) {
                    //called when there is an error
                    //console.log(e.message);
                }
            });
        } else {
            return false;
        }

    }


    function downloadFile(id)
    {
        location.href = "<?php echo base_url()?>job/download/resume?file_id="+id;
    }
</script>

<div class="row">
    <div class="col-md-12 col-xs-12">

        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="btn-group pull-right">
                    <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i
                            class="fa fa-bars"></i> Export
                        Data
                    </button>
                    <ul class="dropdown-menu">
                        <li><a href="#"
                               onClick="$('#customers2').tableExport({type:'csv',escape:'false'});"><img
                                    src='<?= base_url() ?>bootstrap/images/admin/icons/csv.png' width="24"/>
                                CSV</a></li>
                        <li><a href="#"
                               onClick="$('#customers2').tableExport({type:'txt',escape:'false'});"><img
                                    src='<?= base_url() ?>bootstrap/images/admin/icons/txt.png' width="24"/>
                                TXT</a></li>
                        <li class="divider"></li>
                        <li><a href="#"
                               onClick="$('#customers2').tableExport({type:'excel',escape:'false'});"><img
                                    src='<?= base_url() ?>bootstrap/images/admin/icons/xls.png' width="24"/>
                                XLS</a></li>
                        <li><a href="#"
                               onClick="$('#customers2').tableExport({type:'doc',escape:'false'});"><img
                                    src='<?= base_url() ?>bootstrap/images/admin/icons/word.png'
                                    width="24"/> Word</a></li>
                        <li><a href="#"
                               onClick="$('#customers2').tableExport({type:'powerpoint',escape:'false'});"><img
                                    src='<?= base_url() ?>bootstrap/images/admin/icons/ppt.png' width="24"/>
                                PowerPoint</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="#"
                               onClick="$('#customers2').tableExport({type:'png',escape:'false'});"><img
                                    src='<?= base_url() ?>bootstrap/images/admin/icons/png.png' width="24"/>
                                PNG</a></li>
                        <li><a href="#"
                               onClick="$('#customers2').tableExport({type:'pdf',escape:'false'});"><img
                                    src='<?= base_url() ?>bootstrap/images/admin/icons/pdf.png' width="24"/>
                                PDF</a></li>
                    </ul>
                </div>

            </div>
            <div class="panel-body">
                <table id="customers2" class="table datatable">
                    <thead>
                    <tr>
                        <th>Job Industry</th>
                        <th>Full Name</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th>Experience</th>
                        <th>Date Submitted</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php
                    if (isset($jobs_applied) && count($jobs_applied)) {
                        foreach ($jobs_applied as $value) {
                            ?>
                            <tr>
                                <td>
                                    <?= $value['industry_name'] ?>
                                </td>
                                <td>
                                    <?= $value['name'] ?>
                                </td>
                                <td>
                                    <?= $value['phone'] ?>
                                </td>
                                <td>
                                    <?= $value['email'] ?>
                                </td>
                                <td>
                                    <?= $value['experiance'] ?>
                                </td>
                                <td>
                                    <?= $value['created'] ?>
                                </td>
                                <td>
                                    <?php if($value['file_id']): ?>
                                    <a onclick="downloadFile('<?= $value['file_id'] ?>')" style="padding: 3px 1px;" class="btn"
                                       href="javascript:;"><i
                                            class="fa fa-download"></i></a>
                                    <?php endif; ?>
                                </td>
                            </tr>

                            <?php
                        }
                    }
                    ?>


                    </tbody>
                </table>

            </div>
        </div>

    </div>
</div>