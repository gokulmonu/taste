<script>
    function deleteCategory(categoryId) {
        if (confirm("Are you sure want to delete this category and its subcategory?")) {
            var postData = {};
            postData['category_id'] = categoryId;
            $.ajax({
                url: '<?php echo base_url()?>category/delete',
                type: 'POST',
                data: postData,
                success: function (data) {
                    window.location.href = '<?php  echo base_url() . "category/"?>';
                },
                error: function (e) {
                    //called when there is an error
                    //console.log(e.message);
                }
            });
        } else {
            return false;
        }

    }
</script>

<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="col-md-6 col-xs-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Toolbar</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span style="line-height: 29px;"
                                                                     class="fa fa-angle-down"></span></a></li>
                        <li><a href="#" class="panel-remove"><span style="line-height: 29px;"
                                                                   class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <button
                            onclick="location.href='<?php echo base_url() ?>job/edit/<?php echo $id ?>';"
                            class="btn btn-info"><i class="glyphicon fa fa-edit"></i> Edit
                        </button>
                        <button onclick="deleteCategory('<?php echo $id ?>')"
                                class="btn btn-info"><i class="glyphicon fa fa-trash-o"></i> Delete
                        </button>

                        <button onclick="location.href='<?php echo base_url() ?>job/list';" class="btn btn-info"><i
                                class="glyphicon fa fa-chevron-circle-left"></i> Back
                        </button>
                    </div>

                </div>
            </div>
        </div>
        <!-- END DEFAULT BUTTONS -->
        <!-- START TABS -->
        <div class="panel panel-default tabs">
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#tab-first" role="tab" data-toggle="tab">Details</a></li>
            </ul>
            <div class="panel-body tab-content">
                <div class="tab-pane active" id="tab-first">
                    <div class="col-md-6 col-xs-6">
                    <p>
                        <span><b>Job Title: </b><?php echo $title ?></span><br>
                        <span><b>Active: </b>Yes</span><br>
                        <span><b>Tags: </b><?php echo $_tags ?></span><br>
                        <span><b>Minimum Work Experience: </b><?php echo $min_work_exp ?></span><br>
                        <span><b>Maximum Work Experience: </b><?php echo $max_work_exp ?></span><br>
                        <span><b>Price Type: </b><?php echo $price_type ?></span><br>
                        <span><b>Minimum Annual Salary: </b><?php echo $min_annual_ctc ?></span><br>
                        <span><b>Maximum Annual Salary: </b><?php echo $max_annual_ctc ?></span><br>
                        <span><b>Other Salary Details: </b><?php echo $other_salary_details ?></span><br>
                        <span><b>No Of Vacancies: </b><?php echo $no_of_vacancies ?></span><br>
                        <span><b>Job Location: </b><?php echo $location ?></span><br>
                    </p>
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <p>
                        <span><b>Employment Type: </b><?php echo $employment_type ?></span><br>
                        <span><b>UG Qualification: </b><?php echo $selected_ug ?></span><br>
                        <span><b>PG Qualification: </b><?php echo $selected_pg ?></span><br>
                        <span><b>PHd Qualification: </b><?php echo $selected_phd ?></span><br>
                        <span><b>Candidate Profile: </b><?php echo $candidate_profile ?></span><br>
                        <span><b>Company Name: </b><?php echo $company_name ?></span><br>
                        <span><b>Company Website: </b><?php echo $company_website ?></span><br>
                        <span><b>Contact Person: </b><?php echo $contact_person ?></span><br>
                        <span><b>Contact Email: </b><?php echo $contact_email ?></span><br>
                        <span><b>Contact Number: </b><?php echo $contact_number ?></span><br>
                        <span><b>About Company: </b><?php echo $about_company ?></span><br>


                    </p>
                </div>
                </div>
            </div>
        </div>
        <!-- END TABS -->
    </div>
</div>