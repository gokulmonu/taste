<script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDmWegCkNm8UyrFwyFfEr-zzMkvw7V49jU&libraries=places"></script>
<script>
    var minAnnualSalary = '<?php echo isset($min_annual_ctc) ? $min_annual_ctc : ""; ?>';
    var maxAnnualSalary = '<?php echo isset($max_annual_ctc) ? $max_annual_ctc : ""; ?>';
    $(function () {
        $("#price_type").on('change', function () {
            $('#min_annual_salary').find('option').remove().end().append('<option value="">Min Annual Salaray</option>').val("");
            $('#max_annual_salary').find('option').remove().end().append('<option value="">Max Annual Salaray</option>').val("");
            var capital;
            var opt;
            if ($(this).val() == "rs") {
                for (var i = 5; i <= 10; i++) {
                     capital = i * 10000;
                     opt = document.createElement('option');
                    opt.innerHTML = capital.toLocaleString();
                    opt.value = capital.toLocaleString();
                    if(minAnnualSalary == capital.toLocaleString()){
                        opt.setAttribute('selected','selected')
                    }
                    var clone = opt.cloneNode('true');
                    $('#min_annual_salary').append(opt);
                    if(maxAnnualSalary == capital.toLocaleString()){
                        clone.setAttribute('selected','selected')
                    }
                    $('#max_annual_salary').append(clone);
                }
                capital = 100000;
                for (var j = 1; j <= 16; j++) {
                    capital = capital + 25000;
                    opt = document.createElement('option');
                    if(minAnnualSalary == capital.toLocaleString()){
                        opt.setAttribute('selected','selected')
                    }
                    opt.innerHTML = capital.toLocaleString();
                    opt.value = capital.toLocaleString();
                    var clone = opt.cloneNode('true');
                    $('#min_annual_salary').append(opt);

                    if(maxAnnualSalary == capital.toLocaleString()){
                        clone.setAttribute('selected','selected')
                    }
                    $('#max_annual_salary').append(clone);
                }
                capital = 500000;
                for (var c = 1; c <= 10; c++) {
                    capital = capital + 50000;
                    opt = document.createElement('option');
                    if(minAnnualSalary == capital.toLocaleString()){
                        opt.setAttribute('selected','selected')
                    }
                    opt.innerHTML = capital.toLocaleString();
                    opt.value = capital.toLocaleString();
                    var clone = opt.cloneNode('true');
                    $('#min_annual_salary').append(opt);

                    if(maxAnnualSalary == capital.toLocaleString()){
                        clone.setAttribute('selected','selected')
                    }
                    $('#max_annual_salary').append(clone);
                }
                capital = 1000000;
                for (var d = 1; d <= 10; d++) {
                    capital = capital + 100000;
                    opt = document.createElement('option');
                    if(minAnnualSalary == capital.toLocaleString()){
                        opt.setAttribute('selected','selected')
                    }
                    opt.innerHTML = capital.toLocaleString();
                    opt.value = capital.toLocaleString();
                    var clone = opt.cloneNode('true');
                    $('#min_annual_salary').append(opt);

                    if(maxAnnualSalary == capital.toLocaleString()){
                        clone.setAttribute('selected','selected')
                    }
                    $('#max_annual_salary').append(clone);
                }
                capital = 2000000;
                for (var n = 1; n <= 16; n++) {
                    capital = capital + 500000;
                    opt = document.createElement('option');
                    if(minAnnualSalary == capital.toLocaleString()){
                        opt.setAttribute('selected','selected')
                    }
                    opt.innerHTML = capital.toLocaleString();
                    opt.value = capital.toLocaleString();
                    var clone = opt.cloneNode('true');
                    $('#min_annual_salary').append(opt);

                    if(maxAnnualSalary == capital.toLocaleString()){
                        clone.setAttribute('selected','selected')
                    }
                    $('#max_annual_salary').append(clone);
                }
            } else {
                for (var i = 1; i <= 20; i++) {
                     capital = i * 5 * 1000;
                     opt = document.createElement('option');
                    if(minAnnualSalary == capital.toLocaleString()){
                        opt.setAttribute('selected','selected')
                    }
                    opt.innerHTML = capital.toLocaleString();
                    opt.value = capital.toLocaleString();
                    var clone = opt.cloneNode('true');
                    $('#min_annual_salary').append(opt);

                    if(maxAnnualSalary == capital.toLocaleString()){
                        clone.setAttribute('selected','selected')
                    }
                    $('#max_annual_salary').append(clone);
                }
            }
        });
        $("#job_form").submit(function (event) {
            event.preventDefault();
            var formData = getFormData();
            $('#error_container').hide();
            $.ajax({
                url: '<?php echo base_url()?>job/validate',
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(formData),
                success: function (data) {
                    $('#product_name_error').html('');
                    if (data.success) {
                        window.location.href = '<?php echo base_url() ?>job/list';
                    }

                    if (!data.success) {
                        var alertMsg = "";
                        for(var key in data){
                            if(data.hasOwnProperty(key)){
                                if(typeof data[key]['label'] !== "undefined"){
                                    alertMsg+= data[key]['label']+"<br>";
                                }
                            }

                        }alert(alertMsg);
                        $('#error_container').html(alertMsg).show();
                    }

                },
                error: function (e) {
                }
            });

        });

        function getFormData() {

            var data = {};
            data['id'] = $('#id').val();
            data['title'] = $('#job_title').val();
            data['tags'] = $('#tags').val();
            data['min_work_exp'] = $('#min_work_experience').val();
            data['max_work_exp'] = $('#max_work_experience').val();
            data['price_type'] = $('#price_type').val();
            data['min_annual_ctc'] = $('#min_annual_salary').val();
            data['max_annual_ctc'] = $('#max_annual_salary').val();
            data['other_salary_details'] = $('#other_salary').val();
            data['no_of_vacancies'] = $('#no_of_vacancy').val();
            data['location'] = $('#job_location').val();
            data['industry_id'] = $('#industry').val();
            data['ug_qualification'] = $('#ug_qualification').val();
            data['pg_qualification'] = $('#pg_qualification').val();
            data['phd_qualification'] = $('#phd_qualification').val();
            data['company_name'] = $('#company_name').val();
            data['company_website'] = $('#company_website').val();
            data['contact_person'] = $('#contact_person').val();
            data['contact_number'] = $('#contact_number').val();
            data['contact_email'] = $('#contact_email').val();
            data['candidate_profile'] = $('#candidate_description').val();
            data['about_company'] = $('#about_company').val();
            data['employment_type'] = $('input[name=emp_type]:checked').val();
            return data;
        }
        $("#price_type").trigger('change');
        initialize();
    });
    function initialize() {
        var input = document.getElementById('job_location');
        new google.maps.places.Autocomplete(input);
    }

    google.maps.event.addDomListener(window, 'load', initialize);
</script>

<!-- START WIZARD WITH SUBMIT BUTTON -->
<div class="block">
    <div class="alert alert-danger" role="alert" id="error_container" style="display: none">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span
                class="sr-only">Close</span></button>
        <span id="error_msg"></span>
    </div>
    <h4>Job Add/Edit</h4>
    <form role="form" class="form-horizontal" id="job_form">
        <div class="wizard show-submit">
            <ul>
                <li>
                    <a href="#step-5">
                        <span class="stepNumber">1</span>
                        <span class="stepDesc">Job<br/><small>Specify details of the position/job</small></span>
                    </a>
                </li>
                <li>
                    <a href="#step-6">
                        <span class="stepNumber">2</span>
                        <span class="stepDesc">Desired Candidate Profile<br/><small>Specify the kind of person you are looking for this job</small></span>
                    </a>
                </li>
                <li>
                    <a href="#step-7">
                        <span class="stepNumber">3</span>
                        <span class="stepDesc">Advertise Yourself<br/><small>Information about employer</small></span>
                    </a>
                </li>
            </ul>
            <div id="step-5">
                <input type="hidden" id="id" value="<?php echo isset($id) ? $id : 0 ?>">
                <div class="form-group">
                    <label class="col-md-2 control-label">Job Title / Designation*</label>

                    <div class="col-md-6 col-xs-12">
                        <input type="text" id="job_title" placeholder="Job Title" class="form-control"
                               value="<?php echo isset($title) ? $title : ""; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Keywords</label>
                    <div class="col-md-6 col-xs-12">
                        <input type="text" class="tagsinput" id="tags" value="<?php echo isset($_tags) ? $_tags : ""; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Work Experience</label>
                    <div class="col-md-6 col-xs-12">
                        <select id="min_work_experience" class="form-control">
                            <option value="">Minimum work experience</option>
                            <?php for ($i = 1; $i <= 30; $i++): ?>
                                <option value="<?= $i ?>"
                                        <?php if (isset($min_work_exp) && $min_work_exp == $i){ ?>selected<?php } ?>><?= $i ?></option>
                            <?php endfor; ?>
                        </select>To<br>
                        <select id="max_work_experience" class="form-control">
                            <option value="">Maximum work experience</option>
                            <?php for ($i = 1; $i <= 30; $i++): ?>
                                <option value="<?= $i ?>"
                                        <?php if (isset($max_work_exp) && $max_work_exp == $i){ ?>selected<?php } ?>><?= $i ?></option>
                            <?php endfor; ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Annual CTC</label>
                    <div class="col-md-6 col-xs-12">
                        <select id="price_type" class="form-control">
                            <option value="">--select--</option>
                            <option <?php if (isset($price_type) && $price_type == "rs"){ ?>selected<?php } ?> value="rs">Rs</option>
                            <option <?php if (isset($price_type) && $price_type == "us$"){ ?>selected<?php } ?> value="us$">US $</option>
                        </select><br>
                        <select id="min_annual_salary" class="form-control">
                            <option value="">Min Annual Salaray</option>
                        </select>
                        To<br>
                        <select id="max_annual_salary" class="form-control">
                            <option value="">Max Annual Salaray</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Other Salary Details</label>
                    <div class="col-md-6 col-xs-12">
                        <textarea class="form-control" rows="5" id="other_salary"
                                  placeholder="Specify salary details like incentive,reimbursements etc"><?php echo isset($other_salary_details) ? $other_salary_details : ""; ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Number of Vacancies *</label>

                    <div class="col-md-6 col-xs-12">
                        <input type="text" id="no_of_vacancy" placeholder="Vacancy available" class="form-control"
                               value="<?php echo isset($no_of_vacancies) ? $no_of_vacancies : ""; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Job Location *</label>

                    <div class="col-md-6 col-xs-12">
                        <input type="text" id="job_location" placeholder="" class="form-control"
                               value="<?php echo isset($location) ? $location : ""; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Employment Type</label>
                    <div class="col-md-6 col-xs-12">
                        <div class="radio">
                            <label>
                                <input type="radio" name="emp_type" id="optionsRadios1" value="permanent"
                                       <?php if (isset($employment_type) && $employment_type == "permanent"){ ?>checked<?php } ?>/>
                                Permanent
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="emp_type" id="optionsRadios2" value="temporary"
                                       <?php if (isset($employment_type) && $employment_type == "temporary"){ ?>checked<?php } ?>/>
                                Temporary
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="emp_type" id="optionsRadios3" value="free_lancer"
                                       <?php if (isset($employment_type) && $employment_type == "free_lancer"){ ?>checked<?php } ?>/>
                                Free Lancer
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                     <label class="col-md-2 control-label">Industry *</label>
                     <div class="col-md-6 col-xs-12">
                         <select id="industry" class="form-control">
                             <option value="" >--select--</option>
                             <?php if(isset($category['rows'])): ?>
                             <?php foreach($category['rows'] as $cat):?>
                                     <option <?php if (isset($industry_id) && $industry_id == $cat['id']){ ?>selected<?php } ?> value="<?= $cat['id']?>"><?= $cat['name']?></option>
                             <?php endforeach; ?>
                             <?php endif; ?>
                            </select>
                     </div>
                     <!--<ul id="indValContainer2" class="w350">
                         <li class="nauk_ddHead">--Frequently Used--</li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#25" class="">IT-Software / Software
                                 Services</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#7" class="">BPO / Call Centre / ITES</a>
                         </li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#4" class="">Automobile / Auto Anciliary /
                                 Auto Components</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#12" class="">Construction / Engineering /
                                 Cement / Metals</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#14" class="spSelradio sel">Banking / Financial Services / Broking</a></li>
                         <li class="nauk_ddHead">-------------------------------------------------------------</li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#8" class="">Accounting / Finance</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#32" class="">Advertising / PR / MR /Event Management</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#33" class="">Agriculture / Dairy</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#56" class="">Animation / Gaming</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#30" class="">Architecture / Interior Design</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#46" class="">Aviation / Aerospace Firms</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#50" class="">Brewery / Distillery</a>
                         </li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#65" class="">Broadcasting</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#60" class="">Ceramics / Sanitary ware</a>
                         </li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#6" class="">Chemicals / PetroChemical /Plastic / Rubber</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#10" class="">Consumer Electronics / Appliances / Durables</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#18" class="">Courier / Transportation / Freight / Warehousing</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#26" class="">Education / Teaching / Training</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#55" class="">Electricals / Switchgears</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#13" class="">Export / Import</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#47" class="">Facility Management</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#41" class="">Fertilizers / Pesticides</a>
                         </li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#9" class="">FMCG / Foods / Beverage</a>
                         </li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#57" class="">Food Processing</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#31" class="">Fresher / Trainee / Entry
                                 Level</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#35" class="">Gems / Jewellery</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#49" class="">Glass / Glassware</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#42" class="">Government / Defence</a>
                         </li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#61" class="">Heat Ventilation / Air
                                 Conditioning</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#16" class="">Industrial Products / Heavy
                                 Machinery</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#17" class="">Insurance</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#53" class="">Iron and Steel</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#15" class="">IT-Hardware &amp;
                                 Networking</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#48" class="">KPO / Research /
                                 Analytics</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#36" class="">Legal</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#19" class="">Media / Entertainment /
                                 Internet</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#63" class="">Internet / Ecommerce</a>
                         </li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#66" class="">Leather</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#20" class="">Medical / Healthcare /
                                 Hospitals</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#67" class="">Medical Devices /
                                 Equipments</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#54" class="">Mining / Quarrying</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#37" class="">NGO / Social Services /
                                 Regulators / Industry Associations</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#21" class="">Office Equipment /
                                 Automation</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#23" class="">Oil and Gas / Energy / Power
                                 / Infrastructure</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#43" class="">Pulp and Paper</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#22" class="">Pharma / Biotech / Clinical
                                 Research</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#38" class="">Printing / Packaging</a>
                         </li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#58" class="">Publishing</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#39" class="">Real Estate / Property</a>
                         </li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#34" class="">Recruitment / Staffing</a>
                         </li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#24" class="">Retail / Wholesale</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#40" class="">Security / Law
                                 Enforcement</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#28" class="">Semiconductors /
                                 Electronics</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#44" class="">Shipping / Marine</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#52" class="">Strategy / Management
                                 Consulting Firms</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#64" class="">Sugar</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#27" class="">Telecom/ISP</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#3" class="">Textiles / Garments /
                                 Accessories</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#2" class="">Travel / Hotels / Restaurants
                                 / Airlines / Railways</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#45" class="">Tyres</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#51" class="">Water Treatment / Waste
                                 Management</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#59" class="">Wellness / Fitness / Sports
                                 / Beauty</a></li>
                         <li class="spradio"><a href="javascript:void(0)" rel="#29" class="">Other</a></li>
                     </ul>-->
                 </div>

            </div>
            <div id="step-6">

                <div class="block">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Specify UG Qualification</label>
                        <div class="col-md-6 col-xs-12">
                            <select id="ug_qualification" multiple class="form-control select">
                                <?php foreach ($ug_qualification as $pg): ?>
                                    <option <?php if(in_array($pg['id'],$selected_ug)): ?>selected="selected"<?php endif; ?> value="<?= $pg['id']; ?>"><?= $pg['name']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Specify PG Qualification</label>
                        <div class="col-md-6 col-xs-12">
                            <select id="pg_qualification" multiple class="form-control select">
                                <?php foreach ($pg_qualification as $pg): ?>
                                    <option <?php if(in_array($pg['id'],$selected_pg)): ?>selected="selected"<?php endif; ?> value="<?= $pg['id']; ?>"><?= $pg['name']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Specify Doctorate/Ph.D</label>
                        <div class="col-md-6 col-xs-12">
                            <select id="phd_qualification" multiple class="form-control select">
                                <?php foreach ($phd_qualification as $pg): ?>
                                    <option <?php if(in_array($pg['id'],$selected_phd)): ?>selected="selected"<?php endif; ?> value="<?= $pg['id']; ?>"><?= $pg['name']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <h6>Describe Candidate Profile</h6>
                    <textarea class="summernote" id="candidate_description">
                                    <?php echo isset($candidate_profile) ? $candidate_profile : ""; ?>
                    </textarea>
                </div>


            </div>
            <div id="step-7">
                <div class="block">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Company Name *</label>

                        <div class="col-md-6 col-xs-12">
                            <input type="text" id="company_name" placeholder="" class="form-control"
                                   value="<?php echo isset($company_name) ? $company_name : ""; ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Company Website</label>

                        <div class="col-md-6 col-xs-12">
                            <input type="text" id="company_website" placeholder="" class="form-control"
                                   value="<?php echo isset($company_website) ? $company_website : ""; ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Contact Person</label>

                        <div class="col-md-6 col-xs-12">
                            <input type="text" id="contact_person" placeholder="" class="form-control"
                                   value="<?php echo isset($contact_person) ? $contact_person : ""; ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Contact Number</label>

                        <div class="col-md-6 col-xs-12">
                            <input type="text" id="contact_number" placeholder="" class="form-control"
                                   value="<?php echo isset($contact_number) ? $contact_number : ""; ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Contact Email</label>

                        <div class="col-md-6 col-xs-12">
                            <input type="text" id="contact_email" placeholder="" class="form-control"
                                   value="<?php echo isset($contact_email) ? $contact_email : ""; ?>"/>
                        </div>
                    </div>
                    <h6>About Company</h6>
                    <textarea class="summernote" id="about_company">
                                    <?php echo isset($about_company) ? $about_company : ""; ?>
                    </textarea>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- END WIZARD WITH SUBMIT BUTTON -->
