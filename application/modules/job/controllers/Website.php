<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Website extends MY_Controller
{
    public function __construct()
    {
        parent::__construct("website");
        $this->load->library('form');
    }

    public function applyJob()
    {

        $id = $this->input->get('job_id');
        $postData = [
            'parent_id' => 0
        ];
        $this->load->model('category/category_model');
        $query = $this->category_model->getSearchQuery($postData);
        $jobDetails['category'] = $this->category_model->getListByQuery($query, 1, 9999);
        $jobDetails['job_id'] = $id;
        $this->load->template('apply_job', $jobDetails);
    }

    public function jobList()
    {
        $phrase = $this->uri->segment(4);
        $page = $this->uri->segment(3);
        $result = $this->getJobs($phrase, $page);
        $resultArr['data'] = $result;
        $resultArr['page'] = $page;
        $resultArr['phrase'] = $phrase;

        $this->load->template('job_list', $resultArr);
    }

    public function searchJobs()
    {
        $result = $this->getJobs();
        $resultArr['data'] = $result;
        $this->load->view('job_search_result', $resultArr);
    }

    private function getJobs($phrase = "", $page = 1)
    {
        if(empty($phrase)){
            $phrase = $this->input->post('phrase');
        }
        if(empty($page)){
            $page = $this->input->post('page');
        }
        $jobIds = [];
        if($phrase){
            $this->load->model('tag_model');
            $q = $this->tag_model->getSearchQuery($phrase);
            $tagResult = $this->tag_model->getListByQuery($q, 1, 99999);
            if(isset($tagResult['rows']) && !empty($tagResult['rows'])){
                $jobIds = array_column($tagResult['rows'],'object_id');
            }
        }
        $this->load->model('job/job_model');
        $query = $this->job_model->getSearchQuery(['phrase' => $phrase], $jobIds);
        $result = $this->job_model->getListByQuery($query, $page, '10');
        return $result;
    }

    public function viewJob()
    {
        $jobId = $this->uri->segment(4);
        $tag = "";
        $this->load->model('job/job_model');
        $jobDetails = $this->job_model->getData($jobId);
        if ($jobDetails) {
            $this->load->model('category/category_model');
            $industryData = $this->category_model->getData($jobDetails['industry_id']);
            $jobDetails['industry_name'] = isset($industryData['name']) ? $industryData['name'] : "";
        }

        $this->load->model('tag_model', 'tag_model');
        $tags = $this->tag_model->listByFields(['object_ref' => 'sb_job', 'object_id' => $jobId]);
        if ($tags) {
            $tag = implode(',', array_column($tags, 'name'));
        }
        $this->load->model('job/qualification_model');
        $selectedUgTitle = "";
        $selectedPgTitle = "";
        $selectedPhdTitle = "";
        $selectedUg = $this->qualification_model->listByFields(['object_ref' => 'sb_ug_qualification', 'job_id' => $jobId]);
        if ($selectedUg) {
            $this->load->model('job/qualification_ug_model');
            foreach ($selectedUg as $k => $ug) {
                $selectedUgName = $this->qualification_ug_model->getData($ug['object_id']);
                $selectedUgTitle .= $selectedUgName['name'] . ",";
            }
        }
        $jobDetails['selected_ug'] = trim($selectedUgTitle,",");
        $selectedPg = $this->qualification_model->listByFields(['object_ref' => 'sb_pg_qualification', 'job_id' => $jobId]);
        if ($selectedPg) {
            $this->load->model('job/qualification_pg_model');
            foreach ($selectedPg as $k => $ug) {
                $selectedPgName = $this->qualification_pg_model->getData($ug['object_id']);
                $selectedPgTitle .= $selectedPgName['name'] . ",";
            }
        }
        $jobDetails['selected_pg'] = trim($selectedPgTitle,",");
        $selectedPhd = $this->qualification_model->listByFields(['object_ref' => 'sb_phd_qualification', 'job_id' => $jobId]);
        if ($selectedPhd) {
            $this->load->model('job/qualification_phd_model');
            foreach ($selectedPhd as $k => $ug) {
                $selectedPhdName = $this->qualification_phd_model->getData($ug['object_id']);
                $selectedPhdTitle .= $selectedPhdName['name'] . ",";
            }
        }
        $jobDetails['selected_phd'] = trim($selectedPhdTitle,",");
        $jobDetails['_tags'] = $tag;
        $this->load->template('job_view', $jobDetails);
    }


    public function validateResume()
    {
        $postData = [
            'name' => $this->input->post('name'),
            'phone' => $this->input->post('phone'),
            'email' => $this->input->post('email'),
            'experiance' => $this->input->post('experiance'),
            'industry_id' => $this->input->post('industry_id'),
            'job_id' => $this->input->post('job_id'),
        ];
        $this->form->setRules($postData['email'], 'email_error', 'Please Enter Email',"email");
        $this->form->setRules($postData['experiance'], 'experiance_error', 'Please Enter Your Experience');
        $this->form->setRules($postData['industry_id'], 'industry_id_error', 'Please Select Industry');
        $validationArr = $this->form->run();
        if (is_array($validationArr) && count($validationArr) && $validationArr != false) {
            header('Content-Type: application/json');
            echo json_encode($validationArr);
        }else{
            $this->load->library('upload_lib');
            $resultArr = $this->upload_lib->doUpload(false, "uploads/".uniqid(session_id()));
            if (count($resultArr) && !isset($resultArr['error'])) {
                $this->load->model('file_model', 'file_model');
                $tempData = [
                    'file_path' => $resultArr['upload_data']['full_path'],
                    'file_name' => $resultArr['upload_data']['file_name'],
                    'file_extension' => $resultArr['upload_data']['file_type'],
                ];
                $fileId = $this->file_model->add($tempData);
                if ($fileId) {
                    $postData['file_id'] = $fileId;
                }
            }
            $this->load->model('job/resume_model');
            $resumeId = $this->resume_model->add($postData);
            $result = ['id' => $resumeId, 'success' => true];
            header('Content-Type: application/json');
            echo json_encode($result);
        }

    }

}