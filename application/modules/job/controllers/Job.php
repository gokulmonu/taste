<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Job extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form');
    }

    public function listing()
    {
        $this->load->model('job/job_model');
        $jobs['jobs'] = $this->job_model->getAll();
        $this->load->moduleTemplate('job/list', $jobs);
    }

    public function itemView()
    {
        $jobId = $this->uri->segment(3);
        if ($jobId) {
            $tag = "";
            $this->load->model('job/job_model');
            $jobDetails = $this->job_model->getData($jobId);

            $this->load->model('tag_model', 'tag_model');
            $tags = $this->tag_model->listByFields(['object_ref' => 'sb_job', 'object_id' => $jobId]);
            if ($tags) {
                $tag = implode(',', array_column($tags, 'name'));
            }
            $this->load->model('job/qualification_model');
            $selectedUgTitle = "";
            $selectedPgTitle = "";
            $selectedPhdTitle = "";
            $selectedUg = $this->qualification_model->listByFields(['object_ref' => 'sb_ug_qualification', 'job_id' => $jobId]);
            if ($selectedUg) {
                $this->load->model('job/qualification_ug_model');
                foreach ($selectedUg as $k => $ug) {
                    $selectedUgName = $this->qualification_ug_model->getData($ug['object_id']);
                    $selectedUgTitle .= $selectedUgName['name'] . ",";
                }
            }
            $jobDetails['selected_ug'] = trim($selectedUgTitle,",");
            $selectedPg = $this->qualification_model->listByFields(['object_ref' => 'sb_pg_qualification', 'job_id' => $jobId]);
            if ($selectedPg) {
                $this->load->model('job/qualification_pg_model');
                foreach ($selectedPg as $k => $ug) {
                    $selectedPgName = $this->qualification_pg_model->getData($ug['object_id']);
                    $selectedPgTitle .= $selectedPgName['name'] . ",";
                }
            }
            $jobDetails['selected_pg'] = trim($selectedPgTitle,",");
            $selectedPhd = $this->qualification_model->listByFields(['object_ref' => 'sb_phd_qualification', 'job_id' => $jobId]);
            if ($selectedPhd) {
                $this->load->model('job/qualification_phd_model');
                foreach ($selectedPhd as $k => $ug) {
                    $selectedPhdName = $this->qualification_phd_model->getData($ug['object_id']);
                    $selectedPhdTitle .= $selectedPhdName['name'] . ",";
                }
            }
            $jobDetails['selected_phd'] = trim($selectedPhdTitle,",");
            $jobDetails['_tags'] = $tag;
            $this->load->moduleTemplate('job/view', $jobDetails);
        }
    }

    public function add()
    {
        $jobDetails = [];
        $tag = "";
        $mode = $this->uri->segment(2);
        $selectedUgIds = [];
        $selectedPgIds = [];
        $selectedPhdIds = [];
        if ($mode == "edit") {
            $jobId = $this->uri->segment(3);
            if ($jobId) {
                $this->load->model('job/job_model');
                $jobDetails = $this->job_model->getData($jobId);

                $this->load->model('tag_model', 'tag_model');
                $tags = $this->tag_model->listByFields(['object_ref' => 'sb_job', 'object_id' => $jobId]);
                if ($tags) {
                    $tag = implode(',', array_column($tags, 'name'));
                }
                $this->load->model('job/qualification_model');

                $selectedUg = $this->qualification_model->listByFields(['object_ref' => 'sb_ug_qualification', 'job_id' => $jobId]);
                if ($selectedUg) {
                    $selectedUgIds = array_column($selectedUg, 'object_id');
                }

                $selectedPg = $this->qualification_model->listByFields(['object_ref' => 'sb_pg_qualification', 'job_id' => $jobId]);
                if ($selectedPg) {
                    $selectedPgIds = array_column($selectedPg, 'object_id');
                }

                $selectedPhd = $this->qualification_model->listByFields(['object_ref' => 'sb_phd_qualification', 'job_id' => $jobId]);
                if ($selectedPhd) {
                    $selectedPhdIds = array_column($selectedPhd, 'object_id');
                }

            }
        }
        $jobDetails['selected_ug'] = $selectedUgIds;
        $jobDetails['selected_pg'] = $selectedPgIds;
        $jobDetails['selected_phd'] = $selectedPhdIds;
        $jobDetails['category'] = $this->categorySearch();
        $this->load->model('job/qualification_pg_model');
        $this->load->model('job/qualification_ug_model');
        $this->load->model('job/qualification_phd_model');
        $jobDetails['pg_qualification'] = $this->qualification_pg_model->getAll();
        $jobDetails['ug_qualification'] = $this->qualification_ug_model->getAll();
        $jobDetails['phd_qualification'] = $this->qualification_phd_model->getAll();
        $jobDetails['_tags'] = $tag;

        $this->load->moduleTemplate('job/edit', $jobDetails);
    }

    protected function categorySearch()
    {
        $postData = [
            'parent_id' => 0
        ];
        $this->load->model('category/category_model');
        $query = $this->category_model->getSearchQuery($postData);
        $result = $this->category_model->getListByQuery($query, 1, 9999);
        return $result;
    }

    public function validate()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        $this->form->setRules($data['title'], 'title_error', 'Please Enter Job Title');
        $this->form->setRules($data['no_of_vacancies'], 'no_of_vacancies_error', 'Please Enter Number Of Vacancy');
        $this->form->setRules($data['location'], 'location_error', 'Please Enter Job Location');
        $this->form->setRules($data['industry_id'], 'industry_id_error', 'Please Select Industry');
        $this->form->setRules($data['company_name'], 'company_name_error', 'Please Enter Company Name');
        $validationArr = $this->form->run();
        if (is_array($validationArr) && count($validationArr) && $validationArr != false) {
            header('Content-Type: application/json');
            echo json_encode($validationArr);
        } else {
            $this->load->model('job/job_model', 'job_model');
            if (isset($data['id']) && !empty($data['id'])) {
                $temp = [
                    'id' => $data['id']
                ];
                $this->job_model->update($data, $temp);
            } else {
                $jobId = $this->job_model->add($data);

            }
            if (!isset($jobId) && empty($jobId)) $jobId = $data['id'];
            $this->addTags($data['tags'], $jobId);
            $this->addQualification($jobId, $data['ug_qualification'], "sb_ug_qualification");
            $this->addQualification($jobId, $data['pg_qualification'], "sb_pg_qualification");
            $this->addQualification($jobId, $data['phd_qualification'], "sb_phd_qualification");
            $result = ['id' => $jobId, 'success' => true];
            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    private function addTags($tags, $objectId)
    {
        $tagsList = explode(',', $tags);
        if ($tagsList) {
            $this->load->model('tag_model', 'tag_model');
            foreach ($tagsList as $tag) {
                if (!$this->tag_model->loadByFields(['name' => $tag, 'object_ref' => 'sb_job'])) {
                    $this->tag_model->add(['name' => $tag, 'object_ref' => 'sb_job', 'object_id' => $objectId]);
                }
            }
        } else {
            $removedTags = $this->tag_model->listByFields(['object_ref' => 'sb_job', 'object_id' => $objectId]);
            if ($removedTags) {
                foreach ($removedTags as $rm) {
                    $this->tag_model->delete(['id' => $rm['id']]);
                }

            }
        }
    }

    private function addQualification($jobId, $qualifications = [], $type = '')
    {
        $existingQualificationsIds = [];
        $this->load->model('job/qualification_model', 'qualification_model');
        $existingQualifications = $this->qualification_model->listByFields(['object_ref' => $type, 'job_id' => $jobId]);
        if ($existingQualifications) {
            $existingQualificationsIds = array_column($existingQualifications, 'object_id');
        }

        $currentQualificationIds = $qualifications;
        if (!is_array($currentQualificationIds)) $currentQualificationIds = [];
        $newItems = array_diff($currentQualificationIds, $existingQualificationsIds);
        $deletedItems = array_diff($existingQualificationsIds, $currentQualificationIds);
        if ($newItems) {
            foreach ($newItems as $nw) {
                $this->qualification_model->add(['object_ref' => $type, 'job_id' => $jobId, 'object_id' => $nw]);
            }
        }
        if ($deletedItems) {
            foreach ($deletedItems as $dl) {
                $this->qualification_model->delete(['object_ref' => $type, 'job_id' => $jobId, 'object_id' => $dl]);
            }
        }
    }

    public function delete()
    {
        $jobId = $this->input->post('job_id');
        if ($jobId) {
            $this->load->model('job/job_model', 'job_model');
            $categoryIds = $this->job_model->delete(['id' => $jobId]);
        }
    }

    public function jobApplied()
    {
        $this->load->model('job/resume_model');
        $jobs['jobs_applied'] = $this->resume_model->getAll();
        if($jobs['jobs_applied']){
            foreach ($jobs['jobs_applied'] as $key => $value){
                $this->load->model('category/category_model');
                $industryData = $this->category_model->getData($value['industry_id']);
                $jobs['jobs_applied'][$key]['industry_name'] = isset($industryData['name']) ? $industryData['name']:"";
            }
        }

        $bc = [];
        $bc['job/submitted/list'] = "Jobs";
        $jobs['breadcrumbs'] = $bc;

        $pageTitle = [];
        $pageTitle['title'] = "Jobs Applied";
        $pageTitle['icon'] = "fa fa-list";
        $jobs['page_header'] = $pageTitle;
        $this->load->moduleTemplate('job/applied_list', $jobs);
    }

    public function downloadFile()
    {
        $fileId = $_GET['file_id'];
        $this->load->model('file_model', 'file_model');
        $fileDetails = $this->file_model->getData($fileId);
        if($fileDetails)
        {
            // required for IE
            if(ini_get('zlib.output_compression')) { ini_set('zlib.output_compression', 'Off'); }

            // Build the headers to push out the file properly.
            header('Pragma: public');     // required
            header('Expires: 0');         // no cache
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Last-Modified: '.gmdate ('D, d M Y H:i:s', filemtime ($fileDetails['file_path'])).' GMT');
            header('Cache-Control: private',false);
            header('Content-Type: '.$fileDetails['file_extension']);  // Add the mime type from Code igniter.
            header('Content-Disposition: attachment; filename="'.basename($fileDetails['file_name']).'"');  // Add the file name
            header('Content-Transfer-Encoding: binary');
            header('Content-Length: '.filesize($fileDetails['file_path'])); // provide file size
            header('Connection: close');
            readfile($fileDetails['file_path']); // push it out
            exit();
        }
    }


}