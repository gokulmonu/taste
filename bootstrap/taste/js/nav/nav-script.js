$(document).ready(function() {
		
   $('.mobile-menu').mobile_menu();
   
   
	   // ============================================================================
	// Megamenu
	// ============================================================================
	
	
	   $(".main-menu").accessibleMegaMenu({
			   /* prefix for generated unique id attributes, which are required 
				  to indicate aria-owns, aria-controls and aria-labelledby */
			   uuidPrefix: "accessible-megamenu",
			   /* css class used to define the megamenu styling */
			   menuClass: "nav-menu",
			   /* css class for a top-level navigation item in the megamenu */
			   topNavItemClass: "nav-item",
			   /* css class for a megamenu panel */
			   panelClass: "sub-nav",
			   /* css class for a group of items within a megamenu panel */
			   panelGroupClass: "sub-nav-group",
			   /* css class for the hover state */
			   hoverClass: "hover",
			   /* css class for the focus state */
			   focusClass: "focus",
			   /* css class for the open state */
			   openClass: "open"
		   });
	

	// ============================================================================
// Bootsptap Dropdown Effect
// ============================================================================


   // Add slideup & fadein animation to dropdown
   $('.dropdown').on('show.bs.dropdown', function(e){
      var $dropdown = $(this).find('.dropdown-menu');
      var orig_margin_top = parseInt($dropdown.css('margin-top'),10);
      $dropdown.css({'margin-top': (orig_margin_top + 10) + 'px', opacity: 0}).animate({'margin-top': orig_margin_top + 'px', opacity: 1}, 300, function(){
         $(this).css({'margin-top':''});
      });
   });
   // Add slidedown & fadeout animation to dropdown
   $('.dropdown').on('hide.bs.dropdown', function(e){
      var $dropdown = $(this).find('.dropdown-menu');
      var orig_margin_top = parseInt($dropdown.css('margin-top'),10);
      $dropdown.css({'margin-top': orig_margin_top + 'px', opacity: 1, display: 'block'}).animate({'margin-top': (orig_margin_top + 10) + 'px', opacity: 0}, 300, function(){
         $(this).css({'margin-top':'', display:''});
      });
   });




								
});