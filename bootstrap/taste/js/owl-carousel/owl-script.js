
$(document).ready(function(){

    $('.slider-active').owlCarousel({
        margin: 0,
        loop: true,
        nav: false,
        dots:true,
        smartSpeed: 1200,
        navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
        responsive: {
            0: {
                items: 1,
            },
            450: {
                items: 1,
            },
            768: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });

     // slider-active
     $(".slider-active").on('translate.owl.carousel', function() {
        $('.slider-items h2').removeClass('slideInUp animated').hide();
        $('.slider-items p').removeClass('slideInUp animated').hide();
        $('.slider-items ul').removeClass('slideInUp animated').hide();
    });

    $(".slider-active").on('translated.owl.carousel', function() {
        $('.owl-item.active .slider-items h2').addClass('slideInUp animated').show();
        $('.owl-item.active .slider-items p').addClass('slideInUp animated').show();
        $('.owl-item.active .slider-items ul').addClass('slideInUp animated').show();
    });


    $('.testi-slider').owlCarousel({
        margin: 0,
        loop: true,
        nav: false,
        dots:true,
        smartSpeed: 1200,
        navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
        responsive: {
            0: {
                items: 1,
            },
            450: {
                items: 1,
            },
            768: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });


});